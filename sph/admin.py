from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Customer


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('username', )
