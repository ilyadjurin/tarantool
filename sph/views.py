# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.views.generic import CreateView

from sph.models import Customer


class PersonCreateView(CreateView):
    model = Customer
    fields = ('username', 'email', 'password')

    def form_valid(self, form):
        customer = Customer.objects.get_or_create(username=self.request.POST['username'],
                                                  email=self.request.POST['email'])
        customer[0].set_password(self.request.POST['password'])
        customer[0].save(update_fields=['password'])
        return redirect('/en/index.html')
