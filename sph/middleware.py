from django.shortcuts import redirect


class StaticMiddleware:

    @staticmethod
    def make_url(request):
        path_list = [request.get_host()]
        return request.environ.get('wsgi.url_scheme') + '://' + '/'.join(path_list) + '/accounts/login/'

    @staticmethod
    def get_parts(path):
        path_list = path.split('/')
        if not path_list[0]:
            del path_list[0]
        if not path_list[-1]:
            del path_list[-1]

        subdomen = path_list[0] if path_list else None
        return subdomen

    @classmethod
    def get_redirect_url(cls, request, path):
        subdomen = cls.get_parts(path)

        if subdomen == 'en' or subdomen == 'ru':
            if request.user.is_anonymous:
                return cls.make_url(request)

    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        redirect_url = StaticMiddleware.get_redirect_url(request, request.path)
        return redirect(redirect_url) if redirect_url else self.get_response(request)
