import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User, AbstractUser


class Customer(AbstractUser):
    uid = models.UUIDField(
        _('Customer UID'),
        default=uuid.uuid4,
        editable=False,
        unique=True
    )
    phone = models.CharField(
        _('Phone number'),
        max_length=17,
    )
