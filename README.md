## Система аутентификации для ресурса Tarantool

for python >= 3.6

### Запуск приложения в режиме разработки :

- python3 manage.py runserver

### Страница аутентификации :

- /accounts/login/

### Страница регистрации :

- /accounts/registration/

### Разлогин :

- /accounts/logout/

#### Работу приложения без развертки можно протестировать на ресурсе http://tarantool.autoton.biz

### Данные для первичного входа :

user: admin

password: qwerty
