from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path
from sph.views import PersonCreateView


def redirect_view(request):
    return redirect('/en/index.html')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', redirect_view),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/registration/', PersonCreateView.as_view(), name='customer_form'),

]

if settings.DEBUG:
    urlpatterns += static(settings.DOCS_URL_EN, document_root=settings.DOCS_ROOT_EN)
    urlpatterns += static(settings.DOCS_URL_RU, document_root=settings.DOCS_ROOT_RU)
