��    8     �              �3     �3  �  �3  #  W5  $   {6  �   �6  �   �7  `   /8     �8     �8     �8  =   �8  D   �8  \   69     �9  '   �9  +   �9     �9  #   :  
   1:     <:     S:     _:     t:  $   �:  &   �:     �:     �:     �:     
;  K   ;    k;  `   �<  c   �<  �   H=  �   �=  {   O>  i   �>  c   5?  j   �?  �  @  �   �A     �B     �B     �B     �B  �   �B     �C     �C  
   �C     �C    �C     �D  0   �D  0   !E  .   RE  -   �E  +   �E  +   �E  +   F  5   3F  -   iF  -   �F  )   �F  1   �F  )   !G  )   KG  1   uG  )   �G  1   �G  )   H  -   -H  5   [H  1   �H  5   �H  1   �H  )   +I  1   UI  *   �I     �I     �I  $   �I  $   J     5J  *   RJ  '   }J  *   �J  v   �J  �   GK  �   �K  �   QL  �   �L  �   cM  �   �M  �   �N  P  O  `   \P  l   �P  l   *Q  M   �Q  4   �Q  Y  R  �  tS  ?  CU  �  �V     wX     �X  �  �X    %[  N   C\  P   �\  �   �\  �   w]  �   U^  �  _  �   �a  �  5b  �   �c  �   kd  �   �d  �   �e  �   �f  A   ng  �  �g  �  �i  �   k  $  �k  h   m  Y   �m  o   �m  ?   Rn  �   �n  �   $o  ,   �o  �   �o  g   �p  �   q     �q  O   �q  �   r     �r     �r     �r  A   �r     :s  "   Qs  �   ts  �   �s  2  �t  &   �u  &   v  )   Ev  &   ov  )   �v  &   �v  I  �v  e   1x  �   �x  #   4y    Xy  M   _z  N   �z     �z     {  \  "{  G   |  �   �|     j}     r}  2   �}     �}  �  �}      X  G   y  k   �  W   -�  �   ��  C   *�  H   n�  H   ��  E    �  B   F�  �   ��  ?   �  �   P�     �  ?   ��  �  7�  O   Ɇ  6   �     P�  v   d�  �   ۇ     ��  I   ��  U  ��  <   O�  7   ��  o   Ċ  >   4�  �   s�  �   �  c  ی  n   ?�  �   ��  �   ^�  G   ��  �   @�  "  #�  (   F�  (   o�  *   ��     Ò  d   ϒ  _   4�  '   ��    ��  I   є  �  �  �  ؖ  �   h�  Z   +�     ��  �  ��     #�     9�  �   G�  +  ��  q   �     ~�  �   ��  �   _�     �     ��     �     �     )�     6�     F�     U�     b�     o�     |�     ��     ��     ��     ��     Š     ՠ     �     ��     �     �     #�     1�     3�  Q   9�  z   ��  ,   �  �   3�  b    �  =   c�     ��  �   ��     B�  �   N�  L   �  ?   T�  �   ��  �   C�  R   ̦    �    6�  b   D�  �   ��  N   H�  �   ��  =   |�  H   ��     �     "�  9   8�  |   r�  *   �  C   �  #   ^�  3   ��  =   ��  5   ��     *�  2   3�  s   f�  �   ڮ  �   ��  �   H�  M   ϰ  "   �  j   @�  I   ��     ��     �  <   +�  �   h�     
�  �   #�  #   ۳  s   ��  $   s�  �   ��  I   )�  1   s�  9   ��  -   ߵ  '   �  )   5�  5   _�  5   ��  I   ˶  L   �  7   b�  7   ��  8   ҷ  5   �  ;   A�  (   }�  E   ��  E   �  8   2�  E   k�  F   ��  6   ��  H   /�  H   x�  H   ��  �  
�  +   ��  0   �  4   �  .   R�  X   ��  3   ڽ     �     �     ,�     9�  	   B�     L�     Y�     b�     o�     w�     ��     ��     ��     ��  	   ��     ��     ;     پ     �  	   �     ��     �     �     #�     0�  :   8�  M   s�  -   ��  3   �  {   #�  "   ��  �   ��  W   ��  �   ��  �   ��  �   C�  i   (�  7   ��  T   ��  ,   �  @   L�  )   ��  6   ��  Z   ��     I�     _�  �  u�  �  �  �   ��  �   J�  n   �  b  s�  F   ��  :   �  6  X�  @   ��  G   ��  C   �  #  \�  h   ��  $  ��  �   �  �   ��  H   ��  �   �  �   �  s  ��  	  ��  *  �  �   0�  �   ��  E  ~�  z   ��  8  ?�  �  x�  �   1�  �   �  �  ��  6  ��    ��  Y   �  !   f�  �   ��  �  �  G   ��    ��      �  C   �  )   a�    ��  �   ��  ,  X�  �  ��  �  4�  C   ��  >   $�  �   c�  �   +�  :   ��  �   �  ?  ��  �   �  �   ��  �   ��  �   U�  S  ��  	  ?�  `   I�  �   ��  ,   ]�  �   ��  �   Q�  7  	�     A  &   S     z  "   �  %   �      �  6    +   9 V   e C   � C     y   D U   � �    �   � v   A R   � �       � 	   � .   � 2   � -       ; 2   [ 0   � "   � 6   � (    X   B D   � C   � V   $ �   { x   	 �   �	 v   �
 �   �
 3   � B   � ?    X   Q X   � c    S   g    � T   �      
   8 #   C    g    �    �    � ,   � W    :   ^    �    �    � 8   � 8    8   ; /   t �   � $   ^    �    � %   � *   �                $   -    /    K    Q "   g    � a   �    �            * :   B ,   }    � Y   � f    i   z U   � ^   : i   � W    ]   [ g   � j   !    � �  � �   s   � :    w   O �   �    � R   �    � V  � 8   U     �     �  f  �     $"    1" +   K"    w"    �"    �"    �"    �" �  �" /   =' %   m' Y   �' W   �'    E( 	   ^( *   h( +   �( :   �(    �( ,   )   4) :   L+ #   �+ E   �+    �+    �+    ,    , h   %,    �,    �, 0   �,    �, 
   �, 
   �, J   �, O   ?- 1   �- 1   �-    �- ?   . _   L.    �.    �. #   �.    �.    �.    �.    /    //    M/    l/    y/ 	   �/    �/    �/    �/    �/    �/    �/    �/    �/ Z   0    g0    �0    �0     �0    �0 2   �0 ,   1 '   <1 (   d1    �1 2   �1 	   �1    �1    �1 1   �1    12 _   B2 3   �2    �2    �2 �   �2    �3    �3 	   �3 �   �3    .4    :4    G4    L4    X4 C   v4 -   �4 H   �4 *   15    \5    {5 <   �5    �5 3   �5 6   �5 :   16 :   l6 :   �6 :   �6 b   7 '   �7 4   �7 5   �7    8    "8 #   )8    M8 5   [8    �8    �8 [   �8 4   �8    /9 E  79   }:   �; �   �=   ]> f   a? n   �? M   7@ <   �@ c   �@ &  &A :   MC   �C �   �E >   -F $   lF E   �F /   �F �   G (   �G .   H Y  6H �   �J �   TK R   1L �   �L E   M O   ^M D   �M 7   �M 4   +N 2   `N #   �N +   �N A   �N Q   %O T   wO     �O Y   �O �   GP (   �P ?    Q �   @Q ?   �Q =   )R Q   gR �   �R �   QS ,   1T R   ^T Y   �T $   U .   0U G   _U 6   �U �   �U M   aV �   �V �  �W =   %Y b  cY =  �Z   \ �  ] $   �e 8   �e ,  f 9   2g O   lg +   �g _   �g )   Hh 1   rh O   �h N  �h    Ck m   dm j  �m �  =o 2  q �   Dr E   "s �  hs '  v E  Ew �   �x   �y    �{    �{ ;   �{    �{    | 4  5| ,   j} $   �}    �}    �}    �}    ~ &   *~    Q~ /   i~ /   �~ 0   �~ 3   �~ 0   . <   _ >   � O   � K   +� @   w�    �� "   �� 3   �� $   � "   9� "   \� =   � =   �� A   �� &   =� F   d� /   ��    ۂ d   ��    `� &   o� �   ��    1�    :�    I�    Z� +   z� (   �� `   τ    0� �  3�    Ն �  ؆ #  �� $   �� �   ؉ �   Ŋ `   g�    ȋ    ̋    ދ =   � D   )� \   n�    ˌ '   ܌ +   �    0� #   E� 
   i�    t�    ��    ��    �� $   Ǎ &   �    �     �    6�    B� K   W�   �� `   �� c   � �   �� �   � {   �� i   � c   m� j   ђ �  <� �   ��    Õ    Ǖ    ˕    � �   �    ��    ֖ 
   �    ��   �    � 0   (� 0   Y� .   �� -   �� +   � +   � +   ?� 5   k� -   �� -   ϙ )   �� 1   '� )   Y� )   �� 1   �� )   ߚ 1   	� )   ;� -   e� 5   �� 1   ɛ 5   �� 1   1� )   c� 1   �� *   ��    �    � $   #� $   H�    m� *   �� '   �� *   ݝ v   � �   � �   � �   �� �   � �   �� �   3� �   �� P  C� `   �� l   �� l   b� M   Ϥ 4   � Y  R� �  �� ?  {� �  ��    ��    ͫ �  ګ   ]� N   {� P   ʯ �   � �   �� �   �� �  G� �   ɴ �  m� �   � �   �� �   3� �   !� �   � A   �� �  � �  �� �   D� $  2� h   W� Y   �� o   � ?   �� �   �� �   \� ,   �� �    � g   �� �   >�    �� O   �� �   9�    ��    ��    � A   0�    r� "   �� �   �� �   /� 2  �� &   /� &   V� )   }� &   �� )   �� &   �� I  � e   i� �   �� #   l�   �� M   �� N   ��    4�    R� \  Z� G   �� �   ��    ��    �� 2   ��    �� �  
�     �� G   �� k   �� W   e� �   �� C   b� H   �� H   �� E   8� B   ~� �   �� ?   H� �   ��    "� ?   /� �  o� O   � 6   Q�    �� v   �� �   �    �� I   �� U  7� <   �� 7   �� o   � >   r� �   �� �   D� c  � n   }� �   �� �   �� G   6� �   ~� "  a� (   �� (   �� *   ��    � d   � _   r� '   ��   �� I   � �  Y� �  � �   �� Z   i�    �� �  ��    a� !   w� �   �� +  2� q   ^�    �� �   �� �   ��    =�    J�    _�    v�    ��    ��    ��    ��    ��    ��    ��    ��    �    �    4�    @�    V�    l�    ��    ��    ��    ��    ��    �� Q   �� z   � ,   �� �   �� b   �� =   ��    4� �   8�    �� �   �� L   �� ?   �� �   '� �   �� R   _�   ��   �� b   �� �   :� N   �� �   *� =   � H   M�    ��    �� 9   �� |     *   �  C   �  #   �  3    =   I 5   �    � 2   � s   � �   m �   ; �   � M   b "   � j   � I   >    �    � <   � �   �    � �   � #   n s   � $    �   + I   � 1   	 9   8	 -   r	 '   �	 )   �	 5   �	 5   (
 I   ^
 L   �
 7   �
 7   - 8   e 5   � ;   � (    E   9 E    8   � E   � F   D 6   � H   � H    H   T �  � +   S 0    4   � .   � X    3   m    �    �    �    � 	   �    �    �    �        
        #    /    ; 	   G    Q    `    l    x 	   �    �    �    �    �    � :   � M    -   T 3   � {   � "   2 �   U W   0 �   � �    �   � i   � 7   % T   ] ,   � @   � )     6   J Z   � +   �     �  ! �  � �   > �   � n   � b   F   �  :   �  6  ! @   ;" G   |" C   �" #  # h   ,$ $  �$ �   �% �   �& H   {' �   �' �   �( s  3) 	  �* *  �+ �   �, �   - E  *. z   p/ 8  �/ �  $1 �   �2 �   �3 �  v4 6  h6   �8 Y   �9 !   : �   4: �  �: G   E<   �<    �> C   �> )   ?   7? �   I@ ,  A �  1B �  �C C   �E >   �E �   F �   �F :   �G �   �G ?  {H �   �I �   �J �   IK �   L S  �L 	  �M `   �N �   VO ,   	P �   6P �   �P 7  �Q    �S &   �S    &T "   DT %   gT     �T 6   �T +   �T V   U C   hU C   �U y   �U U   jV �   �V �   cW v   �W R   dX �   �X    EY 	   MY .   WY 2   �Y -   �Y    �Y 2   Z 0   :Z "   kZ 6   �Z (   �Z X   �Z D   G[ C   �[ V   �[ �   '\ x   �\ �   9] v   -^ �   �^ 3   F_ B   z_ ?   �_ X   �_ X   V` c   �` S   a    ga T   na     �a 
   �a #   �a    b    .b    Jb    ib ,   �b W   �b :   
c    Ec    bc    gc 8   uc 8   �c 8   �c /    d �   Pd $   
e    /e    Ne %   me *   �e    �e    �e    �e    �e   �e    �f    �f    �f "   g    6g a   Dg    �g    �g    �g    �g :   �g ,   )h    Vh Y   eh f   �h i   &i U   �i ^   �i i   Ej W   �j ]   k g   ek j   �k    8l �  Ml �   n   �n :   �p w   �p �   sq    =r R   ?r    �r V  �r 8   t    :t    Pt f  it    �u    �u +   �u    #v    2v    Fv    Vv    Xv �  ^v /   �z %   { Y   ?{ W   �{    �{ 	   
| *   | +   ?| :   k|    �| ,   �|   �| :   �~ #   3 E   W    �    �    �    � h   �    :�    ?� 0   L�    }� 
   �� 
   �� J   �� O   � 1   ;� 1   m�    �� ?   �� _   ��    X�    e� #   g�    ��    ��    ��    ǂ    ۂ    ��    �    %� 	   4�    >�    N�    e�    s�    w�    {�    ��    �� Z   ��    �    /�    B�     Q�    r� 2   �� ,   �� '   � (   �    9� 2   H� 	   {�    ��    �� 1   ��    ݅ _   � 3   N�    ��    �� �   ��    -�    A� 	   F� �   P�    ڇ    �    �    ��    � C   "� -   f� H   �� *   ݈    �    '� <   /�    l� 3   r� 6   �� :   ݉ :   � :   S� :   �� b   Ɋ '   ,� 4   T� 5   ��    ��    ΋ #   Ջ    �� 5   �    =�    D� [   J� 4   ��    ی E  �   )�   I� �   `�   	� f   � n   t� M   � <   1� c   n� &  Ҕ :   ��   4� �   ;� >   ٙ $   � E   =� /   �� �   �� (   �� .   �� Y  � �   <� �    � R   ݟ �   0� E   Ġ O   
� D   Z� 7   �� 4   ס 2   � #   ?� +   c� A   �� Q   Ѣ T   #�     x� Y   �� �   � (   �� ?   �� �   � ?   �� =   ե Q   � �   e� �   �� ,   ݧ R   
� Y   ]� $   �� .   ܨ G   � 6   S� �   �� M   � �   [� �  6� =   Ѭ b  � =  r�   �� �  ʰ $   S� 8   x� ,  �� 9   ޺ O   � +   h� _   �� )   �� 1   � O   P� N  ��    � m   � j  ~� �  �� 2  �� �   �� E   �� �  � '  �� E  �� �   7�   5�    D�    c� ;   l�    ��    �� 4  �� ,   � $   C�    h�    ��    ��    �� &   ��    �� /   � /   E� 0   u� 3   �� 0   �� <   � >   H� O   �� K   �� @   #�    d� "   i� 3   �� $   �� "   �� "   � =   +� =   i� A   �� &   �� F   � /   W�    �� d   ��    � &   � �   B�    ��    ��    ��    � +   &� (   R� `   {�    ��  "" "Crypto" is short for "Cryptography", which generally refers to the production of a digest value from a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's crypto module supports ten types of cryptographic hash functions (AES_, DES_, DSS_, MD4_, MD5_, MDC2_, RIPEMD_, SHA-0_, SHA-1_, SHA-2_). Some of the crypto functionality is also present in the :ref:`digest` module. The functions in crypto are: #!/usr/bin/tarantool
local tap = require('tap')
test = tap.test("my test name")
test:plan(2)
test:ok(2 * 2 == 4, "2 * 2 is 4")
test:test("some subtests for test2", function(test)
    test:plan(2)
    test:is(2 + 2, 4, "2 + 2 is 4")
    test:isnt(2 + 3, 4, "2 + 3 is not 4")
end)
test:check() $ socat TCP:localhost:3302 ./tmp.txt $ ~/tarantool/src/tarantool
tarantool> box.cfg{log_level=3, logger='tarantool.txt'}
tarantool> log = require('log')
tarantool> log.error('Error')
tarantool> log.info('Info %s', box.info.version)
tarantool> os.exit()
$ less tarantool.txt 'R' if the socket is now readable, 'W' if the socket is now writable, 'RW' if the socket is now both readable and writable, '' (empty string) if timeout expired; '``SEEK_END``' = end of file, '``SEEK_CUR``' = current position, '``SEEK_SET``' = start of file. 'a' 'b' - big-endian, 'false' = c2 'fixmap' if metatable is 'map' = 80 otherwise 'fixarray' = 90 'fixmap(0)' = 80 -- nil is not stored when it is a missing map value 'fixmap(1)' + 'positive fixint' (for the key) + 'positive fixint' (for the value) = 81 00 05 'fixstr' = a1 61 'float 64' = cb 3f f8 00 00 00 00 00 00 'h' - endianness depends on host (default), 'l' - little-endian, 'n' - endianness depends on network 'nil' = c0 'positive fixint' = 7f 'true' = c3 'uint 16' = cd ff ff 'uint 32' = ce ff ff ff ff (the expected output is 3304160206). **Common Types and MsgPack Encodings** **Example:** **Format specifiers** **Result:** **Socket functions** **array** encoding: 92 a1 41 a1 42
**map** encoding:   82 01 a1 41 02 a1 42 -- Benchmark a function which sleeps 10 seconds.
-- NB: bench() will not calculate sleep time.
-- So the returned value will be {a number less than 10, 88}.
clock = require('clock')
fiber = require('fiber')
function f(param)
  fiber.sleep(param)
  return 88
end
clock.bench(f,10) -- Disassemble hexadecimal 97 which is the x86 code for xchg eax, edi
jit.dis_x86.disass('\x97') -- Disassemble hexadecimal 97 which is the x86-64 code for xchg eax, edi
jit.dis_x64.disass('\x97') -- Show the machine code of a Lua "for" loop
jit.dump.on('m')
local x = 0;
for i = 1, 1e6 do
  x = x + i
end
print(x)
jit.dump.off() -- Show what LuaJIT is doing for a Lua "for" loop
jit.v.on()
local x = 0
for i = 1, 1e6 do
    x = x + i
end
print(x)
jit.v.off() -- This will print an approximate number of years since 1970.
clock = require('clock')
print(clock.time() / (365*24*60*60)) -- This will print nanoseconds in the CPU since the start.
clock = require('clock')
print(clock.proc64()) -- This will print nanoseconds since the start.
clock = require('clock')
print(clock.monotonic64()) -- This will print seconds in the thread since the start.
clock = require('clock')
print(clock.thread64()) -- When nil is assigned to a Lua-table field, the field is null
tarantool> {nil, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a Lua-table field, the field is json.NULL
tarantool> {json.NULL, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a JSON field, the field is null
tarantool> json.encode({field2 = json.NULL, field1 = 'a', field3 = 'c'}
---
- '{"field2":null,"field1":"a","field3":"c"}'
... ---
- - host: 188.93.56.70
    family: AF_INET
    type: SOCK_STREAM
    protocol: tcp
    port: 80
  - host: 188.93.56.70
    family: AF_INET
    type: SOCK_DGRAM
    protocol: udp
    port: 80
... 1.5 127 16-byte binary string 16-byte string 2...0 [5257] main/101/interactive C> version 1.7.0-355-ga4f762d
2...1 [5257] main/101/interactive C> log level 3
2...1 [5261] main/101/spawner C> initialized
2...0 [5257] main/101/interactive [C]:-1 E> Error 36-byte binary string 36-byte hexadecimal string 4294967295 65535 :ref:`csv.iterate() <csv-iterate>` is the low level of :ref:`csv.load() <csv-load>` and :ref:`csv.dump() <csv-dump>`. To illustrate that, here is a function which is the same as the :ref:`csv.load() <csv-load>` function, as seen in `the Tarantool source code`_. :ref:`socket() <socket-socket>` :ref:`socket.getaddrinfo() <socket-getaddrinfo>` :ref:`socket.tcp_connect() <socket-tcp_connect>` :ref:`socket.tcp_server() <socket-tcp_server>` :ref:`socket_object:accept() <socket-accept>` :ref:`socket_object:close() <socket-close>` :ref:`socket_object:errno() <socket-error>` :ref:`socket_object:error() <socket-error>` :ref:`socket_object:getsockopt() <socket-getsockopt>` :ref:`socket_object:linger() <socket-linger>` :ref:`socket_object:listen() <socket-listen>` :ref:`socket_object:name() <socket-name>` :ref:`socket_object:nonblock() <socket-nonblock>` :ref:`socket_object:peer() <socket-peer>` :ref:`socket_object:read() <socket-read>` :ref:`socket_object:readable() <socket-readable>` :ref:`socket_object:recv() <socket-recv>` :ref:`socket_object:recvfrom() <socket-recvfrom>` :ref:`socket_object:send() <socket-send>` :ref:`socket_object:sendto() <socket-sendto>` :ref:`socket_object:setsockopt() <socket-setsockopt>` :ref:`socket_object:shutdown() <socket-shutdown>` :ref:`socket_object:sysconnect() <socket-sysconnect>` :ref:`socket_object:syswrite() <socket-syswrite>` :ref:`socket_object:wait() <socket-wait>` :ref:`socket_object:writable() <socket-writable>` :ref:`socket_object:write() <socket-send>` :ref:`uuid() <uuid-__call>` :ref:`uuid.bin() <uuid-bin>` :ref:`uuid.frombin() <uuid-frombin>` :ref:`uuid.fromstr() <uuid-fromstr>` :ref:`uuid.str() <uuid-str>` :ref:`uuid_object:bin() <uuid-object_bin>` :ref:`uuid_object:isnil() <uuid-isnil>` :ref:`uuid_object:str() <uuid-object_str>` :samp:`chunk-size = {number}` -- number of characters to read at once (usually for file-IO efficiency), default = 4096 :samp:`conn.space.{space-name}:delete(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:delete(...)`. :samp:`conn.space.{space-name}:get(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:get(...)`. :samp:`conn.space.{space-name}:insert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:insert(...)`. :samp:`conn.space.{space-name}:replace(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:replace(...)`. :samp:`conn.space.{space-name}:select`:code:`{...}` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:select`:code:`{...}`. :samp:`conn.space.{space-name}:update(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:update(...)`. :samp:`conn.space.{space-name}:upsert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:upsert(...)`. :samp:`conn:eval({Lua-string})` evaluates and executes the expression in Lua-string, which may be any statement or series of statements. An :ref:`execute privilege <authentication-privileges>` is required; if the user does not have it, an administrator may grant it with :samp:`box.schema.user.grant({username}, 'execute', 'universe')`. :samp:`delimiter = {string}` -- single-byte character to designate end-of-field, default = comma :samp:`quote_char = {string}` -- single-byte character to designate encloser of string, default = quote mark :samp:`skip_head_lines = {number}` -- number of lines to skip at the start (usually for a header), default 0 :samp:`{function parameters}` = whatever values are required by the function. :samp:`{function}` = function or function reference; A "UUID" is a `Universally unique identifier`_. If an application requires that a value be unique only within a single computer or on a single database, then a simple counter is better than a UUID, because getting a UUID is time-consuming (it requires a syscall_). For clusters of computers, or widely distributed applications, UUIDs are better. A "digest" is a value which is returned by a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's digest module supports several types of cryptographic hash functions (AES_, MD4_, MD5_, SHA-0_, SHA-1_, SHA-2_) as well as a checksum function (CRC32_), two functions for base64_, and two non-cryptographic hash functions (guava_, murmur_). Some of the digest functionality is also present in the :ref:`crypto <crypto>` module. A fiber has all the features of a Lua coroutine_ and all the programming concepts that apply for Lua coroutines will apply for fibers as well. However, Tarantool has made some enhancements for fibers and has used fibers internally. So, although use of coroutines is possible and supported, use of fibers is recommended. A fiber is a set of instructions which are executed with cooperative multitasking. Fibers managed by the fiber module are associated with a user-supplied function called the *fiber function*. A fiber has three possible states: **running**, **suspended** or **dead**. When a fiber is created with :ref:`fiber.create() <fiber-create>`, it is running. When a fiber yields control with :ref:`fiber.sleep() <fiber-sleep>`, it is suspended. When a fiber ends (because the fiber function ends), it is dead. A list of strings or numbers. A nil object A runaway fiber can be stopped with :ref:`fiber_object.cancel <fiber_object-cancel>`. However, :ref:`fiber_object.cancel <fiber_object-cancel>` is advisory — it works only if the runaway fiber calls :ref:`fiber.testcancel() <fiber-testcancel>` occasionally. Most ``box.*`` functions, such as :ref:`box.space...delete() <box_space-delete>` or :ref:`box.space...update() <box_space-update>`, do call :ref:`fiber.testcancel() <fiber-testcancel>` but :ref:`box.space...select{} <box_space-select>` does not. In practice, a runaway fiber can only become unresponsive if it does many computations and does not check whether it has been cancelled. A special use of ``console.start()`` is with :ref:`initialization files <index-init_label>`. Normally, if one starts the tarantool server with :samp:`tarantool {initialization file}` there is no console. This can be remedied by adding these lines at the end of the initialization file: A table containing these fields: "host", "family", "type", "protocol", "port". A value comparable to Lua "nil" which may be useful as a placeholder in a tuple. Accept a new client connection and create a new connected socket. It is good practice to set the socket's blocking mode explicitly after accepting. Additionally one can see the uptime and the server version and the process id. Those information items can also be accessed with :ref:`box.info <box_introspection-box_info>` but use of the tarantool module is recommended. After ``message_content, message_sender = recvfrom(1)`` the value of ``message_content`` might be a string containing 'X' and the value of ``message_sender`` might be a table containing All ``net.box`` methods are fiber-safe, that is, it is safe to share and use the same connection object across multiple concurrent fibers. In fact, it's perhaps the best programming practice with Tarantool. When multiple fibers use the same connection, all requests are pipelined through the same network socket, but each fiber gets back a correct response. Reducing the number of active sockets lowers the overhead of system calls and increases the overall server performance. There are, however, cases when a single connection is not enough — for example when it's necessary to prioritize requests or to use different authentication ids. All fibers are part of the fiber registry. This registry can be searched with :ref:`fiber.find() <fiber-find>` - via fiber id (fid), which is a numeric identifier. All remote calls support execution timeouts. Using a wrapper object makes the remote connection API compatible with the local one, removing the need for a separate ``timeout`` argument, which the local version would ignore. Once a request is sent, it cannot be revoked from the remote server even if a timeout expires: the timeout expiration only aborts the wait for the remote server response, not the request itself. Also, some MsgPack configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Also, some YAML configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Bind a socket to the given host/port. A UDP socket after binding can be used to receive data (see :ref:`socket_object.recvfrom <socket-recvfrom>`). A TCP socket can be used to accept new connections, after it has been put in listen mode. By default strict mode is off, unless tarantool was built with the ``-DCMAKE_BUILD_TYPE=Debug`` option -- see the description of build options in section :ref:`building-from-source <building_from_source>`. By saying ``require('tarantool')``, one can answer some questions about how the tarantool server was built, such as "what flags were used", or "what was the version of the compiler". CSV-table has 3 fields, field#2 has "," so result has quote marks Call ``fiber.channel()`` to allocate space and get a channel object, which will be called channel for examples in this section. Call the other ``fiber-ipc`` routines, via channel, to send messages, receive messages, or check ipc status. Message exchange is synchronous. The channel is garbage collected when no one is using it, as with any other Lua object. Use object-oriented syntax, for example ``channel:put(message)`` rather than ``fiber.channel.put(message)``. Call ``require('net.box')`` to get a ``net.box`` object, which will be called ``net_box`` for examples in this section. Call ``net_box.new()`` to connect and get a connection object, which will be called ``conn`` for examples in this section. Call the other ``net.box()`` routines, passing ``conn:``, to execute requests on the remote box. Call :ref:`conn:close <socket-close>` to disconnect. Cancel a fiber. Running and suspended fibers can be cancelled. After a fiber has been cancelled, attempts to operate on it will cause errors, for example :ref:`fiber_object:id() <fiber_object-id>` will cause ``error: the fiber is dead``. Change the fiber name. By default the Tarantool server's interactive-mode fiber is named 'interactive' and new fibers created due to :ref:`fiber.create <fiber-create>` are named 'lua'. Giving fibers distinct names makes it easier to distinguish them when using :ref:`fiber.info <fiber-info>`. Change the size of an open file. Differs from ``fio.truncate``, which changes the size of a closed file. Check if the current fiber has been cancelled and throw an exception if this is the case. Check whether the first argument equals the second argument. Displays extensive message if the result is false. Check whether the specified channel is empty (has no messages). Check whether the specified channel is empty and has readers waiting for a message (because they have issued ``channel:get()`` and then blocked). Check whether the specified channel is full and has writers waiting (because they have issued ``channel:put()`` and then blocked due to lack of room). Check whether the specified channel is full. Checks the number of tests performed. This check should only be done after all planned tests are complete, so ordinarily ``taptest:check()`` will only appear at the end of a script. Clears the record of errors, so functions like `box.error()` or `box.error.last()` will have no effect. Close (destroy) a socket. A closed socket should not be used any more. A socket is closed automatically when its userdata is garbage collected by Lua. Close a connection. Close a file that was opened with ``fio.open``. For details type "man 2 close". Close the channel. All waiters in the channel will be woken up. All following ``channel:put()`` or ``channel:get()`` operations will return an error (``nil``). Commas designate end-of-field, Common file manipulations Common pathname manipulations Concatenate partial string, separated by '/' to form a path name. Configuration settings Connect a socket to a remote host. Connect an existing socket to a remote host. The argument values are the same as in tcp_connect(). The host must be an IP address. Connect to the server at :ref:`URI <index-uri>`, change the prompt from ':samp:`tarantool>`' to ':samp:`{uri}>`', and act henceforth as a client until the user ends the session or types :code:`control-D`. Connection objects are garbage collected just like any other objects in Lua, so an explicit destruction is not mandatory. However, since close() is a system call, it is good programming practice to close a connection explicitly when it is no longer needed, to avoid lengthy stalls of the garbage collector. Convert a JSON string to a Lua object. Convert a Lua object to a JSON string. Convert a Lua object to a MsgPack string. Convert a Lua object to a YAML string. Convert a MsgPack string to a Lua object. Convert a YAML string to a Lua object. Convert a string or a Lua number to a 64-bit integer. The result can be used in arithmetic, and the arithmetic will be 64-bit integer arithmetic rather than floating-point arithmetic. (Operations on an unconverted Lua number use floating-point arithmetic.) The ``tonumber64()`` function is added by Tarantool; the name is global. Counterpart to ``pickle.pack()``. Warning: if format specifier 'A' is used, it must be the last item. Create a new TCP or UDP socket. The argument values are the same as in the `Linux socket(2) man page <http://man7.org/linux/man-pages/man2/socket.2.html>`_. Create a new communication channel. Create a new connection. The connection is established on demand, at the time of the first request. It is re-established automatically after a disconnect. The returned ``conn`` object supports methods for making remote requests, such as select, update or delete. Create and start a fiber. The fiber is created and begins to run immediately. Create or delete a directory. For details type "man 2 mkdir" or "man 2 rmdir". Display a diagnostic message. Either: Emulate a request error, with text based on one of the pre-defined Tarantool errors defined in the file `errcode.h <https://github.com/tarantool/tarantool/blob/1.7/src/box/errcode.h>`_ in the source tree. Lua constants which correspond to those Tarantool errors are defined as members of ``box.error``, for example ``box.error.NO_SUCH_USER == 45``. Ensure that changes are written to disk. For details type "man 2 sync". Ensure that file changes are written to disk, for an open file. Compare ``fio.sync``, which is for all files. For details type "man 2 fsync" or "man 2 fdatasync". Example Example Of Fiber Use Example showing use of most of the net.box methods Execute a PING command. Execute a function, provided it has not been executed before. A passed value is checked to see whether the function has already been executed. If it has been executed before, nothing happens. If it has not been executed before, the function is invoked. For an explanation why ``box.once`` is useful, see the section :ref:`Preventing Duplicate Actions <index-preventing_duplicate_actions>`. Execute by passing to the shell. Exit the program. If this is done on the server, then the server stops. Fetch a message from a channel. If the channel is empty, ``channel:get()`` blocks until there is a message. Find out how many messages are on the channel. The answer is 0 if the channel is empty. Flags can be passed as a number or as string constants, for example '``O_RDONLY``', '``O_WRONLY``', '``O_RDWR``'. Flags can be combined by enclosing them in braces. For a list of available options, read `the source code of bc.lua`_. For a list of available options, read `the source code of dis_x64.lua`_. For a list of available options, read `the source code of dis_x86.lua`_. For a list of available options, read `the source code of dump.lua`_. For a list of available options, read `the source code of v.lua`_. For all examples in this section the socket name will be sock and the function invocations will look like ``sock:function_name(...)``. For example, in Python, install the ``crcmod`` package and say: For example, the following code will interpret 0/0 (which is "not a number") and 1/0 (which is "infinity") as special values rather than nulls or errors: For example: For more information on, read article about `Encryption Modes`_ For the local tarantool server there is a pre-created always-established connection object named :samp:`{net_box}.self`. Its purpose is to make polymorphic use of the ``net_box`` API easier. Therefore :samp:`conn = {net_box}.new('localhost:3301')` can be replaced by :samp:`conn = {net_box}.self`. However, there is an important difference between the embedded connection and a remote one. With the embedded connection, requests which do not modify data do not yield. When using a remote connection, due to :ref:`the implicit rules <atomic-the_implicit_yield_rules>` any request can yield, and database state may have changed by the time it regains control. Form a Lua iterator function for going through CSV records one field at a time. Four choices of block cipher modes are also available: Function `box.once` Functions to create and delete links. For details type "man readlink", "man 2 link", "man 2 symlink", "man 2 unlink".. Get CSV-formatted input from ``readable`` and return a table as output. Usually ``readable`` is either a string or a file opened for reading. Usually :samp:`{options}` is not specified. Get environment variable. Get socket flags. For a list of possible flags see ``sock:setsockopt()``. Get table input from ``csv-table`` and return a CSV-formatted string as output. Or, get table input from ``csv-table`` and put the output in ``writable``. Usually :samp:`{options}` is not specified. Usually ``writable``, if specified, is a file opened for writing. :ref:`csv.dump() <csv-dump>` is the reverse of :ref:`csv.load() <csv-load>`. Get the id of the fiber (fid), to be used in later displays. Getting the same results from digest and crypto modules Given a full path name, remove all but the final part (the file name). Also remove the suffix, if it is passed. Given a full path name, remove the final part (the file name). Here are examples for all the common types, with the Lua-table representation on the left, with the MsgPack format name and encoding on the right. Here is an example of the tcp_server function, reading strings from the client and printing them. On the client side, the Linux socat utility will be used to ship a whole file for the tcp_server function to read. Here is an example with datagrams. Set up two connections on 127.0.0.1 (localhost): ``sock_1`` and ``sock_2``. Using ``sock_2``, send a message to ``sock_1``. Using ``sock_1``, receive a message. Display the received message. Close both connections. |br| This is not a useful way for a computer to communicate with itself, but shows that the system works. If a later user calls the ``password_check()`` function and enters the wrong password, the result is an error. If the Tarantool server at :samp:`uri` requires authentication, the connection might look something like: :code:`console.connect('admin:secretpassword@distanthost.com:3301')`. If timeout is provided, and the channel doesn't become empty for the duration of the timeout, ``channel:put()`` returns false. Otherwise it returns true. In Perl, install the ``Digest::CRC`` module and run the following code: In the following example, the user creates two functions, ``password_insert()`` which inserts a SHA-1_ digest of the word "**^S^e^c^ret Wordpass**" into a tuple set, and ``password_check()`` which requires input of a password. In this example a connection is made over the internet between the Tarantool server and tarantool.org, then an HTTP "head" message is sent, and a response is received: "``HTTP/1.1 200 OK``". This is not a useful way to communicate with this particular site, but shows that the system works. Incremental methods in the crypto module Incremental methods in the digest module Indicate how many tests will be performed. Initialize. Initiates incremental MurmurHash. See :ref:`incremental methods <digest-incremental_digests>` notes. Initiates incremental crc32. See :ref:`incremental methods <digest-incremental_digests>` notes. Leading or trailing spaces are ignored, Like all Lua objects, dead fibers are garbage collected. The garbage collector frees pool allocator memory owned by the fiber, resets all fiber data, and returns the fiber (now called a fiber carcass) to the fiber pool. The carcass can be reused when another fiber is created. Line feeds, or line feeds plus carriage returns, designate end-of-record, Listen on :ref:`URI <index-uri>`. The primary way of listening for incoming requests is via the connection-information string, or URI, specified in :code:`box.cfg{listen=...}`. The alternative way of listening is via the URI specified in :code:`console.listen(...)`. This alternative way is called "administrative" or simply :ref:`"admin port" <administration-admin_ports>`. The listening is usually over a local host with a Unix domain socket. Local storage within the fiber. The storage can contain any number of named values, subject to memory limitations. Naming may be done with :samp:`{fiber_object}.storage.{name}` or :samp:`fiber_object}.storage['{name}'].` or with a number :samp:`{fiber_object}.storage[{number}]`. Values may be either numbers or strings. The storage is garbage-collected when :samp:`{fiber_object}:cancel()` happens. Locate a fiber by its numeric id and cancel it. In other words, :ref:`fiber.kill() <fiber-kill>` combines :ref:`fiber.find() <fiber-find>` and :ref:`fiber_object:cancel() <fiber_object-cancel>`. Lua `escape sequences`_ such as \\n or \\10 are legal within strings but not within files, Lua code Lua fun, also known as the Lua Functional Library, takes advantage of the features of LuaJIT to help users create complex functions. Inside the module are "sequence processors" such as map, filter, reduce, zip -- they take a user-written function as an argument and run it against every element in a sequence, which can be faster or more convenient than a user-written loop. Inside the module are "generators" such as range, tabulate, and rands -- they return a bounded or boundless series of values. Within the module are "reducers", "filters", "composers" ... or, in short, all the important features found in languages like Standard ML, Haskell, or Erlang. Lua iterator function Lua reference Make a fiber, associate function_x with the fiber, and start function_x. It will immediately "detach" so it will be running independently of the caller. Make the function which will be associated with the fiber. This function contains an infinite loop (``while 0 == 0`` is always true). Each iteration of the loop adds 1 to a global variable named gvar, then goes to sleep for 2 seconds. The sleep causes an implicit :ref:`fiber.yield() <fiber-yield>`. Manage the rights to file objects, or ownership of file objects. For details type "man 2 chown" or "man 2 chmod". Miscellaneous Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits are significant if flags include `O_CREATE` or `O_TMPFILE`. Mode bits can be combined by enclosing them in braces. Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits can be combined by enclosing them in braces. Module `box` Module `clock` Module `console` Module `crypto` Module `csv` Module `digest` Module `fiber` Module `fio` Module `fun` Module `jit` Module `json` Module `log` Module `msgpack` Module `net.box` Module `os` Module `pickle` Module `socket` Module `strict` Module `tap` Module `tarantool` Module `uuid` Module `yaml` N Names Now watch what happens on the first shell. The strings "A", "B", "C" are printed. On Linux the listen ``backlog`` backlog may be from /proc/sys/net/core/somaxconn, on BSD the backlog may be ``SOMAXCONN``. On the first shell, start Tarantool and say: On the second shell, create a file that contains a few lines. The contents don't matter. Suppose the first line contains A, the second line contains B, the third line contains C. Call this file "tmp.txt". On the second shell, use the socat utility to ship the tmp.txt file to the server's host and port: Open a file in preparation for reading or writing or seeking. Or: Output a user-generated message to the :ref:`log file <cfg_logging-logger>`, given log_level_function_name = ``error`` or ``warn`` or ``info`` or ``debug``. Parameters: Parameters: (string) format-string = instructions; (string) time-since-epoch = number of seconds since 1970-01-01. If time-since-epoch is omitted, it is assumed to be the current time. Parameters: (string) name = name of file or directory which will be removed. Parameters: (string) variable-name = environment variable name. Parse and execute an arbitrary chunk of Lua code. This function is mainly useful to define and run Lua code without having to introduce changes to the global Lua environment. Pass or return a cipher derived from the string, key, and (optionally, sometimes) initialization vector. The four choices of algorithms: Pass or return a digest derived from the string. The twelve choices of algorithms: Pause for a while, while the detached function runs. Then ... Cancel the fiber. Then, once again ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit more depending how long the pause lasted). This time the status is dead because the cancel worked. Pause for a while, while the detached function runs. Then ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit depending how long the pause lasted). The status is suspended because the fiber spends almost all its time sleeping or yielding. Perform non-random-access read or write on a file. For details type "man 2 read" or "man 2 write". Perform read/write random-access operation on a file, without affecting the current seek position of the file. For details type "man 2 pread" or "man 2 pwrite". Possible errors: If there is a compilation error, it is raised as a Lua error. Possible errors: On error, returns an empty string, followed by status, errno, errstr. In case the writing side has closed its end, returns the remainder read from the socket (possibly an empty string), followed by "eof" status. Possible errors: Returns nil, status, errno, errstr on error. Possible errors: cancel is not permitted for the specified fiber object. Possible errors: nil on error. Possible errors: nil. Possible errors: on error, returns status, errno, errstr. Possible errors: the connection will fail if the target Tarantool server was not initiated with :code:`box.cfg{listen=...}`. Possible errors: unknown format specifier. Prints a trace of LuaJIT's progress compiling and interpreting code Prints the byte code of a function. Prints the i386 assembler code of a string of bytes Prints the intermediate or machine code of following Lua code Prints the x86-64 assembler code of a string of bytes Purposes Quote marks may enclose fields or parts of fields, Read ``size`` bytes from a connected socket. An internal read-ahead buffer is used to reduce the cost of this call. Read from a connected socket until some condition is true, and return the bytes that were read. Reading goes on until ``limit`` bytes have been read, or a delimiter has been read, or a timeout has expired. Readable file :file:`./file.csv` contains two CSV records. Explanation of fio is in section :ref:`fio <fio-section>`. Source CSV file and example respectively: Readable string contains 2-byte character = Cyrillic Letter Palochka: (This displays a palochka if and only if character set = UTF-8.) Readable string has 3 fields, field#2 has comma and space so use quote marks: Receive a message on a UDP socket. Recursive version of ``taptest:is(...)``, which can be be used to compare tables as well as scalar values. Reduce file size to a specified value. For details type "man 2 truncate". Remove file or directory. Rename a file or directory. Rename a file or directory. For details type "man 2 rename". Retrieve information about the last error that occurred on a socket, if any. Errors do not cause throwing of exceptions so these functions are usually necessary. Return a formatted date. Return a list of files that match an input string. The list is constructed with a single flag that controls the behavior of the function: GLOB_NOESCAPE. For details type "man 3 glob". Return a name for a temporary file. Return all available data from the socket buffer if non-blocking. Rarely used. For details see `this description`_. Return information about all fibers. Return statistics about an open file. This differs from ``fio.stat`` which return statistics about a closed file. For details type "man 2 stat". Return the name of a directory that can be used to store temporary files. Return the name of the current working directory. Return the number of CPU seconds since the program start. Return the number of seconds since the epoch. Return the status of the current fiber. Return the status of the specified fiber. Returns 128-bit binary string = digest made with MD4. Returns 128-bit binary string = digest made with MD5. Returns 128-byte string = hexadecimal of a digest calculated with sha512. Returns 160-bit binary string = digest made with SHA-0.|br| Not recommended. Returns 160-bit binary string = digest made with SHA-1. Returns 224-bit binary string = digest made with SHA-2. Returns 256-bit binary string =  digest made with SHA-2. Returns 256-bit binary string = digest made with AES. Returns 32-bit binary string = digest made with MurmurHash. Returns 32-bit checksum made with CRC32. Returns 32-byte string = hexadecimal of a digest calculated with md4. Returns 32-byte string = hexadecimal of a digest calculated with md5. Returns 384-bit binary string =  digest made with SHA-2. Returns 40-byte string = hexadecimal of a digest calculated with sha. Returns 40-byte string = hexadecimal of a digest calculated with sha1. Returns 512-bit binary tring = digest made with SHA-2. Returns 56-byte string = hexadecimal of a digest calculated with sha224. Returns 64-byte string = hexadecimal of a digest calculated with sha256. Returns 96-byte string = hexadecimal of a digest calculated with sha384. Returns a description of the last error, as a Lua table with five members: "line" (number) Tarantool source file line number, "code" (number) error's number, "type", (string) error's C++ class, "message" (string) error's message, "file" (string) Tarantool source file. Additionally, if the error is a system error (for example due to a failure in socket or file io), there may be a sixth member: "errno" (number) C standard error number. Returns a number made with consistent hash. Returns a regular string from a base64 encoding. Returns array of random bytes with length = integer. Returns base64 encoding from a regular string. Returns information about a file object. For details type "man 2 lstat" or "man 2 stat". Round Trip: from string to table and back to string SO_ACCEPTCONN SO_BINDTODEVICE SO_BROADCAST SO_DEBUG SO_DOMAIN SO_DONTROUTE SO_ERROR SO_KEEPALIVE SO_MARK SO_OOBINLINE SO_PASSCRED SO_PEERCRED SO_PRIORITY SO_PROTOCOL SO_RCVBUF SO_RCVBUFFORCE SO_RCVLOWAT SO_RCVTIMEO SO_REUSEADDR SO_SNDBUF SO_SNDBUFFORCE SO_SNDLOWAT SO_SNDTIMEO SO_TIMESTAMP SO_TYPE See also :ref:`box.session.storage <box_session-storage>`. See also :ref:`fiber.time64 <fiber-time64>` and :ref:`os.clock() <os-clock>`. Semicolon instead of comma for the delimiter: Send a message on a UDP socket to a specified host. Send a message using a channel. If the channel is full, ``channel:put()`` blocks until there is a free slot in the channel. Send data over a connected socket. Serializing 'A' and 'B' with different ``__serialize`` values causes different results. To show this, here is a routine which encodes `{'A','B'}` both as an array and as a map, then displays each result in hexadecimal. Serializing 'A' and 'B' with different ``__serialize`` values causes different results: Set or clear the SO_LINGER flag. For a description of the flag, see the `Linux man page <http://man7.org/linux/man-pages/man1/loginctl.1.html>`_. Set socket flags. The argument values are the same as in the `Linux getsockopt(2) man page <http://man7.org/linux/man-pages/man2/setsockopt.2.html>`_. The ones that Tarantool accepts are: Set the auto-completion flag. If auto-completion is `true`, and the user is using tarantool as a client, then hitting the TAB key may cause tarantool to complete a word automatically. The default auto-completion value is `true`. Set the mask bits used when creating files or directories. For a detailed description type "man 2 umask". Setting SO_LINGER is done with ``sock:linger(active)``. Shift position in the file to the specified position. For details type "man 2 seek". Show whether connection is active or closed. Shutdown a reading end, a writing end, or both ends of a socket. Start listening for incoming connections. Start the console on the current interactive terminal. Start two shells. The first shell will be the server. The second shell will be the client. Submodule `box.error` Submodule `fiber-ipc` Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports for all crypto functions.. Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports with crc32 and with murmur... TAP version 13
1..2
ok - 2 * 2 is 4
    # Some subtests for test2
    1..2
    ok - 2 + 2 is 4,
    ok - 2 + 3 is not 4
    # Some subtests for test2: end
ok - some subtests for test2 Tarantool supports file input/output with an API that is similar to POSIX syscalls. All operations are performed asynchronously. Multiple fibers can access the same file simultaneously. Test whether a value has a particular type. Displays a long message if the value is not of the specified type. The "admin" address is the URI to listen on. It has no default value, so it must be specified if connections will occur via an admin port. The parameter is expressed with URI = Universal Resource Identifier format, for example "/tmpdir/unix_domain_socket.sock", or a numeric TCP port. Connections are often made with telnet. A typical port value is 3313. The 'Error' line is visible in tarantool.txt preceded by the letter E. The 'Info' line is not present because the log_level is 3. The :code:`strict` module has functions for turning "strict mode" on or off. When strict mode is on, an attempt to use an undeclared global variable will cause an error. A global variable is considered "undeclared" if it has never had a value assigned to it. Often this is an indication of a programming error. The JSON output structure can be specified with ``__serialize``: The MsgPack Specification_ page explains that the first encoding means: The MsgPack output structure can be specified with ``__serialize``: The Tarantool server puts all diagnostic messages in a log file specified by the :ref:`logger <cfg_logging-logger>` configuration parameter. Diagnostic messages may be either system-generated by the server's internal code, or user-generated with the ``log.log_level_function_name`` function. The `YAML collection style <http://yaml.org/spec/1.1/#id930798>`_ can be specified with ``__serialize``: The ``box.error`` function is for raising an error. The difference between this function and Lua's built-in ``error()`` function is that when the error reaches the client, its error code is preserved. In contrast, a Lua error would always be presented to the client as :errcode:`ER_PROC_LUA`. The ``clock`` module returns time values derived from the Posix / C CLOCK_GETTIME_ function or equivalent. Most functions in the module return a number of seconds; functions whose names end in "64" return a 64-bit number of nanoseconds. The ``fiber-ipc`` submodule allows sending and receiving messages between different processes. The words "different processes" in this context mean different connections, different sessions, or different fibers. The ``fiber`` module allows for creating, running and managing *fibers*. The ``jit`` module has functions for tracing the LuaJIT Just-In-Time compiler's progress, showing the byte-code or assembler output that the compiler produces, and in general providing information about what LuaJIT does with Lua code. The ``msgpack`` module takes strings in MsgPack_ format and decodes them, or takes a series of non-MsgPack values and encodes them. The ``net.box`` module contains connectors to remote database systems. One variant, to be discussed later, is for connecting to MySQL or MariaDB or PostgreSQL — that variant is the subject of the :ref:`SQL DBMS modules <dbms_modules>` appendix. In this section the subject is the built-in variant, ``net.box``. This is for connecting to tarantool servers via a network. The ``sock:name()`` function is used to get information about the near side of the connection. If a socket was bound to ``xyz.com:45``, then ``sock:name`` will return information about ``[host:xyz.com, port:45]``. The equivalent POSIX function is ``getsockname()``. The ``sock:peer()`` function is used to get information about the far side of a connection. If a TCP connection has been made to a distant host ``tarantool.org:80``, ``sock:peer()`` will return information about ``[host:tarantool.org, port:80]``. The equivalent POSIX function is ``getpeername()``. The ``socket.getaddrinfo()`` function is useful for finding information about a remote site so that the correct arguments for ``sock:sysconnect()`` can be passed. The ``socket.tcp_server()`` function makes Tarantool act as a server that can accept connections. Usually the same objective is accomplished with ``box.cfg{listen=...)``. The ``socket`` module allows exchanging data via BSD sockets with a local or remote host in connection-oriented (TCP) or datagram-oriented (UDP) mode. Semantics of the calls in the ``socket`` API closely follow semantics of the corresponding POSIX calls. Function names and signatures are mostly compatible with `luasocket`_. The ``yaml`` module takes strings in YAML_ format and decodes them, or takes a series of non-YAML values and encodes them. The above code means: use `tcp_server()` to wait for a connection from any host on port 3302. When it happens, enter a loop that reads on the socket and prints what it reads. The "delimiter" for the read function is "\\n" so each `read()` will read a string as far as the next line feed, including the line feed. The actual output will be a line containing the current timestamp, a module name, 'E' or 'W' or 'I' or 'D' or 'R' depending on ``log_level_function_name``, and ``message``. Output will not occur if ``log_level_function_name`` is for a type greater than :ref:`log_level <cfg_logging-log_level>`. Messages may contain C-style format specifiers %d or %s, so :samp:`log.error('...%d...%s',{x},{y})` will work if x is a number and y is a string. The all-zero UUID value can be expressed as uuid.NULL, or as ``uuid.fromstr('00000000-0000-0000-0000-000000000000')``. The comparison with an all-zero value can also be expressed as ``uuid_with_type_cdata == uuid.NULL``. The console module allows one Tarantool server to access another Tarantool server, and allows one Tarantool server to start listening on an :ref:`admin port <administration-admin_ports>`. The console.connect function allows one Tarantool server, in interactive mode, to access another Tarantool server. Subsequent requests will appear to be handled locally, but in reality the requests are being sent to the remote server and the local server is acting as a client. Once connection is successful, the prompt will change and subsequent requests are sent to, and executed on, the remote server. Results are displayed on the local server. To return to local mode, enter :code:`control-D`. The contents of the ``box`` library can be inspected at runtime with ``box``, with no arguments. The submodules inside the box library are: ``box.schema``, ``box.tuple``, ``box.space``, ``box.index``, ``box.cfg``, ``box.info``, ``box.slab``, ``box.stat``. Every submodule contains one or more Lua functions. A few submodules contain members as well as functions. The functions allow data definition (create alter drop), data manipulation (insert delete update upsert select replace), and introspection (inspecting contents of spaces, accessing server configuration). The crc32 and crc32_update functions use the `CRC-32C (Castagnoli)`_ polynomial value: ``0x1EDC6F41`` / ``4812730177``. If it is necessary to be compatible with other checksum functions in other programming languages, ensure that the other functions use the same polynomial value. The csv module handles records formatted according to Comma-Separated-Values (CSV) rules. The default formatting rules are: The following functions are equivalent. For example, the ``digest`` function and the ``crypto`` function will both produce the same result. The full documentation is `On the luafun section of github`_. However, the first chapter can be skipped because installation is already done, it's inside Tarantool. All that is needed is the usual :code:`require` request. After that, all the operations described in the Lua fun manual will work, provided they are preceded by the name returned by the :code:`require` request. For example: The function that can determine whether a UUID is an all-zero value is: The functions for setting up and connecting are ``socket``, ``sysconnect``, ``tcp_connect``. The functions for sending data are ``send``, ``sendto``, ``write``, ``syswrite``. The functions for receiving data are ``recv``, ``recvfrom``, ``read``. The functions for waiting before sending/receiving data are ``wait``, ``readable``, ``writable``. The functions for setting flags are ``nonblock``, ``setsockopt``. The functions for stopping and disconnecting are ``shutdown``, ``close``. The functions for error checking are ``errno``, ``error``. The functions in digest are: The functions that can convert between different types of UUID are: The functions that can return a UUID are: The guava function uses the `Consistent Hashing`_ algorithm of the Google guava library. The first parameter should be a hash code; the second parameter should be the number of buckets; the returned value will be an integer between 0 and the number of buckets. For example, The json module provides JSON manipulation routines. It is based on the `Lua-CJSON module by Mark Pulford`_. For a complete manual on Lua-CJSON please read `the official documentation`_. The monotonic time. Derived from C function clock_gettime(CLOCK_MONOTONIC). Monotonic time is similar to wall clock time but is not affected by changes to or from daylight saving time, or by changes done by a user. This is the best function to use with benchmarks that need to calculate elapsed time. The os module contains the functions :ref:`execute() <os-execute>`, :ref:`rename() <os-rename>`, :ref:`getenv() <os-getenv>`, :ref:`remove() <os-remove>`, :ref:`date() <os-date>`, :ref:`exit() <os-exit>`, :ref:`time() <os-time>`, :ref:`clock() <os-clock>`, :ref:`tmpname() <os-tmpname>`. Most of these functions are described in the Lua manual Chapter 22 `The Operating System Library <https://www.lua.org/pil/contents.html#22>`_. The other potential problem comes from fibers which never get scheduled, because they are not subscribed to any events, or because no relevant events occur. Such morphing fibers can be killed with :ref:`fiber.kill() <fiber-kill>` at any time, since :ref:`fiber.kill() <fiber-kill>` sends an asynchronous wakeup event to the fiber, and :ref:`fiber.testcancel() <fiber-testcancel>` is checked whenever such a wakeup event occurs. The output from the above script will look approximately like this: The possible options which can be passed to csv functions are: The processor time. Derived from C function clock_gettime(CLOCK_PROCESS_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a CPU. The result of ``tap.test`` is an object, which will be called taptest in the rest of this discussion, which is necessary for ``taptest:plan()`` and all the other methods. The result of the json.encode request will look like this: The same configuration settings exist for json, for :ref:`MsgPack <msgpack-module>`, and for :ref:`YAML <yaml-module>`. >>>>>>> Fix every NOTE to be highlighted on site + JSON cfg rewritten The tap module streamlines the testing of other modules. It allows writing of tests in the `TAP protocol`_. The results from the tests can be parsed by standard TAP-analyzers so they can be passed to utilities such as `prove`_. Thus one can run tests and then use the results for statistics, decision-making, and so on. The thread time. Derived from C function clock_gettime(CLOCK_THREAD_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a thread within a CPU. The time that a function takes within a processor. This function uses clock.proc(), therefore it calculates elapsed CPU time. Therefore it is not useful for showing actual elapsed time. The wall clock time. Derived from C function clock_gettime(CLOCK_REALTIME). This is the best function for knowing what the official time is, as determined by the system administrator. There are configuration settings which affect the way that Tarantool encodes invalid numbers or types. They are all boolean ``true``/``false`` values There are no restrictions on the types of requests that can be entered, except those which are due to privilege restrictions -- by default the login to the remote server is done with user name = 'guest'. The remote server could allow for this by granting at least one privilege: :code:`box.schema.user.grant('guest','execute','universe')`. This example will work with the sandbox configuration described in the preface. That is, there is a space named tester with a numeric primary key. Assume that the database is nearly empty. Assume that the tarantool server is running on ``localhost 127.0.0.1:3301``. This function may be useful before invoking a function which might otherwise block indefinitely. This is a basic function which is used by other functions. Depending on the value of ``condition``, print 'ok' or 'not ok' along with debugging information. Displays the message. This is the negation of ``taptest:is(...)``. To run this example: put the script in a file named ./tap.lua, then make tap.lua executable by saying ``chmod a+x ./tap.lua``, then execute using Tarantool as a script processor by saying ./tap.lua. To use Tarantool binary protocol primitives from Lua, it's necessary to convert Lua variables to binary format. The ``pickle.pack()`` helper function is prototyped after Perl 'pack_'. Typically a socket session will begin with the setup functions, will set one or more flags, will have a loop with sending and receiving functions, will end with the teardown functions -- as an example at the end of this section will show. Throughout, there may be error-checking and waiting functions for synchronization. To prevent a fiber containing socket functions from "blocking" other fibers, the :ref:`implicit yield rules <atomic-the_implicit_yield_rules>` will cause a yield so that other processes may take over, as is the norm for cooperative multitasking. URL or IP address UUID converted from cdata input value. UUID in 16-byte binary string UUID in 36-byte hexadecimal string Use of a TCP socket over the Internet Use of a UDP socket on localhost Use tcp_server to accept file contents sent with socat Wait for connection to be active or closed. Wait until something is either readable or writable, or until a timeout value expires. Wait until something is readable, or until a timeout value expires. Wait until something is writable, or until a timeout value expires. When called with a Lua-table argument, the code and reason have any user-desired values. The result will be those values. When called without arguments, ``box.error()`` re-throws whatever the last error was. When enclosed by quote marks, commas and line feeds and spaces are treated as ordinary characters, and a pair of quote marks "" is treated as a single quote mark. Will display ``# bad plan: ...`` if the number of completed tests is not equal to the number of tests specified by ``taptest:plan(...)``. Write as much as possible data to the socket buffer if non-blocking. Rarely used. For details see `this description`_. Yield control to the scheduler. Equivalent to :ref:`fiber.sleep(0) <fiber-sleep>`. Yield control to the transaction processor thread and sleep for the specified number of seconds. Only the current fiber can be made to sleep. [0] = 5 [0] = nil ``__serialize = "map" or "mapping"`` for a map ``__serialize = "seq" or "sequence"`` for an array ``__serialize="map"`` for a Flow Mapping map. ``__serialize="map"`` for a map ``__serialize="mapping"`` for a Block Mapping map, ``__serialize="seq"`` for a Flow Sequence array, ``__serialize="seq"`` for an array ``__serialize="sequence"`` for a Block Sequence array, ``byte-order`` can be one of next flags: ``cfg.encode_invalid_as_nil`` - use null for all unrecognizable types (default is false) ``cfg.encode_invalid_numbers`` - allow nan and inf (default is true) ``cfg.encode_load_metatables`` - load metatables (default is false) ``cfg.encode_use_tostring`` - use tostring for unrecognizable types (default is false) ``conn:call('func', '1', '2', '3')`` is the remote-call equivalent of ``func('1', '2', '3')``. That is, ``conn:call`` is a remote stored-procedure call. ``fh:pwrite`` returns true if success, false if failure. ``fh:pread`` returns the data that was read, or nil if failure. ``fh:read`` and ``fh:write`` affect the seek position within the file, and this must be taken into account when working on the same file from multiple fibers. It is possible to limit or prevent file access from other fibers with ``fiber.ipc``. ``fh:write`` returns true if success, false if failure. ``fh:read`` returns the data that was read, or nil if failure. ``fio.link`` and ``fio.symlink`` and ``fio.unlink`` return true if success, false if failure. ``fio.readlink`` returns the link value if success, nil if failure. ``sock:nonblock()`` returns the current flag value. ``sock:nonblock(false)`` sets the flag to false and returns false. ``sock:nonblock(true)`` sets the flag to true and returns true. ``socket.getaddrinfo('tarantool.org', 'http')`` will return variable information such as ``taptest:fail('x')`` is equivalent to ``taptest:ok(false, 'x')``. Displays the message. ``taptest:skip('x')`` is equivalent to ``taptest:ok(true, 'x' .. '# skip')``. Displays the message. ``timeout(...)`` is a wrapper which sets a timeout for the request that follows it. a UUID a binary string containing all arguments, packed according to the format specifiers. a connected socket, if no error. a function a possible option is `wait_connect` a socket object on success a string formatted as JSON. a string formatted as MsgPack. a string formatted as YAML. a string of the requested length on success. a string, or any object which has a read() method, formatted according to the CSV rules a table which can be formatted according to the CSV rules. a value that will be checked a, A actual result aes128 - aes-128 (with 192-bit binary strings using AES) aes192 - aes-192 (with 192-bit binary strings using AES) aes256 - aes-256 (with 256-bit binary strings using AES) an arbitrary name to give for the test outputs. an empty string if there is nothing more to read, or a nil value if error, or a string up to ``limit`` bytes long, which may include the bytes that matched the ``delimiter`` expression. an expression which is true or false an unconnected socket, or nil. and the second encoding means: any object which has a write() method arguments, that must be passed to function b, B bool boolean boolean. box.cfg{}
socket = require('socket')
socket.tcp_server('0.0.0.0', 3302, function(s)
    while true do
      local request
      request = s:read("\n");
      if request == "" or request == nil then
        break
      end
      print(request)
    end
  end) cbc - Cipher Block Chaining cdata cfb - Cipher Feedback changed name of file or directory. client/server conn = net_box.new('localhost:3301')
conn = net_box.new('127.0.0.1:3306', {wait_connect = false}) conn object conn:call('function5') conn:close() conn:eval('return 5+5') conn:timeout(0.5).space.tester:update({1}, {{'=', 2, 15}}) console = require('console')
console.start() converted UUID converts Lua variable to a 1-byte integer, and stores the integer in the resulting string converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 4-byte float, and stores the float in the resulting string converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, big converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 8-byte double, and stores the double in the resulting string converts Lua variable to a sequence of bytes, and stores the sequence in the resulting string converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, low byte first created fiber object crypto = require('crypto')

-- print aes-192 digest of 'AB', with one step, then incrementally
print(crypto.cipher.aes192.cbc.encrypt('AB', 'key'))
c = crypto.cipher.aes192.cbc.encrypt.new()
c:init()
c:update('A', 'key')
c:update('B', 'key')
print(c:result())
c:free()

-- print sha-256 digest of 'AB', with one step, then incrementally
print(crypto.digest.sha256('AB'))
c = crypto.digest.sha256.new()
c:init()
c:update('A')
c:update('B')
print(c:result())
c:free() crypto.cipher.aes192.cbc.encrypt('string', 'key', 'initialization')
crypto.cipher.aes256.ecb.decrypt('string', 'key', 'initialization') crypto.cipher.aes256.cbc.encrypt('string', 'key') == digest.aes256cbc.encrypt('string', 'key')
crypto.digest.md4('string') == digest.md4('string')
crypto.digest.md5('string') == digest.md5('string')
crypto.digest.sha('string') == digest.sha('string')
crypto.digest.sha1('string') == digest.sha1('string')
crypto.digest.sha224('string') == digest.sha224('string')
crypto.digest.sha256('string') == digest.sha256('string')
crypto.digest.sha384('string') == digest.sha384('string')
crypto.digest.sha512('string') == digest.sha512('string') crypto.digest.md4('string')
crypto.digest.sha512('string') current system time (in microseconds since the epoch) as a 64-bit integer. The time is taken from the event loop clock. current system time (in seconds since the epoch) as a Lua number. The time is taken from the event loop clock, which makes this call very cheap, but still useful for constructing artificial tuple keys. d des    - des (with 56-bit binary strings using DES, though DES is not recommended) details about the file. digest = require('digest')

-- print crc32 of 'AB', with one step, then incrementally
print(digest.crc32('AB'))
c = digest.crc32.new()
c:update('A')
c:update('B')
print(c:result())

-- print murmur hash of 'AB', with one step, then incrementally
print(digest.murmur('AB'))
m = digest.murmur.new()
m:update('A')
m:update('B')
print(m:result()) directory name, that is, path name except for file name. dss - dss (using DSS) dss1 - dss (using DSS-1) due to :ref:`the implicit yield rules <atomic-the_implicit_yield_rules>` a local :samp:`box.space.{space-name}:select`:code:`{...}` does not yield, but a remote :samp:`conn.space.{space-name}:select`:code:`{...}` call does yield, so global variables or database tuples data may change when a remote :samp:`conn.space.{space-name}:select`:code:`{...}` occurs. dumped_value ecb - Electronic Codebook either a scalar value or a Lua table value. error checking existing file name. expected result f false fiber = require('fiber')
channel = fiber.channel(10)
function consumer_fiber()
    while true do
        local task = channel:get()
        ...
    end
end

function consumer2_fiber()
    while true do
        -- 10 seconds
        local task = channel:get(10)
        if task ~= nil then
            ...
        else
            -- timeout
        end
    end
end

function producer_fiber()
    while true do
        task = box.space...:select{...}
        ...
        if channel:is_empty() then
            -- channel is empty
        end

        if channel:is_full() then
            -- channel is full
        end

        ...
        if channel:has_readers() then
            -- there are some fibers
            -- that are waiting for data
        end
        ...

        if channel:has_writers() then
            -- there are some fibers
            -- that are waiting for readers
        end
        channel:put(task)
    end
end

function producer2_fiber()
    while true do
        task = box.space...select{...}
        -- 10 seconds
        if channel:put(task, 10) then
            ...
        else
            -- timeout
        end
    end
end fiber object for the currently scheduled fiber. fiber object for the specified fiber. fiber object, for example the fiber object returned by :ref:`fiber.create <fiber-create>` fields which describe the file's block size, creation time, size, and other attributes. file handle (later - fh) file name file-handle as returned by ``fio.open()``. fixarray(2), fixstr(1), "A", fixstr(1), "B" fixmap(2), key(1), fixstr(1), "A", key(2), fixstr(2), "B". flag setting function f()
  print("D")
end
jit.bc.dump(f) function hexdump(bytes)
    local result = ''
    for i = 1, #bytes do
        result = result .. string.format("%x", string.byte(bytes, i)) .. ' '
    end
    return result
end

msgpack = require('msgpack')
m1 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "seq"
                          }))
m2 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "map"
                          }))
print('array encoding: ', hexdump(m1))
print('map encoding: ', hexdump(m2)) host - a number, 0 (zero), meaning "all local interfaces"; host - a string containing "unix/"; host - a string representation of an IPv4 address or an IPv6 address; i, I id of the fiber. information iterator function json = require('json')
json.cfg{encode_invalid_numbers = true}
x = 0/0
y = 1/0
json.encode({1, x, y, 2}) l, L linked name. list of files whose names match the input string loaded_value lua_object mask bits. maximum number of bytes to read for example 50 means "stop after 50 bytes" maximum number of seconds to wait for example 50 means "stop after 50 seconds". md4 - md4 (with 128-bit binary strings using MD4) md5 - md5 (with 128-bit binary strings using MD5) mdc2 - mdc2 (using MDC2) message, a table containing "host", "family" and "port" fields. message_sender.host = '18.44.0.1'
message_sender.family = 'AF_INET'
message_sender.port = 43065 msgpack.NULL n name of existing file or directory, name of test name of the fiber. net_box.self:is_connected() net_box.self:ping() net_box.self:wait_connected() new active and timeout values. new channel. new group uid. new name. new permissions new socket if success. new user uid. nil num number number of a pre-defined error number of bytes to read number of context switches, backtrace, id, total memory, used memory, name for each fiber. number of seconds to sleep. number or number64 number, string numeric identifier of the fiber. ofb - Output Feedback offset within file where reading or writing begins one of ``'l'``, ``'b'``, ``'h'`` or ``'n'``. one or more strings to be concatenated. optional. see :ref:`above <csv-options>` original name. part of the message which will accompany the error path name path name of file. path of directory. path-name, which may contain wildcard characters. port - a number. port - a number. If a port number is 0 (zero), the socket will be bound to a random local port. port - a string containing a path to a unix socket. port number position to seek to positive integer as great as the maximum number of slots (spaces for ``get`` or ``put`` messages) that might be pending at any given time. previous mask bits. q, Q receiving result for ``sock:errno()``, result for ``sock:error()``. If there is no error, then ``sock:errno()`` will return 0 and ``sock:error()``. ripemd160 - rtype: table s, S same as nil scalar values to be formatted seconds or nanoseconds since epoch (1970-01-01 00:00:00), adjusted. seconds or nanoseconds since processor start. seconds or nanoseconds since the last time that the computer was booted. seconds or nanoseconds since thread start. see :ref:`above <csv-options>` sending separator for example '?' means "stop after a question mark" setup sha - sha (with 160-bit binary strings using SHA-0) sha1 - sha-1 (with 160-bit binary strings using SHA-1) sha224 - sha-224 (with 224-bit binary strings using SHA-2) sha256 - sha-256 (with 256-bit binary strings using SHA-2) sha384 - sha-384 (with 384-bit binary strings using SHA-2) sha512 - sha-512(with 512-bit binary strings using SHA-2). socket = require('socket')
sock = socket('AF_INET', 'SOCK_STREAM', 'tcp')
sock:sysconnect(0, 3301) socket('AF_INET', 'SOCK_STREAM', 'tcp') socket.SHUT_RD, socket.SHUT_WR, or socket.SHUT_RDWR. socket.tcp_server('localhost', 3302, function () end) state checking string string containing format specifiers string, table string, which is written to ``writable`` if specified suffix table table. first element = seconds of CPU time; second element = whatever the function returns. tap = require('tap')
taptest = tap.test('test-name') taptest tarantool> -- input in file.csv is:
tarantool> -- a,"b,c ",d
tarantool> -- a\\211\\128b
tarantool> fio = require('fio')
---
...
tarantool> f = fio.open('./file.csv', {'O_RDONLY'})
---
...
tarantool> csv.load(f, {chunk_size = 4096})
---
- - - a
    - 'b,c '
    - d
  - - a\\211\\128b
...
tarantool> f:close(nn)
---
- true
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.error()
---
- error: Arbitrary message
...
tarantool> box.error(box.error.FUNCTION_ACCESS_DENIED, 'A', 'B', 'C')
---
- error: A access denied for user 'B' to function 'C'
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.schema.space.create('#')
---
- error: Invalid identifier '#' (expected letters, digits or an underscore)
...
tarantool> box.error.last()
---
- line: 278
  code: 70
  type: ClientError
  message: Invalid identifier '#' (expected letters, digits or an underscore)
  file: /tmp/buildd/tarantool-1.7.0.252.g1654e31~precise/src/box/key_def.cc
...
tarantool> box.error.clear()
---
...
tarantool> box.error.last()
---
- null
... tarantool> console = require('console')
---
...
tarantool> console.connect('198.18.44.44:3301')
---
...
198.18.44.44:3301> -- prompt is telling us that server is remote tarantool> console = require('console')
---
...
tarantool> console.listen('unix/:/tmp/X.sock')
... main/103/console/unix/:/tmp/X I> started
---
- fd: 6
  name:
    host: unix/
    family: AF_UNIX
    type: SOCK_STREAM
    protocol: 0
    port: /tmp/X.sock
... tarantool> csv = require('csv')
---
...
tarantool> csv.dump({'a','b,c ','d'})
---
- 'a,"b,c ",d

'
... tarantool> csv = require('csv')
---
...
tarantool> csv.load('a,"b,c ",d')
---
- - - a
    - 'b,c '
    - d
... tarantool> csv.load('a,b;c,d', {delimiter = ';'})
---
- - - a,b
    - c,d
... tarantool> csv.load('a\\211\\128b')
---
- - - a\211\128b
... tarantool> csv_table = csv.load('a,b,c')
---
...
tarantool> csv.dump(csv_table)
---
- 'a,b,c

'
... tarantool> digest = require('digest')
---
...
tarantool> function password_insert()
         >   box.space.tester:insert{1234, digest.sha1('^S^e^c^ret Wordpass')}
         >   return 'OK'
         > end
---
...
tarantool> function password_check(password)
         >   local t = box.space.tester:select{12345}
         >   if digest.sha1(password) == t[2] then
         >     return 'Password is valid'
         >   else
         >     return 'Password is not valid'
         >   end
         > end
---
...
tarantool> password_insert()
---
- 'OK'
... tarantool> digest.guava(10863919174838991, 11)
---
- 8
... tarantool> dostring('abc')
---
error: '[string "abc"]:1: ''='' expected near ''<eof>'''
...
tarantool> dostring('return 1')
---
- 1
...
tarantool> dostring('return ...', 'hello', 'world')
---
- hello
- world
...
tarantool> dostring([[
         >   local f = function(key)
         >     local t = box.space.tester:select{key}
         >     if t ~= nil then
         >       return t[1]
         >     else
         >       return nil
         >     end
         >   end
         >   return f(...)]], 1)
---
- null
... tarantool> fh = fio.open('/home/username/tmp.txt', {'O_RDWR', 'O_APPEND'})
---
...
tarantool> fh -- display file handle returned by fio.open
---
- fh: 11
... tarantool> fh:close() -- where fh = file-handle
---
- true
... tarantool> fh:fsync()
---
- true
... tarantool> fh:pread(25, 25)
---
- |
  elete from t8//
  insert in
... tarantool> fh:seek(20, 'SEEK_SET')
---
- 20
... tarantool> fh:stat()
---
- inode: 729866
  rdev: 0
  size: 100
  atime: 140942855
  mode: 33261
  mtime: 1409430660
  nlink: 1
  uid: 1000
  blksize: 4096
  gid: 1000
  ctime: 1409430660
  dev: 2049
  blocks: 8
... tarantool> fh:truncate(0)
---
- true
... tarantool> fh:write('new data')
---
- true
... tarantool> fiber = require('fiber')
---
...
tarantool> function f () fiber.sleep(1000); end
---
...
tarantool> fiber_function = fiber:create(f)
---
- error: '[string "fiber_function = fiber:create(f)"]:1: fiber.create(function, ...):
    bad arguments'
...
tarantool> fiber_function = fiber.create(f)
---
...
tarantool> fiber_function.storage.str1 = 'string'
---
...
tarantool> fiber_function.storage['str1']
---
- string
...
tarantool> fiber_function:cancel()
---
...
tarantool> fiber_function.storage['str1']
---
- error: '[string "return fiber_function.storage[''str1'']"]:1: the fiber is dead'
... tarantool> fiber = require('fiber')
---
...
tarantool> function function_name()
         >   fiber.sleep(1000)
         > end
---
...
tarantool> fiber_object = fiber.create(function_name)
---
... tarantool> fiber = require('fiber')
tarantool> function function_x()
         >   gvar = 0
         >   while 0 == 0 do
         >     gvar = gvar + 1
         >     fiber.sleep(2)
         >   end
         > end
---
... tarantool> fiber.find(101)
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.info()
---
- 101:
    csw: 7
    backtrace: []
    fid: 101
    memory:
      total: 65776
      used: 0
    name: interactive
... tarantool> fiber.kill(fiber.id())
---
- error: fiber is cancelled
... tarantool> fiber.self()
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.self():cancel()
---
- error: fiber is cancelled
... tarantool> fiber.self():name('non-interactive')
---
... tarantool> fiber.self():name()
---
- interactive
... tarantool> fiber.self():status()
---
- running
... tarantool> fiber.sleep(1.5)
---
... tarantool> fiber.status()
---
- running
... tarantool> fiber.testcancel()
---
- error: fiber is cancelled
... tarantool> fiber.time(), fiber.time()
---
- 1448466279.2415
- 1448466279.2415
... tarantool> fiber.time(), fiber.time64()
---
- 1448466351.2708
- 1448466351270762
... tarantool> fiber.yield()
---
... tarantool> fiber_object = fiber.self()
---
...
tarantool> fiber_object:id()
---
- 101
... tarantool> fiber_of_x:cancel()
---
...
tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  dead . gvar= 421
---
... tarantool> fid = fiber_of_x:id()
---
... tarantool> fio.basename('/path/to/my.lua', '.lua')
---
- my
... tarantool> fio.chmod('/home/username/tmp.txt', tonumber('0755', 8))
---
- true
...
tarantool> fio.chown('/home/username/tmp.txt', 'username', 'username')
---
- true
... tarantool> fio.cwd()
---
- /home/username/tarantool_sandbox
... tarantool> fio.dirname('path/to/my.lua')
---
- 'path/to/'
... tarantool> fio.glob('/etc/x*')
---
- - /etc/xdg
  - /etc/xml
  - /etc/xul-ext
... tarantool> fio.link('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
...
tarantool> fio.unlink('/home/username/tmp.txt2')
---
- true
... tarantool> fio.lstat('/etc')
---
- inode: 1048577
  rdev: 0
  size: 12288
  atime: 1421340698
  mode: 16877
  mtime: 1424615337
  nlink: 160
  uid: 0
  blksize: 4096
  gid: 0
  ctime: 1424615337
  dev: 2049
  blocks: 24
... tarantool> fio.mkdir('/etc')
---
- false
... tarantool> fio.pathjoin('/etc', 'default', 'myfile')
---
- /etc/default/myfile
... tarantool> fio.rename('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
... tarantool> fio.sync()
---
- true
... tarantool> fio.tempdir()
---
- /tmp/lG31e7
... tarantool> fio.truncate('/home/username/tmp.txt', 99999)
---
- true
... tarantool> fio.umask(tonumber('755', 8))
---
- 493
... tarantool> fun = require('fun')
---
...
tarantool> for _k, a in fun.range(3) do
         >   print(a)
         > end
1
2
3
---
... tarantool> gvar = 0

tarantool> fiber_of_x = fiber.create(function_x)
---
... tarantool> json = require('json')
---
...
tarantool> json.decode('123')
---
- 123
...
tarantool> json.decode('[123, "hello"]')
---
- [123, 'hello']
...
tarantool> json.decode('{"hello": "world"}').hello
---
- world
... tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- '["A","B"]'
...
tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="map"}))
---
- '{"1":"A","2":"B"}'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- '[{"f2":"B","f1":"A"}]'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="seq"})})
---
- '[[]]'
... tarantool> json.encode({1, x, y, 2})
---
- '[1,nan,inf,2]
... tarantool> json=require('json')
---
...
tarantool> json.encode(123)
---
- '123'
...
tarantool> json.encode({123})
---
- '[123]'
...
tarantool> json.encode({123, 234, 345})
---
- '[123,234,345]'
...
tarantool> json.encode({abc = 234, cde = 345})
---
- '{"cde":345,"abc":234}'
...
tarantool> json.encode({hello = {'world'}})
---
- '{"hello":["world"]}'
... tarantool> load = function(readable, opts)
         >   opts = opts or {}
         >   local result = {}
         >   for i, tup in csv.iterate(readable, opts) do
         >     result[i] = tup
         >   end
         >   return result
         > end
---
...
tarantool> load('a,b,c')
---
- - - a
    - b
    - c
... tarantool> msgpack = require('msgpack')
---
...
tarantool> y = msgpack.encode({'a',1,'b',2})
---
...
tarantool> z = msgpack.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> box.space.tester:insert{20, msgpack.NULL, 20}
---
- [20, null, 20]
... tarantool> net_box = require('net.box')
---
...
tarantool> function example()
         >   local conn, wtuple
         >   if net_box.self:ping() then
         >     table.insert(ta, 'self:ping() succeeded')
         >     table.insert(ta, '  (no surprise -- self connection is pre-established)')
         >   end
         >   if box.cfg.listen == '3301' then
         >     table.insert(ta,'The local server listen address = 3301')
         >   else
         >     table.insert(ta, 'The local server listen address is not 3301')
         >     table.insert(ta, '(  (maybe box.cfg{...listen="3301"...} was not stated)')
         >     table.insert(ta, '(  (so connect will fail)')
         >   end
         >   conn = net_box.new('127.0.0.1:3301')
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester.')
         >   conn.space.tester:insert{800, 'data'}
         >   table.insert(ta, 'conn insert done on tester, index 0')
         >   table.insert(ta, '  primary key value = 800.')
         >   wtuple = conn.space.tester:select{800}
         >   table.insert(ta, 'conn select done on tester, index 0')
         >   table.insert(ta, '  number of fields = ' .. #wtuple)
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester')
         >   conn.space.tester:replace{800, 'New data', 'Extra data'}
         >   table.insert(ta, 'conn:replace done on tester')
         >   conn:timeout(0.5).space.tester:update({800}, {{'=', 2, 'Fld#1'}})
         >   table.insert(ta, 'conn update done on tester')
         >   conn:close()
         >   table.insert(ta, 'conn close done')
         > end
---
...
tarantool> ta = {}
---
...
tarantool> example()
---
...
tarantool> ta
---
- - self:ping() succeeded
  - '  (no surprise -- self connection is pre-established)'
  - The local server listen address = 3301
  - conn delete done on tester.
  - conn insert done on tester, index 0
  - '  primary key value = 800.'
  - conn select done on tester, index 0
  - '  number of fields = 1'
  - conn delete done on tester
  - conn:replace done on tester
  - conn update done on tester
  - conn close done
... tarantool> os.clock()
---
- 0.05
... tarantool> os.date("%A %B %d")
---
- Sunday April 24
... tarantool> os.execute('ls -l /usr')
total 200
drwxr-xr-x   2 root root 65536 Apr 22 15:49 bin
drwxr-xr-x  59 root root 20480 Apr 18 07:58 include
drwxr-xr-x 210 root root 65536 Apr 18 07:59 lib
drwxr-xr-x  12 root root  4096 Apr 22 15:49 local
drwxr-xr-x   2 root root 12288 Jan 31 09:50 sbin
---
... tarantool> os.exit()
user@user-shell:~/tarantool_sandbox$ tarantool> os.getenv('PATH')
---
- /usr/local/sbin:/usr/local/bin:/usr/sbin
... tarantool> os.remove('file')
---
- true
... tarantool> os.rename('local','foreign')
---
- null
- 'local: No such file or directory'
- 2
... tarantool> os.time()
---
- 1461516945
... tarantool> os.tmpname()
---
- /tmp/lua_7SW1m2
... tarantool> password_insert('Secret Password')
---
- 'Password is not valid'
... tarantool> pickle = require('pickle')
---
...
tarantool> box.space.tester:insert{0, 'hello world'}
---
- [0, 'hello world']
...
tarantool> box.space.tester:update({0}, {{'=', 2, 'bye world'}})
---
- [0, 'bye world']
...
tarantool> box.space.tester:update({0}, {
         >   {'=', 2, pickle.pack('iiA', 0, 3, 'hello')}
         > })
---
- [0, "\0\0\0\0\x03\0\0\0hello"]
...
tarantool> box.space.tester:update({0}, {{'=', 2, 4}})
---
- [0, 4]
...
tarantool> box.space.tester:update({0}, {{'+', 2, 4}})
---
- [0, 8]
...
tarantool> box.space.tester:update({0}, {{'^', 2, 4}})
---
- [0, 12]
... tarantool> pickle = require('pickle')
---
...
tarantool> tuple = box.space.tester:replace{0}
---
...
tarantool> string.len(tuple[1])
---
- 1
...
tarantool> pickle.unpack('b', tuple[1])
---
- 48
...
tarantool> pickle.unpack('bsi', pickle.pack('bsi', 255, 65535, 4294967295))
---
- 255
- 65535
- 4294967295
...
tarantool> pickle.unpack('ls', pickle.pack('ls', tonumber64('18446744073709551615'), 65535))
---
...
tarantool> num, num64, str = pickle.unpack('slA', pickle.pack('slA', 666,
         > tonumber64('666666666666666'), 'string'))
---
... tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  suspended . gvar= 399
---
... tarantool> socket = require('socket')
---
...
tarantool> sock = socket.tcp_connect('tarantool.org', 80)
---
...
tarantool> type(sock)
---
- table
...
tarantool> sock:error()
---
- null
...
tarantool> sock:send("HEAD / HTTP/1.0rnHost: tarantool.orgrnrn")
---
- true
...
tarantool> sock:read(17)
---
- "HTTP/1.1 200 OKrn"
...
tarantool> sock:close()
---
- true
... tarantool> socket = require('socket')
---
...
tarantool> sock_1 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_1:bind('127.0.0.1')
---
- true
...
tarantool> sock_2 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_2:sendto('127.0.0.1', sock_1:name().port,'X')
---
- true
...
tarantool> message = sock_1:recvfrom()
---
...
tarantool> message
---
- X
...
tarantool> sock_1:close()
---
- true
...
tarantool> sock_2:close()
---
- true
... tarantool> strict = require('strict')
---
...
tarantool> strict.on()
---
...
tarantool> a = b -- strict mode is on so this will cause an error
---
- error: ... variable ''b'' is not declared'
...
tarantool> strict.off()
---
...
tarantool> a = b -- strict mode is off so this will not cause an error
---
... tarantool> taptest:ok(true, 'x')
ok - x
---
- true
...
tarantool> tap = require('tap')
---
...
tarantool> taptest = tap.test('test-name')
TAP version 13
---
...
tarantool> taptest:ok(1 + 1 == 2, 'X')
ok - X
---
- true
... tarantool> taptest:skip('message')
ok - message # skip
---
- true
... tarantool> tarantool = require('tarantool')
---
...
tarantool> tarantool
---
- build:
    target: Linux-x86_64-RelWithDebInfo
    options: cmake . -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_BACKTRACE=ON
    mod_format: so
    flags: ' -fno-common -fno-omit-frame-pointer -fno-stack-protector -fexceptions
      -funwind-tables -fopenmp -msse2 -std=c11 -Wall -Wextra -Wno-sign-compare -Wno-strict-aliasing
      -fno-gnu89-inline'
    compiler: /usr/bin/x86_64-linux-gnu-gcc /usr/bin/x86_64-linux-gnu-g++
  uptime: 'function: 0x408668e0'
  version: 1.7.0-66-g9093daa
  pid: 'function: 0x40866900'
...
tarantool> tarantool.pid()
---
- 30155
...
tarantool> tarantool.uptime()
---
- 108.64641499519
... tarantool> type(123456789012345), type(tonumber64(123456789012345))
---
- number
- number
...
tarantool> i = tonumber64('1000000000')
---
...
tarantool> type(i), i / 2, i - 2, i * 2, i + 2, i % 2, i ^ 2
---
- number
- 500000000
- 999999998
- 2000000000
- 1000000002
- 0
- 1000000000000000000
... tarantool> uuid = require('uuid')
---
...
tarantool> uuid(), uuid.bin(), uuid.str()
---
- 16ffedc8-cbae-4f93-a05e-349f3ab70baa
- !!binary FvG+Vy1MfUC6kIyeM81DYw==
- 67c999d2-5dce-4e58-be16-ac1bcb93160f
...
tarantool> uu = uuid()
---
...
tarantool> #uui:bin(), #uu:str(), type(uu), uu:isnil()
---
- 16
- 36
- cdata
- false
... tarantool> yaml = require('yaml')
---
...
tarantool> y = yaml.encode({'a', 1, 'b', 2})
---
...
tarantool> z = yaml.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> if yaml.NULL == nil then print('hi') end
hi
---
... tarantool> yaml = require('yaml')
---
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="sequence"}))
---
- |
  ---
  - A
  - B
  ...
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- |
  ---
  ['A', 'B']
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- |
  ---
  - {'f2': 'B', 'f1': 'A'}
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="mapping"})})
---
- |
  ---
  - f2: B
    f1: A
  ...
... tcp_connect('127.0.0.1', 3301) teardown the :ref:`URI <index-uri>` of the target for the connection the URI of the local server the URI of the remote server the ``NO_SUCH_USER`` message is "``User '%s' is not found``" -- it includes one "``%s``" component which will be replaced with errtext. Thus a call to ``box.error(box.error.NO_SUCH_USER, 'joe')`` or ``box.error(45, 'joe')`` will result in an error with the accompanying message "``User 'joe' is not found``". the function to be associated with the fiber the id of the fiber to be cancelled. the message to be displayed. the new name of the fiber. the new position if success the number of bytes sent. the number of bytes that were decoded. the number of messages. the original contents formatted as a Lua table. the original contents formatted as a Lua table; the original value reformatted as a JSON string. the original value reformatted as a MsgPack string. the original value reformatted as a YAML string. the socket object value may change if sysconnect() succeeds. the specified fiber does not exist or cancel is not permitted. the status of ``fiber``. One of: “dead”, “suspended”, or “running”. the status of fiber. One of: “dead”, “suspended”, or “running”. the value placed on the channel by an earlier ``channel:put()``. true true for success, false for error. true if blocked users are waiting. Otherwise false. true if connected, false on failure. true if success, false if failure. true if success, false on failure. true if the socket is now readable, false if timeout expired; true if the socket is now writable, false if timeout expired; true if the specified channel is already closed. Otherwise false. true if the specified channel is empty true if the specified channel is full (has no room for a new message). true if the value is all zero, otherwise false. true on success, false on error true on success, false on error. For example, if sock is already closed, sock:close() returns false. true or false. true when connected, false on failure. use Digest::CRC;
$d = Digest::CRC->new(width => 32, poly => 0x1EDC6F41, init => 0xFFFFFFFF, refin => 1, refout => 1);
$d->add('string');
print $d->digest; userdata value to write what to execute. what will be passed to function whatever is returned by the Lua code chunk. whatever is specified in errcode-number. zero or more scalar values which will be appended to, or substitute for, items in the Lua chunk. {} Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-19 12:23+0300
PO-Revision-Date: 2016-08-19 13:23+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 "" "Crypto" is short for "Cryptography", which generally refers to the production of a digest value from a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's crypto module supports ten types of cryptographic hash functions (AES_, DES_, DSS_, MD4_, MD5_, MDC2_, RIPEMD_, SHA-0_, SHA-1_, SHA-2_). Some of the crypto functionality is also present in the :ref:`digest` module. The functions in crypto are: #!/usr/bin/tarantool
local tap = require('tap')
test = tap.test("my test name")
test:plan(2)
test:ok(2 * 2 == 4, "2 * 2 is 4")
test:test("some subtests for test2", function(test)
    test:plan(2)
    test:is(2 + 2, 4, "2 + 2 is 4")
    test:isnt(2 + 3, 4, "2 + 3 is not 4")
end)
test:check() $ socat TCP:localhost:3302 ./tmp.txt $ ~/tarantool/src/tarantool
tarantool> box.cfg{log_level=3, logger='tarantool.txt'}
tarantool> log = require('log')
tarantool> log.error('Error')
tarantool> log.info('Info %s', box.info.version)
tarantool> os.exit()
$ less tarantool.txt 'R' if the socket is now readable, 'W' if the socket is now writable, 'RW' if the socket is now both readable and writable, '' (empty string) if timeout expired; '``SEEK_END``' = end of file, '``SEEK_CUR``' = current position, '``SEEK_SET``' = start of file. 'a' 'b' - big-endian, 'false' = c2 'fixmap' if metatable is 'map' = 80 otherwise 'fixarray' = 90 'fixmap(0)' = 80 -- nil is not stored when it is a missing map value 'fixmap(1)' + 'positive fixint' (for the key) + 'positive fixint' (for the value) = 81 00 05 'fixstr' = a1 61 'float 64' = cb 3f f8 00 00 00 00 00 00 'h' - endianness depends on host (default), 'l' - little-endian, 'n' - endianness depends on network 'nil' = c0 'positive fixint' = 7f 'true' = c3 'uint 16' = cd ff ff 'uint 32' = ce ff ff ff ff (the expected output is 3304160206). **Common Types and MsgPack Encodings** **Example:** **Format specifiers** **Result:** **Socket functions** **array** encoding: 92 a1 41 a1 42
**map** encoding:   82 01 a1 41 02 a1 42 -- Benchmark a function which sleeps 10 seconds.
-- NB: bench() will not calculate sleep time.
-- So the returned value will be {a number less than 10, 88}.
clock = require('clock')
fiber = require('fiber')
function f(param)
  fiber.sleep(param)
  return 88
end
clock.bench(f,10) -- Disassemble hexadecimal 97 which is the x86 code for xchg eax, edi
jit.dis_x86.disass('\x97') -- Disassemble hexadecimal 97 which is the x86-64 code for xchg eax, edi
jit.dis_x64.disass('\x97') -- Show the machine code of a Lua "for" loop
jit.dump.on('m')
local x = 0;
for i = 1, 1e6 do
  x = x + i
end
print(x)
jit.dump.off() -- Show what LuaJIT is doing for a Lua "for" loop
jit.v.on()
local x = 0
for i = 1, 1e6 do
    x = x + i
end
print(x)
jit.v.off() -- This will print an approximate number of years since 1970.
clock = require('clock')
print(clock.time() / (365*24*60*60)) -- This will print nanoseconds in the CPU since the start.
clock = require('clock')
print(clock.proc64()) -- This will print nanoseconds since the start.
clock = require('clock')
print(clock.monotonic64()) -- This will print seconds in the thread since the start.
clock = require('clock')
print(clock.thread64()) -- When nil is assigned to a Lua-table field, the field is null
tarantool> {nil, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a Lua-table field, the field is json.NULL
tarantool> {json.NULL, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a JSON field, the field is null
tarantool> json.encode({field2 = json.NULL, field1 = 'a', field3 = 'c'}
---
- '{"field2":null,"field1":"a","field3":"c"}'
... ---
- - host: 188.93.56.70
    family: AF_INET
    type: SOCK_STREAM
    protocol: tcp
    port: 80
  - host: 188.93.56.70
    family: AF_INET
    type: SOCK_DGRAM
    protocol: udp
    port: 80
... 1.5 127 16-byte binary string 16-byte string 2...0 [5257] main/101/interactive C> version 1.7.0-355-ga4f762d
2...1 [5257] main/101/interactive C> log level 3
2...1 [5261] main/101/spawner C> initialized
2...0 [5257] main/101/interactive [C]:-1 E> Error 36-byte binary string 36-byte hexadecimal string 4294967295 65535 :ref:`csv.iterate() <csv-iterate>` is the low level of :ref:`csv.load() <csv-load>` and :ref:`csv.dump() <csv-dump>`. To illustrate that, here is a function which is the same as the :ref:`csv.load() <csv-load>` function, as seen in `the Tarantool source code`_. :ref:`socket() <socket-socket>` :ref:`socket.getaddrinfo() <socket-getaddrinfo>` :ref:`socket.tcp_connect() <socket-tcp_connect>` :ref:`socket.tcp_server() <socket-tcp_server>` :ref:`socket_object:accept() <socket-accept>` :ref:`socket_object:close() <socket-close>` :ref:`socket_object:errno() <socket-error>` :ref:`socket_object:error() <socket-error>` :ref:`socket_object:getsockopt() <socket-getsockopt>` :ref:`socket_object:linger() <socket-linger>` :ref:`socket_object:listen() <socket-listen>` :ref:`socket_object:name() <socket-name>` :ref:`socket_object:nonblock() <socket-nonblock>` :ref:`socket_object:peer() <socket-peer>` :ref:`socket_object:read() <socket-read>` :ref:`socket_object:readable() <socket-readable>` :ref:`socket_object:recv() <socket-recv>` :ref:`socket_object:recvfrom() <socket-recvfrom>` :ref:`socket_object:send() <socket-send>` :ref:`socket_object:sendto() <socket-sendto>` :ref:`socket_object:setsockopt() <socket-setsockopt>` :ref:`socket_object:shutdown() <socket-shutdown>` :ref:`socket_object:sysconnect() <socket-sysconnect>` :ref:`socket_object:syswrite() <socket-syswrite>` :ref:`socket_object:wait() <socket-wait>` :ref:`socket_object:writable() <socket-writable>` :ref:`socket_object:write() <socket-send>` :ref:`uuid() <uuid-__call>` :ref:`uuid.bin() <uuid-bin>` :ref:`uuid.frombin() <uuid-frombin>` :ref:`uuid.fromstr() <uuid-fromstr>` :ref:`uuid.str() <uuid-str>` :ref:`uuid_object:bin() <uuid-object_bin>` :ref:`uuid_object:isnil() <uuid-isnil>` :ref:`uuid_object:str() <uuid-object_str>` :samp:`chunk-size = {number}` -- number of characters to read at once (usually for file-IO efficiency), default = 4096 :samp:`conn.space.{space-name}:delete(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:delete(...)`. :samp:`conn.space.{space-name}:get(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:get(...)`. :samp:`conn.space.{space-name}:insert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:insert(...)`. :samp:`conn.space.{space-name}:replace(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:replace(...)`. :samp:`conn.space.{space-name}:select`:code:`{...}` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:select`:code:`{...}`. :samp:`conn.space.{space-name}:update(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:update(...)`. :samp:`conn.space.{space-name}:upsert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:upsert(...)`. :samp:`conn:eval({Lua-string})` evaluates and executes the expression in Lua-string, which may be any statement or series of statements. An :ref:`execute privilege <authentication-privileges>` is required; if the user does not have it, an administrator may grant it with :samp:`box.schema.user.grant({username}, 'execute', 'universe')`. :samp:`delimiter = {string}` -- single-byte character to designate end-of-field, default = comma :samp:`quote_char = {string}` -- single-byte character to designate encloser of string, default = quote mark :samp:`skip_head_lines = {number}` -- number of lines to skip at the start (usually for a header), default 0 :samp:`{function parameters}` = whatever values are required by the function. :samp:`{function}` = function or function reference; A "UUID" is a `Universally unique identifier`_. If an application requires that a value be unique only within a single computer or on a single database, then a simple counter is better than a UUID, because getting a UUID is time-consuming (it requires a syscall_). For clusters of computers, or widely distributed applications, UUIDs are better. A "digest" is a value which is returned by a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's digest module supports several types of cryptographic hash functions (AES_, MD4_, MD5_, SHA-0_, SHA-1_, SHA-2_) as well as a checksum function (CRC32_), two functions for base64_, and two non-cryptographic hash functions (guava_, murmur_). Some of the digest functionality is also present in the :ref:`crypto <crypto>` module. A fiber has all the features of a Lua coroutine_ and all the programming concepts that apply for Lua coroutines will apply for fibers as well. However, Tarantool has made some enhancements for fibers and has used fibers internally. So, although use of coroutines is possible and supported, use of fibers is recommended. A fiber is a set of instructions which are executed with cooperative multitasking. Fibers managed by the fiber module are associated with a user-supplied function called the *fiber function*. A fiber has three possible states: **running**, **suspended** or **dead**. When a fiber is created with :ref:`fiber.create() <fiber-create>`, it is running. When a fiber yields control with :ref:`fiber.sleep() <fiber-sleep>`, it is suspended. When a fiber ends (because the fiber function ends), it is dead. A list of strings or numbers. A nil object A runaway fiber can be stopped with :ref:`fiber_object.cancel <fiber_object-cancel>`. However, :ref:`fiber_object.cancel <fiber_object-cancel>` is advisory — it works only if the runaway fiber calls :ref:`fiber.testcancel() <fiber-testcancel>` occasionally. Most ``box.*`` functions, such as :ref:`box.space...delete() <box_space-delete>` or :ref:`box.space...update() <box_space-update>`, do call :ref:`fiber.testcancel() <fiber-testcancel>` but :ref:`box.space...select{} <box_space-select>` does not. In practice, a runaway fiber can only become unresponsive if it does many computations and does not check whether it has been cancelled. A special use of ``console.start()`` is with :ref:`initialization files <index-init_label>`. Normally, if one starts the tarantool server with :samp:`tarantool {initialization file}` there is no console. This can be remedied by adding these lines at the end of the initialization file: A table containing these fields: "host", "family", "type", "protocol", "port". A value comparable to Lua "nil" which may be useful as a placeholder in a tuple. Accept a new client connection and create a new connected socket. It is good practice to set the socket's blocking mode explicitly after accepting. Additionally one can see the uptime and the server version and the process id. Those information items can also be accessed with :ref:`box.info <box_introspection-box_info>` but use of the tarantool module is recommended. After ``message_content, message_sender = recvfrom(1)`` the value of ``message_content`` might be a string containing 'X' and the value of ``message_sender`` might be a table containing All ``net.box`` methods are fiber-safe, that is, it is safe to share and use the same connection object across multiple concurrent fibers. In fact, it's perhaps the best programming practice with Tarantool. When multiple fibers use the same connection, all requests are pipelined through the same network socket, but each fiber gets back a correct response. Reducing the number of active sockets lowers the overhead of system calls and increases the overall server performance. There are, however, cases when a single connection is not enough — for example when it's necessary to prioritize requests or to use different authentication ids. All fibers are part of the fiber registry. This registry can be searched with :ref:`fiber.find() <fiber-find>` - via fiber id (fid), which is a numeric identifier. All remote calls support execution timeouts. Using a wrapper object makes the remote connection API compatible with the local one, removing the need for a separate ``timeout`` argument, which the local version would ignore. Once a request is sent, it cannot be revoked from the remote server even if a timeout expires: the timeout expiration only aborts the wait for the remote server response, not the request itself. Also, some MsgPack configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Also, some YAML configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Bind a socket to the given host/port. A UDP socket after binding can be used to receive data (see :ref:`socket_object.recvfrom <socket-recvfrom>`). A TCP socket can be used to accept new connections, after it has been put in listen mode. By default strict mode is off, unless tarantool was built with the ``-DCMAKE_BUILD_TYPE=Debug`` option -- see the description of build options in section :ref:`building-from-source <building_from_source>`. By saying ``require('tarantool')``, one can answer some questions about how the tarantool server was built, such as "what flags were used", or "what was the version of the compiler". CSV-table has 3 fields, field#2 has "," so result has quote marks Call ``fiber.channel()`` to allocate space and get a channel object, which will be called channel for examples in this section. Call the other ``fiber-ipc`` routines, via channel, to send messages, receive messages, or check ipc status. Message exchange is synchronous. The channel is garbage collected when no one is using it, as with any other Lua object. Use object-oriented syntax, for example ``channel:put(message)`` rather than ``fiber.channel.put(message)``. Call ``require('net.box')`` to get a ``net.box`` object, which will be called ``net_box`` for examples in this section. Call ``net_box.new()`` to connect and get a connection object, which will be called ``conn`` for examples in this section. Call the other ``net.box()`` routines, passing ``conn:``, to execute requests on the remote box. Call :ref:`conn:close <socket-close>` to disconnect. Cancel a fiber. Running and suspended fibers can be cancelled. After a fiber has been cancelled, attempts to operate on it will cause errors, for example :ref:`fiber_object:id() <fiber_object-id>` will cause ``error: the fiber is dead``. Change the fiber name. By default the Tarantool server's interactive-mode fiber is named 'interactive' and new fibers created due to :ref:`fiber.create <fiber-create>` are named 'lua'. Giving fibers distinct names makes it easier to distinguish them when using :ref:`fiber.info <fiber-info>`. Change the size of an open file. Differs from ``fio.truncate``, which changes the size of a closed file. Check if the current fiber has been cancelled and throw an exception if this is the case. Check whether the first argument equals the second argument. Displays extensive message if the result is false. Check whether the specified channel is empty (has no messages). Check whether the specified channel is empty and has readers waiting for a message (because they have issued ``channel:get()`` and then blocked). Check whether the specified channel is full and has writers waiting (because they have issued ``channel:put()`` and then blocked due to lack of room). Check whether the specified channel is full. Checks the number of tests performed. This check should only be done after all planned tests are complete, so ordinarily ``taptest:check()`` will only appear at the end of a script. Clears the record of errors, so functions like `box.error()` or `box.error.last()` will have no effect. Close (destroy) a socket. A closed socket should not be used any more. A socket is closed automatically when its userdata is garbage collected by Lua. Close a connection. Close a file that was opened with ``fio.open``. For details type "man 2 close". Close the channel. All waiters in the channel will be woken up. All following ``channel:put()`` or ``channel:get()`` operations will return an error (``nil``). Commas designate end-of-field, Common file manipulations Common pathname manipulations Concatenate partial string, separated by '/' to form a path name. Configuration settings Connect a socket to a remote host. Connect an existing socket to a remote host. The argument values are the same as in tcp_connect(). The host must be an IP address. Connect to the server at :ref:`URI <index-uri>`, change the prompt from ':samp:`tarantool>`' to ':samp:`{uri}>`', and act henceforth as a client until the user ends the session or types :code:`control-D`. Connection objects are garbage collected just like any other objects in Lua, so an explicit destruction is not mandatory. However, since close() is a system call, it is good programming practice to close a connection explicitly when it is no longer needed, to avoid lengthy stalls of the garbage collector. Convert a JSON string to a Lua object. Convert a Lua object to a JSON string. Convert a Lua object to a MsgPack string. Convert a Lua object to a YAML string. Convert a MsgPack string to a Lua object. Convert a YAML string to a Lua object. Convert a string or a Lua number to a 64-bit integer. The result can be used in arithmetic, and the arithmetic will be 64-bit integer arithmetic rather than floating-point arithmetic. (Operations on an unconverted Lua number use floating-point arithmetic.) The ``tonumber64()`` function is added by Tarantool; the name is global. Counterpart to ``pickle.pack()``. Warning: if format specifier 'A' is used, it must be the last item. Create a new TCP or UDP socket. The argument values are the same as in the `Linux socket(2) man page <http://man7.org/linux/man-pages/man2/socket.2.html>`_. Create a new communication channel. Create a new connection. The connection is established on demand, at the time of the first request. It is re-established automatically after a disconnect. The returned ``conn`` object supports methods for making remote requests, such as select, update or delete. Create and start a fiber. The fiber is created and begins to run immediately. Create or delete a directory. For details type "man 2 mkdir" or "man 2 rmdir". Display a diagnostic message. Either: Emulate a request error, with text based on one of the pre-defined Tarantool errors defined in the file `errcode.h <https://github.com/tarantool/tarantool/blob/1.7/src/box/errcode.h>`_ in the source tree. Lua constants which correspond to those Tarantool errors are defined as members of ``box.error``, for example ``box.error.NO_SUCH_USER == 45``. Ensure that changes are written to disk. For details type "man 2 sync". Ensure that file changes are written to disk, for an open file. Compare ``fio.sync``, which is for all files. For details type "man 2 fsync" or "man 2 fdatasync". Example Example Of Fiber Use Example showing use of most of the net.box methods Execute a PING command. Execute a function, provided it has not been executed before. A passed value is checked to see whether the function has already been executed. If it has been executed before, nothing happens. If it has not been executed before, the function is invoked. For an explanation why ``box.once`` is useful, see the section :ref:`Preventing Duplicate Actions <index-preventing_duplicate_actions>`. Execute by passing to the shell. Exit the program. If this is done on the server, then the server stops. Fetch a message from a channel. If the channel is empty, ``channel:get()`` blocks until there is a message. Find out how many messages are on the channel. The answer is 0 if the channel is empty. Flags can be passed as a number or as string constants, for example '``O_RDONLY``', '``O_WRONLY``', '``O_RDWR``'. Flags can be combined by enclosing them in braces. For a list of available options, read `the source code of bc.lua`_. For a list of available options, read `the source code of dis_x64.lua`_. For a list of available options, read `the source code of dis_x86.lua`_. For a list of available options, read `the source code of dump.lua`_. For a list of available options, read `the source code of v.lua`_. For all examples in this section the socket name will be sock and the function invocations will look like ``sock:function_name(...)``. For example, in Python, install the ``crcmod`` package and say: For example, the following code will interpret 0/0 (which is "not a number") and 1/0 (which is "infinity") as special values rather than nulls or errors: For example: For more information on, read article about `Encryption Modes`_ For the local tarantool server there is a pre-created always-established connection object named :samp:`{net_box}.self`. Its purpose is to make polymorphic use of the ``net_box`` API easier. Therefore :samp:`conn = {net_box}.new('localhost:3301')` can be replaced by :samp:`conn = {net_box}.self`. However, there is an important difference between the embedded connection and a remote one. With the embedded connection, requests which do not modify data do not yield. When using a remote connection, due to :ref:`the implicit rules <atomic-the_implicit_yield_rules>` any request can yield, and database state may have changed by the time it regains control. Form a Lua iterator function for going through CSV records one field at a time. Four choices of block cipher modes are also available: Функция `box.once` Functions to create and delete links. For details type "man readlink", "man 2 link", "man 2 symlink", "man 2 unlink".. Get CSV-formatted input from ``readable`` and return a table as output. Usually ``readable`` is either a string or a file opened for reading. Usually :samp:`{options}` is not specified. Get environment variable. Get socket flags. For a list of possible flags see ``sock:setsockopt()``. Get table input from ``csv-table`` and return a CSV-formatted string as output. Or, get table input from ``csv-table`` and put the output in ``writable``. Usually :samp:`{options}` is not specified. Usually ``writable``, if specified, is a file opened for writing. :ref:`csv.dump() <csv-dump>` is the reverse of :ref:`csv.load() <csv-load>`. Get the id of the fiber (fid), to be used in later displays. Getting the same results from digest and crypto modules Given a full path name, remove all but the final part (the file name). Also remove the suffix, if it is passed. Given a full path name, remove the final part (the file name). Here are examples for all the common types, with the Lua-table representation on the left, with the MsgPack format name and encoding on the right. Here is an example of the tcp_server function, reading strings from the client and printing them. On the client side, the Linux socat utility will be used to ship a whole file for the tcp_server function to read. Here is an example with datagrams. Set up two connections on 127.0.0.1 (localhost): ``sock_1`` and ``sock_2``. Using ``sock_2``, send a message to ``sock_1``. Using ``sock_1``, receive a message. Display the received message. Close both connections. |br| This is not a useful way for a computer to communicate with itself, but shows that the system works. If a later user calls the ``password_check()`` function and enters the wrong password, the result is an error. If the Tarantool server at :samp:`uri` requires authentication, the connection might look something like: :code:`console.connect('admin:secretpassword@distanthost.com:3301')`. If timeout is provided, and the channel doesn't become empty for the duration of the timeout, ``channel:put()`` returns false. Otherwise it returns true. In Perl, install the ``Digest::CRC`` module and run the following code: In the following example, the user creates two functions, ``password_insert()`` which inserts a SHA-1_ digest of the word "**^S^e^c^ret Wordpass**" into a tuple set, and ``password_check()`` which requires input of a password. In this example a connection is made over the internet between the Tarantool server and tarantool.org, then an HTTP "head" message is sent, and a response is received: "``HTTP/1.1 200 OK``". This is not a useful way to communicate with this particular site, but shows that the system works. Incremental methods in the crypto module Incremental methods in the digest module Indicate how many tests will be performed. Initialize. Initiates incremental MurmurHash. See :ref:`incremental methods <digest-incremental_digests>` notes. Initiates incremental crc32. See :ref:`incremental methods <digest-incremental_digests>` notes. Leading or trailing spaces are ignored, Like all Lua objects, dead fibers are garbage collected. The garbage collector frees pool allocator memory owned by the fiber, resets all fiber data, and returns the fiber (now called a fiber carcass) to the fiber pool. The carcass can be reused when another fiber is created. Line feeds, or line feeds plus carriage returns, designate end-of-record, Listen on :ref:`URI <index-uri>`. The primary way of listening for incoming requests is via the connection-information string, or URI, specified in :code:`box.cfg{listen=...}`. The alternative way of listening is via the URI specified in :code:`console.listen(...)`. This alternative way is called "administrative" or simply :ref:`"admin port" <administration-admin_ports>`. The listening is usually over a local host with a Unix domain socket. Local storage within the fiber. The storage can contain any number of named values, subject to memory limitations. Naming may be done with :samp:`{fiber_object}.storage.{name}` or :samp:`fiber_object}.storage['{name}'].` or with a number :samp:`{fiber_object}.storage[{number}]`. Values may be either numbers or strings. The storage is garbage-collected when :samp:`{fiber_object}:cancel()` happens. Locate a fiber by its numeric id and cancel it. In other words, :ref:`fiber.kill() <fiber-kill>` combines :ref:`fiber.find() <fiber-find>` and :ref:`fiber_object:cancel() <fiber_object-cancel>`. Lua `escape sequences`_ such as \\n or \\10 are legal within strings but not within files, Lua code Lua fun, also known as the Lua Functional Library, takes advantage of the features of LuaJIT to help users create complex functions. Inside the module are "sequence processors" such as map, filter, reduce, zip -- they take a user-written function as an argument and run it against every element in a sequence, which can be faster or more convenient than a user-written loop. Inside the module are "generators" such as range, tabulate, and rands -- they return a bounded or boundless series of values. Within the module are "reducers", "filters", "composers" ... or, in short, all the important features found in languages like Standard ML, Haskell, or Erlang. Lua iterator function Справочник по Lua API Make a fiber, associate function_x with the fiber, and start function_x. It will immediately "detach" so it will be running independently of the caller. Make the function which will be associated with the fiber. This function contains an infinite loop (``while 0 == 0`` is always true). Each iteration of the loop adds 1 to a global variable named gvar, then goes to sleep for 2 seconds. The sleep causes an implicit :ref:`fiber.yield() <fiber-yield>`. Manage the rights to file objects, or ownership of file objects. For details type "man 2 chown" or "man 2 chmod". Разное Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits are significant if flags include `O_CREATE` or `O_TMPFILE`. Mode bits can be combined by enclosing them in braces. Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits can be combined by enclosing them in braces. Module `box` Модуль `clock` Модуль `console` Модуль `crypto` Module `csv` Модуль `digest` Модуль `fiber` Module `fio` Module `fun` Module `jit` Module `json` Module `log` Модуль `msgpack` Модуль `net.box` Module `os` Модуль `pickle` Модуль `socket` Модуль `strict` Module `tap` Модуль `tarantool` Module `uuid` Module `yaml` N Names Now watch what happens on the first shell. The strings "A", "B", "C" are printed. On Linux the listen ``backlog`` backlog may be from /proc/sys/net/core/somaxconn, on BSD the backlog may be ``SOMAXCONN``. On the first shell, start Tarantool and say: On the second shell, create a file that contains a few lines. The contents don't matter. Suppose the first line contains A, the second line contains B, the third line contains C. Call this file "tmp.txt". On the second shell, use the socat utility to ship the tmp.txt file to the server's host and port: Open a file in preparation for reading or writing or seeking. Or: Output a user-generated message to the :ref:`log file <cfg_logging-logger>`, given log_level_function_name = ``error`` or ``warn`` or ``info`` or ``debug``. Parameters: Parameters: (string) format-string = instructions; (string) time-since-epoch = number of seconds since 1970-01-01. If time-since-epoch is omitted, it is assumed to be the current time. Parameters: (string) name = name of file or directory which will be removed. Parameters: (string) variable-name = environment variable name. Parse and execute an arbitrary chunk of Lua code. This function is mainly useful to define and run Lua code without having to introduce changes to the global Lua environment. Pass or return a cipher derived from the string, key, and (optionally, sometimes) initialization vector. The four choices of algorithms: Pass or return a digest derived from the string. The twelve choices of algorithms: Pause for a while, while the detached function runs. Then ... Cancel the fiber. Then, once again ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit more depending how long the pause lasted). This time the status is dead because the cancel worked. Pause for a while, while the detached function runs. Then ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit depending how long the pause lasted). The status is suspended because the fiber spends almost all its time sleeping or yielding. Perform non-random-access read or write on a file. For details type "man 2 read" or "man 2 write". Perform read/write random-access operation on a file, without affecting the current seek position of the file. For details type "man 2 pread" or "man 2 pwrite". Possible errors: If there is a compilation error, it is raised as a Lua error. Possible errors: On error, returns an empty string, followed by status, errno, errstr. In case the writing side has closed its end, returns the remainder read from the socket (possibly an empty string), followed by "eof" status. Possible errors: Returns nil, status, errno, errstr on error. Possible errors: cancel is not permitted for the specified fiber object. Possible errors: nil on error. Possible errors: nil. Possible errors: on error, returns status, errno, errstr. Possible errors: the connection will fail if the target Tarantool server was not initiated with :code:`box.cfg{listen=...}`. Possible errors: unknown format specifier. Prints a trace of LuaJIT's progress compiling and interpreting code Prints the byte code of a function. Prints the i386 assembler code of a string of bytes Prints the intermediate or machine code of following Lua code Prints the x86-64 assembler code of a string of bytes Purposes Quote marks may enclose fields or parts of fields, Read ``size`` bytes from a connected socket. An internal read-ahead buffer is used to reduce the cost of this call. Read from a connected socket until some condition is true, and return the bytes that were read. Reading goes on until ``limit`` bytes have been read, or a delimiter has been read, or a timeout has expired. Readable file :file:`./file.csv` contains two CSV records. Explanation of fio is in section :ref:`fio <fio-section>`. Source CSV file and example respectively: Readable string contains 2-byte character = Cyrillic Letter Palochka: (This displays a palochka if and only if character set = UTF-8.) Readable string has 3 fields, field#2 has comma and space so use quote marks: Receive a message on a UDP socket. Recursive version of ``taptest:is(...)``, which can be be used to compare tables as well as scalar values. Reduce file size to a specified value. For details type "man 2 truncate". Remove file or directory. Rename a file or directory. Rename a file or directory. For details type "man 2 rename". Retrieve information about the last error that occurred on a socket, if any. Errors do not cause throwing of exceptions so these functions are usually necessary. Return a formatted date. Return a list of files that match an input string. The list is constructed with a single flag that controls the behavior of the function: GLOB_NOESCAPE. For details type "man 3 glob". Return a name for a temporary file. Return all available data from the socket buffer if non-blocking. Rarely used. For details see `this description`_. Return information about all fibers. Return statistics about an open file. This differs from ``fio.stat`` which return statistics about a closed file. For details type "man 2 stat". Return the name of a directory that can be used to store temporary files. Return the name of the current working directory. Return the number of CPU seconds since the program start. Return the number of seconds since the epoch. Return the status of the current fiber. Return the status of the specified fiber. Returns 128-bit binary string = digest made with MD4. Returns 128-bit binary string = digest made with MD5. Returns 128-byte string = hexadecimal of a digest calculated with sha512. Returns 160-bit binary string = digest made with SHA-0.|br| Not recommended. Returns 160-bit binary string = digest made with SHA-1. Returns 224-bit binary string = digest made with SHA-2. Returns 256-bit binary string =  digest made with SHA-2. Returns 256-bit binary string = digest made with AES. Returns 32-bit binary string = digest made with MurmurHash. Returns 32-bit checksum made with CRC32. Returns 32-byte string = hexadecimal of a digest calculated with md4. Returns 32-byte string = hexadecimal of a digest calculated with md5. Returns 384-bit binary string =  digest made with SHA-2. Returns 40-byte string = hexadecimal of a digest calculated with sha. Returns 40-byte string = hexadecimal of a digest calculated with sha1. Returns 512-bit binary tring = digest made with SHA-2. Returns 56-byte string = hexadecimal of a digest calculated with sha224. Returns 64-byte string = hexadecimal of a digest calculated with sha256. Returns 96-byte string = hexadecimal of a digest calculated with sha384. Returns a description of the last error, as a Lua table with five members: "line" (number) Tarantool source file line number, "code" (number) error's number, "type", (string) error's C++ class, "message" (string) error's message, "file" (string) Tarantool source file. Additionally, if the error is a system error (for example due to a failure in socket or file io), there may be a sixth member: "errno" (number) C standard error number. Returns a number made with consistent hash. Returns a regular string from a base64 encoding. Returns array of random bytes with length = integer. Returns base64 encoding from a regular string. Returns information about a file object. For details type "man 2 lstat" or "man 2 stat". Round Trip: from string to table and back to string SO_ACCEPTCONN SO_BINDTODEVICE SO_BROADCAST SO_DEBUG SO_DOMAIN SO_DONTROUTE SO_ERROR SO_KEEPALIVE SO_MARK SO_OOBINLINE SO_PASSCRED SO_PEERCRED SO_PRIORITY SO_PROTOCOL SO_RCVBUF SO_RCVBUFFORCE SO_RCVLOWAT SO_RCVTIMEO SO_REUSEADDR SO_SNDBUF SO_SNDBUFFORCE SO_SNDLOWAT SO_SNDTIMEO SO_TIMESTAMP SO_TYPE See also :ref:`box.session.storage <box_session-storage>`. See also :ref:`fiber.time64 <fiber-time64>` and :ref:`os.clock() <os-clock>`. Semicolon instead of comma for the delimiter: Send a message on a UDP socket to a specified host. Send a message using a channel. If the channel is full, ``channel:put()`` blocks until there is a free slot in the channel. Send data over a connected socket. Serializing 'A' and 'B' with different ``__serialize`` values causes different results. To show this, here is a routine which encodes `{'A','B'}` both as an array and as a map, then displays each result in hexadecimal. Serializing 'A' and 'B' with different ``__serialize`` values causes different results: Set or clear the SO_LINGER flag. For a description of the flag, see the `Linux man page <http://man7.org/linux/man-pages/man1/loginctl.1.html>`_. Set socket flags. The argument values are the same as in the `Linux getsockopt(2) man page <http://man7.org/linux/man-pages/man2/setsockopt.2.html>`_. The ones that Tarantool accepts are: Set the auto-completion flag. If auto-completion is `true`, and the user is using tarantool as a client, then hitting the TAB key may cause tarantool to complete a word automatically. The default auto-completion value is `true`. Set the mask bits used when creating files or directories. For a detailed description type "man 2 umask". Setting SO_LINGER is done with ``sock:linger(active)``. Shift position in the file to the specified position. For details type "man 2 seek". Show whether connection is active or closed. Shutdown a reading end, a writing end, or both ends of a socket. Start listening for incoming connections. Start the console on the current interactive terminal. Start two shells. The first shell will be the server. The second shell will be the client. Вложенный модуль `box.error` Модуль `fiber-ipc` Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports for all crypto functions.. Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports with crc32 and with murmur... TAP version 13
1..2
ok - 2 * 2 is 4
    # Some subtests for test2
    1..2
    ok - 2 + 2 is 4,
    ok - 2 + 3 is not 4
    # Some subtests for test2: end
ok - some subtests for test2 Tarantool supports file input/output with an API that is similar to POSIX syscalls. All operations are performed asynchronously. Multiple fibers can access the same file simultaneously. Test whether a value has a particular type. Displays a long message if the value is not of the specified type. The "admin" address is the URI to listen on. It has no default value, so it must be specified if connections will occur via an admin port. The parameter is expressed with URI = Universal Resource Identifier format, for example "/tmpdir/unix_domain_socket.sock", or a numeric TCP port. Connections are often made with telnet. A typical port value is 3313. The 'Error' line is visible in tarantool.txt preceded by the letter E. The 'Info' line is not present because the log_level is 3. The :code:`strict` module has functions for turning "strict mode" on or off. When strict mode is on, an attempt to use an undeclared global variable will cause an error. A global variable is considered "undeclared" if it has never had a value assigned to it. Often this is an indication of a programming error. The JSON output structure can be specified with ``__serialize``: The MsgPack Specification_ page explains that the first encoding means: The MsgPack output structure can be specified with ``__serialize``: The Tarantool server puts all diagnostic messages in a log file specified by the :ref:`logger <cfg_logging-logger>` configuration parameter. Diagnostic messages may be either system-generated by the server's internal code, or user-generated with the ``log.log_level_function_name`` function. The `YAML collection style <http://yaml.org/spec/1.1/#id930798>`_ can be specified with ``__serialize``: The ``box.error`` function is for raising an error. The difference between this function and Lua's built-in ``error()`` function is that when the error reaches the client, its error code is preserved. In contrast, a Lua error would always be presented to the client as :errcode:`ER_PROC_LUA`. The ``clock`` module returns time values derived from the Posix / C CLOCK_GETTIME_ function or equivalent. Most functions in the module return a number of seconds; functions whose names end in "64" return a 64-bit number of nanoseconds. The ``fiber-ipc`` submodule allows sending and receiving messages between different processes. The words "different processes" in this context mean different connections, different sessions, or different fibers. The ``fiber`` module allows for creating, running and managing *fibers*. The ``jit`` module has functions for tracing the LuaJIT Just-In-Time compiler's progress, showing the byte-code or assembler output that the compiler produces, and in general providing information about what LuaJIT does with Lua code. The ``msgpack`` module takes strings in MsgPack_ format and decodes them, or takes a series of non-MsgPack values and encodes them. The ``net.box`` module contains connectors to remote database systems. One variant, to be discussed later, is for connecting to MySQL or MariaDB or PostgreSQL — that variant is the subject of the :ref:`SQL DBMS modules <dbms_modules>` appendix. In this section the subject is the built-in variant, ``net.box``. This is for connecting to tarantool servers via a network. The ``sock:name()`` function is used to get information about the near side of the connection. If a socket was bound to ``xyz.com:45``, then ``sock:name`` will return information about ``[host:xyz.com, port:45]``. The equivalent POSIX function is ``getsockname()``. The ``sock:peer()`` function is used to get information about the far side of a connection. If a TCP connection has been made to a distant host ``tarantool.org:80``, ``sock:peer()`` will return information about ``[host:tarantool.org, port:80]``. The equivalent POSIX function is ``getpeername()``. The ``socket.getaddrinfo()`` function is useful for finding information about a remote site so that the correct arguments for ``sock:sysconnect()`` can be passed. The ``socket.tcp_server()`` function makes Tarantool act as a server that can accept connections. Usually the same objective is accomplished with ``box.cfg{listen=...)``. The ``socket`` module allows exchanging data via BSD sockets with a local or remote host in connection-oriented (TCP) or datagram-oriented (UDP) mode. Semantics of the calls in the ``socket`` API closely follow semantics of the corresponding POSIX calls. Function names and signatures are mostly compatible with `luasocket`_. The ``yaml`` module takes strings in YAML_ format and decodes them, or takes a series of non-YAML values and encodes them. The above code means: use `tcp_server()` to wait for a connection from any host on port 3302. When it happens, enter a loop that reads on the socket and prints what it reads. The "delimiter" for the read function is "\\n" so each `read()` will read a string as far as the next line feed, including the line feed. The actual output will be a line containing the current timestamp, a module name, 'E' or 'W' or 'I' or 'D' or 'R' depending on ``log_level_function_name``, and ``message``. Output will not occur if ``log_level_function_name`` is for a type greater than :ref:`log_level <cfg_logging-log_level>`. Messages may contain C-style format specifiers %d or %s, so :samp:`log.error('...%d...%s',{x},{y})` will work if x is a number and y is a string. The all-zero UUID value can be expressed as uuid.NULL, or as ``uuid.fromstr('00000000-0000-0000-0000-000000000000')``. The comparison with an all-zero value can also be expressed as ``uuid_with_type_cdata == uuid.NULL``. The console module allows one Tarantool server to access another Tarantool server, and allows one Tarantool server to start listening on an :ref:`admin port <administration-admin_ports>`. The console.connect function allows one Tarantool server, in interactive mode, to access another Tarantool server. Subsequent requests will appear to be handled locally, but in reality the requests are being sent to the remote server and the local server is acting as a client. Once connection is successful, the prompt will change and subsequent requests are sent to, and executed on, the remote server. Results are displayed on the local server. To return to local mode, enter :code:`control-D`. The contents of the ``box`` library can be inspected at runtime with ``box``, with no arguments. The submodules inside the box library are: ``box.schema``, ``box.tuple``, ``box.space``, ``box.index``, ``box.cfg``, ``box.info``, ``box.slab``, ``box.stat``. Every submodule contains one or more Lua functions. A few submodules contain members as well as functions. The functions allow data definition (create alter drop), data manipulation (insert delete update upsert select replace), and introspection (inspecting contents of spaces, accessing server configuration). The crc32 and crc32_update functions use the `CRC-32C (Castagnoli)`_ polynomial value: ``0x1EDC6F41`` / ``4812730177``. If it is necessary to be compatible with other checksum functions in other programming languages, ensure that the other functions use the same polynomial value. The csv module handles records formatted according to Comma-Separated-Values (CSV) rules. The default formatting rules are: The following functions are equivalent. For example, the ``digest`` function and the ``crypto`` function will both produce the same result. The full documentation is `On the luafun section of github`_. However, the first chapter can be skipped because installation is already done, it's inside Tarantool. All that is needed is the usual :code:`require` request. After that, all the operations described in the Lua fun manual will work, provided they are preceded by the name returned by the :code:`require` request. For example: The function that can determine whether a UUID is an all-zero value is: The functions for setting up and connecting are ``socket``, ``sysconnect``, ``tcp_connect``. The functions for sending data are ``send``, ``sendto``, ``write``, ``syswrite``. The functions for receiving data are ``recv``, ``recvfrom``, ``read``. The functions for waiting before sending/receiving data are ``wait``, ``readable``, ``writable``. The functions for setting flags are ``nonblock``, ``setsockopt``. The functions for stopping and disconnecting are ``shutdown``, ``close``. The functions for error checking are ``errno``, ``error``. The functions in digest are: The functions that can convert between different types of UUID are: The functions that can return a UUID are: The guava function uses the `Consistent Hashing`_ algorithm of the Google guava library. The first parameter should be a hash code; the second parameter should be the number of buckets; the returned value will be an integer between 0 and the number of buckets. For example, The json module provides JSON manipulation routines. It is based on the `Lua-CJSON module by Mark Pulford`_. For a complete manual on Lua-CJSON please read `the official documentation`_. The monotonic time. Derived from C function clock_gettime(CLOCK_MONOTONIC). Monotonic time is similar to wall clock time but is not affected by changes to or from daylight saving time, or by changes done by a user. This is the best function to use with benchmarks that need to calculate elapsed time. The os module contains the functions :ref:`execute() <os-execute>`, :ref:`rename() <os-rename>`, :ref:`getenv() <os-getenv>`, :ref:`remove() <os-remove>`, :ref:`date() <os-date>`, :ref:`exit() <os-exit>`, :ref:`time() <os-time>`, :ref:`clock() <os-clock>`, :ref:`tmpname() <os-tmpname>`. Most of these functions are described in the Lua manual Chapter 22 `The Operating System Library <https://www.lua.org/pil/contents.html#22>`_. The other potential problem comes from fibers which never get scheduled, because they are not subscribed to any events, or because no relevant events occur. Such morphing fibers can be killed with :ref:`fiber.kill() <fiber-kill>` at any time, since :ref:`fiber.kill() <fiber-kill>` sends an asynchronous wakeup event to the fiber, and :ref:`fiber.testcancel() <fiber-testcancel>` is checked whenever such a wakeup event occurs. The output from the above script will look approximately like this: The possible options which can be passed to csv functions are: The processor time. Derived from C function clock_gettime(CLOCK_PROCESS_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a CPU. The result of ``tap.test`` is an object, which will be called taptest in the rest of this discussion, which is necessary for ``taptest:plan()`` and all the other methods. The result of the json.encode request will look like this: The same configuration settings exist for json, for :ref:`MsgPack <msgpack-module>`, and for :ref:`YAML <yaml-module>`. >>>>>>> Fix every NOTE to be highlighted on site + JSON cfg rewritten The tap module streamlines the testing of other modules. It allows writing of tests in the `TAP protocol`_. The results from the tests can be parsed by standard TAP-analyzers so they can be passed to utilities such as `prove`_. Thus one can run tests and then use the results for statistics, decision-making, and so on. The thread time. Derived from C function clock_gettime(CLOCK_THREAD_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a thread within a CPU. The time that a function takes within a processor. This function uses clock.proc(), therefore it calculates elapsed CPU time. Therefore it is not useful for showing actual elapsed time. The wall clock time. Derived from C function clock_gettime(CLOCK_REALTIME). This is the best function for knowing what the official time is, as determined by the system administrator. There are configuration settings which affect the way that Tarantool encodes invalid numbers or types. They are all boolean ``true``/``false`` values There are no restrictions on the types of requests that can be entered, except those which are due to privilege restrictions -- by default the login to the remote server is done with user name = 'guest'. The remote server could allow for this by granting at least one privilege: :code:`box.schema.user.grant('guest','execute','universe')`. This example will work with the sandbox configuration described in the preface. That is, there is a space named tester with a numeric primary key. Assume that the database is nearly empty. Assume that the tarantool server is running on ``localhost 127.0.0.1:3301``. This function may be useful before invoking a function which might otherwise block indefinitely. This is a basic function which is used by other functions. Depending on the value of ``condition``, print 'ok' or 'not ok' along with debugging information. Displays the message. This is the negation of ``taptest:is(...)``. To run this example: put the script in a file named ./tap.lua, then make tap.lua executable by saying ``chmod a+x ./tap.lua``, then execute using Tarantool as a script processor by saying ./tap.lua. To use Tarantool binary protocol primitives from Lua, it's necessary to convert Lua variables to binary format. The ``pickle.pack()`` helper function is prototyped after Perl 'pack_'. Typically a socket session will begin with the setup functions, will set one or more flags, will have a loop with sending and receiving functions, will end with the teardown functions -- as an example at the end of this section will show. Throughout, there may be error-checking and waiting functions for synchronization. To prevent a fiber containing socket functions from "blocking" other fibers, the :ref:`implicit yield rules <atomic-the_implicit_yield_rules>` will cause a yield so that other processes may take over, as is the norm for cooperative multitasking. URL or IP address UUID converted from cdata input value. UUID in 16-byte binary string UUID in 36-byte hexadecimal string Use of a TCP socket over the Internet Use of a UDP socket on localhost Use tcp_server to accept file contents sent with socat Wait for connection to be active or closed. Wait until something is either readable or writable, or until a timeout value expires. Wait until something is readable, or until a timeout value expires. Wait until something is writable, or until a timeout value expires. When called with a Lua-table argument, the code and reason have any user-desired values. The result will be those values. When called without arguments, ``box.error()`` re-throws whatever the last error was. When enclosed by quote marks, commas and line feeds and spaces are treated as ordinary characters, and a pair of quote marks "" is treated as a single quote mark. Will display ``# bad plan: ...`` if the number of completed tests is not equal to the number of tests specified by ``taptest:plan(...)``. Write as much as possible data to the socket buffer if non-blocking. Rarely used. For details see `this description`_. Yield control to the scheduler. Equivalent to :ref:`fiber.sleep(0) <fiber-sleep>`. Yield control to the transaction processor thread and sleep for the specified number of seconds. Only the current fiber can be made to sleep. [0] = 5 [0] = nil ``__serialize = "map" or "mapping"`` for a map ``__serialize = "seq" or "sequence"`` for an array ``__serialize="map"`` for a Flow Mapping map. ``__serialize="map"`` for a map ``__serialize="mapping"`` for a Block Mapping map, ``__serialize="seq"`` for a Flow Sequence array, ``__serialize="seq"`` for an array ``__serialize="sequence"`` for a Block Sequence array, ``byte-order`` can be one of next flags: ``cfg.encode_invalid_as_nil`` - use null for all unrecognizable types (default is false) ``cfg.encode_invalid_numbers`` - allow nan and inf (default is true) ``cfg.encode_load_metatables`` - load metatables (default is false) ``cfg.encode_use_tostring`` - use tostring for unrecognizable types (default is false) ``conn:call('func', '1', '2', '3')`` is the remote-call equivalent of ``func('1', '2', '3')``. That is, ``conn:call`` is a remote stored-procedure call. ``fh:pwrite`` returns true if success, false if failure. ``fh:pread`` returns the data that was read, or nil if failure. ``fh:read`` and ``fh:write`` affect the seek position within the file, and this must be taken into account when working on the same file from multiple fibers. It is possible to limit or prevent file access from other fibers with ``fiber.ipc``. ``fh:write`` returns true if success, false if failure. ``fh:read`` returns the data that was read, or nil if failure. ``fio.link`` and ``fio.symlink`` and ``fio.unlink`` return true if success, false if failure. ``fio.readlink`` returns the link value if success, nil if failure. ``sock:nonblock()`` returns the current flag value. ``sock:nonblock(false)`` sets the flag to false and returns false. ``sock:nonblock(true)`` sets the flag to true and returns true. ``socket.getaddrinfo('tarantool.org', 'http')`` will return variable information such as ``taptest:fail('x')`` is equivalent to ``taptest:ok(false, 'x')``. Displays the message. ``taptest:skip('x')`` is equivalent to ``taptest:ok(true, 'x' .. '# skip')``. Displays the message. ``timeout(...)`` is a wrapper which sets a timeout for the request that follows it. a UUID a binary string containing all arguments, packed according to the format specifiers. a connected socket, if no error. a function a possible option is `wait_connect` a socket object on success a string formatted as JSON. a string formatted as MsgPack. a string formatted as YAML. a string of the requested length on success. a string, or any object which has a read() method, formatted according to the CSV rules a table which can be formatted according to the CSV rules. a value that will be checked a, A actual result aes128 - aes-128 (with 192-bit binary strings using AES) aes192 - aes-192 (with 192-bit binary strings using AES) aes256 - aes-256 (with 256-bit binary strings using AES) an arbitrary name to give for the test outputs. an empty string if there is nothing more to read, or a nil value if error, or a string up to ``limit`` bytes long, which may include the bytes that matched the ``delimiter`` expression. an expression which is true or false an unconnected socket, or nil. and the second encoding means: any object which has a write() method arguments, that must be passed to function b, B bool boolean boolean. box.cfg{}
socket = require('socket')
socket.tcp_server('0.0.0.0', 3302, function(s)
    while true do
      local request
      request = s:read("\n");
      if request == "" or request == nil then
        break
      end
      print(request)
    end
  end) cbc - Cipher Block Chaining cdata cfb - Cipher Feedback changed name of file or directory. client/server conn = net_box.new('localhost:3301')
conn = net_box.new('127.0.0.1:3306', {wait_connect = false}) conn object conn:call('function5') conn:close() conn:eval('return 5+5') conn:timeout(0.5).space.tester:update({1}, {{'=', 2, 15}}) console = require('console')
console.start() converted UUID converts Lua variable to a 1-byte integer, and stores the integer in the resulting string converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 4-byte float, and stores the float in the resulting string converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, big converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 8-byte double, and stores the double in the resulting string converts Lua variable to a sequence of bytes, and stores the sequence in the resulting string converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, low byte first created fiber object crypto = require('crypto')

-- print aes-192 digest of 'AB', with one step, then incrementally
print(crypto.cipher.aes192.cbc.encrypt('AB', 'key'))
c = crypto.cipher.aes192.cbc.encrypt.new()
c:init()
c:update('A', 'key')
c:update('B', 'key')
print(c:result())
c:free()

-- print sha-256 digest of 'AB', with one step, then incrementally
print(crypto.digest.sha256('AB'))
c = crypto.digest.sha256.new()
c:init()
c:update('A')
c:update('B')
print(c:result())
c:free() crypto.cipher.aes192.cbc.encrypt('string', 'key', 'initialization')
crypto.cipher.aes256.ecb.decrypt('string', 'key', 'initialization') crypto.cipher.aes256.cbc.encrypt('string', 'key') == digest.aes256cbc.encrypt('string', 'key')
crypto.digest.md4('string') == digest.md4('string')
crypto.digest.md5('string') == digest.md5('string')
crypto.digest.sha('string') == digest.sha('string')
crypto.digest.sha1('string') == digest.sha1('string')
crypto.digest.sha224('string') == digest.sha224('string')
crypto.digest.sha256('string') == digest.sha256('string')
crypto.digest.sha384('string') == digest.sha384('string')
crypto.digest.sha512('string') == digest.sha512('string') crypto.digest.md4('string')
crypto.digest.sha512('string') current system time (in microseconds since the epoch) as a 64-bit integer. The time is taken from the event loop clock. current system time (in seconds since the epoch) as a Lua number. The time is taken from the event loop clock, which makes this call very cheap, but still useful for constructing artificial tuple keys. d des    - des (with 56-bit binary strings using DES, though DES is not recommended) details about the file. digest = require('digest')

-- print crc32 of 'AB', with one step, then incrementally
print(digest.crc32('AB'))
c = digest.crc32.new()
c:update('A')
c:update('B')
print(c:result())

-- print murmur hash of 'AB', with one step, then incrementally
print(digest.murmur('AB'))
m = digest.murmur.new()
m:update('A')
m:update('B')
print(m:result()) directory name, that is, path name except for file name. dss - dss (using DSS) dss1 - dss (using DSS-1) due to :ref:`the implicit yield rules <atomic-the_implicit_yield_rules>` a local :samp:`box.space.{space-name}:select`:code:`{...}` does not yield, but a remote :samp:`conn.space.{space-name}:select`:code:`{...}` call does yield, so global variables or database tuples data may change when a remote :samp:`conn.space.{space-name}:select`:code:`{...}` occurs. dumped_value ecb - Electronic Codebook either a scalar value or a Lua table value. error checking existing file name. expected result f false fiber = require('fiber')
channel = fiber.channel(10)
function consumer_fiber()
    while true do
        local task = channel:get()
        ...
    end
end

function consumer2_fiber()
    while true do
        -- 10 seconds
        local task = channel:get(10)
        if task ~= nil then
            ...
        else
            -- timeout
        end
    end
end

function producer_fiber()
    while true do
        task = box.space...:select{...}
        ...
        if channel:is_empty() then
            -- channel is empty
        end

        if channel:is_full() then
            -- channel is full
        end

        ...
        if channel:has_readers() then
            -- there are some fibers
            -- that are waiting for data
        end
        ...

        if channel:has_writers() then
            -- there are some fibers
            -- that are waiting for readers
        end
        channel:put(task)
    end
end

function producer2_fiber()
    while true do
        task = box.space...select{...}
        -- 10 seconds
        if channel:put(task, 10) then
            ...
        else
            -- timeout
        end
    end
end fiber object for the currently scheduled fiber. fiber object for the specified fiber. fiber object, for example the fiber object returned by :ref:`fiber.create <fiber-create>` fields which describe the file's block size, creation time, size, and other attributes. file handle (later - fh) file name file-handle as returned by ``fio.open()``. fixarray(2), fixstr(1), "A", fixstr(1), "B" fixmap(2), key(1), fixstr(1), "A", key(2), fixstr(2), "B". flag setting function f()
  print("D")
end
jit.bc.dump(f) function hexdump(bytes)
    local result = ''
    for i = 1, #bytes do
        result = result .. string.format("%x", string.byte(bytes, i)) .. ' '
    end
    return result
end

msgpack = require('msgpack')
m1 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "seq"
                          }))
m2 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "map"
                          }))
print('array encoding: ', hexdump(m1))
print('map encoding: ', hexdump(m2)) host - a number, 0 (zero), meaning "all local interfaces"; host - a string containing "unix/"; host - a string representation of an IPv4 address or an IPv6 address; i, I id of the fiber. information iterator function json = require('json')
json.cfg{encode_invalid_numbers = true}
x = 0/0
y = 1/0
json.encode({1, x, y, 2}) l, L linked name. list of files whose names match the input string loaded_value lua_object mask bits. maximum number of bytes to read for example 50 means "stop after 50 bytes" maximum number of seconds to wait for example 50 means "stop after 50 seconds". md4 - md4 (with 128-bit binary strings using MD4) md5 - md5 (with 128-bit binary strings using MD5) mdc2 - mdc2 (using MDC2) message, a table containing "host", "family" and "port" fields. message_sender.host = '18.44.0.1'
message_sender.family = 'AF_INET'
message_sender.port = 43065 msgpack.NULL n name of existing file or directory, name of test name of the fiber. net_box.self:is_connected() net_box.self:ping() net_box.self:wait_connected() new active and timeout values. new channel. new group uid. new name. new permissions new socket if success. new user uid. nil num number number of a pre-defined error number of bytes to read number of context switches, backtrace, id, total memory, used memory, name for each fiber. number of seconds to sleep. number or number64 number, string numeric identifier of the fiber. ofb - Output Feedback offset within file where reading or writing begins one of ``'l'``, ``'b'``, ``'h'`` or ``'n'``. one or more strings to be concatenated. optional. see :ref:`above <csv-options>` original name. part of the message which will accompany the error path name path name of file. path of directory. path-name, which may contain wildcard characters. port - a number. port - a number. If a port number is 0 (zero), the socket will be bound to a random local port. port - a string containing a path to a unix socket. port number position to seek to positive integer as great as the maximum number of slots (spaces for ``get`` or ``put`` messages) that might be pending at any given time. previous mask bits. q, Q receiving result for ``sock:errno()``, result for ``sock:error()``. If there is no error, then ``sock:errno()`` will return 0 and ``sock:error()``. ripemd160 - rtype: table s, S same as nil scalar values to be formatted seconds or nanoseconds since epoch (1970-01-01 00:00:00), adjusted. seconds or nanoseconds since processor start. seconds or nanoseconds since the last time that the computer was booted. seconds or nanoseconds since thread start. see :ref:`above <csv-options>` sending separator for example '?' means "stop after a question mark" setup sha - sha (with 160-bit binary strings using SHA-0) sha1 - sha-1 (with 160-bit binary strings using SHA-1) sha224 - sha-224 (with 224-bit binary strings using SHA-2) sha256 - sha-256 (with 256-bit binary strings using SHA-2) sha384 - sha-384 (with 384-bit binary strings using SHA-2) sha512 - sha-512(with 512-bit binary strings using SHA-2). socket = require('socket')
sock = socket('AF_INET', 'SOCK_STREAM', 'tcp')
sock:sysconnect(0, 3301) socket('AF_INET', 'SOCK_STREAM', 'tcp') socket.SHUT_RD, socket.SHUT_WR, or socket.SHUT_RDWR. socket.tcp_server('localhost', 3302, function () end) state checking string string containing format specifiers string, table string, which is written to ``writable`` if specified suffix table table. first element = seconds of CPU time; second element = whatever the function returns. tap = require('tap')
taptest = tap.test('test-name') taptest tarantool> -- input in file.csv is:
tarantool> -- a,"b,c ",d
tarantool> -- a\\211\\128b
tarantool> fio = require('fio')
---
...
tarantool> f = fio.open('./file.csv', {'O_RDONLY'})
---
...
tarantool> csv.load(f, {chunk_size = 4096})
---
- - - a
    - 'b,c '
    - d
  - - a\\211\\128b
...
tarantool> f:close(nn)
---
- true
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.error()
---
- error: Arbitrary message
...
tarantool> box.error(box.error.FUNCTION_ACCESS_DENIED, 'A', 'B', 'C')
---
- error: A access denied for user 'B' to function 'C'
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.schema.space.create('#')
---
- error: Invalid identifier '#' (expected letters, digits or an underscore)
...
tarantool> box.error.last()
---
- line: 278
  code: 70
  type: ClientError
  message: Invalid identifier '#' (expected letters, digits or an underscore)
  file: /tmp/buildd/tarantool-1.7.0.252.g1654e31~precise/src/box/key_def.cc
...
tarantool> box.error.clear()
---
...
tarantool> box.error.last()
---
- null
... tarantool> console = require('console')
---
...
tarantool> console.connect('198.18.44.44:3301')
---
...
198.18.44.44:3301> -- prompt is telling us that server is remote tarantool> console = require('console')
---
...
tarantool> console.listen('unix/:/tmp/X.sock')
... main/103/console/unix/:/tmp/X I> started
---
- fd: 6
  name:
    host: unix/
    family: AF_UNIX
    type: SOCK_STREAM
    protocol: 0
    port: /tmp/X.sock
... tarantool> csv = require('csv')
---
...
tarantool> csv.dump({'a','b,c ','d'})
---
- 'a,"b,c ",d

'
... tarantool> csv = require('csv')
---
...
tarantool> csv.load('a,"b,c ",d')
---
- - - a
    - 'b,c '
    - d
... tarantool> csv.load('a,b;c,d', {delimiter = ';'})
---
- - - a,b
    - c,d
... tarantool> csv.load('a\\211\\128b')
---
- - - a\211\128b
... tarantool> csv_table = csv.load('a,b,c')
---
...
tarantool> csv.dump(csv_table)
---
- 'a,b,c

'
... tarantool> digest = require('digest')
---
...
tarantool> function password_insert()
         >   box.space.tester:insert{1234, digest.sha1('^S^e^c^ret Wordpass')}
         >   return 'OK'
         > end
---
...
tarantool> function password_check(password)
         >   local t = box.space.tester:select{12345}
         >   if digest.sha1(password) == t[2] then
         >     return 'Password is valid'
         >   else
         >     return 'Password is not valid'
         >   end
         > end
---
...
tarantool> password_insert()
---
- 'OK'
... tarantool> digest.guava(10863919174838991, 11)
---
- 8
... tarantool> dostring('abc')
---
error: '[string "abc"]:1: ''='' expected near ''<eof>'''
...
tarantool> dostring('return 1')
---
- 1
...
tarantool> dostring('return ...', 'hello', 'world')
---
- hello
- world
...
tarantool> dostring([[
         >   local f = function(key)
         >     local t = box.space.tester:select{key}
         >     if t ~= nil then
         >       return t[1]
         >     else
         >       return nil
         >     end
         >   end
         >   return f(...)]], 1)
---
- null
... tarantool> fh = fio.open('/home/username/tmp.txt', {'O_RDWR', 'O_APPEND'})
---
...
tarantool> fh -- display file handle returned by fio.open
---
- fh: 11
... tarantool> fh:close() -- where fh = file-handle
---
- true
... tarantool> fh:fsync()
---
- true
... tarantool> fh:pread(25, 25)
---
- |
  elete from t8//
  insert in
... tarantool> fh:seek(20, 'SEEK_SET')
---
- 20
... tarantool> fh:stat()
---
- inode: 729866
  rdev: 0
  size: 100
  atime: 140942855
  mode: 33261
  mtime: 1409430660
  nlink: 1
  uid: 1000
  blksize: 4096
  gid: 1000
  ctime: 1409430660
  dev: 2049
  blocks: 8
... tarantool> fh:truncate(0)
---
- true
... tarantool> fh:write('new data')
---
- true
... tarantool> fiber = require('fiber')
---
...
tarantool> function f () fiber.sleep(1000); end
---
...
tarantool> fiber_function = fiber:create(f)
---
- error: '[string "fiber_function = fiber:create(f)"]:1: fiber.create(function, ...):
    bad arguments'
...
tarantool> fiber_function = fiber.create(f)
---
...
tarantool> fiber_function.storage.str1 = 'string'
---
...
tarantool> fiber_function.storage['str1']
---
- string
...
tarantool> fiber_function:cancel()
---
...
tarantool> fiber_function.storage['str1']
---
- error: '[string "return fiber_function.storage[''str1'']"]:1: the fiber is dead'
... tarantool> fiber = require('fiber')
---
...
tarantool> function function_name()
         >   fiber.sleep(1000)
         > end
---
...
tarantool> fiber_object = fiber.create(function_name)
---
... tarantool> fiber = require('fiber')
tarantool> function function_x()
         >   gvar = 0
         >   while 0 == 0 do
         >     gvar = gvar + 1
         >     fiber.sleep(2)
         >   end
         > end
---
... tarantool> fiber.find(101)
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.info()
---
- 101:
    csw: 7
    backtrace: []
    fid: 101
    memory:
      total: 65776
      used: 0
    name: interactive
... tarantool> fiber.kill(fiber.id())
---
- error: fiber is cancelled
... tarantool> fiber.self()
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.self():cancel()
---
- error: fiber is cancelled
... tarantool> fiber.self():name('non-interactive')
---
... tarantool> fiber.self():name()
---
- interactive
... tarantool> fiber.self():status()
---
- running
... tarantool> fiber.sleep(1.5)
---
... tarantool> fiber.status()
---
- running
... tarantool> fiber.testcancel()
---
- error: fiber is cancelled
... tarantool> fiber.time(), fiber.time()
---
- 1448466279.2415
- 1448466279.2415
... tarantool> fiber.time(), fiber.time64()
---
- 1448466351.2708
- 1448466351270762
... tarantool> fiber.yield()
---
... tarantool> fiber_object = fiber.self()
---
...
tarantool> fiber_object:id()
---
- 101
... tarantool> fiber_of_x:cancel()
---
...
tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  dead . gvar= 421
---
... tarantool> fid = fiber_of_x:id()
---
... tarantool> fio.basename('/path/to/my.lua', '.lua')
---
- my
... tarantool> fio.chmod('/home/username/tmp.txt', tonumber('0755', 8))
---
- true
...
tarantool> fio.chown('/home/username/tmp.txt', 'username', 'username')
---
- true
... tarantool> fio.cwd()
---
- /home/username/tarantool_sandbox
... tarantool> fio.dirname('path/to/my.lua')
---
- 'path/to/'
... tarantool> fio.glob('/etc/x*')
---
- - /etc/xdg
  - /etc/xml
  - /etc/xul-ext
... tarantool> fio.link('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
...
tarantool> fio.unlink('/home/username/tmp.txt2')
---
- true
... tarantool> fio.lstat('/etc')
---
- inode: 1048577
  rdev: 0
  size: 12288
  atime: 1421340698
  mode: 16877
  mtime: 1424615337
  nlink: 160
  uid: 0
  blksize: 4096
  gid: 0
  ctime: 1424615337
  dev: 2049
  blocks: 24
... tarantool> fio.mkdir('/etc')
---
- false
... tarantool> fio.pathjoin('/etc', 'default', 'myfile')
---
- /etc/default/myfile
... tarantool> fio.rename('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
... tarantool> fio.sync()
---
- true
... tarantool> fio.tempdir()
---
- /tmp/lG31e7
... tarantool> fio.truncate('/home/username/tmp.txt', 99999)
---
- true
... tarantool> fio.umask(tonumber('755', 8))
---
- 493
... tarantool> fun = require('fun')
---
...
tarantool> for _k, a in fun.range(3) do
         >   print(a)
         > end
1
2
3
---
... tarantool> gvar = 0

tarantool> fiber_of_x = fiber.create(function_x)
---
... tarantool> json = require('json')
---
...
tarantool> json.decode('123')
---
- 123
...
tarantool> json.decode('[123, "hello"]')
---
- [123, 'hello']
...
tarantool> json.decode('{"hello": "world"}').hello
---
- world
... tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- '["A","B"]'
...
tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="map"}))
---
- '{"1":"A","2":"B"}'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- '[{"f2":"B","f1":"A"}]'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="seq"})})
---
- '[[]]'
... tarantool> json.encode({1, x, y, 2})
---
- '[1,nan,inf,2]
... tarantool> json=require('json')
---
...
tarantool> json.encode(123)
---
- '123'
...
tarantool> json.encode({123})
---
- '[123]'
...
tarantool> json.encode({123, 234, 345})
---
- '[123,234,345]'
...
tarantool> json.encode({abc = 234, cde = 345})
---
- '{"cde":345,"abc":234}'
...
tarantool> json.encode({hello = {'world'}})
---
- '{"hello":["world"]}'
... tarantool> load = function(readable, opts)
         >   opts = opts or {}
         >   local result = {}
         >   for i, tup in csv.iterate(readable, opts) do
         >     result[i] = tup
         >   end
         >   return result
         > end
---
...
tarantool> load('a,b,c')
---
- - - a
    - b
    - c
... tarantool> msgpack = require('msgpack')
---
...
tarantool> y = msgpack.encode({'a',1,'b',2})
---
...
tarantool> z = msgpack.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> box.space.tester:insert{20, msgpack.NULL, 20}
---
- [20, null, 20]
... tarantool> net_box = require('net.box')
---
...
tarantool> function example()
         >   local conn, wtuple
         >   if net_box.self:ping() then
         >     table.insert(ta, 'self:ping() succeeded')
         >     table.insert(ta, '  (no surprise -- self connection is pre-established)')
         >   end
         >   if box.cfg.listen == '3301' then
         >     table.insert(ta,'The local server listen address = 3301')
         >   else
         >     table.insert(ta, 'The local server listen address is not 3301')
         >     table.insert(ta, '(  (maybe box.cfg{...listen="3301"...} was not stated)')
         >     table.insert(ta, '(  (so connect will fail)')
         >   end
         >   conn = net_box.new('127.0.0.1:3301')
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester.')
         >   conn.space.tester:insert{800, 'data'}
         >   table.insert(ta, 'conn insert done on tester, index 0')
         >   table.insert(ta, '  primary key value = 800.')
         >   wtuple = conn.space.tester:select{800}
         >   table.insert(ta, 'conn select done on tester, index 0')
         >   table.insert(ta, '  number of fields = ' .. #wtuple)
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester')
         >   conn.space.tester:replace{800, 'New data', 'Extra data'}
         >   table.insert(ta, 'conn:replace done on tester')
         >   conn:timeout(0.5).space.tester:update({800}, {{'=', 2, 'Fld#1'}})
         >   table.insert(ta, 'conn update done on tester')
         >   conn:close()
         >   table.insert(ta, 'conn close done')
         > end
---
...
tarantool> ta = {}
---
...
tarantool> example()
---
...
tarantool> ta
---
- - self:ping() succeeded
  - '  (no surprise -- self connection is pre-established)'
  - The local server listen address = 3301
  - conn delete done on tester.
  - conn insert done on tester, index 0
  - '  primary key value = 800.'
  - conn select done on tester, index 0
  - '  number of fields = 1'
  - conn delete done on tester
  - conn:replace done on tester
  - conn update done on tester
  - conn close done
... tarantool> os.clock()
---
- 0.05
... tarantool> os.date("%A %B %d")
---
- Sunday April 24
... tarantool> os.execute('ls -l /usr')
total 200
drwxr-xr-x   2 root root 65536 Apr 22 15:49 bin
drwxr-xr-x  59 root root 20480 Apr 18 07:58 include
drwxr-xr-x 210 root root 65536 Apr 18 07:59 lib
drwxr-xr-x  12 root root  4096 Apr 22 15:49 local
drwxr-xr-x   2 root root 12288 Jan 31 09:50 sbin
---
... tarantool> os.exit()
user@user-shell:~/tarantool_sandbox$ tarantool> os.getenv('PATH')
---
- /usr/local/sbin:/usr/local/bin:/usr/sbin
... tarantool> os.remove('file')
---
- true
... tarantool> os.rename('local','foreign')
---
- null
- 'local: No such file or directory'
- 2
... tarantool> os.time()
---
- 1461516945
... tarantool> os.tmpname()
---
- /tmp/lua_7SW1m2
... tarantool> password_insert('Secret Password')
---
- 'Password is not valid'
... tarantool> pickle = require('pickle')
---
...
tarantool> box.space.tester:insert{0, 'hello world'}
---
- [0, 'hello world']
...
tarantool> box.space.tester:update({0}, {{'=', 2, 'bye world'}})
---
- [0, 'bye world']
...
tarantool> box.space.tester:update({0}, {
         >   {'=', 2, pickle.pack('iiA', 0, 3, 'hello')}
         > })
---
- [0, "\0\0\0\0\x03\0\0\0hello"]
...
tarantool> box.space.tester:update({0}, {{'=', 2, 4}})
---
- [0, 4]
...
tarantool> box.space.tester:update({0}, {{'+', 2, 4}})
---
- [0, 8]
...
tarantool> box.space.tester:update({0}, {{'^', 2, 4}})
---
- [0, 12]
... tarantool> pickle = require('pickle')
---
...
tarantool> tuple = box.space.tester:replace{0}
---
...
tarantool> string.len(tuple[1])
---
- 1
...
tarantool> pickle.unpack('b', tuple[1])
---
- 48
...
tarantool> pickle.unpack('bsi', pickle.pack('bsi', 255, 65535, 4294967295))
---
- 255
- 65535
- 4294967295
...
tarantool> pickle.unpack('ls', pickle.pack('ls', tonumber64('18446744073709551615'), 65535))
---
...
tarantool> num, num64, str = pickle.unpack('slA', pickle.pack('slA', 666,
         > tonumber64('666666666666666'), 'string'))
---
... tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  suspended . gvar= 399
---
... tarantool> socket = require('socket')
---
...
tarantool> sock = socket.tcp_connect('tarantool.org', 80)
---
...
tarantool> type(sock)
---
- table
...
tarantool> sock:error()
---
- null
...
tarantool> sock:send("HEAD / HTTP/1.0rnHost: tarantool.orgrnrn")
---
- true
...
tarantool> sock:read(17)
---
- "HTTP/1.1 200 OKrn"
...
tarantool> sock:close()
---
- true
... tarantool> socket = require('socket')
---
...
tarantool> sock_1 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_1:bind('127.0.0.1')
---
- true
...
tarantool> sock_2 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_2:sendto('127.0.0.1', sock_1:name().port,'X')
---
- true
...
tarantool> message = sock_1:recvfrom()
---
...
tarantool> message
---
- X
...
tarantool> sock_1:close()
---
- true
...
tarantool> sock_2:close()
---
- true
... tarantool> strict = require('strict')
---
...
tarantool> strict.on()
---
...
tarantool> a = b -- strict mode is on so this will cause an error
---
- error: ... variable ''b'' is not declared'
...
tarantool> strict.off()
---
...
tarantool> a = b -- strict mode is off so this will not cause an error
---
... tarantool> taptest:ok(true, 'x')
ok - x
---
- true
...
tarantool> tap = require('tap')
---
...
tarantool> taptest = tap.test('test-name')
TAP version 13
---
...
tarantool> taptest:ok(1 + 1 == 2, 'X')
ok - X
---
- true
... tarantool> taptest:skip('message')
ok - message # skip
---
- true
... tarantool> tarantool = require('tarantool')
---
...
tarantool> tarantool
---
- build:
    target: Linux-x86_64-RelWithDebInfo
    options: cmake . -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_BACKTRACE=ON
    mod_format: so
    flags: ' -fno-common -fno-omit-frame-pointer -fno-stack-protector -fexceptions
      -funwind-tables -fopenmp -msse2 -std=c11 -Wall -Wextra -Wno-sign-compare -Wno-strict-aliasing
      -fno-gnu89-inline'
    compiler: /usr/bin/x86_64-linux-gnu-gcc /usr/bin/x86_64-linux-gnu-g++
  uptime: 'function: 0x408668e0'
  version: 1.7.0-66-g9093daa
  pid: 'function: 0x40866900'
...
tarantool> tarantool.pid()
---
- 30155
...
tarantool> tarantool.uptime()
---
- 108.64641499519
... tarantool> type(123456789012345), type(tonumber64(123456789012345))
---
- number
- number
...
tarantool> i = tonumber64('1000000000')
---
...
tarantool> type(i), i / 2, i - 2, i * 2, i + 2, i % 2, i ^ 2
---
- number
- 500000000
- 999999998
- 2000000000
- 1000000002
- 0
- 1000000000000000000
... tarantool> uuid = require('uuid')
---
...
tarantool> uuid(), uuid.bin(), uuid.str()
---
- 16ffedc8-cbae-4f93-a05e-349f3ab70baa
- !!binary FvG+Vy1MfUC6kIyeM81DYw==
- 67c999d2-5dce-4e58-be16-ac1bcb93160f
...
tarantool> uu = uuid()
---
...
tarantool> #uui:bin(), #uu:str(), type(uu), uu:isnil()
---
- 16
- 36
- cdata
- false
... tarantool> yaml = require('yaml')
---
...
tarantool> y = yaml.encode({'a', 1, 'b', 2})
---
...
tarantool> z = yaml.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> if yaml.NULL == nil then print('hi') end
hi
---
... tarantool> yaml = require('yaml')
---
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="sequence"}))
---
- |
  ---
  - A
  - B
  ...
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- |
  ---
  ['A', 'B']
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- |
  ---
  - {'f2': 'B', 'f1': 'A'}
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="mapping"})})
---
- |
  ---
  - f2: B
    f1: A
  ...
... tcp_connect('127.0.0.1', 3301) teardown the :ref:`URI <index-uri>` of the target for the connection the URI of the local server the URI of the remote server the ``NO_SUCH_USER`` message is "``User '%s' is not found``" -- it includes one "``%s``" component which will be replaced with errtext. Thus a call to ``box.error(box.error.NO_SUCH_USER, 'joe')`` or ``box.error(45, 'joe')`` will result in an error with the accompanying message "``User 'joe' is not found``". the function to be associated with the fiber the id of the fiber to be cancelled. the message to be displayed. the new name of the fiber. the new position if success the number of bytes sent. the number of bytes that were decoded. the number of messages. the original contents formatted as a Lua table. the original contents formatted as a Lua table; the original value reformatted as a JSON string. the original value reformatted as a MsgPack string. the original value reformatted as a YAML string. the socket object value may change if sysconnect() succeeds. the specified fiber does not exist or cancel is not permitted. the status of ``fiber``. One of: “dead”, “suspended”, or “running”. the status of fiber. One of: “dead”, “suspended”, or “running”. the value placed on the channel by an earlier ``channel:put()``. true true for success, false for error. true if blocked users are waiting. Otherwise false. true if connected, false on failure. true if success, false if failure. true if success, false on failure. true if the socket is now readable, false if timeout expired; true if the socket is now writable, false if timeout expired; true if the specified channel is already closed. Otherwise false. true if the specified channel is empty true if the specified channel is full (has no room for a new message). true if the value is all zero, otherwise false. true on success, false on error true on success, false on error. For example, if sock is already closed, sock:close() returns false. true or false. true when connected, false on failure. use Digest::CRC;
$d = Digest::CRC->new(width => 32, poly => 0x1EDC6F41, init => 0xFFFFFFFF, refin => 1, refout => 1);
$d->add('string');
print $d->digest; userdata value to write what to execute. what will be passed to function whatever is returned by the Lua code chunk. whatever is specified in errcode-number. zero or more scalar values which will be appended to, or substitute for, items in the Lua chunk. {} 