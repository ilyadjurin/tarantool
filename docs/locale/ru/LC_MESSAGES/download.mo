��    D     <
              \  �  ]  �  �  �  i  �  �  �  u"  �  �%  �  �)  �  -  '   �0  )   �0  (   �0  �   1  D   �1  C   �1  @   2  <   W2  @   �2  <   �2  9   3  "   L3  2   o3     �3     �3     �3  <   �3  4   34      h4  B   �4  :   �4     5     5  6   75  6   n5     �5  ,   �5  &   �5  )   6  ,   66     c6  �   z6     7  &   7  �   A7  _   �7  &   Z8  p   �8  P   �8     C9  �   W9     �9  J   �9     I:  P   L:  @   �:  D   �:  :   #;  8   ^;  =   �;  i   �;  #   ?<  >   c<  �   �<  7   E=     }=     �=  	   �=  
   �=     �=     �=  \   �=  �   F>  H   ?  4   ^?     �?     �?  �   �?  9   X@     �@  s   �@  0   A  !   ?A  4   aA  1   �A     �A     �A     �A     B     B     &B     9B     LB     bB     wB     �B  0   �B  <   �B     C     C     C      C  '   :C     bC  X   fC  J   �C     
D     D  �   D     �D     �D  (   �D     E  &    E     GE  3   KE  =   E  )   �E  '   �E  0   F     @F  >   EF  (   �F  )   �F     �F     �F  D   �F  .   9G  2   hG     �G  *   �G  %   �G  
   H     H     H  e   !H     �H  )   �H     �H  -   �H  q   �H  �   `I  	   �I  
   	J     J  +   1J  /   ]J  m   �J  -   �J  3   )K  5   ]K  +   �K  1   �K  1   �K  9   #L  c   ]L  5   �L  5   �L  Y   -M  ,   �M  /   �M  1   �M  /   N  W   FN  3   �N  1   �N  -   O  -   2O  7   `O  1   �O  ,   �O     �O     �O      P    P     R    !R  �   2S  M   �S     T  4   T  ]   BT  F   �T  t   �T     \U  +   cU  ,   �U  &   �U     �U     �U  %   �U  G   V  1   [V     �V  �   �V  C   5W  :   yW  G   �W  8   �W     5X  (   :X     cX     iX  (   nX     �X     �X     �X  5   �X  a   Y  !   cY  /   �Y  $   �Y  	   �Y  
   �Y  V   �Y  �   FZ  `   �Z     ][  >   c[     �[  	   �[     �[      �[     �[     \     \     .\     J\     f\     y\     �\     �\     �\     �\     �\  �   �\  �   �]  9   +^  E   e^  
   �^  ;   �^     �^  N   _  �   ^_  �   �_  �   �`  �   5a     �a  C   �a  C   0b  r   tb  I   �b  �   1c     �c  �   Rd  �   �d  �   �e  t   "f     �f  ;   �f  =   �f  ;   &g  -   bg     �g     �g     �g  P   �g  /   h     Ih     dh     uh     �h  
   �h     �h     �h     �h  
   �h     �h  4   �h     !i     .i     =i     Pi  "   bi     �i     �i     �i  _  �i  a  ,l  a  �n  _  �p  [  Ps  _  �u  K  x  _  Xz  a  �|  a    _  |�  [  ܃  _  8�  K  ��  _  �  a  D�  a  ��  _  �  [  h�  _  Ė  K  $�  _  p�  a  Н  a  2�  _  ��  [  ��  _  P�  K  ��  p  ��  p  m�  p  ް  p  O�  p  ��  p  1�  p  ��  p  �     ��     ��  �  ��  �  \�  �  ��  �  h�  �  ��  �  t�  �  ��  �  ��  �  �  '   ��  )   ��  (   ��  �   �  D   ��  C   ��  @   �  <   V�  @   ��  <   ��  9   �  "   K�  2   n�     ��     ��     ��  <   ��  4   2�      g�  B   ��  :   ��     �     �  6   6�  6   m�     ��  ,   ��  &   ��  )   �  ,   5�     b�  �   y�     �  &   �  �   @�  _   ��  &   Y�  p   ��  P   ��     B�  �   V�     ��  J   ��     H�  P   K�  @   ��  D   ��  :   "�  8   ]�  =   ��  i   ��  #   >�  >   b�  �   ��  7   D�     |�     ��  	   ��  
   ��     ��     ��  \   ��  �   E�  H   �  4   ]�     ��     ��  �   ��  9   W�     ��  s   ��  0   �  !   >�  4   `�  1   ��     ��     ��     ��     �     �     %�     8�     K�     a�     v�     ��  0   ��  <   ��     �     �     �      �  '   9�     a�  X   e�  J   ��     	�     �  �   �     ��     ��  (   ��     �  &   �     F�  3   J�  =   ~�  )   ��  '   ��  0   �     ?�  >   D�  (   ��  )   ��     ��     ��  D   ��  .   8�  2   g�     ��  *   ��  %   ��  
   �     �     �  e    �     ��  )   ��     ��  -   ��  q   ��  �   _�  	   ��  
   �     �  +   0�  /   \�  m   ��  -   ��  3   (�  5   \�  +   ��  1   ��  1   ��  9   "�  c   \�  5   ��  5   ��  Y   ,�  ,   ��  /   ��  1   ��  /   �  W   E�  3   ��  1   ��  -   �  -   1�  7   _�  1   ��  ,   ��     ��     ��     ��    �     �     �  �   1  M   �       4    ]   A F   � t   �    [ +   b ,   � &   �    �    � %   � G    1   Z    � �   � C   4 :   x G   � 8   �    4 (   9    b    h (   m    �    �    � 5   � a     !   b /   � $   � 	   � 
   � V   � �   E `   �    \ >   b    � 	   �    �     �    �    	    	    -	    I	    e	    x	    �	    �	    �	    �	    �	 �   �	 �   �
 9   * E   d 
   � ;   �    � N    �   ] �   � �   � �   4    � C   � C   / r   s I   � �   0    � �   Q �   � �   � t   !    � ;   � =   � ;   % -   a    �    �    � P   � /       H    c    t    � 
   �    �    �    � 
   �    � 4   �         -    <    O "   a    �    �    � _  � a  + a  � _  � [  O  _  �" K  % _  W' a  �) a  , _  {. [  �0 _  73 K  �5 _  �7 a  C: a  �< _  ? [  gA _  �C K  #F _  oH a  �J a  1M _  �O [  �Q _  OT K  �V p  �X p  l[ p  �] p  N` p  �b p  0e p  �g p  j    �l    �l  # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_7' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_8' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_9' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_2_0' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_7' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_8' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_9' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_2_0' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool $ snap install tarantool --channel=beta $ snap install tarantool --channel=stable $ tarantoolctl rocks install module-name .Net connector for Tarantool 1.6+. Offers a full-featured support of Tarantool's binary protocol, async API and multiplexing. |star| .Net connector for Tarantool 1.6. Based on the Akka.Net I/O package. :currentversion:`1.9 (stable)`   :doc:`2.0 (alpha) <connectors_20>` :currentversion:`1.9 (stable)`  :doc:`2.0 (alpha) <download_20>` :currentversion:`1.9 (stable)` :doc:`2.0 (alpha) <rocks_20>` :doc:`1.9 (stable) <connectors>`   :currentversion:`2.0 (alpha)` :doc:`1.9 (stable) <download>` :currentversion:`2.0 (alpha)` :doc:`1.9 (stable) <rocks>` :currentversion:`2.0 (alpha)` :doc:`Amazon Linux <amazon-linux>` :doc:`Building from source <building-from-source>` :doc:`Debian <debian>` :doc:`Docker Hub <docker-hub>` :doc:`Downloads <download>` > :doc:`Downloads <download>` > :doc:`Connectors <connectors>` :doc:`Downloads <download>` > :doc:`Modules <rocks>` :doc:`Downloads <download_20>` > :doc:`Downloads <download_20>` > :doc:`Connectors <connectors_20>` :doc:`Downloads <download_20>` > :doc:`Modules <rocks_20>` :doc:`Fedora <fedora>` :doc:`FreeBSD <freebsd>` :doc:`Learn more → <os-installation/1.9/docker-hub>` :doc:`Learn more → <os-installation/2.0/docker-hub>` :doc:`OS X <os-x>` :doc:`RHEL/CentOS 6 and 7 <rhel-centos-6-7>` :doc:`Snappy Package <snappy-package>` :doc:`Tarantool 1.9 downloads <download>` :doc:`Tarantool 2.0 downloads <download_20>` :doc:`Ubuntu <ubuntu>` A plain msgpack serialization library. Use it if you wish to write your own connector or embed Tarantool protocol into your application. ASN1 BER format reader Also, look at the `Fresh Ports`_ page. Amazon Linux is based on RHEL 6 / CentOS 6. We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. An exhaustive list of all Tarantool modules, installable with ``luarocks`` or ``tarantoolctl``. Apache Avro schema tools for Tarantool Application-level library that provides sharding, re-sharding and client-side reliable replication for Tarantool Authorization module for Tarantool providing API for user registration and login Available versions: Built-in net.box module. Ships together with any Tarantool package. See more `here <https://tarantool.org/en/doc/1.7/reference/reference_lua/net_box.html>`_. |star| C C connector for Tarantool 1.6+. Supports sync and async I/O models. |star| C# Centralized system for collecting and manipulating metrics from multiple clients Connect from Tarantool to applications which speak MQTT protocol Connect from Tarantool to applications which speak the MQTT protocol Connect remotely to a Tarantool instance via an admin port Connect to a MySQL database from a Tarantool application Connect to a PostgreSQL database from a Tarantool application Connector for working with Tarantool 1.6 from nginx with an embedded Lua module or with OpenResty. |star| Convert data between character sets Copy and paste the script below to the *root* terminal prompt: Copy and paste the script below to the terminal prompt (if you want the version that comes with Ubuntu, start with the lines that follow the '# install' comment): Copy and paste the script below to the terminal prompt: Data formats / Serialization Database administration Databases Databases_ Date and time Development support Download a `Lua manifest file <https://github.com/tarantool/rocks/blob/gh-pages/manifest>`_. EV connector for Tarantool 1.6+. Asynchronous, fast, supports schemas (incl. fields) for on-the-fly tuple-to-hash and backward transformations, supports Types::Serializer for transparent conversion to JSON. Easy, terse, readable and fast check of the Lua functions argument types Efficiently store JSON documents in Tarantool spaces Enterprise downloads Erlang Erlang connector for Tarantool 1.6+. Supports pools of async connects (OTP supervisor based), automatic connection restore, transparent erlang map <-> Lua table. |star| Erlang connector for Tarantool 1.7+. Based on simplepool. Erlang_ Expiration daemon module to turn Tarantool into a persistent memcache replacement with your own expiration strategy Export Tarantool application metrics to Graphite Fast specialized XML trade parser Faster analogs to the standard 'os' functions in Lua Feature-rich command-line argument parser for Lua For Debian "jessie": For Debian "stretch": For Debian "wheezy": For Fedora 25: For Fedora 26: For RHEL/CentOS 6: For RHEL/CentOS 7: For Ubuntu "precise": For Ubuntu "trusty": For Ubuntu "wily": For Ubuntu "xenial": Full-featured geospatial extension for Tarantool Functional programming primitives that work well with LuaJIT Geo Geo_ Go Go connector for Tarantool 1.6+. Go connector for Tarantool 1.6+. |star| Go_ HTTP client with support for HTTPS and keepalive; uses routines in the 'libcurl' library Here is a bunch of free downloads and whatever you may need for Tarantool. I18n I18n_ If you’re looking for the latest version of a client driver, prefer rocks and gems to rpms and debs, or want to try out an alternative, choose a driver from a community-maintained list. JSON manipulation routines Java Java connector for Tarantool 1.6+ |star| Java_ Logical dump and restore for Tarantool Lua Lua code profiler based on Google Performance Tools Lua connector for Tarantool 1.6 on OpenResty nginx cosockets. Lua wrapper for the 'ccronexpr' C library Lua wrapper for the 'decNumber' library LuaJIT FFI bindings to ICU date and time library Lua_ Manipulation routines for CSV (Comma-Separated-Values) records Memcached protocol wrapper for Tarantool Memcached protocol wrapper for Tarantool. Miscellaneous Miscellaneous_ Module to connect remotely to a Tarantool instance via a binary port Module to handle errors produced by POSIX APIs Module to prohibit use of undeclared Lua variables MsgPack encoder/decoder Native Elixir connector for Tarantool 1.6. Net.box connection pool for Tarantool Networking Networking_ NginX NginX upstream module for Tarantool 1.6+. Features REST, JSON API, websockets, load balancing. |star| NginX_ Node connector for Tarantool 1.6+. |star| Node.js Non-blocking routines for socket input/output Note: initialization scripts, ``systemd`` units and ``tarantoolctl`` utility are not included in Snappy packages. Official Tarantool images for Docker come with batteries on board: modules, connectors and perks are pre-installed so that you can get up and running quickly. OpenResty OpenResty_ Operating systems/Interfaces Owner: `@Awety <https://github.com/Awety>`_ Owner: `@KlonD90 <https://github.com/KlonD90>`_ Owner: `@aensidhe <https://github.com/aensidhe>`_, `@roman-kozachenko <https://github.com/roman-kozachenko>`_ Owner: `@bigbes <https://github.com/bigbes>`_ Owner: `@brigadier <https://github.com/brigadier>`_ Owner: `@csteenberg <https://github.com/csteenberg>`_ Owner: `@dedok <https://github.com/dedok>`_ Owner: `@dgreenru <https://github.com/dgreenru>`_ Owner: `@donmikel <https://github.com/donmikel>`_ Owner: `@funny-falcon <https://github.com/funny-falcon>`_ Owner: `@funny-falcon <https://github.com/funny-falcon>`_, `@mialinx <https://github.com/mialinx>`_ Owner: `@hengestone <https://github.com/hengestone>`_ Owner: `@igorcoding <https://github.com/igorcoding>`_ Owner: `@igorcoding <https://github.com/igorcoding>`_, `@mons <https://github.com/mons>`_ Owner: `@mumez <https://github.com/mumez/>`_ Owner: `@perusio <https://github.com/perusio>`_ Owner: `@rtisisyk <https://github.com/rtisisyk>`_ Owner: `@rtsisyk <https://github.com/rtsisyk>`_ Owner: `@rybakit <https://github.com/rybakit>`_, `@nekufa <https://github.com/nekufa>`_ Owner: `@shveenkov <https://github.com/shveenkov>`_ Owner: `@spscream <https://github.com/spscream>`_ Owner: `@stofel <https://github.com/stofel>`_ Owner: `@thekvs <https://github.com/thekvs>`_ Owner: `@tonyfreeman <https://github.com/tonyfreeman>`_ Owner: `@viciious <https://github.com/viciious>`_ PECL PHP connector for Tarantool 1.6+ |star| PHP PHP_ Perl Perl client for Tarantool 1.6+. Fast, based on AnyEvent (async requests out of the box), provides automatic schema loading and on-fly reloading (which enables one to use spaces' and indexes' names in queries), supports all common tarantool statements to be requested natively (select / insert / delete / update / replace / upsert) or through lua function call. The connection is fully customizable (different timeouts can be set), fault-tolerant (reconnect on fails), and can be lazy initialized (to connect on first request). |star| Perl_ Pharo Smalltalk connector for Tarantool 1.6+. Includes object-oriented wrapper classes for easier use, automatic connection handling (pooling, reconnect). An additional module (`Tarantube <http://smalltalkhub.com/#!/~MasashiUmezawa/Tarantube>`_) provides queue interfaces. Please consult with the Tarantool documentation for :ref:`build-from-source <building_from_source>` instructions on your system. Port of the LPeg, Roberto Ierusalimschy's Parsing Expression Grammars library Power tools Prometheus library to collect metrics from Tarantool Pure Lua connector for Tarantool 1.7+. Works on nginx cosockets and plain Lua sockets. |star| Pure PHP connector for Tarantool 1.6+. Includes a client and a mapper. Pure Python connector for Tarantool 1.6+, also available from `pypi <http://pypi.python.org/pypi/tarantool>`_ |star| Python Python 3.4 asyncio driver for Tarantool 1.6 Python 3.5 asyncio driver for Tarantool 1.6+ Python Gevent driver for Tarantool 1.6 Python_ R R connector for Tarantool 1.6+ |star| Reader for Tarantool’s snapshot files and write-ahead-log (WAL) files Regular expression library binding (PCRE flavour) Routines for file input/output Routines to get time values derived from the Posix/C 'CLOCK_GETTIME' function or equivalent. Useful for accurate clock and benchmarking. Routines to work with "digest", a value returned by a hash function Routines to work with various cryptographic hash functions Routines to work with “digest”, a value returned by a hash function Routines to write messages to the built-in Tarantool log Ruby Ruby connector for Tarantool 1.6+ |star| Ruby_ Rust Rust connector for Tarantool 1.6+ |star| Rust_ SMTP client for Tarantool Security/Encryption Send messages from Tarantool to Mail.Ru Agent and ICQ Set of persistent in-memory queues to create task queues, add and take jobs, monitor failed tasks Sharding based on virtual buckets Simple tool to benchmark Tarantool internal API Simple watchdog module for Tarantool Smalltalk Smalltalk_ Smart algorithm to iterate over a space and make updates without freezing the database Snappy package manager is already pre-installed on Ubuntu Xenial and newer. For other distros, you may need to install ``snapd``. See http://snapcraft.io/ for detailed instructions. Snaps are universal Linux packages which can be installed across a range of Linux distributions. Swift Swift connector and stored procedures for Tarantool 1.7 |star| Swift_ Tarantool Tarantool - Amazon Linux Tarantool - Building from source Tarantool - Connectors Tarantool - Debian Tarantool - Docker Hub Tarantool - Downloads (1.9) Tarantool - Downloads (2.0) Tarantool - Fedora Tarantool - FreeBSD Tarantool - OS X Tarantool - RHEL/CentOS 6 and 7 Tarantool - Rocks Tarantool - Snappy package Tarantool - Ubuntu Tarantool 1.7 is unavailable as a snappy package. Please consider `Tarantool 1.9 <https://tarantool.io/en/download/os-installation/1.9/snappy-package.html>`_. Tarantool 1.7 is unavailable for Mac OS X. Please consider `Tarantool 1.9 <https://tarantool.io/en/download/os-installation/1.9/os-x.html>`_. Tarantool is available from the FreeBSD Ports collection. Tarantool is temporarily unavailable in the FreeBSD Ports collection. Tarantool_ Templates to create new Tarantool modules in Lua, C and C++ Terminal manipulation module The recommended connector for each language is marked with a star |starimage|. To get the latest source files for version 1.7, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 1.8, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 1.9, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 2.0, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To install a module, say: Tools to print call traces, insert watchpoints, inspect Lua objects Tools to write nice unit tests conforming to Test Anything Protocol Want your connector listed here? Please drop us a line at `support@tarantool.org <mailto:support@tarantool.org>`_. Want your module listed here? Please drop us a line at doc@tarantool.org. We maintain an always up-to-date Debian GNU/Linux package repository. At the moment, the repository contains builds for Debian "stretch", "jessie" and "wheezy". We maintain an always up-to-date Fedora package repository. At the moment, the repository contains builds for Fedora 25 and 26. We maintain an always up-to-date Ubuntu package repository. At the moment, the repository contains builds for Ubuntu "xenial", "wily", "trusty", "precise". We maintain an always up-to-date package repository for the derivatives of RHEL 6 and 7. You may need to enable the `EPEL`_ repository for some packages. We're building packages for a variety of operating systems. Choose your OS and follow the installation instructions. Hosting is powered by |packagecloud| With your browser, go to the `FreeBSD Ports`_ page. Enter the search term: `tarantool`. Choose the package you want. YAML encoder/decoder You can install Tarantool 1.8 (Beta) from a Snappy package: You can install Tarantool 1.9 (Stable) from a Snappy package: You can install Tarantool 2.0 (Beta) from a Snappy package: You can install Tarantool using ``homebrew``: ZooKeeper client for Tarantool `C <#c-language>`_ `C#`_ `Carrier-grade edition <https://tarantool.io/try-it>`_ for critical deployments. `Data formats / Serialization <#data-formats>`_ `Database administration`_ `Date and time`_ `Development support`_ `E <#erlang>`_ `G <#go>`_ `J <#java>`_ `L <#lua>`_ `N <#node-js>`_ `Node.js`_ `O <#openresty>`_ `Operating systems/Interfaces <#operating-systems>`_ `P <#perl>`_ `Power tools`_ `R <#r-language>`_ `S <#smalltalk>`_ `Security/Encryption <#security>`_ `T <#tarantool>`_ brew install tarantool brew install tarantool --HEAD curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_7/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "https://packagecloud.io/tarantool/1_7/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_8/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "https://packagecloud.io/tarantool/1_8/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_9/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "https://packagecloud.io/tarantool/1_9/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/2_0/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "https://packagecloud.io/tarantool/2_0/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_7'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_7'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_8'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_8'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_9'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_9'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_2_0'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_2_0'
sudo dnf -y install tarantool |point| |star| Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-24 15:47+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_7' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_8' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_9' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=EnterpriseLinux-6 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/el/6/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=EnterpriseLinux-6 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_2_0' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_7' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_8' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_9' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool # Clean up yum cache
yum clean all

# Enable EPEL repository
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
rm -f /etc/yum.repos.d/*tarantool*.repo
tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=EnterpriseLinux-7 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/el/7/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=EnterpriseLinux-7 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
yum makecache -y --disablerepo='*' --enablerepo='tarantool_2_0' --enablerepo='epel'

# Install Tarantool
yum -y install tarantool $ snap install tarantool --channel=beta $ snap install tarantool --channel=stable $ tarantoolctl rocks install module-name .Net connector for Tarantool 1.6+. Offers a full-featured support of Tarantool's binary protocol, async API and multiplexing. |star| .Net connector for Tarantool 1.6. Based on the Akka.Net I/O package. :currentversion:`1.9 (stable)`   :doc:`2.0 (alpha) <connectors_20>` :currentversion:`1.9 (stable)`  :doc:`2.0 (alpha) <download_20>` :currentversion:`1.9 (stable)` :doc:`2.0 (alpha) <rocks_20>` :doc:`1.9 (stable) <connectors>`   :currentversion:`2.0 (alpha)` :doc:`1.9 (stable) <download>` :currentversion:`2.0 (alpha)` :doc:`1.9 (stable) <rocks>` :currentversion:`2.0 (alpha)` :doc:`Amazon Linux <amazon-linux>` :doc:`Building from source <building-from-source>` :doc:`Debian <debian>` :doc:`Docker Hub <docker-hub>` :doc:`Downloads <download>` > :doc:`Downloads <download>` > :doc:`Connectors <connectors>` :doc:`Downloads <download>` > :doc:`Modules <rocks>` :doc:`Downloads <download_20>` > :doc:`Downloads <download_20>` > :doc:`Connectors <connectors_20>` :doc:`Downloads <download_20>` > :doc:`Modules <rocks_20>` :doc:`Fedora <fedora>` :doc:`FreeBSD <freebsd>` :doc:`Learn more → <os-installation/1.9/docker-hub>` :doc:`Learn more → <os-installation/2.0/docker-hub>` :doc:`OS X <os-x>` :doc:`RHEL/CentOS 6 and 7 <rhel-centos-6-7>` :doc:`Snappy Package <snappy-package>` :doc:`Tarantool 1.9 downloads <download>` :doc:`Tarantool 2.0 downloads <download_20>` :doc:`Ubuntu <ubuntu>` A plain msgpack serialization library. Use it if you wish to write your own connector or embed Tarantool protocol into your application. ASN1 BER format reader Also, look at the `Fresh Ports`_ page. Amazon Linux is based on RHEL 6 / CentOS 6. We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. An exhaustive list of all Tarantool modules, installable with ``luarocks`` or ``tarantoolctl``. Apache Avro schema tools for Tarantool Application-level library that provides sharding, re-sharding and client-side reliable replication for Tarantool Authorization module for Tarantool providing API for user registration and login Available versions: Built-in net.box module. Ships together with any Tarantool package. See more `here <https://tarantool.org/en/doc/1.7/reference/reference_lua/net_box.html>`_. |star| C C connector for Tarantool 1.6+. Supports sync and async I/O models. |star| C# Centralized system for collecting and manipulating metrics from multiple clients Connect from Tarantool to applications which speak MQTT protocol Connect from Tarantool to applications which speak the MQTT protocol Connect remotely to a Tarantool instance via an admin port Connect to a MySQL database from a Tarantool application Connect to a PostgreSQL database from a Tarantool application Connector for working with Tarantool 1.6 from nginx with an embedded Lua module or with OpenResty. |star| Convert data between character sets Copy and paste the script below to the *root* terminal prompt: Copy and paste the script below to the terminal prompt (if you want the version that comes with Ubuntu, start with the lines that follow the '# install' comment): Copy and paste the script below to the terminal prompt: Data formats / Serialization Database administration Databases Databases_ Date and time Development support Download a `Lua manifest file <https://github.com/tarantool/rocks/blob/gh-pages/manifest>`_. EV connector for Tarantool 1.6+. Asynchronous, fast, supports schemas (incl. fields) for on-the-fly tuple-to-hash and backward transformations, supports Types::Serializer for transparent conversion to JSON. Easy, terse, readable and fast check of the Lua functions argument types Efficiently store JSON documents in Tarantool spaces Enterprise downloads Erlang Erlang connector for Tarantool 1.6+. Supports pools of async connects (OTP supervisor based), automatic connection restore, transparent erlang map <-> Lua table. |star| Erlang connector for Tarantool 1.7+. Based on simplepool. Erlang_ Expiration daemon module to turn Tarantool into a persistent memcache replacement with your own expiration strategy Export Tarantool application metrics to Graphite Fast specialized XML trade parser Faster analogs to the standard 'os' functions in Lua Feature-rich command-line argument parser for Lua For Debian "jessie": For Debian "stretch": For Debian "wheezy": For Fedora 25: For Fedora 26: For RHEL/CentOS 6: For RHEL/CentOS 7: For Ubuntu "precise": For Ubuntu "trusty": For Ubuntu "wily": For Ubuntu "xenial": Full-featured geospatial extension for Tarantool Functional programming primitives that work well with LuaJIT Geo Geo_ Go Go connector for Tarantool 1.6+. Go connector for Tarantool 1.6+. |star| Go_ HTTP client with support for HTTPS and keepalive; uses routines in the 'libcurl' library Here is a bunch of free downloads and whatever you may need for Tarantool. I18n I18n_ If you’re looking for the latest version of a client driver, prefer rocks and gems to rpms and debs, or want to try out an alternative, choose a driver from a community-maintained list. JSON manipulation routines Java Java connector for Tarantool 1.6+ |star| Java_ Logical dump and restore for Tarantool Lua Lua code profiler based on Google Performance Tools Lua connector for Tarantool 1.6 on OpenResty nginx cosockets. Lua wrapper for the 'ccronexpr' C library Lua wrapper for the 'decNumber' library LuaJIT FFI bindings to ICU date and time library Lua_ Manipulation routines for CSV (Comma-Separated-Values) records Memcached protocol wrapper for Tarantool Memcached protocol wrapper for Tarantool. Miscellaneous Miscellaneous_ Module to connect remotely to a Tarantool instance via a binary port Module to handle errors produced by POSIX APIs Module to prohibit use of undeclared Lua variables MsgPack encoder/decoder Native Elixir connector for Tarantool 1.6. Net.box connection pool for Tarantool Networking Networking_ NginX NginX upstream module for Tarantool 1.6+. Features REST, JSON API, websockets, load balancing. |star| NginX_ Node connector for Tarantool 1.6+. |star| Node.js Non-blocking routines for socket input/output Note: initialization scripts, ``systemd`` units and ``tarantoolctl`` utility are not included in Snappy packages. Official Tarantool images for Docker come with batteries on board: modules, connectors and perks are pre-installed so that you can get up and running quickly. OpenResty OpenResty_ Operating systems/Interfaces Owner: `@Awety <https://github.com/Awety>`_ Owner: `@KlonD90 <https://github.com/KlonD90>`_ Owner: `@aensidhe <https://github.com/aensidhe>`_, `@roman-kozachenko <https://github.com/roman-kozachenko>`_ Owner: `@bigbes <https://github.com/bigbes>`_ Owner: `@brigadier <https://github.com/brigadier>`_ Owner: `@csteenberg <https://github.com/csteenberg>`_ Owner: `@dedok <https://github.com/dedok>`_ Owner: `@dgreenru <https://github.com/dgreenru>`_ Owner: `@donmikel <https://github.com/donmikel>`_ Owner: `@funny-falcon <https://github.com/funny-falcon>`_ Owner: `@funny-falcon <https://github.com/funny-falcon>`_, `@mialinx <https://github.com/mialinx>`_ Owner: `@hengestone <https://github.com/hengestone>`_ Owner: `@igorcoding <https://github.com/igorcoding>`_ Owner: `@igorcoding <https://github.com/igorcoding>`_, `@mons <https://github.com/mons>`_ Owner: `@mumez <https://github.com/mumez/>`_ Owner: `@perusio <https://github.com/perusio>`_ Owner: `@rtisisyk <https://github.com/rtisisyk>`_ Owner: `@rtsisyk <https://github.com/rtsisyk>`_ Owner: `@rybakit <https://github.com/rybakit>`_, `@nekufa <https://github.com/nekufa>`_ Owner: `@shveenkov <https://github.com/shveenkov>`_ Owner: `@spscream <https://github.com/spscream>`_ Owner: `@stofel <https://github.com/stofel>`_ Owner: `@thekvs <https://github.com/thekvs>`_ Owner: `@tonyfreeman <https://github.com/tonyfreeman>`_ Owner: `@viciious <https://github.com/viciious>`_ PECL PHP connector for Tarantool 1.6+ |star| PHP PHP_ Perl Perl client for Tarantool 1.6+. Fast, based on AnyEvent (async requests out of the box), provides automatic schema loading and on-fly reloading (which enables one to use spaces' and indexes' names in queries), supports all common tarantool statements to be requested natively (select / insert / delete / update / replace / upsert) or through lua function call. The connection is fully customizable (different timeouts can be set), fault-tolerant (reconnect on fails), and can be lazy initialized (to connect on first request). |star| Perl_ Pharo Smalltalk connector for Tarantool 1.6+. Includes object-oriented wrapper classes for easier use, automatic connection handling (pooling, reconnect). An additional module (`Tarantube <http://smalltalkhub.com/#!/~MasashiUmezawa/Tarantube>`_) provides queue interfaces. Please consult with the Tarantool documentation for :ref:`build-from-source <building_from_source>` instructions on your system. Port of the LPeg, Roberto Ierusalimschy's Parsing Expression Grammars library Power tools Prometheus library to collect metrics from Tarantool Pure Lua connector for Tarantool 1.7+. Works on nginx cosockets and plain Lua sockets. |star| Pure PHP connector for Tarantool 1.6+. Includes a client and a mapper. Pure Python connector for Tarantool 1.6+, also available from `pypi <http://pypi.python.org/pypi/tarantool>`_ |star| Python Python 3.4 asyncio driver for Tarantool 1.6 Python 3.5 asyncio driver for Tarantool 1.6+ Python Gevent driver for Tarantool 1.6 Python_ R R connector for Tarantool 1.6+ |star| Reader for Tarantool’s snapshot files and write-ahead-log (WAL) files Regular expression library binding (PCRE flavour) Routines for file input/output Routines to get time values derived from the Posix/C 'CLOCK_GETTIME' function or equivalent. Useful for accurate clock and benchmarking. Routines to work with "digest", a value returned by a hash function Routines to work with various cryptographic hash functions Routines to work with “digest”, a value returned by a hash function Routines to write messages to the built-in Tarantool log Ruby Ruby connector for Tarantool 1.6+ |star| Ruby_ Rust Rust connector for Tarantool 1.6+ |star| Rust_ SMTP client for Tarantool Security/Encryption Send messages from Tarantool to Mail.Ru Agent and ICQ Set of persistent in-memory queues to create task queues, add and take jobs, monitor failed tasks Sharding based on virtual buckets Simple tool to benchmark Tarantool internal API Simple watchdog module for Tarantool Smalltalk Smalltalk_ Smart algorithm to iterate over a space and make updates without freezing the database Snappy package manager is already pre-installed on Ubuntu Xenial and newer. For other distros, you may need to install ``snapd``. See http://snapcraft.io/ for detailed instructions. Snaps are universal Linux packages which can be installed across a range of Linux distributions. Swift Swift connector and stored procedures for Tarantool 1.7 |star| Swift_ Tarantool Tarantool - Amazon Linux Tarantool - Building from source Tarantool - Connectors Tarantool - Debian Tarantool - Docker Hub Tarantool - Downloads (1.9) Tarantool - Downloads (2.0) Tarantool - Fedora Tarantool - FreeBSD Tarantool - OS X Tarantool - RHEL/CentOS 6 and 7 Tarantool - Rocks Tarantool - Snappy package Tarantool - Ubuntu Tarantool 1.7 is unavailable as a snappy package. Please consider `Tarantool 1.9 <https://tarantool.io/en/download/os-installation/1.9/snappy-package.html>`_. Tarantool 1.7 is unavailable for Mac OS X. Please consider `Tarantool 1.9 <https://tarantool.io/en/download/os-installation/1.9/os-x.html>`_. Tarantool is available from the FreeBSD Ports collection. Tarantool is temporarily unavailable in the FreeBSD Ports collection. Tarantool_ Templates to create new Tarantool modules in Lua, C and C++ Terminal manipulation module The recommended connector for each language is marked with a star |starimage|. To get the latest source files for version 1.7, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 1.8, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 1.9, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To get the latest source files for version 2.0, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To install a module, say: Tools to print call traces, insert watchpoints, inspect Lua objects Tools to write nice unit tests conforming to Test Anything Protocol Want your connector listed here? Please drop us a line at `support@tarantool.org <mailto:support@tarantool.org>`_. Want your module listed here? Please drop us a line at doc@tarantool.org. We maintain an always up-to-date Debian GNU/Linux package repository. At the moment, the repository contains builds for Debian "stretch", "jessie" and "wheezy". We maintain an always up-to-date Fedora package repository. At the moment, the repository contains builds for Fedora 25 and 26. We maintain an always up-to-date Ubuntu package repository. At the moment, the repository contains builds for Ubuntu "xenial", "wily", "trusty", "precise". We maintain an always up-to-date package repository for the derivatives of RHEL 6 and 7. You may need to enable the `EPEL`_ repository for some packages. We're building packages for a variety of operating systems. Choose your OS and follow the installation instructions. Hosting is powered by |packagecloud| With your browser, go to the `FreeBSD Ports`_ page. Enter the search term: `tarantool`. Choose the package you want. YAML encoder/decoder You can install Tarantool 1.8 (Beta) from a Snappy package: You can install Tarantool 1.9 (Stable) from a Snappy package: You can install Tarantool 2.0 (Beta) from a Snappy package: You can install Tarantool using ``homebrew``: ZooKeeper client for Tarantool `C <#c-language>`_ `C#`_ `Carrier-grade edition <https://tarantool.io/try-it>`_ for critical deployments. `Data formats / Serialization <#data-formats>`_ `Database administration`_ `Date and time`_ `Development support`_ `E <#erlang>`_ `G <#go>`_ `J <#java>`_ `L <#lua>`_ `N <#node-js>`_ `Node.js`_ `O <#openresty>`_ `Operating systems/Interfaces <#operating-systems>`_ `P <#perl>`_ `Power tools`_ `R <#r-language>`_ `S <#smalltalk>`_ `Security/Encryption <#security>`_ `T <#tarantool>`_ brew install tarantool brew install tarantool --HEAD curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_7/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "https://packagecloud.io/tarantool/1_7/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.8/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.8/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_8/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_8.list
echo "https://packagecloud.io/tarantool/1_8/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_8.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.9/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "deb-src http://download.tarantool.org/tarantool/1.9/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.9/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/1_9/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_1_9.list
echo "https://packagecloud.io/tarantool/1_9/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_1_9.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/debian/ jessie main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/debian/ jessie main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/debian/ stretch main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/debian/ stretch main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ precise main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ precise main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ trusty main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ trusty main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ wily main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ wily main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/2.0/ubuntu/ xenial main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "deb-src http://download.tarantool.org/tarantool/2.0/ubuntu/ xenial main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/2.0/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb https://packagecloud.io/tarantool/2_0/debian/ wheezy main" > /etc/apt/sources.list.d/tarantool_2_0.list
echo "https://packagecloud.io/tarantool/2_0/debian/ wheezy main" >> /etc/apt/sources.list.d/tarantool_2_0.list

# install
sudo apt-get update
sudo apt-get -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_7'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_7.repo <<- EOF
[tarantool_1_7]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_7-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.7/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.7/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_7'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_8'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_8.repo <<- EOF
[tarantool_1_8]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_8-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.8/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.8/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_8'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_9'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_9.repo <<- EOF
[tarantool_1_9]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_9-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.9/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.9/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_9'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=Fedora-25 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/25/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=Fedora-25 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/25/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_2_0'
sudo dnf -y install tarantool sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_2_0.repo <<- EOF
[tarantool_2_0]
name=Fedora-26 - Tarantool
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/26/x86_64/
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_2_0-source]
name=Fedora-26 - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/2.0/fedora/26/SRPMS
gpgkey=http://download.tarantool.org/tarantool/2.0/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_2_0'
sudo dnf -y install tarantool |point| |star| 