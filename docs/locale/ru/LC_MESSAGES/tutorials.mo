��    �      4              L
  �  M
  �    �   �      �     �  �  �  �   �  3  [  S  �  �  �  �  �  q  b     �  
   �     �     �  �   
  �   �  F   �  h   �  I   0  �   z  #     �   ?  �    g  �     3!  
   G!  �   R!  m   �!  s   c"  v   �"  )   N#  �   x#  �   -$  �   �$  	   �%  9   �%  :   �%  ;   #&  A   _&  '   �&  	   �&  B  �&     (  G   (  �   g(  �   )  Y   �)  w   �)  d   m*  �   �*  #  b+    �,  M   �-  :   �-  G   .  �  Y.  V   �1  �   ?2  �  3  i   �4  :  5     O6  5   f6  8   �6    �6  -  �8  �   :  �   �:  �   Q;  L   <     T<  )   b<  A   �<  8   �<  <   =  <   D=  5  �=  [  �?  �  C    �D  �  �E  �  SG  �  J  �  L  5   �N  L   �N  �   6O  k  �O  �   gQ    6R  �   8S  Y   �S  w   ;T    �T  �   �U  �   NV  �   �V  �   uW  �   `X  g   OY  q   �Y     )Z  E  IZ    �[  �   �\  )  r]  :   �_  �  �_  �  �c  !   Mf     of     �f  �  �f  �  Sh  �   Pj  E  �j  e  7l  �   �m  i  Zn  �   �o  �   �p  	   \q  �   fq  t  fr  R   �s    .t    6u  �   Pv  �   �v    �w  �   �x  �  qy    G{  <   `|  �  �|  j   &�  7  ��  �   Ɋ  �   S�  �   �  �   ��  1  ��  5   Ϗ  �  �  *   ��  �  ��     G�  V   Y�  G   ��  @   ��  y   9�  �   ��  W  ��    �  �   ��  ;  ��  |  ؙ  �  U�  2   �  X   4�  �  ��  �  /�  �  �  �   ݤ      f�     ��  �  ��  �   |�  3  =�  S  q�  �  Ū  �  u�  q  D�     ��  
   ư     Ѱ     ް  �   �  �   ��  F   b�  h   ��  I   �  �   \�  #   ��  �   !�  �  ��  g  ��  5   �  .   K�  �   z�  m   �  s   ��  v   ��  )   v�  �   ��  �   U�  �   �  	   ̼  9   ּ  :   �  ;   K�  A   ��  '   ɽ  	   �  B  ��     >�  G   G�  �   ��  �   <�  Y   ��  w   �  d   ��  �   ��  #  ��    ��  M   ��  :   ��  G   9�  �  ��  V   �  �   g�  �  =�  i   ��  :  <�  ?   w�  r   ��  8   *�    c�  -  e�  �   ��  �   ,�  �   ��  L   ��  0   ��  )   �  A   =�  8   �  <   ��  <   ��  5  2�  [  h�  �  ��    I�  �  e�  �  �  �  ��  �  ��  5   d�  L   ��  �   ��  k  ��  �   �    ��  �   ��  Y   ��  w   ��    d�  �   q�  �   ��  �   ��  �   &�  �   �  g    �  q   h�  M   ��  E  (�    n�  �   ��  )  Q�  :   {�  �  ��  �  d�  !   ,�     N�     b�  �  ��  �  2  �   / E  � e   �   | i  9 �   � �   �    ;	 �   N	 t  N
 R   �       �   8 �   �   k �   � �  Y   / <   H �  � j   ! 7  y! �   �" �   ;# �   �# �   �$ 1  �% 5   �' �  �' *   x) �  �)    /+ V   A+ G   �+ @   �+ y   !, �   �, W  �-   �. �   �/ ;  �0 |  �1 �  =5 2   �6 X   7  #include "module.h"
#include "msgpuck.h"
int harder(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  uint32_t arg_count = mp_decode_array(&args);
  printf("arg_count = %d\n", arg_count);
  uint32_t field_count = mp_decode_array(&args);
  printf("field_count = %d\n", field_count);
  uint32_t val;
  int i;
  for (i = 0; i < field_count; ++i)
  {
    val = mp_decode_uint(&args);
    printf("val=%d.\n", val);
  }
  return 0;
} #include "module.h"
#include "msgpuck.h"
int hardest(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  uint32_t space_id = box_space_id_by_name("capi_test", strlen("capi_test"));
  char tuple[1024];
  char *tuple_pointer = tuple;
  tuple_pointer = mp_encode_array(tuple_pointer, 2);
  tuple_pointer = mp_encode_uint(tuple_pointer, 10000);
  tuple_pointer = mp_encode_str(tuple_pointer, "String 2", 8);
  int n = box_insert(space_id, tuple, tuple_pointer, NULL);
  return n;
} #include "module.h"
int easy(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  printf("hello world\n");
  return 0;
} **An example in the test suite** **Cleaning up** **LINE 3: WHY "LOCAL".** This line declares all the variables that will be used in the function. Actually it's not necessary to declare all variables at the start, and in a long function it would be better to declare variables just before using them. In fact it's not even necessary to declare variables at all, but an undeclared variable is "global". That's not desirable for any of the variables that are declared in line 1, because all of them are for use only within the function. **LINE 5: START THE MAIN LOOP.** Everything inside this "``for``" loop will be repeated as long as there is another index key. A tuple is fetched and can be referenced with variable :code:`t`. **LINE 5: WHY "PAIRS()".** Our job is to go through all the rows and there are two ways to do it: with :ref:`box.space.space_object:pairs() <box_space-pairs>` or with ``variable = select(...)`` followed by :samp:`for i, {n}, 1 do {some-function}(variable[i]) end`. We preferred ``pairs()`` for this example. **LINE 6: MEANING.** The function is :ref:`json.decode <json-decode>` which means decode a JSON string, and the parameter is t[2] which is a reference to a JSON string. There's a bit of hard coding here, we're assuming that the second field in the tuple is where the JSON string was inserted. For example, we're assuming a tuple looks like **LINE 6: WHY "PCALL".** If we simply said ``lua_table = json.decode(t[2]))``, then the function would abort with an error if it encountered something wrong with the JSON string - a missing colon, for example. By putting the function inside "``pcall``" (`protected call`_), we're saying: we want to intercept that sort of error, so if there's a problem just set ``is_valid_json = false`` and we will know what to do about it later. **LINE 8.** At last we are ready to get the JSON field value from the Lua table that came from the JSON string. The value in field_name, which is the parameter for the whole function, must be a name of a JSON field. For example, inside the JSON string ``'{"Hello": "world", "Quantity": 15}'``, there are two JSON fields: "Hello" and "Quantity". If the whole function is invoked with ``sum_json_field("Quantity")``, then ``field_value = lua_table[field_name]`` is effectively the same as ``field_value = lua_table["Quantity"]`` or even ``field_value = lua_table.Quantity``. Those are just three different ways of saying: for the Quantity field in the Lua table, get the value and put it in variable :code:`field_value`. **LINE 9: WHY "IF".** Suppose that the JSON string is well formed but the JSON field is not a number, or is missing. In that case, the function would be aborted when there was an attempt to add it to the sum. By first checking ``type(field_value) == "number"``, we avoid that abortion. Anyone who knows that the database is in perfect shape can skip this kind of thing. **Preparation** **easy.c** **harder.c** **hardest.c** -- if tester is left over from some previous test, destroy it
box.space.tester:drop()
box.schema.space.create('tester')
box.space.tester:create_index('primary', {parts = {1, 'unsigned'}}) ... Well, actually it won't always look like this because ``math.random()`` produces random numbers. But for the illustration purposes it won't matter what the random string values are. :ref:`Indexed pattern search <c_lua_tutorial-indexed_pattern_search>`. :ref:`Insert one million tuples with a Lua stored procedure <c_lua_tutorial-insert_one_million_tuples>`, :ref:`Sum a JSON field for all tuples <c_lua_tutorial-sum_a_json_field>`, After following the instructions, and seeing that the results are what is described here, users should feel confident about writing their own stored procedures. And now the screen looks like this: And the function is complete. Time to test it. Starting with an empty database, defined the same way as the sandbox database that was introduced in :ref:`first database <user_guide_getting_started-first_database>`, At this point the harder() function will start using functions defined in msgpuck.h, which are documented in `http://rtsisyk.github.io/msgpuck <http://rtsisyk.github.io/msgpuck>`_. The routines that begin with "mp" are msgpuck functions that handle data formatted according to the MsgPack_ specification. Passes and returns are always done with this format so one must become acquainted with msgpuck to become proficient with the C API. Both module.h and msgpuck.h must be on the include path for the C compiler to see them. For example, if module.h address is /usr/local/include/tarantool/module.h, and msgpuck.h address is /usr/local/include/msgpuck/msgpuck.h, and they are not currently on the include path, say |br| :code:`export CPATH=/usr/local/include/tarantool:/usr/local/include/msgpuck` C stored procedures C tutorial Check that these items exist on the computer: |br| * Tarantool 1.7 |br| * A gcc compiler, any modern version should work |br| * "module.h" |br| * "msgpuck.h" |br| Compile the program, producing a library file named easy.so: |br| :code:`gcc -shared -o easy.so -fPIC easy.c` Compile the program, producing a library file named harder.so: |br| :code:`gcc -shared -o harder.so -fPIC harder.c` Compile the program, producing a library file named hardest.so: |br| :code:`gcc -shared -o hardest.so -fPIC hardest.c` Conclusion: calling a C function is easy. Conclusion: decoding parameter values passed to a C function is not easy at first, but there are routines to do the job, and they're documented, and there aren't very many of them. Conclusion: parts of the standard test suite use C stored procedures, and they must work, because releases don't happen if Tarantool doesn't pass the tests. Conclusion: the long description of the C API is there for a good reason. All of the functions in it can be called from C functions which are called from Lua. So C "stored procedures" have full access to the database. Configure Create a file. Name it easy.c. Put these six lines in it. Create a file. Name it harder.c. Put these 17 lines in it: Create a file. Name it hardest.c. Put these 13 lines in it: Create a function that calls another function and sets a variable Create a function that returns a string Delimiter Download the source code of Tarantool. Look in a subdirectory :code:`test/box`. Notice that there is a file named :code:`tuple_bench.test.lua` and another file named :code:`tuple_bench.c`. Examine the Lua file and observe that it is calling a function in the C file, using the same techniques that this tutorial has shown. EXAMPLE: For more about Lua loops see Lua manual `chapter 4.3.4 "Numeric for"`_. For more about Lua math-library functions see Lua users "`Math Library Tutorial`_". For more about Lua string-library functions see Lua users "`String Library Tutorial`_" . For more about Lua strings see Lua manual `chapter 2.4 "Strings"`_ . For more about functions see Lua manual `chapter 5 "Functions"`_. For more about Lua variables see Lua manual `chapter 4.2 "Local Variables and Blocks"`_ . For more about Tarantool insert and replace calls, see Tarantool manual section :ref:`Submodule box.space <box_space>`. For more about Tarantool tuples see Tarantool manual section :ref:`Submodule box.tuple <box_tuple>`. For more on Lua ``os.clock()`` see Lua manual `chapter 22.1 "Date and Time"`_. For more on Lua print() see Lua manual `chapter 5 "Functions"`_. For now, though, it's enough to know that mp_decode_array() returns the number of elements in an array, and mp_decode_uint returns an unsigned integer, from :code:`args`. And there's a side effect: when the decoding finishes, :code:`args` has changed and is now pointing to the next element. Get rid of each of the function tuples with :ref:`box.schema.func.drop <box_schema-func_drop>`, and get rid of the capi_test space with :ref:`box.schema.capi_test:drop() <box_space-drop>`, and remove the .c and .so files that were created for this tutorial. Go back to the shell where the easy.c and the harder.c programs were created. Go back to the shell where the easy.c program was created. Here are three tutorials on using Lua stored procedures with Tarantool: Here is a generic function which takes a field identifier and a search pattern, and returns all tuples that match. |br| * The field must be the first field of a TREE index. |br| * The function will use `Lua pattern matching <http://www.lua.org/manual/5.2/manual.html#6.4.1>`_, which allows "magic characters" in regular expressions. |br| * The initial characters in the pattern, as far as the first magic character, will be used as an index search key. For each tuple that is found via the index, there will be a match of the whole pattern. |br| * To be :ref:`cooperative <atomic-cooperative_multitasking>`, the function should yield after every 10 tuples, unless there is a reason to delay yielding. |br| With this function, we can take advantage of Tarantool's indexes for speed, and take advantage of Lua's pattern matching for flexibility. It does everything that an SQL "LIKE" search can do, and far more. Here is one C tutorial: :ref:`C stored procedures <f_c_tutorial-c_stored_procedures>`. If these requests appear unfamiliar, re-read the descriptions of :ref:`box.schema.func.create <box_schema-func_create>` and :ref:`box.schema.user.grant <box_schema-user_grant>` and :ref:`conn:call <net_box-call>`. In earlier versions of Tarantool, multi-line functions had to be enclosed within "delimiters". They are no longer necessary, and so they will not be used in this tutorial. However, they are still supported. Users who wish to use delimiters, or users of older versions of Tarantool, should check the syntax description for :ref:`declaring a delimiter <administration-setting_delimiter>` before proceeding. In plainer language: create a space named capi_test, and make a connection to self named capi_connection. In this tutorial, which can be followed by anyone with a Tarantool development package and a C compiler, there are three tasks. The first -- :code:`easy.c` -- prints "hello world". The second -- :code:`harder.c` -- decodes a passed parameter value. The third -- :code:`hardest.c` -- uses the C API to do DBMS work. Indexed pattern search Insert one million tuples with a Lua stored procedure Invoke the function with ``sum_json_field("Quantity")``. It is not necessary to destroy the old ``string_function()`` contents, they're simply overwritten. The first assignment invokes a random-number function in Lua's math library; the parameters mean “the number must be an integer between 65 and 90.” The second assignment invokes an integer-to-character function in Lua's string library; the parameter is the code point of the character. Luckily the ASCII value of 'A' is 65 and the ASCII value of 'Z' is 90 so the result will always be a letter between A and Z. It works. We'll just leave, as exercises for future improvement, the possibility that the "hard coding" assumptions could be removed, that there might have to be an overflow check if some field values are huge, and that the function should contain a "yield" instruction if the count of tuples is huge. Its first job is to find the 'easy' function, which should be easy because by default Tarantool looks on the current directory for a file named easy.so. Its second job is to call the 'easy' function. Since the easy() function in easy.c begins with :code:`printf("hello world\n")`, the words "hello world" will appear on the screen. Its third job is to check that the call was successful. Since the easy() function in easy.c ends with :code:`return 0`, there is no error message to display and the request is over. Leave the client running. It will be necessary to enter more requests later. Lua tutorials Make a tuple out of a number and a string Modify main_function to insert a million tuples into the database Modify main_function to insert a tuple into the database Modify the function so it returns a one-letter random string Modify the function so it returns a ten-letter random string NOTE #1 "FIND AN APPROPRIATE INDEX" |br| The caller has passed space_name (a string) and field_no (a number). The requirements are: |br| (a) index type must be "TREE" because for other index types (HASH, BITSET, RTREE) a search with iterator=GE will not return strings in order by string value; |br| (b) field_no must be the first index part; |br| (c) the field must contain strings, because for other data types (such as "unsigned") pattern searches are not possible; |br| If these requirements are not met by any index, then print an error message and return nil. NOTE #2 "DERIVE INDEX SEARCH KEY FROM PATTERN" |br| The caller has passed pattern (a string). The index search key will be the characters in the pattern as far as the first magic character. Lua's magic characters are % ^ $ ( ) . [ ] * + - ?. For example, if the pattern is "ABC.E", the period is a magic character and therefore the index search key will be "ABC". But there is a complication ... If we see "%" followed by a punctuation character, that punctuation character is "escaped" so remove the "%" when making the index search key. For example, if the pattern is "AB%$E", the dollar sign is escaped and therefore the index search key will be "AB$E". Finally there is a check that the index search key length must be at least three -- this is an arbitrary number, and in fact zero would be okay, but short index search keys will cause long search times. NOTE #3 -- "OUTER LOOP: INITIATE" |br| The function's job is to return a result set, just as box.space.select would. We will fill it within an outer loop that contains an inner loop. The outer loop's job is to execute the inner loop, and possibly yield, until the search ends. The inner loop's job is to find tuples via the index, and put them in the result set if they match the pattern. NOTE #4 "INNER LOOP: ITERATOR" |br| The for loop here is using pairs(), see the :ref:`explanation of what index iterators are <box_index-index_pairs>`. Within the inner loop, there will be a local variable named "tuple" which contains the latest tuple found via the index search key. NOTE #5 "INNER LOOP: BREAK IF INDEX KEY IS TOO GREAT" |br| The iterator is GE (Greater or Equal), and we must be more specific: if the search index key has N characters, then the leftmost N characters of the result's index field must not be greater than the search index key. For example, if the search index key is 'ABC', then 'ABCDE' is a potential match, but 'ABD' is a signal that no more matches are possible. NOTE #6 "INNER LOOP: BREAK AFTER EVERY 10 TUPLES -- MAYBE" |br| This chunk of code is for cooperative multitasking. The number 10 is arbitrary, and usually a larger number would be okay. The simple rule would be "after checking 10 tuples, yield, and then resume the search (that is, do the inner loop again) starting after the last value that was found". However, if the index is non-unique or if there is more than one field in the index, then we might have duplicates -- for example {"ABC",1}, {"ABC", 2}, {"ABC", 3}" -- and it would be difficult to decide which "ABC" tuple to resume with. Therefore, if the result's index field is the same as the previous result's index field, there is no break. NOTE #7 "INNER LOOP: ADD TO RESULT SET IF PATTERN MATCHES" |br| Compare the result's index field to the entire pattern. For example, suppose that the caller passed pattern "ABC.E" and there is an indexed field containing "ABCDE". Therefore the initial index search key is "ABC". Therefore a tuple containing an indexed field with "ABCDE" will be found by the iterator, because "ABCDE" > "ABC". In that case string.match will return a value which is not nil. Therefore this tuple can be added to the result set. NOTE #8 "OUTER LOOP: BREAK, OR YIELD AND CONTINUE" |br| There are three conditions which will cause a break from the inner loop: (1) the for loop ends naturally because there are no more index keys which are greater than or equal to the index search key, (2) the index key is too great as described in NOTE #5, (3) it is time for a yield as described in NOTE #6. If condition (1) or condition (2) is true, then there is nothing more to do, the outer loop ends too. If and only if condition (3) is true, the outer loop must yield and then continue. If it does continue, then the inner loop -- the iterator search -- will happen again with a new value for the index search key. Now go back to the client and execute these requests: Now that ``string_function`` exists, we can invoke it from another function. Now that it's a bit clearer how to make a variable, we can change ``string_function()`` so that, instead of returning a fixed literal 'Hello world", it returns a random letter between 'A' and 'Z'. Now that it's clear how to insert one tuple into the database, it's no big deal to figure out how to scale up: instead of inserting with a literal value = 1 for the primary key, insert with a variable value = between 1 and 1 million, in a loop. Since we already saw how to loop, that's a simple thing. The only extra wrinkle that we add here is a timing function. Now that it's clear how to make a 10-letter random string, it's possible to make a tuple that contains a number and a 10-letter random string, by invoking a function in Tarantool's library of Lua functions. Now that it's clear how to make a tuple that contains a number and a 10-letter random string, the only trick remaining is putting that tuple into tester. Remember that tester is the first space that was defined in the sandbox, so it's like a database table. Now that it's clear how to produce one-letter random strings, we can reach our goal of producing a ten-letter string by concatenating ten one-letter strings, in a loop. Now, still on the client, execute this request: |br| :code:`box.space.capi_test:select()` Once again the ``string_function()`` can be invoked from main_function() which can be invoked with ``main_function()``. Once this is done, t will be the value of a new tuple which has two fields. The first field is numeric: 1. The second field is a random string. Once again the ``string_function()`` can be invoked from ``main_function()`` which can be invoked with  ``main_function()``. Read the following Lua code to see how it works. The comments that begin with "SEE NOTE ..." refer to long explanations that follow the code. Requests will be done using tarantool as a :ref:`client <administration-using_tarantool_as_a_client>`. Start tarantool, and enter these requests. Sending ``function-name()`` means “invoke the Lua function.” The effect is that the string which the function returns will end up on the screen. Since this is a test, there are deliberate errors. The "golf club" and the "waffle iron" do not have numeric Quantity fields, so must be ignored. Therefore the real sum of the Quantity field in the JSON strings should be: 15 + 7 = 22. Since this is the grand finale, we will redo the final versions of all the necessary requests: the request that created ``string_function()``, the request that created ``main_function()``, and the request that invokes ``main_function()``. Start Tarantool, cut and paste the code for function ``indexed_pattern_search``, and try the following: Start another shell. Change directory (cd) so that it is the same as the directory that the client is running on. Sum a JSON field for all tuples Tarantool can call C code with :ref:`modules <modules-example_c>`, or with :ref:`ffi <cookbook-ffi_printf>`, or with C stored procedures. This tutorial only is about the third option, C stored procedures. In fact the routines are always "C functions" but the phrase "stored procedure" is commonly used for historical reasons. The "module.h" file will exist if Tarantool 1.7 was installed from source. Otherwise Tarantool's "developer" package must be installed. For example on Ubuntu say |br| :code:`sudo apt-get install tarantool-dev` |br| or on Fedora say |br| :code:`dnf -y install tarantool-devel` The "msgpuck.h" file will exist if Tarantool 1.7 was installed from source. Otherwise the "msgpuck" package must be installed from `https://github.com/rtsisyk/msgpuck <https://github.com/rtsisyk/msgpuck>`_. The :ref:`os.clock() <os-clock>` function will return the number of CPU seconds since the start. Therefore, by getting start_time = number of seconds just before the inserting, and then getting end_time = number of seconds just after the inserting, we can calculate (end_time - start_time) = elapsed time in seconds. We will display that value by putting it in a request without any assignments, which causes Tarantool to send the value to the client, which prints it. (Lua's answer to the C ``printf()`` function, which is ``print()``, will also work.) The function that matters is capi_connection:call('easy'). The new line here is ``box.space.tester:replace(t)``. The name contains 'tester' because the insertion is going to be to tester. The second parameter is the tuple value. To be perfectly correct we could have said ``box.space.tester:insert(t)`` here, rather than ``box.space.tester:replace(t)``, but "replace" means “insert even if there is already a tuple whose primary-key value is a duplicate”, and that makes it easier to re-run the exercise even if the sandbox database isn't empty. Once this is done, tester will contain a tuple with two fields. The first field will be 1. The second field will be a random 10-letter string. Once again the ``string_function(``) can be invoked from ``main_function()`` which can be invoked with ``main_function()``. But ``main_function()`` won't tell the whole story, because it does not return t, it only puts t into the database. To confirm that something got inserted, we'll use a SELECT request. The purpose of the exercise is to show what Lua functions look like inside Tarantool. It will be necessary to employ the Lua math library, the Lua string library, the Tarantool box library, the Tarantool box.tuple library, loops, and concatenations. It should be easy to follow even for a person who has not used either Lua or Tarantool before. The only requirement is a knowledge of how other programming languages work and a memory of the first two chapters of this manual. But for better understanding, follow the comments and the links, which point to the Lua manual or to elsewhere in this Tarantool manual. To further enhance learning, type the statements in with the tarantool client while reading along. The result should look like this: The result will be: The screen now looks like this: The word "``function``" is a Lua keyword -- we're about to go into Lua. The function name is string_function. The function has one executable statement, ``return "hello world"``. The string "hello world" is enclosed in double quotes here, although Lua doesn't care -- one could use single quotes instead. The word "``end``" means “this is the end of the Lua function declaration.” To confirm that the function works, we can say The words "for x = 1,10,1" mean “start with x equals 1, loop until x equals 10, increment x by 1 for each iteration.” The symbol ".." means "concatenate", that is, add the string on the right of the ".." sign to the string on the left of the ".." sign. Since we start by saying that random_string is "" (a blank string), the end result is that random_string has 10 random letters. Once again the ``string_function()`` can be invoked from ``main_function()`` which can be invoked with ``main_function()``. Then we assign a value to ``string_value``, namely, the result of ``string_function()``. Soon we will invoke ``main_function()`` to check that it got the value. Therefore the first displayed line will be "arg_count = 1" because there was only one item passed: passable_table. |br| The second displayed line will be "field_count = 3" because there are three items in the table. |br| The next three lines will be "1" and "2" and "3" because those are the values in the items in the table. This is an exercise assignment: “Assume that inside every tuple there is a string formatted as JSON. Inside that string there is a JSON numeric field. For each tuple, find the numeric field's value and add it to a 'sum' variable. At end, return the 'sum' variable.” The purpose of the exercise is to get experience in one way to read and process tuples. This is an exercise assignment: “Insert one million tuples. Each tuple should have a constantly-increasing numeric primary-key field and a random alphabetic 10-character string field.” This proves that the hardest() function succeeded, but where did box_space_id_by_name() and box_insert() come from? Answer: the C API. The whole C API is documented :ref:`here <index-c_api_reference>`. The function box_space_id_by_name() is documented :ref:`here <box-box_space_id_by_name>`. The function box_insert() is documented :ref:`here <box-box_insert>`. This time the C function is doing three things: (1) finding the numeric identifier of the "capi_test" space by calling box_space_id_by_name(); |br| (2) formatting a tuple using more msgpuck.h functions; |br| (3) inserting a row using box_insert. This time the call is passing a Lua table (passable_table) to the harder() function. The harder() function will see it, it's in the :code:`char *args` parameter. Tutorials We are going to use the "tarantool_sandbox" that was created in section :ref:`first database <user_guide_getting_started-first_database>`. So there is a single space, and a numeric primary key, and a running tarantool server which also serves as a client. We begin by declaring a variable "``string_value``". The word "``local``" means that string_value appears only in ``main_function``. If we didn't use "``local``" then ``string_value`` would be visible everywhere - even by other users using other clients connected to this server! Sometimes that's a very desirable feature for inter-client communication, but not this time. We will start by making a function that returns a fixed string, “Hello world”. What has also been shown is that inserting a million tuples took 37 seconds. The host computer was a Linux laptop. By changing :ref:`wal_mode <cfg_binary_logging_snapshots-wal_mode>` to 'none' before running the test, one can reduce the elapsed time to 4 seconds. What has been shown is that Lua functions are quite expressive (in fact one can do more with Tarantool's Lua stored procedures than one can do with stored procedures in some SQL DBMSs), and that it's straightforward to combine Lua-library functions and Tarantool-library functions. box.cfg{listen=3306}
box.schema.space.create('capi_test')
box.space.capi_test:create_index('primary')
net_box = require('net.box')
capi_connection = net_box:new(3306) box.schema.func.create('easy', {language = 'C'})
box.schema.user.grant('guest', 'execute', 'function', 'easy')
capi_connection:call('easy') box.schema.func.create('harder', {language = 'C'})
box.schema.user.grant('guest', 'execute', 'function', 'harder')
passable_table = {}
table.insert(passable_table, 1)
table.insert(passable_table, 2)
table.insert(passable_table, 3)
capi_connection:call('harder', passable_table) box.schema.func.create('hardest', {language = "C"})
box.schema.user.grant('guest', 'execute', 'function', 'hardest')
box.schema.user.grant('guest', 'read,write', 'space', 'capi_test')
capi_connection:call('hardest') box.space.t:drop()
box.schema.space.create('t')
box.space.t:create_index('primary',{})
box.space.t:create_index('secondary',{unique=false,parts={2,'string',3,'string'}})
box.space.t:insert{1,'A','a'}
box.space.t:insert{2,'AB',''}
box.space.t:insert{3,'ABC','a'}
box.space.t:insert{4,'ABCD',''}
box.space.t:insert{5,'ABCDE','a'}
box.space.t:insert{6,'ABCDE',''}
box.space.t:insert{7,'ABCDEF','a'}
box.space.t:insert{8,'ABCDF',''}
indexed_pattern_search("t", 2, "ABC.E.") box.space.tester:insert{444, '{"Item": "widget", "Quantity": 15}'}
box.space.tester:insert{445, '{"Item": "widget", "Quantity": 7}'}
box.space.tester:insert{446, '{"Item": "golf club", "Quantity": "sunshine"}'}
box.space.tester:insert{447, '{"Item": "waffle iron", "Quantit": 3}'} field[1]: 444
field[2]: '{"Hello": "world", "Quantity": 15}' function indexed_pattern_search(space_name, field_no, pattern)
  -- SEE NOTE #1 "FIND AN APPROPRIATE INDEX"
  if (box.space[space_name] == nil) then
    print("Error: Failed to find the specified space")
    return nil
  end
  local index_no = -1
  for i=0,box.schema.INDEX_MAX,1 do
    if (box.space[space_name].index[i] == nil) then break end
    if (box.space[space_name].index[i].type == "TREE"
        and box.space[space_name].index[i].parts[1].fieldno == field_no
        and (box.space[space_name].index[i].parts[1].type == "scalar"
        or box.space[space_name].index[i].parts[1].type == "string")) then
      index_no = i
      break
    end
  end
  if (index_no == -1) then
    print("Error: Failed to find an appropriate index")
    return nil
  end
  -- SEE NOTE #2 "DERIVE INDEX SEARCH KEY FROM PATTERN"
  local index_search_key = ""
  local index_search_key_length = 0
  local last_character = ""
  local c = ""
  local c2 = ""
  for i=1,string.len(pattern),1 do
    c = string.sub(pattern, i, i)
    if (last_character ~= "%") then
      if (c == '^' or c == "$" or c == "(" or c == ")" or c == "."
                   or c == "[" or c == "]" or c == "*" or c == "+"
                   or c == "-" or c == "?") then
        break
      end
      if (c == "%") then
        c2 = string.sub(pattern, i + 1, i + 1)
        if (string.match(c2, "%p") == nil) then break end
        index_search_key = index_search_key .. c2
      else
        index_search_key = index_search_key .. c
      end
    end
    last_character = c
  end
  index_search_key_length = string.len(index_search_key)
  if (index_search_key_length < 3) then
    print("Error: index search key " .. index_search_key .. " is too short")
    return nil
  end
  -- SEE NOTE #3 "OUTER LOOP: INITIATE"
  local result_set = {}
  local number_of_tuples_in_result_set = 0
  local previous_tuple_field = ""
  while true do
    local number_of_tuples_since_last_yield = 0
    local is_time_for_a_yield = false
    -- SEE NOTE #4 "INNER LOOP: ITERATOR"
    for _,tuple in box.space[space_name].index[index_no]:
    pairs(index_search_key,{iterator = box.index.GE}) do
      -- SEE NOTE #5 "INNER LOOP: BREAK IF INDEX KEY IS TOO GREAT"
      if (string.sub(tuple[field_no], 1, index_search_key_length)
      > index_search_key) then
        break
      end
      -- SEE NOTE #6 "INNER LOOP: BREAK AFTER EVERY 10 TUPLES -- MAYBE"
      number_of_tuples_since_last_yield = number_of_tuples_since_last_yield + 1
      if (number_of_tuples_since_last_yield >= 10
          and tuple[field_no] ~= previous_tuple_field) then
        index_search_key = tuple[field_no]
        is_time_for_a_yield = true
        break
        end
      previous_tuple_field = tuple[field_no]
      -- SEE NOTE #7 "INNER LOOP: ADD TO RESULT SET IF PATTERN MATCHES"
      if (string.match(tuple[field_no], pattern) ~= nil) then
        number_of_tuples_in_result_set = number_of_tuples_in_result_set + 1
        result_set[number_of_tuples_in_result_set] = tuple
      end
    end
    -- SEE NOTE #8 "OUTER LOOP: BREAK, OR YIELD AND CONTINUE"
    if (is_time_for_a_yield ~= true) then
      break
    end
    require('fiber').yield()
  end
  return result_set
end function main_function()
  local string_value
  string_value = string_function()
  return string_value
end function main_function()
  local string_value, t
  for i = 1,1000000,1 do
    string_value = string_function()
    t = box.tuple.new({i,string_value})
    box.space.tester:replace(t)
  end
end
start_time = os.clock()
main_function()
end_time = os.clock()
'insert done in ' .. end_time - start_time .. ' seconds' function main_function()
  local string_value, t
  string_value = string_function()
  t = box.tuple.new({1, string_value})
  return t
end function main_function()
  local string_value, t
  string_value = string_function()
  t = box.tuple.new({1,string_value})
  box.space.tester:replace(t)
end function string_function()
  local random_number
  local random_string
  random_number = math.random(65, 90)
  random_string = string.char(random_number)
  return random_string
end function string_function()
  local random_number
  local random_string
  random_string = ""
  for x = 1,10,1 do
    random_number = math.random(65, 90)
    random_string = random_string .. string.char(random_number)
  end
  return random_string
end function string_function()
  local random_number
  local random_string
  random_string = ""
  for x = 1,10,1 do
    random_number = math.random(65, 90)
    random_string = random_string .. string.char(random_number)
  end
  return random_string
end

function main_function()
  local string_value, t
  for i = 1,1000000,1 do
    string_value = string_function()
    t = box.tuple.new({i,string_value})
    box.space.tester:replace(t)
  end
end
start_time = os.clock()
main_function()
end_time = os.clock()
'insert done in ' .. end_time - start_time .. ' seconds' function string_function()
  return "hello world"
end json = require('json')
function sum_json_field(field_name)
  local v, t, sum, field_value, is_valid_json, lua_table
  sum = 0
  for v, t in box.space.tester:pairs() do
    is_valid_json, lua_table = pcall(json.decode, t[2])
    if is_valid_json then
      field_value = lua_table[field_name]
      if type(field_value) == "number" then sum = sum + field_value end
    end
  end
  return sum
end main_function()
box.space.tester:select{1} meaning that the tuple's first field, the primary key field, is a number while the tuple's second field, the JSON string, is a string. Thus the entire statement means "decode ``t[2]`` (the tuple's second field) as a JSON string; if there's an error set ``is_valid_json = false``; if there's no error set ``is_valid_json = true`` and set ``lua_table =`` a Lua table which has the decoded string". string_function() tarantool> **indexed_pattern_search("t", 2, "ABC.E.")**
---
- - [7, 'ABCDEF', 'a']
... tarantool> box.space.capi_test:select()
---
- - [10000, 'String 2']
... tarantool> capi_connection:call('easy')
hello world
---
- []
... tarantool> capi_connection:call('harder', passable_table)
arg_count = 1
field_count = 3
val=1.
val=2.
val=3.
---
- []
... tarantool> function main_function()
         >   local string_value
         >   string_value = string_function()
         >   return string_value
         > end
---
...
tarantool> main_function()
---
- hello world
...
tarantool> tarantool> function main_function()
         >   local string_value, t
         >   string_value = string_function()
         >   t = box.tuple.new({1,string_value})
         >   box.space.tester:replace(t)
         > end
---
...
tarantool> main_function()
---
...
tarantool> box.space.tester:select{1}
---
- - [1, 'EUJYVEECIL']
...
tarantool> tarantool> function main_function()
         > local string_value, t
         > string_value = string_function()
         > t = box.tuple.new({1, string_value})
         > return t
         > end
---
...
tarantool> main_function()
---
- [1, 'PNPZPCOOKA']
...
tarantool> tarantool> function string_funciton()
         >   return "hello world"
         > end
---
...
tarantool> string_function()
---
- hello world
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_number = math.random(65, 90)
         >   random_string = string.char(random_number)
         >   return random_string
         > end
---
...
tarantool> main_function()
---
- C
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_string = ""
         >   for x = 1,10,1 do
         >     random_number = math.random(65, 90)
         >     random_string = random_string .. string.char(random_number)
         >   end
         >   return random_string
         > end
---
...
tarantool> function main_function()
         >   local string_value, t
         >   for i = 1,1000000,1 do
         >     string_value = string_function()
         >     t = box.tuple.new({i,string_value})
         >     box.space.tester:replace(t)
         >   end
         > end
---
...
tarantool> start_time = os.clock()
---
...
tarantool> main_function()
---
...
tarantool> end_time = os.clock()
---
...
tarantool> 'insert done in ' .. end_time - start_time .. ' seconds'
---
- insert done in 37.62 seconds
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_string = ""
         >   for x = 1,10,1 do
         >     random_number = math.random(65, 90)
         >     random_string = random_string .. string.char(random_number)
         >   end
         >   return random_string
         > end
---
...
tarantool> main_function()
---
- 'ZUDJBHKEFM'
...
tarantool> tarantool> sum_json_field("Quantity")
---
- 22
... then add some tuples where the first field is a number and the second field is a string. Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-23 12:27+0300
PO-Revision-Date: 2016-09-22 21:35+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 #include "module.h"
#include "msgpuck.h"
int harder(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  uint32_t arg_count = mp_decode_array(&args);
  printf("arg_count = %d\n", arg_count);
  uint32_t field_count = mp_decode_array(&args);
  printf("field_count = %d\n", field_count);
  uint32_t val;
  int i;
  for (i = 0; i < field_count; ++i)
  {
    val = mp_decode_uint(&args);
    printf("val=%d.\n", val);
  }
  return 0;
} #include "module.h"
#include "msgpuck.h"
int hardest(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  uint32_t space_id = box_space_id_by_name("capi_test", strlen("capi_test"));
  char tuple[1024];
  char *tuple_pointer = tuple;
  tuple_pointer = mp_encode_array(tuple_pointer, 2);
  tuple_pointer = mp_encode_uint(tuple_pointer, 10000);
  tuple_pointer = mp_encode_str(tuple_pointer, "String 2", 8);
  int n = box_insert(space_id, tuple, tuple_pointer, NULL);
  return n;
} #include "module.h"
int easy(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
  printf("hello world\n");
  return 0;
} **An example in the test suite** **Cleaning up** **LINE 3: WHY "LOCAL".** This line declares all the variables that will be used in the function. Actually it's not necessary to declare all variables at the start, and in a long function it would be better to declare variables just before using them. In fact it's not even necessary to declare variables at all, but an undeclared variable is "global". That's not desirable for any of the variables that are declared in line 1, because all of them are for use only within the function. **LINE 5: START THE MAIN LOOP.** Everything inside this "``for``" loop will be repeated as long as there is another index key. A tuple is fetched and can be referenced with variable :code:`t`. **LINE 5: WHY "PAIRS()".** Our job is to go through all the rows and there are two ways to do it: with :ref:`box.space.space_object:pairs() <box_space-pairs>` or with ``variable = select(...)`` followed by :samp:`for i, {n}, 1 do {some-function}(variable[i]) end`. We preferred ``pairs()`` for this example. **LINE 6: MEANING.** The function is :ref:`json.decode <json-decode>` which means decode a JSON string, and the parameter is t[2] which is a reference to a JSON string. There's a bit of hard coding here, we're assuming that the second field in the tuple is where the JSON string was inserted. For example, we're assuming a tuple looks like **LINE 6: WHY "PCALL".** If we simply said ``lua_table = json.decode(t[2]))``, then the function would abort with an error if it encountered something wrong with the JSON string - a missing colon, for example. By putting the function inside "``pcall``" (`protected call`_), we're saying: we want to intercept that sort of error, so if there's a problem just set ``is_valid_json = false`` and we will know what to do about it later. **LINE 8.** At last we are ready to get the JSON field value from the Lua table that came from the JSON string. The value in field_name, which is the parameter for the whole function, must be a name of a JSON field. For example, inside the JSON string ``'{"Hello": "world", "Quantity": 15}'``, there are two JSON fields: "Hello" and "Quantity". If the whole function is invoked with ``sum_json_field("Quantity")``, then ``field_value = lua_table[field_name]`` is effectively the same as ``field_value = lua_table["Quantity"]`` or even ``field_value = lua_table.Quantity``. Those are just three different ways of saying: for the Quantity field in the Lua table, get the value and put it in variable :code:`field_value`. **LINE 9: WHY "IF".** Suppose that the JSON string is well formed but the JSON field is not a number, or is missing. In that case, the function would be aborted when there was an attempt to add it to the sum. By first checking ``type(field_value) == "number"``, we avoid that abortion. Anyone who knows that the database is in perfect shape can skip this kind of thing. **Preparation** **easy.c** **harder.c** **hardest.c** -- if tester is left over from some previous test, destroy it
box.space.tester:drop()
box.schema.space.create('tester')
box.space.tester:create_index('primary', {parts = {1, 'unsigned'}}) ... Well, actually it won't always look like this because ``math.random()`` produces random numbers. But for the illustration purposes it won't matter what the random string values are. :ref:`Indexed pattern search <c_lua_tutorial-indexed_pattern_search>`. :ref:`Insert one million tuples with a Lua stored procedure <c_lua_tutorial-insert_one_million_tuples>`, :ref:`Sum a JSON field for all tuples <c_lua_tutorial-sum_a_json_field>`, After following the instructions, and seeing that the results are what is described here, users should feel confident about writing their own stored procedures. And now the screen looks like this: And the function is complete. Time to test it. Starting with an empty database, defined the same way as the sandbox database that was introduced in :ref:`first database <user_guide_getting_started-first_database>`, At this point the harder() function will start using functions defined in msgpuck.h, which are documented in `http://rtsisyk.github.io/msgpuck <http://rtsisyk.github.io/msgpuck>`_. The routines that begin with "mp" are msgpuck functions that handle data formatted according to the MsgPack_ specification. Passes and returns are always done with this format so one must become acquainted with msgpuck to become proficient with the C API. Both module.h and msgpuck.h must be on the include path for the C compiler to see them. For example, if module.h address is /usr/local/include/tarantool/module.h, and msgpuck.h address is /usr/local/include/msgpuck/msgpuck.h, and they are not currently on the include path, say |br| :code:`export CPATH=/usr/local/include/tarantool:/usr/local/include/msgpuck` Хранимые процедуры на языке C Практическое задание на C Check that these items exist on the computer: |br| * Tarantool 1.7 |br| * A gcc compiler, any modern version should work |br| * "module.h" |br| * "msgpuck.h" |br| Compile the program, producing a library file named easy.so: |br| :code:`gcc -shared -o easy.so -fPIC easy.c` Compile the program, producing a library file named harder.so: |br| :code:`gcc -shared -o harder.so -fPIC harder.c` Compile the program, producing a library file named hardest.so: |br| :code:`gcc -shared -o hardest.so -fPIC hardest.c` Conclusion: calling a C function is easy. Conclusion: decoding parameter values passed to a C function is not easy at first, but there are routines to do the job, and they're documented, and there aren't very many of them. Conclusion: parts of the standard test suite use C stored procedures, and they must work, because releases don't happen if Tarantool doesn't pass the tests. Conclusion: the long description of the C API is there for a good reason. All of the functions in it can be called from C functions which are called from Lua. So C "stored procedures" have full access to the database. Configure Create a file. Name it easy.c. Put these six lines in it. Create a file. Name it harder.c. Put these 17 lines in it: Create a file. Name it hardest.c. Put these 13 lines in it: Create a function that calls another function and sets a variable Create a function that returns a string Delimiter Download the source code of Tarantool. Look in a subdirectory :code:`test/box`. Notice that there is a file named :code:`tuple_bench.test.lua` and another file named :code:`tuple_bench.c`. Examine the Lua file and observe that it is calling a function in the C file, using the same techniques that this tutorial has shown. EXAMPLE: For more about Lua loops see Lua manual `chapter 4.3.4 "Numeric for"`_. For more about Lua math-library functions see Lua users "`Math Library Tutorial`_". For more about Lua string-library functions see Lua users "`String Library Tutorial`_" . For more about Lua strings see Lua manual `chapter 2.4 "Strings"`_ . For more about functions see Lua manual `chapter 5 "Functions"`_. For more about Lua variables see Lua manual `chapter 4.2 "Local Variables and Blocks"`_ . For more about Tarantool insert and replace calls, see Tarantool manual section :ref:`Submodule box.space <box_space>`. For more about Tarantool tuples see Tarantool manual section :ref:`Submodule box.tuple <box_tuple>`. For more on Lua ``os.clock()`` see Lua manual `chapter 22.1 "Date and Time"`_. For more on Lua print() see Lua manual `chapter 5 "Functions"`_. For now, though, it's enough to know that mp_decode_array() returns the number of elements in an array, and mp_decode_uint returns an unsigned integer, from :code:`args`. And there's a side effect: when the decoding finishes, :code:`args` has changed and is now pointing to the next element. Get rid of each of the function tuples with :ref:`box.schema.func.drop <box_schema-func_drop>`, and get rid of the capi_test space with :ref:`box.schema.capi_test:drop() <box_space-drop>`, and remove the .c and .so files that were created for this tutorial. Go back to the shell where the easy.c and the harder.c programs were created. Go back to the shell where the easy.c program was created. Here are three tutorials on using Lua stored procedures with Tarantool: Here is a generic function which takes a field identifier and a search pattern, and returns all tuples that match. |br| * The field must be the first field of a TREE index. |br| * The function will use `Lua pattern matching <http://www.lua.org/manual/5.2/manual.html#6.4.1>`_, which allows "magic characters" in regular expressions. |br| * The initial characters in the pattern, as far as the first magic character, will be used as an index search key. For each tuple that is found via the index, there will be a match of the whole pattern. |br| * To be :ref:`cooperative <atomic-cooperative_multitasking>`, the function should yield after every 10 tuples, unless there is a reason to delay yielding. |br| With this function, we can take advantage of Tarantool's indexes for speed, and take advantage of Lua's pattern matching for flexibility. It does everything that an SQL "LIKE" search can do, and far more. Here is one C tutorial: :ref:`C stored procedures <f_c_tutorial-c_stored_procedures>`. If these requests appear unfamiliar, re-read the descriptions of :ref:`box.schema.func.create <box_schema-func_create>` and :ref:`box.schema.user.grant <box_schema-user_grant>` and :ref:`conn:call <net_box-call>`. In earlier versions of Tarantool, multi-line functions had to be enclosed within "delimiters". They are no longer necessary, and so they will not be used in this tutorial. However, they are still supported. Users who wish to use delimiters, or users of older versions of Tarantool, should check the syntax description for :ref:`declaring a delimiter <administration-setting_delimiter>` before proceeding. In plainer language: create a space named capi_test, and make a connection to self named capi_connection. In this tutorial, which can be followed by anyone with a Tarantool development package and a C compiler, there are three tasks. The first -- :code:`easy.c` -- prints "hello world". The second -- :code:`harder.c` -- decodes a passed parameter value. The third -- :code:`hardest.c` -- uses the C API to do DBMS work. Индексированный поиск по шаблонам Вставка 1 млн кортежей с помощью хранимой процедуры на языке Lua Invoke the function with ``sum_json_field("Quantity")``. It is not necessary to destroy the old ``string_function()`` contents, they're simply overwritten. The first assignment invokes a random-number function in Lua's math library; the parameters mean “the number must be an integer between 65 and 90.” The second assignment invokes an integer-to-character function in Lua's string library; the parameter is the code point of the character. Luckily the ASCII value of 'A' is 65 and the ASCII value of 'Z' is 90 so the result will always be a letter between A and Z. It works. We'll just leave, as exercises for future improvement, the possibility that the "hard coding" assumptions could be removed, that there might have to be an overflow check if some field values are huge, and that the function should contain a "yield" instruction if the count of tuples is huge. Its first job is to find the 'easy' function, which should be easy because by default Tarantool looks on the current directory for a file named easy.so. Its second job is to call the 'easy' function. Since the easy() function in easy.c begins with :code:`printf("hello world\n")`, the words "hello world" will appear on the screen. Its third job is to check that the call was successful. Since the easy() function in easy.c ends with :code:`return 0`, there is no error message to display and the request is over. Leave the client running. It will be necessary to enter more requests later. Практические задания на Lua Make a tuple out of a number and a string Modify main_function to insert a million tuples into the database Modify main_function to insert a tuple into the database Modify the function so it returns a one-letter random string Modify the function so it returns a ten-letter random string NOTE #1 "FIND AN APPROPRIATE INDEX" |br| The caller has passed space_name (a string) and field_no (a number). The requirements are: |br| (a) index type must be "TREE" because for other index types (HASH, BITSET, RTREE) a search with iterator=GE will not return strings in order by string value; |br| (b) field_no must be the first index part; |br| (c) the field must contain strings, because for other data types (such as "unsigned") pattern searches are not possible; |br| If these requirements are not met by any index, then print an error message and return nil. NOTE #2 "DERIVE INDEX SEARCH KEY FROM PATTERN" |br| The caller has passed pattern (a string). The index search key will be the characters in the pattern as far as the first magic character. Lua's magic characters are % ^ $ ( ) . [ ] * + - ?. For example, if the pattern is "ABC.E", the period is a magic character and therefore the index search key will be "ABC". But there is a complication ... If we see "%" followed by a punctuation character, that punctuation character is "escaped" so remove the "%" when making the index search key. For example, if the pattern is "AB%$E", the dollar sign is escaped and therefore the index search key will be "AB$E". Finally there is a check that the index search key length must be at least three -- this is an arbitrary number, and in fact zero would be okay, but short index search keys will cause long search times. NOTE #3 -- "OUTER LOOP: INITIATE" |br| The function's job is to return a result set, just as box.space.select would. We will fill it within an outer loop that contains an inner loop. The outer loop's job is to execute the inner loop, and possibly yield, until the search ends. The inner loop's job is to find tuples via the index, and put them in the result set if they match the pattern. NOTE #4 "INNER LOOP: ITERATOR" |br| The for loop here is using pairs(), see the :ref:`explanation of what index iterators are <box_index-index_pairs>`. Within the inner loop, there will be a local variable named "tuple" which contains the latest tuple found via the index search key. NOTE #5 "INNER LOOP: BREAK IF INDEX KEY IS TOO GREAT" |br| The iterator is GE (Greater or Equal), and we must be more specific: if the search index key has N characters, then the leftmost N characters of the result's index field must not be greater than the search index key. For example, if the search index key is 'ABC', then 'ABCDE' is a potential match, but 'ABD' is a signal that no more matches are possible. NOTE #6 "INNER LOOP: BREAK AFTER EVERY 10 TUPLES -- MAYBE" |br| This chunk of code is for cooperative multitasking. The number 10 is arbitrary, and usually a larger number would be okay. The simple rule would be "after checking 10 tuples, yield, and then resume the search (that is, do the inner loop again) starting after the last value that was found". However, if the index is non-unique or if there is more than one field in the index, then we might have duplicates -- for example {"ABC",1}, {"ABC", 2}, {"ABC", 3}" -- and it would be difficult to decide which "ABC" tuple to resume with. Therefore, if the result's index field is the same as the previous result's index field, there is no break. NOTE #7 "INNER LOOP: ADD TO RESULT SET IF PATTERN MATCHES" |br| Compare the result's index field to the entire pattern. For example, suppose that the caller passed pattern "ABC.E" and there is an indexed field containing "ABCDE". Therefore the initial index search key is "ABC". Therefore a tuple containing an indexed field with "ABCDE" will be found by the iterator, because "ABCDE" > "ABC". In that case string.match will return a value which is not nil. Therefore this tuple can be added to the result set. NOTE #8 "OUTER LOOP: BREAK, OR YIELD AND CONTINUE" |br| There are three conditions which will cause a break from the inner loop: (1) the for loop ends naturally because there are no more index keys which are greater than or equal to the index search key, (2) the index key is too great as described in NOTE #5, (3) it is time for a yield as described in NOTE #6. If condition (1) or condition (2) is true, then there is nothing more to do, the outer loop ends too. If and only if condition (3) is true, the outer loop must yield and then continue. If it does continue, then the inner loop -- the iterator search -- will happen again with a new value for the index search key. Now go back to the client and execute these requests: Now that ``string_function`` exists, we can invoke it from another function. Now that it's a bit clearer how to make a variable, we can change ``string_function()`` so that, instead of returning a fixed literal 'Hello world", it returns a random letter between 'A' and 'Z'. Now that it's clear how to insert one tuple into the database, it's no big deal to figure out how to scale up: instead of inserting with a literal value = 1 for the primary key, insert with a variable value = between 1 and 1 million, in a loop. Since we already saw how to loop, that's a simple thing. The only extra wrinkle that we add here is a timing function. Now that it's clear how to make a 10-letter random string, it's possible to make a tuple that contains a number and a 10-letter random string, by invoking a function in Tarantool's library of Lua functions. Now that it's clear how to make a tuple that contains a number and a 10-letter random string, the only trick remaining is putting that tuple into tester. Remember that tester is the first space that was defined in the sandbox, so it's like a database table. Now that it's clear how to produce one-letter random strings, we can reach our goal of producing a ten-letter string by concatenating ten one-letter strings, in a loop. Now, still on the client, execute this request: |br| :code:`box.space.capi_test:select()` Once again the ``string_function()`` can be invoked from main_function() which can be invoked with ``main_function()``. Once this is done, t will be the value of a new tuple which has two fields. The first field is numeric: 1. The second field is a random string. Once again the ``string_function()`` can be invoked from ``main_function()`` which can be invoked with  ``main_function()``. Read the following Lua code to see how it works. The comments that begin with "SEE NOTE ..." refer to long explanations that follow the code. Requests will be done using tarantool as a :ref:`client <administration-using_tarantool_as_a_client>`. Start tarantool, and enter these requests. Sending ``function-name()`` means “invoke the Lua function.” The effect is that the string which the function returns will end up on the screen. Since this is a test, there are deliberate errors. The "golf club" and the "waffle iron" do not have numeric Quantity fields, so must be ignored. Therefore the real sum of the Quantity field in the JSON strings should be: 15 + 7 = 22. Since this is the grand finale, we will redo the final versions of all the necessary requests: the request that created ``string_function()``, the request that created ``main_function()``, and the request that invokes ``main_function()``. Start Tarantool, cut and paste the code for function ``indexed_pattern_search``, and try the following: Start another shell. Change directory (cd) so that it is the same as the directory that the client is running on. Подсчет суммы по JSON-полям во всех кортежах Tarantool can call C code with :ref:`modules <modules-example_c>`, or with :ref:`ffi <cookbook-ffi_printf>`, or with C stored procedures. This tutorial only is about the third option, C stored procedures. In fact the routines are always "C functions" but the phrase "stored procedure" is commonly used for historical reasons. The "module.h" file will exist if Tarantool 1.7 was installed from source. Otherwise Tarantool's "developer" package must be installed. For example on Ubuntu say |br| :code:`sudo apt-get install tarantool-dev` |br| or on Fedora say |br| :code:`dnf -y install tarantool-devel` The "msgpuck.h" file will exist if Tarantool 1.7 was installed from source. Otherwise the "msgpuck" package must be installed from `https://github.com/rtsisyk/msgpuck <https://github.com/rtsisyk/msgpuck>`_. The :ref:`os.clock() <os-clock>` function will return the number of CPU seconds since the start. Therefore, by getting start_time = number of seconds just before the inserting, and then getting end_time = number of seconds just after the inserting, we can calculate (end_time - start_time) = elapsed time in seconds. We will display that value by putting it in a request without any assignments, which causes Tarantool to send the value to the client, which prints it. (Lua's answer to the C ``printf()`` function, which is ``print()``, will also work.) The function that matters is capi_connection:call('easy'). The new line here is ``box.space.tester:replace(t)``. The name contains 'tester' because the insertion is going to be to tester. The second parameter is the tuple value. To be perfectly correct we could have said ``box.space.tester:insert(t)`` here, rather than ``box.space.tester:replace(t)``, but "replace" means “insert even if there is already a tuple whose primary-key value is a duplicate”, and that makes it easier to re-run the exercise even if the sandbox database isn't empty. Once this is done, tester will contain a tuple with two fields. The first field will be 1. The second field will be a random 10-letter string. Once again the ``string_function(``) can be invoked from ``main_function()`` which can be invoked with ``main_function()``. But ``main_function()`` won't tell the whole story, because it does not return t, it only puts t into the database. To confirm that something got inserted, we'll use a SELECT request. The purpose of the exercise is to show what Lua functions look like inside Tarantool. It will be necessary to employ the Lua math library, the Lua string library, the Tarantool box library, the Tarantool box.tuple library, loops, and concatenations. It should be easy to follow even for a person who has not used either Lua or Tarantool before. The only requirement is a knowledge of how other programming languages work and a memory of the first two chapters of this manual. But for better understanding, follow the comments and the links, which point to the Lua manual or to elsewhere in this Tarantool manual. To further enhance learning, type the statements in with the tarantool client while reading along. The result should look like this: The result will be: The screen now looks like this: The word "``function``" is a Lua keyword -- we're about to go into Lua. The function name is string_function. The function has one executable statement, ``return "hello world"``. The string "hello world" is enclosed in double quotes here, although Lua doesn't care -- one could use single quotes instead. The word "``end``" means “this is the end of the Lua function declaration.” To confirm that the function works, we can say The words "for x = 1,10,1" mean “start with x equals 1, loop until x equals 10, increment x by 1 for each iteration.” The symbol ".." means "concatenate", that is, add the string on the right of the ".." sign to the string on the left of the ".." sign. Since we start by saying that random_string is "" (a blank string), the end result is that random_string has 10 random letters. Once again the ``string_function()`` can be invoked from ``main_function()`` which can be invoked with ``main_function()``. Then we assign a value to ``string_value``, namely, the result of ``string_function()``. Soon we will invoke ``main_function()`` to check that it got the value. Therefore the first displayed line will be "arg_count = 1" because there was only one item passed: passable_table. |br| The second displayed line will be "field_count = 3" because there are three items in the table. |br| The next three lines will be "1" and "2" and "3" because those are the values in the items in the table. This is an exercise assignment: “Assume that inside every tuple there is a string formatted as JSON. Inside that string there is a JSON numeric field. For each tuple, find the numeric field's value and add it to a 'sum' variable. At end, return the 'sum' variable.” The purpose of the exercise is to get experience in one way to read and process tuples. This is an exercise assignment: “Insert one million tuples. Each tuple should have a constantly-increasing numeric primary-key field and a random alphabetic 10-character string field.” This proves that the hardest() function succeeded, but where did box_space_id_by_name() and box_insert() come from? Answer: the C API. The whole C API is documented :ref:`here <index-c_api_reference>`. The function box_space_id_by_name() is documented :ref:`here <box-box_space_id_by_name>`. The function box_insert() is documented :ref:`here <box-box_insert>`. This time the C function is doing three things: (1) finding the numeric identifier of the "capi_test" space by calling box_space_id_by_name(); |br| (2) formatting a tuple using more msgpuck.h functions; |br| (3) inserting a row using box_insert. This time the call is passing a Lua table (passable_table) to the harder() function. The harder() function will see it, it's in the :code:`char *args` parameter. Практикум We are going to use the "tarantool_sandbox" that was created in section :ref:`first database <user_guide_getting_started-first_database>`. So there is a single space, and a numeric primary key, and a running tarantool server which also serves as a client. We begin by declaring a variable "``string_value``". The word "``local``" means that string_value appears only in ``main_function``. If we didn't use "``local``" then ``string_value`` would be visible everywhere - even by other users using other clients connected to this server! Sometimes that's a very desirable feature for inter-client communication, but not this time. We will start by making a function that returns a fixed string, “Hello world”. What has also been shown is that inserting a million tuples took 37 seconds. The host computer was a Linux laptop. By changing :ref:`wal_mode <cfg_binary_logging_snapshots-wal_mode>` to 'none' before running the test, one can reduce the elapsed time to 4 seconds. What has been shown is that Lua functions are quite expressive (in fact one can do more with Tarantool's Lua stored procedures than one can do with stored procedures in some SQL DBMSs), and that it's straightforward to combine Lua-library functions and Tarantool-library functions. box.cfg{listen=3306}
box.schema.space.create('capi_test')
box.space.capi_test:create_index('primary')
net_box = require('net.box')
capi_connection = net_box:new(3306) box.schema.func.create('easy', {language = 'C'})
box.schema.user.grant('guest', 'execute', 'function', 'easy')
capi_connection:call('easy') box.schema.func.create('harder', {language = 'C'})
box.schema.user.grant('guest', 'execute', 'function', 'harder')
passable_table = {}
table.insert(passable_table, 1)
table.insert(passable_table, 2)
table.insert(passable_table, 3)
capi_connection:call('harder', passable_table) box.schema.func.create('hardest', {language = "C"})
box.schema.user.grant('guest', 'execute', 'function', 'hardest')
box.schema.user.grant('guest', 'read,write', 'space', 'capi_test')
capi_connection:call('hardest') box.space.t:drop()
box.schema.space.create('t')
box.space.t:create_index('primary',{})
box.space.t:create_index('secondary',{unique=false,parts={2,'string',3,'string'}})
box.space.t:insert{1,'A','a'}
box.space.t:insert{2,'AB',''}
box.space.t:insert{3,'ABC','a'}
box.space.t:insert{4,'ABCD',''}
box.space.t:insert{5,'ABCDE','a'}
box.space.t:insert{6,'ABCDE',''}
box.space.t:insert{7,'ABCDEF','a'}
box.space.t:insert{8,'ABCDF',''}
indexed_pattern_search("t", 2, "ABC.E.") box.space.tester:insert{444, '{"Item": "widget", "Quantity": 15}'}
box.space.tester:insert{445, '{"Item": "widget", "Quantity": 7}'}
box.space.tester:insert{446, '{"Item": "golf club", "Quantity": "sunshine"}'}
box.space.tester:insert{447, '{"Item": "waffle iron", "Quantit": 3}'} field[1]: 444
field[2]: '{"Hello": "world", "Quantity": 15}' function indexed_pattern_search(space_name, field_no, pattern)
  -- SEE NOTE #1 "FIND AN APPROPRIATE INDEX"
  if (box.space[space_name] == nil) then
    print("Error: Failed to find the specified space")
    return nil
  end
  local index_no = -1
  for i=0,box.schema.INDEX_MAX,1 do
    if (box.space[space_name].index[i] == nil) then break end
    if (box.space[space_name].index[i].type == "TREE"
        and box.space[space_name].index[i].parts[1].fieldno == field_no
        and (box.space[space_name].index[i].parts[1].type == "scalar"
        or box.space[space_name].index[i].parts[1].type == "string")) then
      index_no = i
      break
    end
  end
  if (index_no == -1) then
    print("Error: Failed to find an appropriate index")
    return nil
  end
  -- SEE NOTE #2 "DERIVE INDEX SEARCH KEY FROM PATTERN"
  local index_search_key = ""
  local index_search_key_length = 0
  local last_character = ""
  local c = ""
  local c2 = ""
  for i=1,string.len(pattern),1 do
    c = string.sub(pattern, i, i)
    if (last_character ~= "%") then
      if (c == '^' or c == "$" or c == "(" or c == ")" or c == "."
                   or c == "[" or c == "]" or c == "*" or c == "+"
                   or c == "-" or c == "?") then
        break
      end
      if (c == "%") then
        c2 = string.sub(pattern, i + 1, i + 1)
        if (string.match(c2, "%p") == nil) then break end
        index_search_key = index_search_key .. c2
      else
        index_search_key = index_search_key .. c
      end
    end
    last_character = c
  end
  index_search_key_length = string.len(index_search_key)
  if (index_search_key_length < 3) then
    print("Error: index search key " .. index_search_key .. " is too short")
    return nil
  end
  -- SEE NOTE #3 "OUTER LOOP: INITIATE"
  local result_set = {}
  local number_of_tuples_in_result_set = 0
  local previous_tuple_field = ""
  while true do
    local number_of_tuples_since_last_yield = 0
    local is_time_for_a_yield = false
    -- SEE NOTE #4 "INNER LOOP: ITERATOR"
    for _,tuple in box.space[space_name].index[index_no]:
    pairs(index_search_key,{iterator = box.index.GE}) do
      -- SEE NOTE #5 "INNER LOOP: BREAK IF INDEX KEY IS TOO GREAT"
      if (string.sub(tuple[field_no], 1, index_search_key_length)
      > index_search_key) then
        break
      end
      -- SEE NOTE #6 "INNER LOOP: BREAK AFTER EVERY 10 TUPLES -- MAYBE"
      number_of_tuples_since_last_yield = number_of_tuples_since_last_yield + 1
      if (number_of_tuples_since_last_yield >= 10
          and tuple[field_no] ~= previous_tuple_field) then
        index_search_key = tuple[field_no]
        is_time_for_a_yield = true
        break
        end
      previous_tuple_field = tuple[field_no]
      -- SEE NOTE #7 "INNER LOOP: ADD TO RESULT SET IF PATTERN MATCHES"
      if (string.match(tuple[field_no], pattern) ~= nil) then
        number_of_tuples_in_result_set = number_of_tuples_in_result_set + 1
        result_set[number_of_tuples_in_result_set] = tuple
      end
    end
    -- SEE NOTE #8 "OUTER LOOP: BREAK, OR YIELD AND CONTINUE"
    if (is_time_for_a_yield ~= true) then
      break
    end
    require('fiber').yield()
  end
  return result_set
end function main_function()
  local string_value
  string_value = string_function()
  return string_value
end function main_function()
  local string_value, t
  for i = 1,1000000,1 do
    string_value = string_function()
    t = box.tuple.new({i,string_value})
    box.space.tester:replace(t)
  end
end
start_time = os.clock()
main_function()
end_time = os.clock()
'insert done in ' .. end_time - start_time .. ' seconds' function main_function()
  local string_value, t
  string_value = string_function()
  t = box.tuple.new({1, string_value})
  return t
end function main_function()
  local string_value, t
  string_value = string_function()
  t = box.tuple.new({1,string_value})
  box.space.tester:replace(t)
end function string_function()
  local random_number
  local random_string
  random_number = math.random(65, 90)
  random_string = string.char(random_number)
  return random_string
end function string_function()
  local random_number
  local random_string
  random_string = ""
  for x = 1,10,1 do
    random_number = math.random(65, 90)
    random_string = random_string .. string.char(random_number)
  end
  return random_string
end function string_function()
  local random_number
  local random_string
  random_string = ""
  for x = 1,10,1 do
    random_number = math.random(65, 90)
    random_string = random_string .. string.char(random_number)
  end
  return random_string
end

function main_function()
  local string_value, t
  for i = 1,1000000,1 do
    string_value = string_function()
    t = box.tuple.new({i,string_value})
    box.space.tester:replace(t)
  end
end
start_time = os.clock()
main_function()
end_time = os.clock()
'insert done in ' .. end_time - start_time .. ' seconds' function string_function()
  return "hello world"
end json = require('json')
function sum_json_field(field_name)
  local v, t, sum, field_value, is_valid_json, lua_table
  sum = 0
  for v, t in box.space.tester:pairs() do
    is_valid_json, lua_table = pcall(json.decode, t[2])
    if is_valid_json then
      field_value = lua_table[field_name]
      if type(field_value) == "number" then sum = sum + field_value end
    end
  end
  return sum
end main_function()
box.space.tester:select{1} meaning that the tuple's first field, the primary key field, is a number while the tuple's second field, the JSON string, is a string. Thus the entire statement means "decode ``t[2]`` (the tuple's second field) as a JSON string; if there's an error set ``is_valid_json = false``; if there's no error set ``is_valid_json = true`` and set ``lua_table =`` a Lua table which has the decoded string". string_function() tarantool> **indexed_pattern_search("t", 2, "ABC.E.")**
---
- - [7, 'ABCDEF', 'a']
... tarantool> box.space.capi_test:select()
---
- - [10000, 'String 2']
... tarantool> capi_connection:call('easy')
hello world
---
- []
... tarantool> capi_connection:call('harder', passable_table)
arg_count = 1
field_count = 3
val=1.
val=2.
val=3.
---
- []
... tarantool> function main_function()
         >   local string_value
         >   string_value = string_function()
         >   return string_value
         > end
---
...
tarantool> main_function()
---
- hello world
...
tarantool> tarantool> function main_function()
         >   local string_value, t
         >   string_value = string_function()
         >   t = box.tuple.new({1,string_value})
         >   box.space.tester:replace(t)
         > end
---
...
tarantool> main_function()
---
...
tarantool> box.space.tester:select{1}
---
- - [1, 'EUJYVEECIL']
...
tarantool> tarantool> function main_function()
         > local string_value, t
         > string_value = string_function()
         > t = box.tuple.new({1, string_value})
         > return t
         > end
---
...
tarantool> main_function()
---
- [1, 'PNPZPCOOKA']
...
tarantool> tarantool> function string_funciton()
         >   return "hello world"
         > end
---
...
tarantool> string_function()
---
- hello world
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_number = math.random(65, 90)
         >   random_string = string.char(random_number)
         >   return random_string
         > end
---
...
tarantool> main_function()
---
- C
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_string = ""
         >   for x = 1,10,1 do
         >     random_number = math.random(65, 90)
         >     random_string = random_string .. string.char(random_number)
         >   end
         >   return random_string
         > end
---
...
tarantool> function main_function()
         >   local string_value, t
         >   for i = 1,1000000,1 do
         >     string_value = string_function()
         >     t = box.tuple.new({i,string_value})
         >     box.space.tester:replace(t)
         >   end
         > end
---
...
tarantool> start_time = os.clock()
---
...
tarantool> main_function()
---
...
tarantool> end_time = os.clock()
---
...
tarantool> 'insert done in ' .. end_time - start_time .. ' seconds'
---
- insert done in 37.62 seconds
...
tarantool> tarantool> function string_function()
         >   local random_number
         >   local random_string
         >   random_string = ""
         >   for x = 1,10,1 do
         >     random_number = math.random(65, 90)
         >     random_string = random_string .. string.char(random_number)
         >   end
         >   return random_string
         > end
---
...
tarantool> main_function()
---
- 'ZUDJBHKEFM'
...
tarantool> tarantool> sum_json_field("Quantity")
---
- 22
... then add some tuples where the first field is a number and the second field is a string. 