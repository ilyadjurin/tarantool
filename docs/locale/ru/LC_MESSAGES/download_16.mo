��    D      <              \  �  ]  �  -  �  �
  9  W  J   �  &   �       �        �  .   �       D   *  
   o  �   z  7         U     v  
   �  A   �     �     �  E   �  %   &     L  z   c  {   �     Z  K   q  J   �  J        S  N   c     �  F   �  &   �  �   %  C   �  A   �     ,     @  G   T     �  0   �  5   �  9     �   Y  �   �    �     �  �   �     �  �   '  H   �  �     K   �  t   �  -   Z  E   �  <   �  <     N   H  C   �  :  �  :  "  *  Q$  L   |&  �  �&  �  f)  �  /+  �  �-  �  �1  9  )5  J   c6  &   �6     �6  �   �6     �7  .   �7     �7  D   �7  
   A8  �   L8  7   �8      '9     H9  
   V9  A   a9     �9     �9  E   �9  %   �9     :  z   5:  {   �:     ,;  K   C;  J   �;  J   �;     %<  N   5<     �<  F   �<  &   �<  �   �<  C   x=  A   �=     �=     >  G   &>     n>  0   �>  5   �>  9   �>  �   +?  �   �?    @     �A  �   �A     yB  �   �B  H   �C  �   �C  K   kD  t   �D  -   ,E  E   ZE  <   �E  <   �E  N   F  C   iF  :  �F  :  �H  *  #K  L   NM  �  �M   # Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/7/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6'

# Install Tarantool
sudo yum -y install tarantool # Enable EPEL repository
sudo yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6' --enablerepo='epel'

# Install Tarantool
sudo yum -y install tarantool # Enable EPEL repository
sudo yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6' --enablerepo='epel'

# Install tarantool
sudo yum -y install tarantool $ brew install tarantool
==> Downloading https://homebrew.bintray.com/bottles/tarantool-1.6.8-653.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring tarantool-1.6.8-653.el_capitan.bottle.tar.gz
/usr/local/Cellar/tarantool/1.6.8-653: 17 files, 2.2M All published releases are available at `<http://tarantool.org/dist/1.6>`_ Also, look at the `Fresh Ports`_ page. Amazon Linux Amazon Linux is based on RHEL 6 / CentOS 6. We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. Available version: 1.6 / 1.7 Available version: 1.6 / :doc:`1.7 <download>` Building from source C connector            `<https://github.com/tarantool/tarantool-c>`_ Connectors Copy and paste the script below to the terminal prompt (if you want the version that comes with Ubuntu, start with the lines that follow the '# install' comment): Copy and paste the script below to the terminal prompt: Debian Stretch, Jessie and newer Debian Wheezy Docker Hub Erlang driver,         `<https://github.com/rtsisyk/etarantool>`_ Fedora FreeBSD Go driver,             `<https://github.com/tarantool/go-tarantool>`_ Hosting is powered by |packagecloud|. In these instructions, In these instructions, ``$release`` is an environment variable which will contain the Debian version code (e.g. "jessie"). In these instructions, ``$release`` is an environment variable which will contain the Ubuntu version code (e.g. "precise"). In these instructions: Java driver,           `Maven repository`_ or `Java connector GitHub page`_ Lua-nginx driver,      `<https://github.com/ziontab/lua-nginx-tarantool>`_ Lua-resty driver,      `<https://github.com/perusio/lua-resty-tarantool>`_ Microsoft Azure Nginx Upstream module, `<https://github.com/tarantool/nginx_upstream_module>`_ OS X PHP PECL driver,       `<https://github.com/tarantool/tarantool-php>`_ Perl driver,           `DR:Tarantool`_ Please consult with the Tarantool documentation for :ref:`build-from-source <building_from_source>` instructions on your system. Pure PHP driver,       `<https://github.com/tarantool-php/client>`_ Python driver,         `<http://pypi.python.org/pypi/tarantool>`_ RHEL 6 and CentOS 6 RHEL 7 and CentOS 7 Ruby driver,           `<https://github.com/tarantool/tarantool-ruby>`_ Tarantool - Downloads (1.6) Tarantool images are available at `Docker Hub`_. Tarantool images are available at `Microsoft Azure`_. Tarantool is available from the FreeBSD Ports collection. To get the latest source files for version 1.6, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To simplify problem analysis and avoid various bugs induced by compilation parameters and environment, it is recommended that production systems use the builds provided on this site. To simplify problem analysis and avoid various bugs induced by compilation parameters and environment, it is recommended that production systems use the builds provided on this site.All published releases are available at http://tarantool.org/dist/1.6Hosting is powered by packagecloud. Ubuntu We maintain an always up-to-date Debian GNU/Linux package repository. At the moment, the repository contains builds for Debian "stretch" and "jessie". For Debian "wheezy", see personal instructions on this page. We maintain an always up-to-date Fedora package repository. At the moment, the repository contains builds for Fedora 23 and 24. We maintain an always up-to-date Ubuntu package repository. At the moment, the repository contains builds for Ubuntu "xenial", "wily", "trusty", "precise". We maintain an always up-to-date package repository for Debian "wheezy". We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. We maintain an always up-to-date package repository for RHEL 7 derivatives. With your browser, go to the `FreeBSD Ports`_ page. Enter the search term: `tarantool`. Choose the package you want. You can install Tarantool using ``homebrew``: ``$basearch`` (i.e. base architecture) must be either i386 or x86_64. ``$releasever`` (i.e. CentOS release version) must be 6, and ``$releasever`` (i.e. CentOS release version) must be 7, and ``$releasever`` (i.e. Fedora release version) must be 23 or 24 or rawhide, and ``$releasever`` (i.e. RHEL / CentOS release version) must be 6, and curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb http://download.tarantool.org/tarantool/1.6/debian/ $release main
deb-src http://download.tarantool.org/tarantool/1.6/debian/ $release main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb http://download.tarantool.org/tarantool/1.6/ubuntu/ $release main
deb-src http://download.tarantool.org/tarantool/1.6/ubuntu/ $release main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb https://packagecloud.io/tarantool/1_6/debian/ wheezy main
deb-src https://packagecloud.io/tarantool/1_6/debian/ wheezy main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool node.js driver,        `<https://github.com/KlonD90/node-tarantool-driver>`_ sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=Fedora-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/fedora/\$releasever/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=Fedora-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/fedora/\$releasever/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_6'
sudo dnf -y install tarantool Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-11 16:19+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 # Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/7/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/7/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6'

# Install Tarantool
sudo yum -y install tarantool # Enable EPEL repository
sudo yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6' --enablerepo='epel'

# Install Tarantool
sudo yum -y install tarantool # Enable EPEL repository
sudo yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
sed 's/enabled=.*/enabled=1/g' -i /etc/yum.repos.d/epel.repo

# Add Tarantool repository
sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=EnterpriseLinux-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=EnterpriseLinux-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/el/6/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

# Update metadata
sudo yum makecache -y --disablerepo='*' --enablerepo='tarantool_1_6' --enablerepo='epel'

# Install tarantool
sudo yum -y install tarantool $ brew install tarantool
==> Downloading https://homebrew.bintray.com/bottles/tarantool-1.6.8-653.el_capitan.bottle.tar.gz
######################################################################## 100.0%
==> Pouring tarantool-1.6.8-653.el_capitan.bottle.tar.gz
/usr/local/Cellar/tarantool/1.6.8-653: 17 files, 2.2M All published releases are available at `<http://tarantool.org/dist/1.6>`_ Also, look at the `Fresh Ports`_ page. Amazon Linux Amazon Linux is based on RHEL 6 / CentOS 6. We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. Available version: 1.6 / 1.7 Available version: 1.6 / :doc:`1.7 <download>` Building from source C connector            `<https://github.com/tarantool/tarantool-c>`_ Connectors Copy and paste the script below to the terminal prompt (if you want the version that comes with Ubuntu, start with the lines that follow the '# install' comment): Copy and paste the script below to the terminal prompt: Debian Stretch, Jessie and newer Debian Wheezy Docker Hub Erlang driver,         `<https://github.com/rtsisyk/etarantool>`_ Fedora FreeBSD Go driver,             `<https://github.com/tarantool/go-tarantool>`_ Hosting is powered by |packagecloud|. In these instructions, In these instructions, ``$release`` is an environment variable which will contain the Debian version code (e.g. "jessie"). In these instructions, ``$release`` is an environment variable which will contain the Ubuntu version code (e.g. "precise"). In these instructions: Java driver,           `Maven repository`_ or `Java connector GitHub page`_ Lua-nginx driver,      `<https://github.com/ziontab/lua-nginx-tarantool>`_ Lua-resty driver,      `<https://github.com/perusio/lua-resty-tarantool>`_ Microsoft Azure Nginx Upstream module, `<https://github.com/tarantool/nginx_upstream_module>`_ OS X PHP PECL driver,       `<https://github.com/tarantool/tarantool-php>`_ Perl driver,           `DR:Tarantool`_ Please consult with the Tarantool documentation for :ref:`build-from-source <building_from_source>` instructions on your system. Pure PHP driver,       `<https://github.com/tarantool-php/client>`_ Python driver,         `<http://pypi.python.org/pypi/tarantool>`_ RHEL 6 and CentOS 6 RHEL 7 and CentOS 7 Ruby driver,           `<https://github.com/tarantool/tarantool-ruby>`_ Tarantool - Downloads (1.6) Tarantool images are available at `Docker Hub`_. Tarantool images are available at `Microsoft Azure`_. Tarantool is available from the FreeBSD Ports collection. To get the latest source files for version 1.6, you can clone or download them from the Tarantool repository at `GitHub`_, or download them as a `tarball`_. To simplify problem analysis and avoid various bugs induced by compilation parameters and environment, it is recommended that production systems use the builds provided on this site. To simplify problem analysis and avoid various bugs induced by compilation parameters and environment, it is recommended that production systems use the builds provided on this site.All published releases are available at http://tarantool.org/dist/1.6Hosting is powered by packagecloud. Ubuntu We maintain an always up-to-date Debian GNU/Linux package repository. At the moment, the repository contains builds for Debian "stretch" and "jessie". For Debian "wheezy", see personal instructions on this page. We maintain an always up-to-date Fedora package repository. At the moment, the repository contains builds for Fedora 23 and 24. We maintain an always up-to-date Ubuntu package repository. At the moment, the repository contains builds for Ubuntu "xenial", "wily", "trusty", "precise". We maintain an always up-to-date package repository for Debian "wheezy". We maintain an always up-to-date package repository for RHEL 6 derivatives. You may need to enable the `EPEL`_ repository for some packages. We maintain an always up-to-date package repository for RHEL 7 derivatives. With your browser, go to the `FreeBSD Ports`_ page. Enter the search term: `tarantool`. Choose the package you want. You can install Tarantool using ``homebrew``: ``$basearch`` (i.e. base architecture) must be either i386 or x86_64. ``$releasever`` (i.e. CentOS release version) must be 6, and ``$releasever`` (i.e. CentOS release version) must be 7, and ``$releasever`` (i.e. Fedora release version) must be 23 or 24 or rawhide, and ``$releasever`` (i.e. RHEL / CentOS release version) must be 6, and curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb http://download.tarantool.org/tarantool/1.6/debian/ $release main
deb-src http://download.tarantool.org/tarantool/1.6/debian/ $release main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb http://download.tarantool.org/tarantool/1.6/ubuntu/ $release main
deb-src http://download.tarantool.org/tarantool/1.6/ubuntu/ $release main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool curl http://download.tarantool.org/tarantool/1.6/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
sudo tee /etc/apt/sources.list.d/tarantool_1_6.list <<- EOF
deb https://packagecloud.io/tarantool/1_6/debian/ wheezy main
deb-src https://packagecloud.io/tarantool/1_6/debian/ wheezy main
EOF

# install
sudo apt-get update
sudo apt-get -y install tarantool node.js driver,        `<https://github.com/KlonD90/node-tarantool-driver>`_ sudo rm -f /etc/yum.repos.d/*tarantool*.repo
sudo tee /etc/yum.repos.d/tarantool_1_6.repo <<- EOF
[tarantool_1_6]
name=Fedora-\$releasever - Tarantool
baseurl=http://download.tarantool.org/tarantool/1.6/fedora/\$releasever/\$basearch/
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
enabled=1

[tarantool_1_6-source]
name=Fedora-\$releasever - Tarantool Sources
baseurl=http://download.tarantool.org/tarantool/1.6/fedora/\$releasever/SRPMS
gpgkey=http://download.tarantool.org/tarantool/1.6/gpgkey
repo_gpgcheck=1
gpgcheck=0
EOF

sudo dnf -q makecache -y --disablerepo='*' --enablerepo='tarantool_1_6'
sudo dnf -y install tarantool 