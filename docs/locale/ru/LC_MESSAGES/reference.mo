��    �     &              L     L  �  L  �   �M  #  �N  �
  �O  u   }Z  T   �Z  #   H[  �   l[  %  S\  
  y^  �  �`  �  Wl  "  o    @r  ]   Yu  g   �u  $   v  �   Dv  �   1w  `   �w     4x     8x     Jx  =   Wx  D   �x  \   �x     7y  '   Hy  +   py     �y  #   �y  
   �y     �y     �y     z     z  $   3z  &   Xz     z     �z     �z     �z     �z  	   �z     �z     �z     {  	   {     {     3{  K   <{     �{  ;   �{     �{  7   �{  4   /|    d|  `   |}  c   �}  �   A~  �   �~  {   H  i   �  c   .�  j   ��  �  ��    ��  �   ̓     ��     ��     ��     ��     ��     Ԅ     �  �   �  �   Å     M�     `�     e�     {�     ��  
   ��     ��          ҆     ؆     �     ��     �     �      �     4�     B�     U�     c�    q�  w   w�  �  �     ��  0   ��  0   �  .   B�  -   q�  +   ��  +   ˋ  +   ��  5   #�  -   Y�  -   ��  )   ��  1   ߌ  )   �  )   ;�  1   e�  )   ��  1   ��  )   �  -   �  5   K�  1   ��  5   ��  1   �  )   �  1   E�  *   w�     ��     ��  $   ۏ  $    �     %�  *   B�  '   m�  *   ��  v   ��  �   7�  �   ��  �   A�  �   ɒ  �   S�  �   �  �   s�  P  ��  =   L�  `   ��  @   �  [   ,�  >   ��  ;   Ǘ  l   �  >   p�  l   ��  P   �  M   m�  4   ��  Y  �  �  J�    �  �   �  �   �  �   ۠  O  |�  )   ̢  ?  ��  �  6�     *�  S   H�     ��  �   ��  �   ��  �  2�    ��  �   ӫ  N   Ӭ  r  "�  T   ��  P   �  �   ;�  �   ϯ  �   {�    Y�  �   j�  �  $�  �   ��  �  J�  �   ��  �   ��  +   �  )   <�  ,   f�  �   ��  �   2�  �  �  �   ͽ     X�  �   i�  z   %�  t   ��     �  �   2�      �  h   ;�  �   ��  �   r�  �   �  �   ��  A   ��  �  �  �  ��  )   k�  �   ��  $  ��  h   ��  Y   �  =  k�  >  ��  o   ��  ?   X�  �   ��  �   *�  ,   ��  �   ��  g   ��  �   �     ��  O   ��  �   �     ��     ��     ��     ��     �  A   !�     c�  v   |�     ��     �  v   "�  s   ��     �  "   %�  �   H�  �   ��  
   ��  2  ��  3   ��  &   
�  &   1�  )   X�  &   ��  )   ��  &   ��  I  ��  e   D�    ��    ��  �   ��  #   ��    ��  M   ��  N   ��     J�     _�  �   q�     �     2�  .   J�     y�  $   ��  b   ��  D   �  V   d�     ��     ��     ��     ��     ��     �     �     -�     9�     H�  	   X�     b�  \  j�  G   ��  �   �  ;  ��  *  ��  �   �     ��     �     �     *�  2   ;�  G   n�  4   ��  %   ��     �  (   -�  +   V�     ��  �  ��       �     A�     Y�     o�  G   ��  m   ��  k   ;�  W   ��     ��  �   �  ?  ��  C   ��  H   @�  H   ��  E   ��  B   �  )   [�  .   ��  �   ��  ?   ;�  O   {�  �   ��     e�  ~   r�  }   ��  z   o�  ?   ��  �  *�  c   ��  �    �  6   ��  �   
�     ��  v   ��  �   D�  �   ��     ��  I   �  U  N�  <   ��  7   ��  o   �  >   ��  Q   ��  N   �  6   i�  �   ��  �   3�  c    �   l �  G     @    R  O i  � n    �   { �    �   	 �   �	 b  o
 �   � q  � �  � l   � N   a �   � �   q /    G   ; �   � �  1 �   � "  �    � (    (   + *   T        � d   � _       d o   q �   � �   � 2   � '   �    I    �  ` �   �   �     q! Z   y!    �! �  �!    q$ �   �$ +   % q   L&    �& �   �& �   �'    ,(    9(    H(    Y(    i(    v(    �(    �(    �(    �(    �(    �(    �(    �(    �(    )    )    *)    9)    I)    Y)    f)    y)    �)    �) �   �)    �*    �*    �* 
   �* A   �* Q   �* H   O+ M   �+ 	   �+ �   �+ �   �, �   F- �   �- z   �.    / A   )/ <   k/ �   �/    >0   S0 �   i1 �   2 O   �2 ,   �2 �   3 b   �3 =   F4 �   �4    5 ]   5 �   w5    6 �    6 L   �6 ?   &7 �   f7 �   8 R   �8   �8   : b   ; �   y; �   < N   �< �   = =   �= H   4>    }>    �> 9   �> |   �> *   i? �   �?    R@ :   e@ ,   �@ C   �@ #   A 3   5A =   iA 5   �A    �A K   �A t   2B    �B 2   �B s   �B �   `C �   .D �   �D M   UE "   �E j   �E I   1F W  {F 	   �G    �G    �G <   H    PH �   \H    �H �   I #   �I s   �I $   gJ �   �J I   K 1   gK 9   �K -   �K '   L )   )L 5   SL 5   �L I   �L L   	M 7   VM 7   �M 8   �M 5   �M ;   5N (   qN E   �N E   �N 8   &O E   _O F   �O 6   �O H   #P H   lP H   �P �  �P +   �R 0   �R 4   S .   FS X   uS    �S 3   �S �   T    �T    �T    �T    �T 	   �T    �T     U    	U    U    U    +U    7U    CU    OU 	   [U    eU    tU    �U    �U 	   �U    �U    �U    �U    �U    �U    �U "   �U :   V M   NV -   �V 3   �V {   �V "   zW �   �W W   xX �   �X �   bY �   Z i   [ 7   m[ T   �[ ,   �[ @   '\    h\ b   �] �   �]    ^ 
  �^ �   �a /   db )   �b /   �b 6   �b Z   %c <   �c �   �c    Id    _d �  ud �  f �   �g 7   Jh �   �h �   i   �i n   �j b  @k   �l F   �m :   �m �  7n 6  p �   Lq @   Dr G   �r C   �r g  s �  yt #  !w �   Ex h   �x $  dy �   �z M   v{ �   �{ H   �| �   �| �   �} s  P~ 	  � *  ΀ �   �� �   �� E  G� z   �� 8  � �  A� �   �� B   ׈ �   � �  ։ 6  ȋ   �� Y   � �   r� !   � Q  6� 3   �� �   �� �   ]� �   L� �   =� �   ɔ �  n� G   � �   ;�   #�    B� C   _� )   ��   ͚ H  ߛ �   (� �  � �   {� ,  ^�    �� ?   �� %   � �   � �  �� �  *� �  ٥ C   �� >   ɧ �   � y  Ш �   J� �   ު :   �� �   ī %   �� +  �� �  ԭ �   y� ,  � g  4� :   �� #   ׳ ?  �� �   ;� �   � �   ɶ �   �� S  � �   k� )  f� 6  �� �  Ǽ   �� �   �� 	  E� `   O� �   �� ,   c� %   �� Z   �� 7   � �   I� <   �� f   � �   o� �   �� <  �� �   � C   � @   G� 7   �� c   �� �   $� �   �� �   �� �   � 7   �� 8   � 6   F� 7   }� 8   �� 3   �� 3   "� 1   V� 9   �� A   �� 4   � 9   9� 4   s� <   �� 8   �� 8   � 4   W� 4   �� =   �� :   �� 5   :� 6   p� 7  �� (   ��    �    �    � &   +�    R� "   p� 6  �� %   ��     �� �   � 6   � +   9� V   e� C   �� C    � ;   D� �   �� C   5� y   y� U   �� �   I� �   �� �   �� �   ��    9�    E�   S� v   k� R   �� �   5�    �� 	   �� .   �� 2   � -   7�    e� 2   �� 0   �� "   �� 6   � (   C� X   l� D   �� C   
� V   N� �   �� x   >� �   �� v   �� �   "� \   �� ,   !� 3   N� B   �� ?   �� X   � X   ^� c   �� S   � d   o�    �� T   ��     0� 
   Q� #   \�    ��    ��    ��    �� ,   �� W   � :   w�    ��    ��    �� 8   �� 8   � 8   T� /   �� �   �� $   w�    ��    �� �   ��    �� %   �� *   ��    �� �   ��    ��    ��    �� ?   �� �   �� O   z� d   �� G   /�   w�    y�    ��    �� "   ��    �� �   �� a   �� s   *�    ��    ��    ��    �� :   �� ,   !�    N� Y   ]� f   �� i   � U   �� ^   �� i   =� W   �� ]   �� g   ]� j   ��    0� �  E� �   �   �� :   �� w   �� �   k�    5� R   7�    �� V  �� 8   ��    2     H  f  a     �    � +   �        *    >    N    P �  V !  � /   	 %   3	 Y   Y	 W   �	    
 	   $
 *   .
 +   Y
 :   �
    �
   �
 ,   �    p   & x   � r    :   � #   � E   � 	   (    2    7    H    T h   f    �    �    � 0   �    ) R   6 ^   �    �      F    
   _ 8   j ~   � ?   "    b 
   � J   � O   8 1   � 1   �    � ?    _   E    �    �    �    � }   � #   d    �    �    �    �    �    �        " 	   1    ;    K    b    p    t    �    �    �    � Z   �    -    I    \     k    � 2   � ,   � '    (   *    S 2   b %   � 	   �    �    � 1   �        0    5 _   F 3   �    �    � �   �    �    �    � 	   �     � �   �    j    v    �    �    � C   � -   � H   $ *   m    �    � <   � @   �    = 3   C 6   w :   � :   � :   $ :   _ !   � �   � �   �  b   Z! '   �! 4   �! 5   "    P"    _" #   f"    �" 5   �"    �" "   �" 3   #    D#    K# [   Q# 4   �#    �# ?  �# E  *% D   p& >   �&   �&   ( Y  +* �  �,    . �   ;. �   �. %   �/ �   �/   ^0 f   b1 n   �1 M   82 <   �2 c   �2 &  '3 :   N5   �5 �   �7 >   .8 $   m8 E   �8 /   �8 �   9 (   �9 .   : Y  7: �   �< �   U= R   2> �   �> E   ? O   _? D   �? 7   �? 4   ,@ 2   a@ #   �@ +   �@ A   �@ Q   &A T   xA     �A Y   �A �   HB (   �B ?   C �   AC ?   �C =   *D Q   hD �   �D �   RE ,   2F R   _F Y   �F $   G .   1G G   `G 6   �G �   �G "  bH >  �J L  �K M   N �   _N �  :O =   �P b  Q =  vR   �S �  �T $   W] 8   |] ,  �] 9   �^ O   _ +   l_ _   �_ )   �_ 1   "` O   T` N  �`    �b m   e �  �e ?   h j  Ph �  �i 2  �k �   �l E   �m �  �m '  �p E  �q �   	s   t �   v    �v     �v K   �v    Ew ;   Nw    �w    �w 4  �w ,   �x $   %y    Jy    gy    �y    �y &   �y    �y /   �y /   'z 0   Wz 3   �z 0   �z <   �z >   *{ O   i{ K   �{ @   |    F|    N| "   S| 3   v| $   �| "   �| "   �| =   } =   S} A   �} &   �} F   �} /   A~    q~ d   �~    �~ &    �   ,    �    �    � �   � �   ̀    T�    e� +   �� (   �� �   ځ �   �� `   \�    �� Z   �� i   � �  ��    '� �  *� �   � #  �� �
  މ u   �� T   � #   b� �   �� %  m� 
  �� �  �� �  q� "  7�   Z� ]   s� g   ѯ $   9� �   ^� �   K� `   ��    N�    R�    d� =   q� D   �� \   ��    Q� '   b� +   ��    �� #   ˳ 
   �    ��    �    �    2� $   M� &   r�    ��    ��    ��    д    � 	   ��    �    �    "� 	   .�    8�    M� K   V�    �� ;   ��    �� 7   � 4   I�   ~� `   �� c   �� �   [� �   � {   b� i   ޹ c   H� j   �� �  �   ׼ �   �    ��    ��    ž    ɾ    ؾ    �    �� �   � �   ݿ    g�    z�    �    ��    �� 
   ��    ��    ��    ��    ��    �    �    �    ,�    :�    N�    \�    o�    }�   �� w   �� �  	�    �� 0   �� 0   +� .   \� -   �� +   �� +   �� +   � 5   =� -   s� -   �� )   �� 1   �� )   +� )   U� 1   � )   �� 1   �� )   � -   7� 5   e� 1   �� 5   �� 1   � )   5� 1   _� *   ��    ��    �� $   �� $   �    ?� *   \� '   �� *   �� v   �� �   Q� �   �� �   [� �   �� �   m� �   � �   �� P  � =   f� `   �� @   � [   F� >   �� ;   �� l   � >   �� l   �� P   6� M   �� 4   �� Y  
� �  d�   3� �  :� �   .� �   �� O  �� )   �� ?  � �  P�    D� S   b�    �� �   �� �   �� �  L�   �� �   �� N   �� r  <� T   �� P   � �   U� �   �� �   ��   s� �   �� �  >� �   �� �  d� �   � �   �� +   *� )   V� ,   �� �   �� �   L� �  � �   ��    r� �   �� z   ?� t   ��    /� �   L� C   :� h   ~� �   �� �   �� �   Z� �   � A   � �  R� �  % )   � �   � $  � h   � Y   T =  � >  � o   + ?   � �   � �   m	 ,   
 �   1
 g   �
 �   O    � O   � �   J    � ,   �    *    I    c A   � +   � v   � ,   f    � v   � s   !    � "   � �   � �   S 
     2  + 3   ^ &   � &   � )   � &   
 )   1 &   [ I  � e   �   2   M �   m #   
   . M   5 N   � 0   �     �       �    � .   �     $   ; b   ` D   � V       _    n    �    �    �    �    �    �    �    � 	   �     \   G   k �   � ;  V *  �  �   �!    �"    �"    �"    �" 2   �" G   # 4   Z# %   �#    �# (   �# +   �#    &$ �  >$     �%    �%    �%    & G   )& m   q& k   �& W   K'    �' �   �' ?  `( C   �) H   �) H   -* E   v* B   �* )   �* .   )+ �   X+ ?   �+ O   , �   o,    	- ~   - }   �- z   . ?   �. �  �. c   `1 �   �1 6   w2 �   �2    ]3 v   q3 �   �3 �   �4    �5 I   �5 U  �5 <   H7 7   �7 o   �7 >   -8 Q   l8 N   �8 6   9 �   D9 �   �9 c  �: �   < �  �<    �> @   �> R  �> i  F@ n   �A �   B �   �B �   �C �   OD b  E �   vF q  &G �  �I l   �K N   L �   TL �   M /   �M G   �M �   'N �  �N �   �P "  rQ    �R (   �R (   �R *   �R #   #S    GS d   SS _   �S    T o   %T �   �T �   �U 2   ZV '   �V   �V I   �W �  X �  �Y �   b[    %\ Z   -\    �\ �  �\    %_ �   ;_ +  �_ q    a    ra �   �a �   Sb    �b    �b    �b    c    c    *c    :c    Oc    ^c    kc    xc    �c    �c    �c    �c    �c    �c    �c    �c    �c    d    d    -d    :d    Hd �   Vd    Ne    \e    ^e 
   de A   oe Q   �e H   f M   Lf 	   �f �   �f �   Yg �   �g �   �h z   Mi    �i A   �i <   j �   \j    �j   k �   l �   �l O   Mm ,   �m �   �m b   �n =   �n �   8o    �o ]   �o �   +p    �p �   �p L   �q ?   �q �   r �   �r R   Rs   �s   �t b   �u �   -v �   �v N   vw �   �w =   �x H   �x    1y    Py 9   fy |   �y *   z �   Hz    { :   { ,   T{ C   �{ #   �{ 3   �{ =   | 5   [|    �| K   �| t   �|    [} 2   m} s   �} �   ~ �   �~ �   � M   	� "   W� j   z� I   � W  /�    ��    ��    �� <   Ԃ    � �   �    �� �   ؃ #   �� s   �� $   (� �   M� I   ޅ 1   (� 9   Z� -   �� '    )   � 5   � 5   J� I   �� L   ʇ 7   � 7   O� 8   �� 5   �� ;   �� (   2� E   [� E   �� 8   � E    � F   f� 6   �� H   � H   -� H   v� �  �� +   u� 0   �� 4   ҍ .   � X   6� C   �� 3   ӎ �   �    ��    ��    ȏ    Տ 	   ޏ    �    ��    ��    �    �     �    ,�    8�    D� 	   P�    Z�    i�    u�    �� 	   ��    ��    ��    ��    ��    ̐    Ԑ "   � :   � M   H� -   �� 3   đ {   �� "   t� �   �� W   r� �   ʓ �   \� �   � i   �� 7   g� T   �� ,   �� @   !�    b� b   �� �   �    y� 
  �� �   �� /   ^� )   �� /   �� 6   � Z   � <   z� �   ��    C�    Y� �  o� �  �� �   �� 7   D� �   |� �   �   �� n   ˥ b  :�   �� F   �� :   �� �  1� 6  � �   F� @   >� G   � C   ǭ g  � �  s� #  � �   ?� h   �� $  ^� �   �� M   p� �   �� H   �� �   ۷ �   Ƹ s  J� 	  �� *  Ȼ �   � �   �� E  A� z   �� 8  � �  ;� �   �� B   �� �   � �  �� 6  ��   �� Y   � �   l� !   � Q  0� 3   �� �   �� �   W� �   F� �   7� �   �� �  h� G   �� �   5�   �    <� C   Y� )   ��   �� H  �� �   "� �  �� �   u� ,  X�    �� ?   �� %   �� �   � �  �� �  $� �  �� C   � >   �� �   � y  �� �   D� �   �� :   �� �   �� %   |� +  �� �  �� �   s� ,  � g  .� :   �� #   �� ?  �� �   5� �   	� �   �� �   {� S  � �   e� )  `� 6  �� �  ��   �� �   �� 	  ?� `   I� �   �� ,   ]� %   �� Z   �� 7   � �   C� <   �� f   � �   i� �   �� <  �  �   � C   � @   A 7   � c   � �    �   � �   � �    7   � 8    6   @ 7   w 8   � 3   � 3    1   P 9   � A   � 4   � 9   3	 4   m	 <   �	 8   �	 8   
 4   Q
 4   �
 =   �
 :   �
 5   4 6   j 7  � (   � 6       9    F &   X     "   � 6  � %   �      �   > 6   / +   f V   � C   � C   - ;   q �   � C   b y   � U     �   v �    �   � �   �    f    r   � v   � R    �   b    � 	   � .    2   1 -   d    � 2   � 0   � "    6   9 (   p X   � D   � C   7 V   { �   � x   k �   � v   � �   O  \   �  ,   N! 3   {! B   �! ?   �! X   2" X   �" c   �" S   H# d   �#    $ T   $     ]$ 
   ~$ #   �$    �$    �$    �$    % ,   % W   L% :   �%    �%    �%    & 8   & 8   H& 8   �& /   �& �   �& $   �'    �'    �' �   (    �( %   �( *   �(    $) �   ))    �)    �)    �) ?   �) �   * O   �* d   �* G   \+   �+    �,    �,    �, "   �,    - �   - a   �- s   W.    �.    �.    �.    �. :   / ,   N/    {/ Y   �/ f   �/ i   K0 U   �0 ^   1 i   j1 W   �1 ]   ,2 g   �2 j   �2    ]3 �  r3 �   D5   �5 :   �7 w    8 �   �8    b9 R   d9    �9 V  �9 8   &;    _;    u; f  �;    �<    = +   =    H=    W=    k=    {=    }= �  �= !  B /   0D %   `D Y   �D W   �D    8E 	   QE *   [E +   �E :   �E    �E   �E ,   G   ;G p   SI x   �I r   =J :   �J #   �J E   K 	   UK    _K    dK    uK    �K h   �K    �K    L    L 0   %L    VL R   cL ^   �L    M    -M F   EM 
   �M 8   �M ~   �M ?   ON    �N 
   O J   O O   eO 1   �O 1   �O    P ?   2P _   rP    �P    �P    �P    Q }   Q #   �Q    �Q    �Q    �Q    �Q    R    #R    BR    OR 	   ^R    hR    xR    �R    �R    �R    �R    �R    �R    �R Z   �R    ZS    vS    �S     �S    �S 2   �S ,   T '   /T (   WT    �T 2   �T %   �T 	   �T    �T    U 1   U    JU    ]U    bU _   sU 3   �U    V    V �   'V    �V    �V    �V 	   �V     �V �   W    �W    �W    �W    �W    �W C   �W -   #X H   QX *   �X    �X    �X <   �X @   )Y    jY 3   pY 6   �Y :   �Y :   Z :   QZ :   �Z !   �Z �   �Z �   �[ b   �\ '   �\ 4   ] 5   G]    }]    �] #   �]    �] 5   �]    �] "   ^ 3   =^    q^    x^ [   ~^ 4   �^    _ ?  _ E  W` D   �a >   �a   !b   Ac Y  Xe �  �g    Hi �   hi �   %j %   �j �   �j   �k f   �l n   �l M   em <   �m c   �m &  Tn :   {p   �p �   �r >   [s $   �s E   �s /   t �   5t (   u .   5u Y  du �   �w �   �x R   _y �   �y E   Fz O   �z D   �z 7   !{ 4   Y{ 2   �{ #   �{ +   �{ A   | Q   S| T   �|     �| Y   } �   u} (   ~ ?   .~ �   n~ ?    =   W Q   � �   � �   � ,   _� R   �� Y   ߁ $   9� .   ^� G   �� 6   Ղ �   � "  �� >  �� L  � M   >� �   �� �  g� =   � b  @� =  ��   � �  �� $   �� 8   �� ,  � 9   � O   I� +   �� _   Ś )   %� 1   O� O   �� N  ћ     � m   A� �  �� ?   =� j  }� �  � 2  �� �   � E   ͨ �  � '  ȫ E  � �   6�   4� �   C�    �     � K   &�    r� ;   {�    ��    Ӳ 4  � ,   %� $   R�    w�    ��    ��    ˴ &   �    � /   $� /   T� 0   �� 3   �� 0   � <   � >   W� O   �� K   � @   2�    s�    {� "   �� 3   �� $   ׷ "   �� "   � =   B� =   �� A   �� &    � F   '� /   n�    �� d   ��    #� &   2� �   Y�    ��    ��    � �   (� �   ��    ��    �� +   �� (   ޼ �   � �   �� `   ��    � Z   �� i   H�  "" "Crypto" is short for "Cryptography", which generally refers to the production of a digest value from a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's crypto module supports ten types of cryptographic hash functions (AES_, DES_, DSS_, MD4_, MD5_, MDC2_, RIPEMD_, SHA-0_, SHA-1_, SHA-2_). Some of the crypto functionality is also present in the :ref:`digest` module. The functions in crypto are: #!/usr/bin/env tarantool
box.cfg{
    listen              = os.getenv("LISTEN_URI"),
    slab_alloc_arena    = 0.1,
    pid_file            = "tarantool.pid",
    rows_per_wal        = 50
}
print('Starting ', arg[1]) #!/usr/bin/tarantool
local tap = require('tap')
test = tap.test("my test name")
test:plan(2)
test:ok(2 * 2 == 4, "2 * 2 is 4")
test:test("some subtests for test2", function(test)
    test:plan(2)
    test:is(2 + 2, 4, "2 + 2 is 4")
    test:isnt(2 + 3, 4, "2 + 3 is not 4")
end)
test:check() $ # Check that the include subdirectory exists
$ # by looking for /usr/include/postgresql/libpq-fe-h.
$ [ -f /usr/include/postgresql/libpq-fe.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the necessary .so file.
$ [ -f /usr/lib/x86_64-linux-gnu/libpq.so ] && echo "OK" || echo "Error"
OK

$ # Check that the psql client can connect using some factory defaults:
$ # port = 5432, user = 'postgres', user password = 'postgres',
$ # database = 'postgres'. These can be changed, provided one changes
$ # them in all places. Insert a row in database postgres, and quit.
$ psql -h 127.0.0.1 -p 5432 -U postgres -d postgres
Password for user postgres:
psql (9.3.10)
SSL connection (cipher: DHE-RSA-AES256-SHA, bits: 256)
Type "help" for help.

postgres=# CREATE TABLE test (s1 INT, s2 VARCHAR(50));
CREATE TABLE
postgres=# INSERT INTO test VALUES (1,'PostgreSQL row');
INSERT 0 1
postgres=# \q
$

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
        ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool master
$ # repository. The resultant display is normal for Ubuntu 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.7
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.7.0.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install pg POSTGRESQL_LIBDIR=/usr/lib/x86_64-linux-gnu --local
Installing http://rocks.tarantool.org/pg-scm-1.rockspec...
... (more info about building the Tarantool/PostgreSQL driver appears here)
pg scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/pg/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the
$ # name of this directory is $HOME/tarantool_sandbox.
$ # (Change "$HOME" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
cd $HOME/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-412-g803b15c
type 'help' for interactive help
tarantool> $ **tarantool**
# OR
$ **tarantool** *options*
# OR
$ **tarantool** *lua-initialization-file* **[** *arguments* **]** $ ./tarantool --version
Tarantool 1.7.0-1216-g73f7154
Target: Linux-x86_64-Debug
... $ :codebold:`tarantool example.lua` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`4: j = 'a' .. i`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`5: print('end of program')`
:codeblue:`(TDB)>` e
:codeblue:`(TDB)`  :codegreen:`Eval mode ON`
:codeblue:`(TDB)>` j
j       a1
:codeblue:`(TDB)>` -e
:codeblue:`(TDB)`  :codegreen:`Eval mode OFF`
:codeblue:`(TDB)>` q $ export LISTEN_URI=3301
$ ~/tarantool/src/tarantool script.lua ARG
... main/101/script.lua C> version 1.7.0-1216-g73f7154
... main/101/script.lua C> log level 5
... main/101/script.lua I> mapping 107374184 bytes for a shared arena...
... main/101/script.lua I> recovery start
... main/101/script.lua I> recovering from './00000000000000000000.snap'
... main/101/script.lua I> primary: bound to 0.0.0.0:3301
... main/102/leave_local_hot_standby I> ready to accept requests
Starting  ARG
... main C> entering the event loop $ export TMDIR=~/mysql-5.5
$ # Check that the include subdirectory exists by looking
$ # for .../include/mysql.h. (If this fails, there's a chance
$ # that it's in .../include/mysql/mysql.h instead.)
$ [ -f $TMDIR/include/mysql.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the
$ # necessary .so file.
$ [ -f $TMDIR/lib/libmysqlclient.so ] && echo "OK" || echo "Error"
OK

$ # Check that the mysql client can connect using some factory
$ # defaults: port = 3306, user = 'root', user password = '',
$ # database = 'test'. These can be changed, provided one uses
$ # the changed values in all places.
$ $TMDIR/bin/mysql --port=3306 -h 127.0.0.1 --user=root \
    --password= --database=test
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.35 MySQL Community Server (GPL)
...
Type 'help;' or '\h' for help. Type '\c' to clear ...

$ # Insert a row in database test, and quit.
mysql> CREATE TABLE IF NOT EXISTS test (s1 INT, s2 VARCHAR(50));
Query OK, 0 rows affected (0.13 sec)
mysql> INSERT INTO test.test VALUES (1,'MySQL row');
Query OK, 1 row affected (0.02 sec)
mysql> QUIT
Bye

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
    ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool
$ # master repository. The resultant display is normal for Ubuntu
$ # 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.6
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.6.6.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib --local
Installing http://rocks.tarantool.org/mysql-scm-1.rockspec...
... (more info about building the Tarantool/MySQL driver appears here)
mysql scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/mysql/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the name
$ # of this directory is /home/pgulutzan/tarantool_sandbox.
$ # (Change "/home/pgulutzan" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
$ cd /home/pgulutzan/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-222-g48b98bb
type 'help' for interactive help
tarantool> $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> cfg = {
         >   servers = {
         >       { uri = 'localhost:3301', zone = '1' },
         >   },
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3301;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' },
         >     { uri = 'localhost:3302', zone = '2' },
         >   },
         >   login = 'admin',
         >   password = 'password',
         >   redundancy = 1,
         >   binary = 3301,
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_2
$ cd ~/tarantool_sandbox_2
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3302}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' };
         >     { uri = 'localhost:3302', zone = '2' };
         >   };
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3302;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now get something out ...
tarantool> shard.tester:select{1} $ ps -ef | grep tarantool
1000     14939 14188  1 10:53 pts/2    00:00:13 tarantool <running> $ ps -ef | grep tarantool
1000     14939 14188  1 10:53 pts/2    00:00:16 tarantool <running>: sessions $ socat TCP:localhost:3302 ./tmp.txt $ ~/tarantool/src/tarantool
tarantool> box.cfg{log_level=3, logger='tarantool.txt'}
tarantool> log = require('log')
tarantool> log.error('Error')
tarantool> log.info('Info %s', box.info.version)
tarantool> os.exit()
$ less tarantool.txt 'R' if the socket is now readable, 'W' if the socket is now writable, 'RW' if the socket is now both readable and writable, '' (empty string) if timeout expired; '``SEEK_END``' = end of file, '``SEEK_CUR``' = current position, '``SEEK_SET``' = start of file. 'a' 'b' - big-endian, 'false' = c2 'fixmap' if metatable is 'map' = 80 otherwise 'fixarray' = 90 'fixmap(0)' = 80 -- nil is not stored when it is a missing map value 'fixmap(1)' + 'positive fixint' (for the key) + 'positive fixint' (for the value) = 81 00 05 'fixstr' = a1 61 'float 64' = cb 3f f8 00 00 00 00 00 00 'h' - endianness depends on host (default), 'l' - little-endian, 'n' - endianness depends on network 'nil' = c0 'positive fixint' = 7f 'true' = c3 'uint 16' = cd ff ff 'uint 32' = ce ff ff ff ff (the expected output is 3304160206). **Common Types and MsgPack Encodings** **Consistent Hash** **Example:** **Format specifiers** **List of error codes** **Logging example:** **Queue** **Redundancy** **Replica** **Result:** **Shard** **Socket functions** **Zone** **array** encoding: 92 a1 41 a1 42
**map** encoding:   82 01 a1 41 02 a1 42 *connection-name*:close() *connection-name*:execute(*sql-statement* [, *parameters*]) *connection-name*:ping() *connection_name* = mysql.connect(*connection options*) *connection_name* = pg.connect(*connection options*) -- Benchmark a function which sleeps 10 seconds.
-- NB: bench() will not calculate sleep time.
-- So the returned value will be {a number less than 10, 88}.
clock = require('clock')
fiber = require('fiber')
function f(param)
  fiber.sleep(param)
  return 88
end
clock.bench(f,10) -- Disassemble hexadecimal 97 which is the x86 code for xchg eax, edi
jit.dis_x86.disass('\x97') -- Disassemble hexadecimal 97 which is the x86-64 code for xchg eax, edi
jit.dis_x64.disass('\x97') -- Show the machine code of a Lua "for" loop
jit.dump.on('m')
local x = 0;
for i = 1, 1e6 do
  x = x + i
end
print(x)
jit.dump.off() -- Show what LuaJIT is doing for a Lua "for" loop
jit.v.on()
local x = 0
for i = 1, 1e6 do
    x = x + i
end
print(x)
jit.v.off() -- This will print an approximate number of years since 1970.
clock = require('clock')
print(clock.time() / (365*24*60*60)) -- This will print nanoseconds in the CPU since the start.
clock = require('clock')
print(clock.proc64()) -- This will print nanoseconds since the start.
clock = require('clock')
print(clock.monotonic64()) -- This will print seconds in the thread since the start.
clock = require('clock')
print(clock.thread64()) -- When nil is assigned to a Lua-table field, the field is null
tarantool> {nil, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a Lua-table field, the field is json.NULL
tarantool> {json.NULL, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a JSON field, the field is null
tarantool> json.encode({field2 = json.NULL, field1 = 'a', field3 = 'c'}
---
- '{"field2":null,"field1":"a","field3":"c"}'
... -- default process_expired_tuple function
local function default_tuple_drop(space_id, args, tuple)
    local key = fun.map(
        function(x) return tuple[x.fieldno] end,
        box.space[space_id].index[0].parts
    ):totable()
    box.space[space_id]:delete(key)
end ---
- - host: 188.93.56.70
    family: AF_INET
    type: SOCK_STREAM
    protocol: tcp
    port: 80
  - host: 188.93.56.70
    family: AF_INET
    type: SOCK_DGRAM
    protocol: udp
    port: 80
... 1 – ``SYSERROR`` 1.5 127 127.0.0.1:3301 16-byte binary string 16-byte string 2 – ``ERROR`` 2...0 [5257] main/101/interactive C> version 1.7.0-355-ga4f762d
2...1 [5257] main/101/interactive C> log level 3
2...1 [5261] main/101/spawner C> initialized
2...0 [5257] main/101/interactive [C]:-1 E> Error 2015-11-30 15:13:06.373 [27469] main/101/interactive I> Log Line #1`
2015-11-30 15:14:25.973 [27469] main/101/interactive I> Log Line #2` 3 – ``CRITICAL`` 3301 36-byte binary string 36-byte hexadecimal string 4 – ``WARNING`` 4294967295 5 – ``INFO`` 6 – ``DEBUG`` 65535 :codebold:`-e` :codebold:`bt` :codebold:`c` :codebold:`e` :codebold:`f` :codebold:`globals` :codebold:`h` :codebold:`locals` :codebold:`n` :codebold:`q` :ref:`csv.iterate() <csv-iterate>` is the low level of :ref:`csv.load() <csv-load>` and :ref:`csv.dump() <csv-dump>`. To illustrate that, here is a function which is the same as the :ref:`csv.load() <csv-load>` function, as seen in `the Tarantool source code`_. :ref:`io_collect_interval <cfg_networking-io_collect_interval>`, |br| :ref:`readahead <cfg_networking-readahead>`  |br| :ref:`panic_on_snap_error <cfg_binary_logging_snapshots-panic_on_snap_error>`, |br| :ref:`panic_on_wal_error <cfg_binary_logging_snapshots-panic_on_wal_error>`, |br| :ref:`rows_per_wal <cfg_binary_logging_snapshots-rows_per_wal>`, |br| :ref:`snap_io_rate_limit <cfg_binary_logging_snapshots-snap_io_rate_limit>`, |br| :ref:`wal_mode <cfg_binary_logging_snapshots-wal_mode>`, |br| :ref:`wal_dir_rescan_delay <cfg_binary_logging_snapshots-wal_dir_rescan_delay>` |br| :ref:`socket() <socket-socket>` :ref:`socket.getaddrinfo() <socket-getaddrinfo>` :ref:`socket.tcp_connect() <socket-tcp_connect>` :ref:`socket.tcp_server() <socket-tcp_server>` :ref:`socket_object:accept() <socket-accept>` :ref:`socket_object:close() <socket-close>` :ref:`socket_object:errno() <socket-error>` :ref:`socket_object:error() <socket-error>` :ref:`socket_object:getsockopt() <socket-getsockopt>` :ref:`socket_object:linger() <socket-linger>` :ref:`socket_object:listen() <socket-listen>` :ref:`socket_object:name() <socket-name>` :ref:`socket_object:nonblock() <socket-nonblock>` :ref:`socket_object:peer() <socket-peer>` :ref:`socket_object:read() <socket-read>` :ref:`socket_object:readable() <socket-readable>` :ref:`socket_object:recv() <socket-recv>` :ref:`socket_object:recvfrom() <socket-recvfrom>` :ref:`socket_object:send() <socket-send>` :ref:`socket_object:sendto() <socket-sendto>` :ref:`socket_object:setsockopt() <socket-setsockopt>` :ref:`socket_object:shutdown() <socket-shutdown>` :ref:`socket_object:sysconnect() <socket-sysconnect>` :ref:`socket_object:syswrite() <socket-syswrite>` :ref:`socket_object:wait() <socket-wait>` :ref:`socket_object:writable() <socket-writable>` :ref:`socket_object:write() <socket-send>` :ref:`uuid() <uuid-__call>` :ref:`uuid.bin() <uuid-bin>` :ref:`uuid.frombin() <uuid-frombin>` :ref:`uuid.fromstr() <uuid-fromstr>` :ref:`uuid.str() <uuid-str>` :ref:`uuid_object:bin() <uuid-object_bin>` :ref:`uuid_object:isnil() <uuid-isnil>` :ref:`uuid_object:str() <uuid-object_str>` :samp:`chunk-size = {number}` -- number of characters to read at once (usually for file-IO efficiency), default = 4096 :samp:`conn.space.{space-name}:delete(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:delete(...)`. :samp:`conn.space.{space-name}:get(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:get(...)`. :samp:`conn.space.{space-name}:insert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:insert(...)`. :samp:`conn.space.{space-name}:replace(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:replace(...)`. :samp:`conn.space.{space-name}:select`:code:`{...}` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:select`:code:`{...}`. :samp:`conn.space.{space-name}:update(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:update(...)`. :samp:`conn.space.{space-name}:upsert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:upsert(...)`. :samp:`conn:eval({Lua-string})` evaluates and executes the expression in Lua-string, which may be any statement or series of statements. An :ref:`execute privilege <authentication-privileges>` is required; if the user does not have it, an administrator may grant it with :samp:`box.schema.user.grant({username}, 'execute', 'universe')`. :samp:`db = {database-name}` - string, default value is blank :samp:`delimiter = {string}` -- single-byte character to designate end-of-field, default = comma :samp:`host = {host-name}` - string, default value = 'localhost' :samp:`pass = {password}` or :samp:`password = {password}` - string, default value is blank :samp:`password = {password}` - string, default value is blank :samp:`port = {port-number}` - number, default value = 3306 :samp:`quote_char = {string}` -- single-byte character to designate encloser of string, default = quote mark :samp:`raise = {true|false}` - boolean, default value is false :samp:`skip_head_lines = {number}` -- number of lines to skip at the start (usually for a header), default 0 :samp:`user = {user-name}` - string, default value is operating-system user name :samp:`{function parameters}` = whatever values are required by the function. :samp:`{function}` = function or function reference; A "UUID" is a `Universally unique identifier`_. If an application requires that a value be unique only within a single computer or on a single database, then a simple counter is better than a UUID, because getting a UUID is time-consuming (it requires a syscall_). For clusters of computers, or widely distributed applications, UUIDs are better. A "digest" is a value which is returned by a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's digest module supports several types of cryptographic hash functions (AES_, MD4_, MD5_, SHA-0_, SHA-1_, SHA-2_) as well as a checksum function (CRC32_), two functions for base64_, and two non-cryptographic hash functions (guava_, murmur_). Some of the digest functionality is also present in the :ref:`crypto <crypto>` module. A complete copy of the data. The shard module handles both sharding and replication. One shard can contain one or more replicas. When a write occurs, the write is attempted on every replica in turn. The shard module does not use the built-in replication feature. A directory where database working files will be stored. The server switches to work_dir with :manpage:`chdir(2)` after start. Can be relative to the current directory. If not specified, defaults to the current directory. Other directory parameters may be relative to work_dir, for example |br| :codenormal:`box.cfg{work_dir='/home/user/A',wal_dir='B',snap_dir='C'}` |br| will put xlog files in /home/user/A/B, snapshot files in /home/user/A/C, and all other files or subdirectories in /home/user/A. A directory where snapshot (.snap) files will be stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. If not specified, defaults to work_dir. See also :ref:`wal_dir <cfg_basic-wal_dir>`. A directory where vinyl files or subdirectories will be stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. If not specified, defaults to work_dir. A directory where write-ahead log (.xlog) files are stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. Sometimes wal_dir and :ref:`snap_dir <cfg_basic-snap_dir>` are specified with different values, so that write-ahead log files and snapshot files can be stored on different disks. If not specified, defaults to work_dir. A duplicate key exists in a unique index. A fiber has all the features of a Lua coroutine_ and all the programming concepts that apply for Lua coroutines will apply for fibers as well. However, Tarantool has made some enhancements for fibers and has used fibers internally. So, although use of coroutines is possible and supported, use of fibers is recommended. A fiber is a set of instructions which are executed with cooperative multitasking. Fibers managed by the fiber module are associated with a user-supplied function called the *fiber function*. A fiber has three possible states: **running**, **suspended** or **dead**. When a fiber is created with :ref:`fiber.create() <fiber-create>`, it is running. When a fiber yields control with :ref:`fiber.sleep() <fiber-sleep>`, it is suspended. When a fiber ends (because the fiber function ends), it is dead. A list of strings or numbers. A method for parsing URIs is illustrated in :ref:`Cookbook recipes <cookbook-uri>`. A nil object A physical location where the nodes are closely connected, with the same security and backup and access points. The simplest example of a zone is a single computer with a single tarantool-server instance. A shard's replicas should be in different zones. A replica also binds to this port, and accepts connections, but these connections can only serve reads until the replica becomes a master. A runaway fiber can be stopped with :ref:`fiber_object.cancel <fiber_object-cancel>`. However, :ref:`fiber_object.cancel <fiber_object-cancel>` is advisory — it works only if the runaway fiber calls :ref:`fiber.testcancel() <fiber-testcancel>` occasionally. Most ``box.*`` functions, such as :ref:`box.space...delete() <box_space-delete>` or :ref:`box.space...update() <box_space-update>`, do call :ref:`fiber.testcancel() <fiber-testcancel>` but :ref:`box.space...select{} <box_space-select>` does not. In practice, a runaway fiber can only become unresponsive if it does many computations and does not check whether it has been cancelled. A special use of ``console.start()`` is with :ref:`initialization files <index-init_label>`. Normally, if one starts the tarantool server with :samp:`tarantool {initialization file}` there is no console. This can be remedied by adding these lines at the end of the initialization file: A subset of the tuples in the database partitioned according to the value returned by the consistent hash function. Usually each shard is on a separate node, or a separate set of nodes (for example if redundancy = 3 then the shard will be on three nodes). A table containing these fields: "host", "family", "type", "protocol", "port". A temporary list of recent update requests. Sometimes called "batching". Since updates to a sharded database can be slow, it may speed up throughput to send requests to a queue rather than wait for the update to finish on ever node. The shard module has functions for adding requests to the queue, which it will process without further intervention. Queuing is optional. A typical value is 3301. The listen parameter may also be set for local hot standby. A value comparable to Lua "nil" which may be useful as a placeholder in a tuple. Accept a new client connection and create a new connected socket. It is good practice to set the socket's blocking mode explicitly after accepting. Add the given string to the server's :ref:`Process title <administration-proctitle>` (what’s shown in the COMMAND column for :samp:`ps -ef` and :samp:`top -c` commands). Additionally one can see the uptime and the server version and the process id. Those information items can also be accessed with :ref:`box.info <box_introspection-box_info>` but use of the tarantool module is recommended. After :ref:`sleeping <fiber-sleep>` for two seconds, when the task has had time to do its iterations through the spaces, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 0". After sleeping for two more seconds, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 1". This shows that the is_tuple_expired() function eventually returned "true" for one of the tuples, because its timestamp field was more than three seconds old. After ``message_content, message_sender = recvfrom(1)`` the value of ``message_content`` might be a string containing 'X' and the value of ``message_sender`` might be a table containing All ``net.box`` methods are fiber-safe, that is, it is safe to share and use the same connection object across multiple concurrent fibers. In fact, it's perhaps the best programming practice with Tarantool. When multiple fibers use the same connection, all requests are pipelined through the same network socket, but each fiber gets back a correct response. Reducing the number of active sockets lowers the overhead of system calls and increases the overall server performance. There are, however, cases when a single connection is not enough — for example when it's necessary to prioritize requests or to use different authentication ids. All fibers are part of the fiber registry. This registry can be searched with :ref:`fiber.find() <fiber-find>` - via fiber id (fid), which is a numeric identifier. All remote calls support execution timeouts. Using a wrapper object makes the remote connection API compatible with the local one, removing the need for a separate ``timeout`` argument, which the local version would ignore. Once a request is sent, it cannot be revoked from the remote server even if a timeout expires: the timeout expiration only aborts the wait for the remote server response, not the request itself. Also, some MsgPack configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Also, some YAML configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. An error occurred during update of a field. An error occurred inside a Lua procedure. Another debugger example can be found here_. As well as executing Lua chunks or defining their own functions, you can exploit Tarantool's storage functionality with the ``box`` module and its submodules. At this point it is a good idea to check that the installation produced a file named ``driver.so``, and to check that this file is on a directory that is searched by the ``require`` request. At this point, if the above explanation is worthwhile, it's clear that ``expirationd.lua`` starts a background routine (fiber) which iterates through all the tuples in a space, sleeps cooperatively so that other fibers can operate at the same time, and - whenever it finds a tuple that has expired - deletes it from this space. Now the "``expirationd_run_task()``" function can be used in a test which creates sample data, lets the daemon run for a while, and prints results. Backtrace -- show the stack (in red), with program/function names and line numbers of whatever has been invoked to reach the current line. Basic parameters Begin by installing luarocks and making sure that tarantool is among the upstream servers, as in the instructions on `rocks.tarantool.org`_, the Tarantool luarocks page. Now execute this: Begin by making a ``require`` request for the mysql driver. We will assume that the name is ``mysql`` in further examples. Begin by making a ``require`` request for the pg driver. We will assume that the name is ``pg`` in further examples. Binary logging and snapshots Bind a socket to the given host/port. A UDP socket after binding can be used to receive data (see :ref:`socket_object.recvfrom <socket-recvfrom>`). A TCP socket can be used to accept new connections, after it has been put in listen mode. Built-in library reference But if the configuration parameters include ``custom_proc_title='sessions'`` then the output looks like: By default strict mode is off, unless tarantool was built with the ``-DCMAKE_BUILD_TYPE=Debug`` option -- see the description of build options in section :ref:`building-from-source <building_from_source>`. By default, the log is sent to the standard error stream (``stderr``). If ``logger`` is specified, the log is sent to a file, or to a pipe, or to the system logger. By saying ``require('tarantool')``, one can answer some questions about how the tarantool server was built, such as "what flags were used", or "what was the version of the compiler". By setting log_level, one can enable logging of all classes below or equal to the given level. Tarantool prints its logs to the standard error stream by default, but this can be changed with the :ref:`logger <cfg_logging-logger>` configuration parameter. CSV-table has 3 fields, field#2 has "," so result has quote marks Call ``fiber.channel()`` to allocate space and get a channel object, which will be called channel for examples in this section. Call the other ``fiber-ipc`` routines, via channel, to send messages, receive messages, or check ipc status. Message exchange is synchronous. The channel is garbage collected when no one is using it, as with any other Lua object. Use object-oriented syntax, for example ``channel:put(message)`` rather than ``fiber.channel.put(message)``. Call ``require('net.box')`` to get a ``net.box`` object, which will be called ``net_box`` for examples in this section. Call ``net_box.new()`` to connect and get a connection object, which will be called ``conn`` for examples in this section. Call the other ``net.box()`` routines, passing ``conn:``, to execute requests on the remote box. Call :ref:`conn:close <socket-close>` to disconnect. Can't modify data on a replication slave. Cancel a fiber. Running and suspended fibers can be cancelled. After a fiber has been cancelled, attempts to operate on it will cause errors, for example :ref:`fiber_object:id() <fiber_object-id>` will cause ``error: the fiber is dead``. Change the fiber name. By default the Tarantool server's interactive-mode fiber is named 'interactive' and new fibers created due to :ref:`fiber.create <fiber-create>` are named 'lua'. Giving fibers distinct names makes it easier to distinguish them when using :ref:`fiber.info <fiber-info>`. Change the size of an open file. Differs from ``fio.truncate``, which changes the size of a closed file. Check if the current fiber has been cancelled and throw an exception if this is the case. Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line: Check whether the first argument equals the second argument. Displays extensive message if the result is false. Check whether the specified channel is empty (has no messages). Check whether the specified channel is empty and has readers waiting for a message (because they have issued ``channel:get()`` and then blocked). Check whether the specified channel is full and has writers waiting (because they have issued ``channel:put()`` and then blocked due to lack of room). Check whether the specified channel is full. Checks the number of tests performed. This check should only be done after all planned tests are complete, so ordinarily ``taptest:check()`` will only appear at the end of a script. Clears the record of errors, so functions like `box.error()` or `box.error.last()` will have no effect. Close (destroy) a socket. A closed socket should not be used any more. A socket is closed automatically when its userdata is garbage collected by Lua. Close a connection. Close a file that was opened with ``fio.open``. For details type "man 2 close". Close the channel. All waiters in the channel will be woken up. All following ``channel:put()`` or ``channel:get()`` operations will return an error (``nil``). Closing connection Command options Commas designate end-of-field, Common file manipulations Common pathname manipulations Concatenate partial string, separated by '/' to form a path name. Configuration parameters Configuration parameters have the form: |br| :extsamp:`{**{box.cfg}**}{[{*{key = value}*} [, {*{key = value ...}*}]]}` Configuration reference Configuration settings Configure tarantool and load mysql module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configure tarantool and load pg module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configuring the storage Connect a socket to a remote host. Connect an existing socket to a remote host. The argument values are the same as in tcp_connect(). The host must be an IP address. Connect to the server at :ref:`URI <index-uri>`, change the prompt from ':samp:`tarantool>`' to ':samp:`{uri}>`', and act henceforth as a client until the user ends the session or types :code:`control-D`. Connecting Connection objects are garbage collected just like any other objects in Lua, so an explicit destruction is not mandatory. However, since close() is a system call, it is good programming practice to close a connection explicitly when it is no longer needed, to avoid lengthy stalls of the garbage collector. Continue till next breakpoint or till program ends. Convert a JSON string to a Lua object. Convert a Lua object to a JSON string. Convert a Lua object to a MsgPack string. Convert a Lua object to a YAML string. Convert a MsgPack string to a Lua object. Convert a YAML string to a Lua object. Convert a string or a Lua number to a 64-bit integer. The result can be used in arithmetic, and the arithmetic will be 64-bit integer arithmetic rather than floating-point arithmetic. (Operations on an unconverted Lua number use floating-point arithmetic.) The ``tonumber64()`` function is added by Tarantool; the name is global. Counterpart to ``pickle.pack()``. Warning: if format specifier 'A' is used, it must be the last item. Create a Lua function that will connect to the MySQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a Lua function that will connect to the PostgreSQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a new TCP or UDP socket. The argument values are the same as in the `Linux socket(2) man page <http://man7.org/linux/man-pages/man2/socket.2.html>`_. Create a new communication channel. Create a new connection. The connection is established on demand, at the time of the first request. It is re-established automatically after a disconnect. The returned ``conn`` object supports methods for making remote requests, such as select, update or delete. Create and start a fiber. The fiber is created and begins to run immediately. Create or delete a directory. For details type "man 2 mkdir" or "man 2 rmdir". Database error codes Debugger Commands Debugger prompts are blue, debugger hints and information are green, and the current line -- line 3 of example.lua -- is the default color. Now enter six debugger commands: Default values are: Deprecated. Do not use. Details are on `the shard section of github`_. Display a diagnostic message. Display a list of debugger commands. Display names and values of variables, for example the control variables of a Lua "for" statement. Display names of variables or functions which are defined as global. Display the fiber id, the program name, and the percentage of memory used, as a table. ER_FIBER_STACK ER_ILLEGAL_PARAMS ER_KEY_PART_COUNT ER_MEMORY_ISSUE ER_NONMASTER ER_NO_SUCH_INDEX ER_NO_SUCH_SPACE ER_PROC_LUA ER_TUPLE_FOUND ER_UPDATE_FIELD ER_WAL_IO Either: Emulate a request error, with text based on one of the pre-defined Tarantool errors defined in the file `errcode.h <https://github.com/tarantool/tarantool/blob/1.7/src/box/errcode.h>`_ in the source tree. Lua constants which correspond to those Tarantool errors are defined as members of ``box.error``, for example ``box.error.NO_SUCH_USER == 45``. Ensure that changes are written to disk. For details type "man 2 sync". Ensure that file changes are written to disk, for an open file. Compare ``fio.sync``, which is for all files. For details type "man 2 fsync" or "man 2 fdatasync". Enter evaluation mode. When the program is in evaluation mode, one can execute certain Lua statements that would be valid in the context. This is particularly useful for displaying the values of the program's variables. Other debugger commands will not work until one exits evaluation mode by typing :codebold:`-e`. Every data-access function in the box module has an analogue in the shard module, so (for example) to insert in table T in a sharded database one simply says ``shard.T:insert{...}`` instead of ``box.space.T:insert{...}``. A ``shard.T:select{}`` request without a primary key will search all shards. Every queued data-access function has an analogue in the shard module. The user must add an operation_id. The details of queued data-access functions, and of maintenance-related functions, are on `the shard section of github`_. Example Example Of Fiber Use Example Session Example setting: Example showing use of most of the net.box methods Example, creating a function which sets each option in a separate line: Example, using a table literal enclosed in {braces}: Example: Shard, Minimal Configuration Example: Shard, Scaling Out Example: shard.init syntax for one shard Example: shard.init syntax for three shards Execute a PING command. Execute a function, provided it has not been executed before. A passed value is checked to see whether the function has already been executed. If it has been executed before, nothing happens. If it has not been executed before, the function is invoked. For an explanation why ``box.once`` is useful, see the section :ref:`Preventing Duplicate Actions <index-preventing_duplicate_actions>`. Execute by passing to the shell. Execute these requests: Executing a statement Exit evaluation mode. Exit the program. If this is done on the server, then the server stops. Failed to write to disk. May mean: failed to record a change in the write-ahead log. Some sort of disk error. Fetch a message from a channel. If the channel is empty, ``channel:get()`` blocks until there is a message. Find out how many messages are on the channel. The answer is 0 if the channel is empty. First some terminology: Flags can be passed as a number or as string constants, for example '``O_RDONLY``', '``O_WRONLY``', '``O_RDWR``'. Flags can be combined by enclosing them in braces. For a commercial-grade example of a Lua rock that works with Tarantool, let us look at expirationd, which Tarantool supplies on GitHub_ with an Artistic license. The expirationd.lua program is lengthy (about 500 lines), so here we will only highlight the matters that will be enhanced by studying the full source later. For a list of available options, read `the source code of bc.lua`_. For a list of available options, read `the source code of dis_x64.lua`_. For a list of available options, read `the source code of dis_x86.lua`_. For a list of available options, read `the source code of dump.lua`_. For a list of available options, read `the source code of v.lua`_. For all MySQL statements, the request is: For all PostgreSQL statements, the request is: For all examples in this section the socket name will be sock and the function invocations will look like ``sock:function_name(...)``. For example, in Python, install the ``crcmod`` package and say: For example, ordinarily :samp:`ps -ef` shows the Tarantool server process thus: For example, the following code will interpret 0/0 (which is "not a number") and 1/0 (which is "infinity") as special values rather than nulls or errors: For example: For example: ``box.cfg{snapshot_period=3600}`` will cause the snapshot daemon to create a new database snapshot once per hour. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/mysql`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/pg`_. For more information on, read article about `Encryption Modes`_ For the local tarantool server there is a pre-created always-established connection object named :samp:`{net_box}.self`. Its purpose is to make polymorphic use of the ``net_box`` API easier. Therefore :samp:`conn = {net_box}.new('localhost:3301')` can be replaced by :samp:`conn = {net_box}.self`. However, there is an important difference between the embedded connection and a remote one. With the embedded connection, requests which do not modify data do not yield. When using a remote connection, due to :ref:`the implicit rules <atomic-the_implicit_yield_rules>` any request can yield, and database state may have changed by the time it regains control. For those who like to see things run, here are the exact steps to get expirationd through the test. Form a Lua iterator function for going through CSV records one field at a time. Use of an iterator is strongly recommended if the amount of data is large (ten or more megabytes). Four choices of block cipher modes are also available: From a user's point of view the MySQL and PostgreSQL rocks are very similar, so the following sections -- "MySQL Example" and "PostgreSQL Example" -- contain some redundancy. Function `box.once` Functions to create and delete links. For details type "man readlink", "man 2 link", "man 2 symlink", "man 2 unlink".. Get CSV-formatted input from ``readable`` and return a table as output. Usually ``readable`` is either a string or a file opened for reading. Usually :samp:`{options}` is not specified. Get ``expirationd.lua``. There are standard ways - it is after all part of a `standard rock <https://luarocks.org/modules/rtsisyk/expirationd>`_  - but for this purpose just copy the contents of expirationd.lua_ to a default directory. Get environment variable. Get socket flags. For a list of possible flags see ``sock:setsockopt()``. Get table input from ``csv-table`` and return a CSV-formatted string as output. Or, get table input from ``csv-table`` and put the output in ``writable``. Usually :samp:`{options}` is not specified. Usually ``writable``, if specified, is a file opened for writing. :ref:`csv.dump() <csv-dump>` is the reverse of :ref:`csv.load() <csv-load>`. Get the id of the fiber (fid), to be used in later displays. Getting the same results from digest and crypto modules Given a full path name, remove all but the final part (the file name). Also remove the suffix, if it is passed. Given a full path name, remove the final part (the file name). Go the site `github.com/tarantool/mysql`_. Follow the instructions there, saying: Go the site `github.com/tarantool/pg`_. Follow the instructions there, saying: Go to the next line, skipping over any function calls. Here are examples for all the common types, with the Lua-table representation on the left, with the MsgPack format name and encoding on the right. Here is an example of the tcp_server function, reading strings from the client and printing them. On the client side, the Linux socat utility will be used to ship a whole file for the tcp_server function to read. Here is an example with datagrams. Set up two connections on 127.0.0.1 (localhost): ``sock_1`` and ``sock_2``. Using ``sock_2``, send a message to ``sock_1``. Using ``sock_1``, receive a message. Display the received message. Close both connections. |br| This is not a useful way for a computer to communicate with itself, but shows that the system works. How many log records to store in a single write-ahead log file. When this limit is reached, Tarantool creates another WAL file named :samp:`{<first-lsn-in-wal>}.xlog`. This can be useful for simple rsync-based backups. How much memory Tarantool allocates to actually store tuples, in gigabytes. When the limit is reached, INSERT or UPDATE requests begin failing with error :errcode:`ER_MEMORY_ISSUE`. While the server does not go beyond the defined limit to allocate tuples, there is additional memory used to store indexes and connection information. Depending on actual configuration and workload, Tarantool can consume up to 20% more than the limit set here. How to ping How verbose the logging is. There are six log verbosity classes: However, because not all platforms are alike, for this example the assumption is that the user must check that the appropriate PostgreSQL files are present and must explicitly state where they are when building the Tarantool/PostgreSQL driver. One can use ``find`` or ``whereis`` to see what directories PostgreSQL files are installed in. If ``logger_nonblock`` equals true, Tarantool does not block on the log file descriptor when it’s not ready for write, and drops the message instead. If :ref:`log_level <cfg_logging-log_level>` is high, and a lot of messages go to the log file, setting ``logger_nonblock`` to true may improve logging performance at the cost of some log messages getting lost. If a later user calls the ``password_check()`` function and enters the wrong password, the result is an error. If one cuts and pastes the above, then the result, showing only the requests and responses for shard.init and shard.tester, should look approximately like this: If one of the URIs is "self" -- that is, if one of the URIs is for the same server that :codenormal:`box.cfg{}` is being executed on -- then it is ignored. Thus it is possible to use the same replication_source specification on multiple servers. If one wishes to start an interactive session on the same terminal after initialization is complete, one can use :ref:`console.start() <console-start>`. If processing a request takes longer than the given value (in seconds), warn about it in the log. Has effect only if :ref:`log_level <cfg_logging-log_level>` is more than or equal to 4 (WARNING). If replication_source is not an empty string, the server is considered to be a Tarantool :ref:`replica <index-box_replication>`. The replica server will try to connect to the master which replication_source specifies with a :ref:`URI <index-uri>` (Universal Resource Identifier), for example :samp:`{konstantin}:{secret_password}@{tarantool.org}:{3301}`. If the Tarantool server at :samp:`uri` requires authentication, the connection might look something like: :code:`console.connect('admin:secretpassword@distanthost.com:3301')`. If the ``logger`` string has the prefix "syslog:", then the string is interpreted as a message for the `syslogd <http://www.rfc-base.org/txt/rfc-5424.txt>`_ program which normally is running in the background of any Unix-like platform. One can optionally specify an ``identity``, a ``facility``, or both. The ``identity`` is an arbitrary string, default value = ``tarantool``, which will be placed at the beginning of all messages. The facility is an abbreviation for the name of one of the `syslog <https://en.wikipedia.org/wiki/Syslog>`_ facilities, default value = ``user``, which tell syslogd where the message should go. If the command to start Tarantool includes :codeitalic:`lua-initialization-file`, then Tarantool begins by invoking the Lua program in the file, which by convention may have the name "``script.lua``". The Lua program may get further arguments from the command line or may use operating-system functions, such as ``getenv()``. The Lua program almost always begins by invoking ``box.cfg()``, if the database server will be used or if ports need to be opened. For example, suppose ``script.lua`` contains the lines If there is an error while reading a write-ahead log file (at server start or to relay to a replica), abort. If there is an error while reading the snapshot file (at server start), abort. If there is more than one replication source in a cluster, specify an array of URIs, for example |br| :codenormal:`box.cfg{replication_source = {`:codeitalic:`uri#1,uri#2`:codenormal:`}}` |br| If timeout is provided, and the channel doesn't become empty for the duration of the timeout, ``channel:put()`` returns false. Otherwise it returns true. Illegal parameters. Malformed protocol message. In Perl, install the ``Digest::CRC`` module and run the following code: In certain circumstances a Unix domain socket may be used where a URI is expected, for example "unix/:/tmp/unix_domain_socket.sock" or simply "/tmp/unix_domain_socket.sock". In the current version of the binary protocol, error message, which is normally more descriptive than error code, is not present in server response. The actual message may contain a file name, a detailed reason or operating system error code. All such messages, however, are logged in the error log. Below follow only general descriptions of some popular codes. A complete list of errors can be found in file `errcode.h`_ in the source tree. In the following example, the user creates two functions, ``password_insert()`` which inserts a SHA-1_ digest of the word "**^S^e^c^ret Wordpass**" into a tuple set, and ``password_check()`` which requires input of a password. In this example a connection is made over the internet between the Tarantool server and tarantool.org, then an HTTP "head" message is sent, and a response is received: "``HTTP/1.1 200 OK``". This is not a useful way to communicate with this particular site, but shows that the system works. In this example: Incremental methods in the crypto module Incremental methods in the digest module Indicate how many tests will be performed. Initialization file Initialize. Initiates incremental MurmurHash. See :ref:`incremental methods <digest-incremental_digests>` notes. Initiates incremental crc32. See :ref:`incremental methods <digest-incremental_digests>` notes. Installation It is not supplied as part of the Tarantool repository; it must be installed separately. Here is the usual way: It will be necessary to install Tarantool's MySQL driver shared library, load it, and use it to connect to a MySQL server. After that, one can pass any MySQL statement to the server and receive results, including multiple result sets. It will be necessary to install Tarantool's PostgreSQL driver shared library, load it, and use it to connect to a PostgreSQL server. After that, one can pass any PostgreSQL statement to the server and receive results. Key part count is not the same as index part count Leading or trailing spaces are ignored, Like all Lua objects, dead fibers are garbage collected. The garbage collector frees pool allocator memory owned by the fiber, resets all fiber data, and returns the fiber (now called a fiber carcass) to the fiber pool. The carcass can be reused when another fiber is created. Line feeds, or line feeds plus carriage returns, designate end-of-record, Listen on :ref:`URI <index-uri>`. The primary way of listening for incoming requests is via the connection-information string, or URI, specified in :code:`box.cfg{listen=...}`. The alternative way of listening is via the URI specified in :code:`console.listen(...)`. This alternative way is called "administrative" or simply :ref:`"admin port" <administration-admin_ports>`. The listening is usually over a local host with a Unix domain socket. Local storage within the fiber. The storage can contain any number of named values, subject to memory limitations. Naming may be done with :samp:`{fiber_object}.storage.{name}` or :samp:`{fiber_object}.storage['{name}'].` or with a number :samp:`{fiber_object}.storage[{number}]`. Values may be either numbers or strings. The storage is garbage-collected when :samp:`{fiber_object}:cancel()` happens. Locate a fiber by its numeric id and cancel it. In other words, :ref:`fiber.kill() <fiber-kill>` combines :ref:`fiber.find() <fiber-find>` and :ref:`fiber_object:cancel() <fiber_object-cancel>`. Logging Lua `escape sequences`_ such as \\n or \\10 are legal within strings but not within files, Lua code Lua fun, also known as the Lua Functional Library, takes advantage of the features of LuaJIT to help users create complex functions. Inside the module are "sequence processors" such as map, filter, reduce, zip -- they take a user-written function as an argument and run it against every element in a sequence, which can be faster or more convenient than a user-written loop. Inside the module are "generators" such as range, tabulate, and rands -- they return a bounded or boundless series of values. Within the module are "reducers", "filters", "composers" ... or, in short, all the important features found in languages like Standard ML, Haskell, or Erlang. Lua iterator function Make a fiber, associate function_x with the fiber, and start function_x. It will immediately "detach" so it will be running independently of the caller. Make the function which will be associated with the fiber. This function contains an infinite loop (``while 0 == 0`` is always true). Each iteration of the loop adds 1 to a global variable named gvar, then goes to sleep for 2 seconds. The sleep causes an implicit :ref:`fiber.yield() <fiber-yield>`. Manage the rights to file objects, or ownership of file objects. For details type "man 2 chown" or "man 2 chmod". Miscellaneous Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits are significant if flags include `O_CREATE` or `O_TMPFILE`. Mode bits can be combined by enclosing them in braces. Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits can be combined by enclosing them in braces. Module `box` Module `clock` Module `console` Module `crypto` Module `csv` Module `digest` Module `expirationd` Module `fiber` Module `fio` Module `fun` Module `jit` Module `json` Module `log` Module `msgpack` Module `net.box` Module `os` Module `pickle` Module `shard` Module `socket` Module `strict` Module `tap` Module `tarantool` Module `tdb` Module `uuid` Module `yaml` Most configuration parameters are for allocating resources, opening ports, and specifying database behavior. All parameters are optional. A few parameters are dynamic, that is, they can be changed at runtime by calling ``box.cfg{}`` a second time. MySQL Example N Names Networking Now start Tarantool, using example.lua as the initialization file Now watch what happens on the first shell. The strings "A", "B", "C" are printed. Now, for the MySQL driver shared library, there are two ways to install: Now, for the PostgreSQL driver shared library, there are two ways to install: Now, say: Number of seconds between periodic scans of the write-ahead-log file directory, when checking for changes to write-ahead-log files for the sake of replication or local hot standby. Observe the result. It contains "MySQL row". So this is the row that was inserted into the MySQL database. And now it's been selected with the Tarantool client. Observe the result. It contains "PostgreSQL row". So this is the row that was inserted into the PostgreSQL database. And now it's been selected with the Tarantool client. Of course, expirationd can be customized to do different things by passing different parameters, which will be evident after looking in more detail at the source code. On Linux the listen ``backlog`` backlog may be from /proc/sys/net/core/somaxconn, on BSD the backlog may be ``SOMAXCONN``. On Terminal #1, say: On Terminal #1: put a message "Log Line #2" in the log file. |br| On Terminal #1: put a message "Log Line #3" in the log file. On Terminal #1: start an interactive Tarantool session, then say the logging will go to `Log_file`, then put a message "Log Line #1" in the log file: On Terminal #2, say: On Terminal #2: use ``kill -HUP`` to send a SIGHUP signal to the Tarantool server. The result of this is: Tarantool will open `Log_file` again, and the next log message will go to `Log_file`. (The same effect could be accomplished by executing log.rotate() on the server.) |br| On Terminal #2: use ``less`` to examine files. `Log_file.bak` will have these lines, except that the date and time will depend on when the example is done: On Terminal #2: use ``mv`` so the log file is now named `Log_file.bak`. The result of this is: the next log message will go to `Log_file.bak`. |br| On Terminal #2: use ``ps`` to find the process ID of the Tarantool server. |br| On the first shell, start Tarantool and say: On the second shell, create a file that contains a few lines. The contents don't matter. Suppose the first line contains A, the second line contains B, the third line contains C. Call this file "tmp.txt". On the second shell, use the socat utility to ship the tmp.txt file to the server's host and port: Open a file in preparation for reading or writing or seeking. Or, download from github tarantool/shard and compile as described in the README. Then, before using the module, say ``shard = require('shard')`` Or: Out of memory: :ref:`slab_alloc_arena <cfg_storage-slab_alloc_arena>` limit has been reached. Output a user-generated message to the :ref:`log file <cfg_logging-logger>`, given log_level_function_name = ``error`` or ``warn`` or ``info`` or ``debug``. Parameters: Parameters: (string) format-string = instructions; (string) time-since-epoch = number of seconds since 1970-01-01. If time-since-epoch is omitted, it is assumed to be the current time. Parameters: (string) name = name of file or directory which will be removed. Parameters: (string) variable-name = environment variable name. Parse and execute an arbitrary chunk of Lua code. This function is mainly useful to define and run Lua code without having to introduce changes to the global Lua environment. Pass or return a cipher derived from the string, key, and (optionally, sometimes) initialization vector. The four choices of algorithms: Pass or return a digest derived from the string. The twelve choices of algorithms: Pause for a while, while the detached function runs. Then ... Cancel the fiber. Then, once again ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit more depending how long the pause lasted). This time the status is dead because the cancel worked. Pause for a while, while the detached function runs. Then ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit depending how long the pause lasted). The status is suspended because the fiber spends almost all its time sleeping or yielding. Perform non-random-access read or write on a file. For details type "man 2 read" or "man 2 write". Perform read/write random-access operation on a file, without affecting the current seek position of the file. For details type "man 2 pread" or "man 2 pwrite". Possible Errors: Redundancy should not be greater than the number of servers; the servers must be alive; two replicas of the same shard should not be in the same zone. Possible errors: If there is a compilation error, it is raised as a Lua error. Possible errors: On error, returns an empty string, followed by status, errno, errstr. In case the writing side has closed its end, returns the remainder read from the socket (possibly an empty string), followed by "eof" status. Possible errors: Returns nil, status, errno, errstr on error. Possible errors: cancel is not permitted for the specified fiber object. Possible errors: nil on error. Possible errors: nil. Possible errors: on error, returns status, errno, errstr. Possible errors: the connection will fail if the target Tarantool server was not initiated with :code:`box.cfg{listen=...}`. Possible errors: unknown format specifier. Possible values for ``facility`` are: auth, authpriv, cron, daemon, ftp, kern, lpr, mail, news, security, syslog, user, uucp, local0, local1, local2, local3, local4, local5, local6, local7. PostgreSQL Example Print an annotated list of all available options and exit. Print product name and version, for example: Prints a trace of LuaJIT's progress compiling and interpreting code Prints the byte code of a function. Prints the i386 assembler code of a string of bytes Prints the intermediate or machine code of following Lua code Prints the x86-64 assembler code of a string of bytes Purposes Put the following program in a default directory and call it "example.lua": Put the server in read-only mode. After this, any requests that try to change data will fail with error ER_READONLY. Quit immediately. Quote marks may enclose fields or parts of fields, Read ``size`` bytes from a connected socket. An internal read-ahead buffer is used to reduce the cost of this call. Read from a connected socket until some condition is true, and return the bytes that were read. Reading goes on until ``limit`` bytes have been read, or a delimiter has been read, or a timeout has expired. Readable file :file:`./file.csv` contains two CSV records. Explanation of fio is in section :ref:`fio <fio-section>`. Source CSV file and example respectively: Readable string contains 2-byte character = Cyrillic Letter Palochka: (This displays a palochka if and only if character set = UTF-8.) Readable string has 3 fields, field#2 has comma and space so use quote marks: Receive a message on a UDP socket. Recursive version of ``taptest:is(...)``, which can be be used to compare tables as well as scalar values. Reduce file size to a specified value. For details type "man 2 truncate". Reduce the throttling effect of :ref:`box.snapshot <admin-snapshot>` on INSERT/UPDATE/DELETE performance by setting a limit on how many megabytes per second it can write to disk. The same can be achieved by splitting :ref:`wal_dir <cfg_basic-wal_dir>` and :ref:`snap_dir <cfg_basic-snap_dir>` locations and moving snapshots to a separate disk. Reference Remove file or directory. Rename a file or directory. Rename a file or directory. For details type "man 2 rename". Replication Retrieve information about the last error that occurred on a socket, if any. Errors do not cause throwing of exceptions so these functions are usually necessary. Return a formatted date. Return a list of files that match an input string. The list is constructed with a single flag that controls the behavior of the function: GLOB_NOESCAPE. For details type "man 3 glob". Return a name for a temporary file. Return all available data from the socket buffer if non-blocking. Rarely used. For details see `this description`_. Return information about all fibers. Return statistics about an open file. This differs from ``fio.stat`` which return statistics about a closed file. For details type "man 2 stat". Return the name of a directory that can be used to store temporary files. Return the name of the current working directory. Return the number of CPU seconds since the program start. Return the number of seconds since the epoch. Return the status of the current fiber. Return the status of the specified fiber. Returns 128-bit binary string = digest made with MD4. Returns 128-bit binary string = digest made with MD5. Returns 128-byte string = hexadecimal of a digest calculated with sha512. Returns 160-bit binary string = digest made with SHA-0.|br| Not recommended. Returns 160-bit binary string = digest made with SHA-1. Returns 224-bit binary string = digest made with SHA-2. Returns 256-bit binary string =  digest made with SHA-2. Returns 256-bit binary string = digest made with AES. Returns 32-bit binary string = digest made with MurmurHash. Returns 32-bit checksum made with CRC32. Returns 32-byte string = hexadecimal of a digest calculated with md4. Returns 32-byte string = hexadecimal of a digest calculated with md5. Returns 384-bit binary string =  digest made with SHA-2. Returns 40-byte string = hexadecimal of a digest calculated with sha. Returns 40-byte string = hexadecimal of a digest calculated with sha1. Returns 512-bit binary tring = digest made with SHA-2. Returns 56-byte string = hexadecimal of a digest calculated with sha224. Returns 64-byte string = hexadecimal of a digest calculated with sha256. Returns 96-byte string = hexadecimal of a digest calculated with sha384. Returns a description of the last error, as a Lua table with five members: "line" (number) Tarantool source file line number, "code" (number) error's number, "type", (string) error's C++ class, "message" (string) error's message, "file" (string) Tarantool source file. Additionally, if the error is a system error (for example due to a failure in socket or file io), there may be a sixth member: "errno" (number) C standard error number. Returns a number made with consistent hash. Returns a regular string from a base64 encoding. Returns array of random bytes with length = integer. Returns base64 encoding from a regular string. Returns information about a file object. For details type "man 2 lstat" or "man 2 stat". Rocks reference Round Trip: from string to table and back to string Run the server as a background task. The :ref:`logger <cfg_logging-logger>` and :ref:`pid_file <cfg_basic-pid_file>` parameters must be non-null for this to work. SO_ACCEPTCONN SO_BINDTODEVICE SO_BROADCAST SO_DEBUG SO_DOMAIN SO_DONTROUTE SO_ERROR SO_KEEPALIVE SO_MARK SO_OOBINLINE SO_PASSCRED SO_PEERCRED SO_PRIORITY SO_PROTOCOL SO_RCVBUF SO_RCVBUFFORCE SO_RCVLOWAT SO_RCVTIMEO SO_REUSEADDR SO_SNDBUF SO_SNDBUFFORCE SO_SNDLOWAT SO_SNDTIMEO SO_TIMESTAMP SO_TYPE SQL DBMS Modules See also :ref:`Modules <modules>`. See also :ref:`box.session.storage <box_session-storage>`. See also :ref:`fiber.time64 <fiber-time64>` and :ref:`os.clock() <os-clock>`. Semicolon instead of comma for the delimiter: Send a message on a UDP socket to a specified host. Send a message using a channel. If the channel is full, ``channel:put()`` blocks until there is a free slot in the channel. Send data over a connected socket. Serializing 'A' and 'B' with different ``__serialize`` values causes different results. To show this, here is a routine which encodes `{'A','B'}` both as an array and as a map, then displays each result in hexadecimal. Serializing 'A' and 'B' with different ``__serialize`` values causes different results: Set or clear the SO_LINGER flag. For a description of the flag, see the `Linux man page <http://man7.org/linux/man-pages/man1/loginctl.1.html>`_. Set socket flags. The argument values are the same as in the `Linux getsockopt(2) man page <http://man7.org/linux/man-pages/man2/setsockopt.2.html>`_. The ones that Tarantool accepts are: Set the auto-completion flag. If auto-completion is `true`, and the user is using tarantool as a client, then hitting the TAB key may cause tarantool to complete a word automatically. The default auto-completion value is `true`. Set the mask bits used when creating files or directories. For a detailed description type "man 2 umask". Setting SO_LINGER is done with ``sock:linger(active)``. Shift position in the file to the specified position. For details type "man 2 seek". Show whether connection is active or closed. Shutdown a reading end, a writing end, or both ends of a socket. Since ``box.cfg`` may contain many configuration parameters and since some of the parameters (such as directory addresses) are semi-permanent, it's best to keep ``box.cfg`` in a Lua file. Typically this Lua file is the initialization file which is specified on the tarantool command line. Size of the largest allocation unit. It can be increased if it is necessary to store large tuples. Size of the smallest allocation unit. It can be decreased if most of the tuples are very small. The value must be between 8 and 1048280 inclusive. Snapshot daemon Some configuration parameters and some functions depend on a URI, or "Universal Resource Identifier". The URI string format is similar to the `generic syntax for a URI schema <http://en.wikipedia.org/wiki/URI_scheme#Generic_syntax>`_. So it may contain (in order) a user name for login, a password, a host name or host IP address, and a port number. Only the port number is always mandatory. The password is mandatory if the user name is specified, unless the user name is 'guest'. So, formally, the URI syntax is ``[host:]port`` or ``[username:password@]host:port``. If host is omitted, then '0.0.0.0' or '[::]' is assumed, meaning respectively any IPv4 address or any IPv6 address, on the local machine. If username:password is omitted, then 'guest' is assumed. Some examples: Some functions in these modules are analogs to functions from `standard Lua libraries <http://www.lua.org/manual/>`_. For better results, we recommend using functions from Tarantool's built-in modules. Specify fiber-WAL-disk synchronization mode as: Start listening for incoming connections. Start the Tarantool server as described before. Start the console on the current interactive terminal. Start two shells. The first shell will be the server. The second shell will be the client. Start with two terminal shells, Terminal #1 and Terminal #2. Store the process id in this file. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. A typical value is “:file:`tarantool.pid`”. Submodule `box.error` Submodule `fiber-ipc` Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports for all crypto functions.. Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports with crc32 and with murmur... TAP version 13
1..2
ok - 2 * 2 is 4
    # Some subtests for test2
    1..2
    ok - 2 + 2 is 4,
    ok - 2 + 3 is not 4
    # Some subtests for test2: end
ok - some subtests for test2 Tarantool is started by entering the following command: Tarantool supplies DBMS connector modules with the module manager for Lua, LuaRocks. So the connector modules may be called "rocks". Tarantool supports file input/output with an API that is similar to POSIX syscalls. All operations are performed asynchronously. Multiple fibers can access the same file simultaneously. Tarantool uses `git describe <http://www.kernel.org/pub/software/scm/git/docs/git-describe.html>`_ to produce its version id, and this id can be used at any time to check out the corresponding source from our `git repository <http://github.com/tarantool/tarantool.git>`_. Test whether a value has a particular type. Displays a long message if the value is not of the specified type. The "admin" address is the URI to listen on. It has no default value, so it must be specified if connections will occur via an admin port. The parameter is expressed with URI = Universal Resource Identifier format, for example "/tmpdir/unix_domain_socket.sock", or a numeric TCP port. Connections are often made with telnet. A typical port value is 3313. The "for" instruction can be translated as "iterate through the index of the space that is being scanned", and within it, if the tuple is "expired" (for example, if the tuple has a timestamp field which is less than the current time), process the tuple as an expired tuple. The 'Error' line is visible in tarantool.txt preceded by the letter E. The 'Info' line is not present because the log_level is 3. The 3-number version follows the standard ``<major>-<minor>-<patch>`` scheme, in which ``<major>`` number is changed only rarely, ``<minor>`` is incremented for each new milestone and indicates possible incompatible changes, and ``<patch>`` stands for the number of bug fix releases made after the start of the milestone. For non-released versions only, there may be a commit number and commit SHA1 to indicate how much this particular build has diverged from the last release. The :code:`strict` module has functions for turning "strict mode" on or off. When strict mode is on, an attempt to use an undeclared global variable will cause an error. A global variable is considered "undeclared" if it has never had a value assigned to it. Often this is an indication of a programming error. The :ref:`snapshot_period <cfg_snapshot_daemon-snapshot_period>` and :ref:`snapshot_count <cfg_snapshot_daemon-snapshot_count>` configuration settings determine how long the intervals are, and how many snapshots should exist before removals occur. The JSON output structure can be specified with ``__serialize``: The MsgPack Specification_ page explains that the first encoding means: The MsgPack output structure can be specified with ``__serialize``: The Tarantool Debugger (abbreviation = tdb) can be used with any Lua program. The operational features include: setting breakpoints, examining variables, going forward one line at a time, backtracing, and showing information about fibers. The display features include: using different colors for different situations, including line numbers, and adding hints. The Tarantool rocks allow for connecting to an SQL server and executing SQL statements the same way that a MySQL or PostgreSQL client does. The SQL statements are visible as Lua methods. Thus Tarantool can serve as a "MySQL Lua Connector" or "PostgreSQL Lua Connector", which would be useful even if that was all Tarantool could do. But of course Tarantool is also a DBMS, so the module also is useful for any operations, such as database copying and accelerating, which work best when the application can work on both SQL and Tarantool inside the same Lua routine. The methods for connect/select/insert/etc. are similar to the ones in the :ref:`net.box <net_box-module>` module. The Tarantool server puts all diagnostic messages in a log file specified by the :ref:`logger <cfg_logging-logger>` configuration parameter. Diagnostic messages may be either system-generated by the server's internal code, or user-generated with the ``log.log_level_function_name`` function. The Tarantool shard module has facilities for creating shards, as well as analogues for the data-manipulation functions of the box library (select, insert, replace, update, delete). The `YAML collection style <http://yaml.org/spec/1.1/#id930798>`_ can be specified with ``__serialize``: The ``box.error`` function is for raising an error. The difference between this function and Lua's built-in ``error()`` function is that when the error reaches the client, its error code is preserved. In contrast, a Lua error would always be presented to the client as :errcode:`ER_PROC_LUA`. The ``clock`` module returns time values derived from the Posix / C CLOCK_GETTIME_ function or equivalent. Most functions in the module return a number of seconds; functions whose names end in "64" return a 64-bit number of nanoseconds. The ``facility`` setting is currently ignored but will be used in the future. The ``fiber-ipc`` submodule allows sending and receiving messages between different processes. The words "different processes" in this context mean different connections, different sessions, or different fibers. The ``fiber`` module allows for creating, running and managing *fibers*. The ``jit`` module has functions for tracing the LuaJIT Just-In-Time compiler's progress, showing the byte-code or assembler output that the compiler produces, and in general providing information about what LuaJIT does with Lua code. The ``msgpack`` module takes strings in MsgPack_ format and decodes them, or takes a series of non-MsgPack values and encodes them. The ``net.box`` module contains connectors to remote database systems. One variant, to be discussed later, is for connecting to MySQL or MariaDB or PostgreSQL — that variant is the subject of the :ref:`SQL DBMS modules <dbms_modules>` appendix. In this section the subject is the built-in variant, ``net.box``. This is for connecting to tarantool servers via a network. The ``sock:name()`` function is used to get information about the near side of the connection. If a socket was bound to ``xyz.com:45``, then ``sock:name`` will return information about ``[host:xyz.com, port:45]``. The equivalent POSIX function is ``getsockname()``. The ``sock:peer()`` function is used to get information about the far side of a connection. If a TCP connection has been made to a distant host ``tarantool.org:80``, ``sock:peer()`` will return information about ``[host:tarantool.org, port:80]``. The equivalent POSIX function is ``getpeername()``. The ``socket.getaddrinfo()`` function is useful for finding information about a remote site so that the correct arguments for ``sock:sysconnect()`` can be passed. The ``socket.tcp_server()`` function makes Tarantool act as a server that can accept connections. Usually the same objective is accomplished with ``box.cfg{listen=...)``. The ``socket`` module allows exchanging data via BSD sockets with a local or remote host in connection-oriented (TCP) or datagram-oriented (UDP) mode. Semantics of the calls in the ``socket`` API closely follow semantics of the corresponding POSIX calls. Function names and signatures are mostly compatible with `luasocket`_. The ``yaml`` module takes strings in YAML_ format and decodes them, or takes a series of non-YAML values and encodes them. The above code means: use `tcp_server()` to wait for a connection from any host on port 3302. When it happens, enter a loop that reads on the socket and prints what it reads. The "delimiter" for the read function is "\\n" so each `read()` will read a string as far as the next line feed, including the line feed. The actual output will be a line containing the current timestamp, a module name, 'E' or 'W' or 'I' or 'D' or 'R' depending on ``log_level_function_name``, and ``message``. Output will not occur if ``log_level_function_name`` is for a type greater than :ref:`log_level <cfg_logging-log_level>`. Messages may contain C-style format specifiers %d or %s, so :samp:`log.error('...%d...%s',{x},{y})` will work if x is a number and y is a string. The all-zero UUID value can be expressed as uuid.NULL, or as ``uuid.fromstr('00000000-0000-0000-0000-000000000000')``. The comparison with an all-zero value can also be expressed as ``uuid_with_type_cdata == uuid.NULL``. The connection-options parameter is a table. Possible options are: The console module allows one Tarantool server to access another Tarantool server, and allows one Tarantool server to start listening on an :ref:`admin port <administration-admin_ports>`. The console.connect function allows one Tarantool server, in interactive mode, to access another Tarantool server. Subsequent requests will appear to be handled locally, but in reality the requests are being sent to the remote server and the local server is acting as a client. Once connection is successful, the prompt will change and subsequent requests are sent to, and executed on, the remote server. Results are displayed on the local server. To return to local mode, enter :code:`control-D`. The contents of the ``box`` library can be inspected at runtime with ``box``, with no arguments. The submodules inside the box library are: ``box.schema``, ``box.tuple``, ``box.space``, ``box.index``, ``box.cfg``, ``box.info``, ``box.slab``, ``box.stat``. Every submodule contains one or more Lua functions. A few submodules contain members as well as functions. The functions allow data definition (create alter drop), data manipulation (insert delete update upsert select replace), and introspection (inspecting contents of spaces, accessing server configuration). The crc32 and crc32_update functions use the `CRC-32C (Castagnoli)`_ polynomial value: ``0x1EDC6F41`` / ``4812730177``. If it is necessary to be compatible with other checksum functions in other programming languages, ensure that the other functions use the same polynomial value. The csv module handles records formatted according to Comma-Separated-Values (CSV) rules. The database-specific requests (``cfg``, :ref:`space.create <box_schema-space_create>`, :ref:`create_index <box_space-create_index>`) should already be familiar. The default formatting rules are: The default user name is ‘guest’. A replica server does not accept data-change requests on the :ref:`listen <cfg_basic-listen>` port. The replication_source parameter is dynamic, that is, to enter master mode, simply set replication_source to an empty string and issue :code:`box.cfg{replication_source=`:samp:`{new-value}`:code:`}`. The default vinyl configuration can be changed with The discussion here in the reference is about incorporating and using two modules that have already been created: the "SQL DBMS rocks" for MySQL and PostgreSQL. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of MySQL had been installed on ~/mysql-5.5. The mysqld server is already running on the local host 127.0.0.1. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of PostgreSQL had been installed on /usr. The PostgreSQL server is already running on the local host 127.0.0.1. The following functions are equivalent. For example, the ``digest`` function and the ``crypto`` function will both produce the same result. The following sections describe all parameters for basic operation, for storage, for binary logging and snapshots, for replication, for networking, and for logging. The full documentation is `On the luafun section of github`_. However, the first chapter can be skipped because installation is already done, it's inside Tarantool. All that is needed is the usual :code:`require` request. After that, all the operations described in the Lua fun manual will work, provided they are preceded by the name returned by the :code:`require` request. For example: The function that can determine whether a UUID is an all-zero value is: The function which will be supplied to expirationd is :codenormal:`is_tuple_expired`, which is saying "if the second field of the tuple is less than the :ref:`current time <fiber-time>`  , then return true, otherwise return false". The functions for setting up and connecting are ``socket``, ``sysconnect``, ``tcp_connect``. The functions for sending data are ``send``, ``sendto``, ``write``, ``syswrite``. The functions for receiving data are ``recv``, ``recvfrom``, ``read``. The functions for waiting before sending/receiving data are ``wait``, ``readable``, ``writable``. The functions for setting flags are ``nonblock``, ``setsockopt``. The functions for stopping and disconnecting are ``shutdown``, ``close``. The functions for error checking are ``errno``, ``error``. The functions in digest are: The functions that can convert between different types of UUID are: The functions that can return a UUID are: The guava function uses the `Consistent Hashing`_ algorithm of the Google guava library. The first parameter should be a hash code; the second parameter should be the number of buckets; the returned value will be an integer between 0 and the number of buckets. For example, The interval between actions by the snapshot daemon, in seconds. If ``snapshot_period`` is set to a value greater than zero, and there is activity which causes change to a database, then the snapshot daemon will call :ref:`box.snapshot <admin-snapshot>` every ``snapshot_period`` seconds, creating a new snapshot file each time. The json module provides JSON manipulation routines. It is based on the `Lua-CJSON module by Mark Pulford`_. For a complete manual on Lua-CJSON please read `the official documentation`_. The key for getting the rock rolling is ``expd = require('expirationd')``. The "``require``" function is what reads in the program; it will appear in many later examples in this manual, when it's necessary to get a module that's not part of the Tarantool kernel. After the Lua variable expd has been assigned the value of the expirationd module, it's possible to invoke the module's ``run_task()`` function. The maximum number of snapshots that may exist on the snap_dir directory before the snapshot daemon will remove old snapshots. If snapshot_count equals zero, then the snapshot daemon does not remove old snapshots. For example: The monotonic time. Derived from C function clock_gettime(CLOCK_MONOTONIC). Monotonic time is similar to wall clock time but is not affected by changes to or from daylight saving time, or by changes done by a user. This is the best function to use with benchmarks that need to calculate elapsed time. The most important function is: The names are similar to the names that PostgreSQL itself uses. The number of replicas in each shard. The number of replicas per shard (redundancy) is 3. The number of servers is 3. The shard module will conclude that there is only one shard. The option names, except for `raise`, are similar to the names that MySQL's mysql client uses, for details see the MySQL manual at `dev.mysql.com/doc/refman/5.6/en/connecting.html`_. The `raise` option should be set to :codenormal:`true` if errors should be raised when encountered. To connect with a Unix socket rather than with TCP, specify ``host = 'unix/'`` and :samp:`port = {socket-name}`. The os module contains the functions :ref:`execute() <os-execute>`, :ref:`rename() <os-rename>`, :ref:`getenv() <os-getenv>`, :ref:`remove() <os-remove>`, :ref:`date() <os-date>`, :ref:`exit() <os-exit>`, :ref:`time() <os-time>`, :ref:`clock() <os-clock>`, :ref:`tmpname() <os-tmpname>`. Most of these functions are described in the Lua manual Chapter 22 `The Operating System Library <https://www.lua.org/pil/contents.html#22>`_. The other potential problem comes from fibers which never get scheduled, because they are not subscribed to any events, or because no relevant events occur. Such morphing fibers can be killed with :ref:`fiber.kill() <fiber-kill>` at any time, since :ref:`fiber.kill() <fiber-kill>` sends an asynchronous wakeup event to the fiber, and :ref:`fiber.testcancel() <fiber-testcancel>` is checked whenever such a wakeup event occurs. The output from the above script will look approximately like this: The possible options which can be passed to csv functions are: The processor time. Derived from C function clock_gettime(CLOCK_PROCESS_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a CPU. The read/write data port number or :ref:`URI <index-uri>` (Universal Resource Identifier) string. Has no default value, so **must be specified** if connections will occur from remote clients that do not use the :ref:`“admin port” <administration-admin_ports>`. Connections made with :samp:`listen={URI}` are sometimes called "binary protocol" or "primary port" connections. The recursion limit was reached when creating a new fiber. This usually indicates that a stored procedure is recursively invoking itself too often. The result of ``tap.test`` is an object, which will be called taptest in the rest of this discussion, which is necessary for ``taptest:plan()`` and all the other methods. The result of the json.encode request will look like this: The same configuration settings exist for json, for :ref:`MsgPack <msgpack-module>`, and for :ref:`YAML <yaml-module>`. >>>>>>> Fix every NOTE to be highlighted on site + JSON cfg rewritten The screen should now look like this: The server will sleep for io_collect_interval seconds between iterations of the event loop. Can be used to reduce CPU load in deployments in which the number of client connections is large, but requests are not so frequent (for example, each connection issues just a handful of requests per second). The shard module distributes according to a hash algorithm, that is, it applies a hash function to a tuple's primary-key value in order to decide which shard the tuple belongs to. The hash function is `consistent`_ so that changing the number of servers will not affect results for many keys. The specific hash function that the shard module uses is :ref:`digest.guava <digest-guava>` in the :codeitalic:`digest` module. The shard package is distributed separately from the main tarantool package. To acquire it, do a separate install. For example on Ubuntu say: The size of the read-ahead buffer associated with a client connection. The larger the buffer, the more memory an active connection consumes and the more requests can be read from the operating system buffer in a single system call. The rule of thumb is to make sure the buffer can contain at least a few dozen requests. Therefore, if a typical tuple in a request is large, e.g. a few kilobytes or even megabytes, the read-ahead buffer size should be increased. If batched request processing is not used, it’s prudent to leave this setting at its default. The snapshot daemon is a fiber which is constantly running. At intervals, it may make new snapshot (.snap) files and then may remove old snapshot files. If the snapshot daemon removes an old snapshot file, it will also remove any write-ahead log (.xlog) files that are older than the snapshot file and contain information that is present in the snapshot file. The specified index in the specified space does not exist. The specified space does not exist. The tap module streamlines the testing of other modules. It allows writing of tests in the `TAP protocol`_. The results from the tests can be parsed by standard TAP-analyzers so they can be passed to utilities such as `prove`_. Thus one can run tests and then use the results for statistics, decision-making, and so on. The thread time. Derived from C function clock_gettime(CLOCK_THREAD_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a thread within a CPU. The time that a function takes within a processor. This function uses clock.proc(), therefore it calculates elapsed CPU time. Therefore it is not useful for showing actual elapsed time. The wall clock time. Derived from C function clock_gettime(CLOCK_REALTIME). This is the best function for knowing what the official time is, as determined by the system administrator. There are configuration settings which affect the way that Tarantool encodes invalid numbers or types. They are all boolean ``true``/``false`` values There are no restrictions on the types of requests that can be entered, except those which are due to privilege restrictions -- by default the login to the remote server is done with user name = 'guest'. The remote server could allow for this by granting at least one privilege: :code:`box.schema.user.grant('guest','execute','universe')`. There are two shards, and each shard contains one replica. This requires two nodes. In real life the two nodes would be two computers, but for this illustration the requirement is merely: start two shells, which we'll call Terminal#1 and Terminal #2. There is only one shard, and that shard contains only one replica. So this isn't illustrating the features of either replication or sharding, it's only illustrating what the syntax is, and what the messages look like, that anyone could duplicate in a minute or two with the magic of cut-and-paste. This describes three shards. Each shard has two replicas. Since the number of servers is 7, and the number of replicas per shard is 2, and dividing 7 / 2 leaves a remainder of 1, one of the servers will not be used. This is not necessarily an error, because perhaps one of the servers in the list is not alive. This example assumes that MySQL 5.5 or MySQL 5.6 or MySQL 5.7 has been installed. Recent MariaDB versions will also work, the MariaDB C connector is used. The package that matters most is the MySQL client developer package, typically named something like libmysqlclient-dev. The file that matters most from this package is libmysqlclient.so or a similar name. One can use ``find`` or ``whereis`` to see what directories these files are installed in. This example assumes that PostgreSQL 8 or PostgreSQL 9 has been installed. More recent versions should also work. The package that matters most is the PostgreSQL developer package, typically named something like libpq-dev. On Ubuntu this can be installed with: This example should give a rough idea of what some functions for fibers should look like. It's assumed that the functions would be referenced in :ref:`fiber.create() <fiber-create>`. This example will work with the sandbox configuration described in the preface. That is, there is a space named tester with a numeric primary key. Assume that the database is nearly empty. Assume that the tarantool server is running on ``localhost 127.0.0.1:3301``. This function may be useful before invoking a function which might otherwise block indefinitely. This is a basic function which is used by other functions. Depending on the value of ``condition``, print 'ok' or 'not ok' along with debugging information. Displays the message. This is the negation of ``taptest:is(...)``. This method may change in the future. This must be called for every shard. The shard-configuration is a table with these fields: This reference covers Tarantool's built-in Lua modules. This reference covers all options and parameters which can be set for Tarantool on the command line or in an initialization file. This reference covers third-party Lua modules for Tarantool. This shows that what was inserted by Terminal #1 can be selected by Terminal #2, via the shard module. This will illustrate how "rotation" works, that is, what happens when the server is writing to a log and signals are used when archiving it. This will open the file ``tarantool.log`` for output on the server’s default directory. If the ``logger`` string has no prefix or has the prefix "file:", then the string is interpreted as a file path. This will start the program ``cronolog`` when the server starts, and will send all log messages to the standard input (``stdin``) of cronolog. If the ``logger`` string begins with '|' or has the prefix "pipe:", then the string is interpreted as a Unix `pipeline <https://en.wikipedia.org/wiki/Pipeline_%28Unix%29>`_. To call another DBMS from Tarantool, the essential requirements are: another DBMS, and Tarantool. The module which connects Tarantool to another DBMS may be called a "connector". Within the module there is a shared library which may be called a "driver". To end a session that began with ``mysql.connect``, the request is: To end a session that began with ``pg.connect``, the request is: To ensure that a connection is working, the request is: To initiate tdb within a Lua program and set a breakpoint, edit the program to include these lines: To run this example: put the script in a file named ./tap.lua, then make tap.lua executable by saying ``chmod a+x ./tap.lua``, then execute using Tarantool as a script processor by saying ./tap.lua. To see all the non-null parameters, say ``box.cfg`` (no parentheses). To see a particular parameter, for example the listen address, say ``box.cfg.listen``. To start the debugging session, execute the Lua program. Execution will stop at the breakpoint, and it will be possible to enter debugging commands. To use Tarantool binary protocol primitives from Lua, it's necessary to convert Lua variables to binary format. The ``pickle.pack()`` helper function is prototyped after Perl 'pack_'. Type: boolean |br| Default: false |br| Dynamic: no |br| Type: boolean |br| Default: false |br| Dynamic: yes |br| Type: boolean |br| Default: true |br| Dynamic: no |br| Type: boolean |br| Default: true |br| Dynamic: yes |br| Type: float |br| Default: 0.5 |br| Dynamic: **yes** |br| Type: float |br| Default: 1.0 |br| Dynamic: no |br| Type: float |br| Default: 1.1 |br| Dynamic: no |br| Type: float |br| Default: 2 |br| Dynamic: no |br| Type: float |br| Default: null |br| Dynamic: **yes** |br| Type: integer or string |br| Default: null |br| Dynamic: yes |br| Type: integer |br| Default: 0 |br| Dynamic: yes |br| Type: integer |br| Default: 1048576 |br| Dynamic: no |br| Type: integer |br| Default: 16 |br| Dynamic: no |br| Type: integer |br| Default: 16320 |br| Dynamic: **yes** |br| Type: integer |br| Default: 5 |br| Dynamic: **yes** |br| Type: integer |br| Default: 500000 |br| Dynamic: no |br| Type: integer |br| Default: 6 |br| Dynamic: yes |br| Type: string |br| Default: "." |br| Dynamic: no |br| Type: string |br| Default: "write" |br| Dynamic: **yes** |br| Type: string |br| Default: null |br| Dynamic: **yes** |br| Type: string |br| Default: null |br| Dynamic: no |br| Type: string |br| Default: null |br| Dynamic: yes |br| Typically a socket session will begin with the setup functions, will set one or more flags, will have a loop with sending and receiving functions, will end with the teardown functions -- as an example at the end of this section will show. Throughout, there may be error-checking and waiting functions for synchronization. To prevent a fiber containing socket functions from "blocking" other fibers, the :ref:`implicit yield rules <atomic-the_implicit_yield_rules>` will cause a yield so that other processes may take over, as is the norm for cooperative multitasking. UNIX user name to switch to after start. URI URI fragment URL or IP address UUID converted from cdata input value. UUID in 16-byte binary string UUID in 36-byte hexadecimal string Ultimately the tuple-expiry process leads to ``default_tuple_drop()`` which does a "delete" of a tuple from its original space. First the fun :ref:`fun <fun-module>` module is used, specifically fun.map_. Remembering that :codenormal:`index[0]` is always the space's primary key, and :codenormal:`index[0].parts[`:codeitalic:`N`:codenormal:`].fieldno` is always the field number for key part :codeitalic:`N`, fun.map() is creating a table from the primary-key values of the tuple. The result of fun.map() is passed to :ref:`space_object:delete() <box_space-delete>`. Use of a TCP socket over the Internet Use of a UDP socket on localhost Use slab_alloc_factor as the multiplier for computing the sizes of memory chunks that tuples are stored in. A lower value may result in less wasted memory depending on the total amount of memory available and the distribution of item sizes. Use tcp_server to accept file contents sent with socat Wait for connection to be active or closed. Wait until something is either readable or writable, or until a timeout value expires. Wait until something is readable, or until a timeout value expires. Wait until something is writable, or until a timeout value expires. We will assume that the name is 'conn' in further examples. What will appear on Terminal #1 is: a loop of error messages saying "Connection refused" and "server check failure". This is normal. It will go on until Terminal #2 process starts. What will appear on Terminal #2, at the end, should look like this: When called with a Lua-table argument, the code and reason have any user-desired values. The result will be those values. When called without arguments, ``box.error()`` re-throws whatever the last error was. When enclosed by quote marks, commas and line feeds and spaces are treated as ordinary characters, and a pair of quote marks "" is treated as a single quote mark. When logging to a file, tarantool reopens the log on SIGHUP. When log is a program, its pid is saved in the :ref:`log.logger_pid <log-logger_pid>` variable. You need to send it a signal to rotate logs. Whenever one hears "daemon" in Tarantool, one should suspect it's being done with a :doc:`fiber<../reference_lua/fiber>`. The program is making a fiber and turning control over to it so it runs occasionally, goes to sleep, then comes back for more. Will display ``# bad plan: ...`` if the number of completed tests is not equal to the number of tests specified by ``taptest:plan(...)``. With GitHub With LuaRocks With sharding, the tuples of a tuple set are distributed to multiple nodes, with a Tarantool database server on each node. With this arrangement, each server is handling only a subset of the total data, so larger loads can be handled by simply adding more computers to a network. Write as much as possible data to the socket buffer if non-blocking. Rarely used. For details see `this description`_. Yield control to the scheduler. Equivalent to :ref:`fiber.sleep(0) <fiber-sleep>`. Yield control to the transaction processor thread and sleep for the specified number of seconds. Only the current fiber can be made to sleep. [0] = 5 [0] = nil ``__serialize = "map" or "mapping"`` for a map ``__serialize = "seq" or "sequence"`` for an array ``__serialize="map"`` for a Flow Mapping map. ``__serialize="map"`` for a map ``__serialize="mapping"`` for a Block Mapping map, ``__serialize="seq"`` for a Flow Sequence array, ``__serialize="seq"`` for an array ``__serialize="sequence"`` for a Block Sequence array, ``byte-order`` can be one of next flags: ``cfg.encode_invalid_as_nil`` - use null for all unrecognizable types (default is false) ``cfg.encode_invalid_numbers`` - allow nan and inf (default is true) ``cfg.encode_load_metatables`` - load metatables (default is false) ``cfg.encode_use_tostring`` - use tostring for unrecognizable types (default is false) ``conn:call('func', '1', '2', '3')`` is the remote-call equivalent of ``func('1', '2', '3')``. That is, ``conn:call`` is a remote stored-procedure call. ``fh:pwrite`` returns true if success, false if failure. ``fh:pread`` returns the data that was read, or nil if failure. ``fh:read`` and ``fh:write`` affect the seek position within the file, and this must be taken into account when working on the same file from multiple fibers. It is possible to limit or prevent file access from other fibers with ``fiber.ipc``. ``fh:write`` returns true if success, false if failure. ``fh:read`` returns the data that was read, or nil if failure. ``fio.link`` and ``fio.symlink`` and ``fio.unlink`` return true if success, false if failure. ``fio.readlink`` returns the link value if success, nil if failure. ``fsync``: fibers wait for their data, :manpage:`fsync(2)` follows each :manpage:`write(2)`; ``none``: write-ahead log is not maintained; ``sock:nonblock()`` returns the current flag value. ``sock:nonblock(false)`` sets the flag to false and returns false. ``sock:nonblock(true)`` sets the flag to true and returns true. ``socket.getaddrinfo('tarantool.org', 'http')`` will return variable information such as ``taptest:fail('x')`` is equivalent to ``taptest:ok(false, 'x')``. Displays the message. ``taptest:skip('x')`` is equivalent to ``taptest:ok(true, 'x' .. '# skip')``. Displays the message. ``timeout(...)`` is a wrapper which sets a timeout for the request that follows it. ``write``: fibers wait for their data to be written to the write-ahead log (no :manpage:`fsync(2)`); a UUID a binary string containing all arguments, packed according to the format specifiers. a connected socket, if no error. a function a possible option is `wait_connect` a socket object on success a string formatted as JSON. a string formatted as MsgPack. a string formatted as YAML. a string of the requested length on success. a string, or any object which has a read() method, formatted according to the CSV rules a table which can be formatted according to the CSV rules. a value that will be checked a, A actual result aes128 - aes-128 (with 192-bit binary strings using AES) aes192 - aes-192 (with 192-bit binary strings using AES) aes256 - aes-256 (with 256-bit binary strings using AES) an arbitrary name to give for the test outputs. an empty string if there is nothing more to read, or a nil value if error, or a string up to ``limit`` bytes long, which may include the bytes that matched the ``delimiter`` expression. an expression which is true or false an unconnected socket, or nil. and `Log_file` will have and suppose the environment variable LISTEN_URI contains 3301, and suppose the command line is ``~/tarantool/src/tarantool script.lua ARG``. Then the screen might look like this: and the second encoding means: any object which has a write() method arguments, that must be passed to function b, B binary (a port number that this host is listening on, on the current host) (distinguishable from the 'listen' port specified by box.cfg) bool boolean boolean. box.cfg{
    snapshot_period = 3600,
    snapshot_count  = 10
} box.cfg{logger = 'syslog:identity=tarantool'}
-- or
box.cfg{logger = 'syslog:facility=user'}
-- or
box.cfg{logger = 'syslog:identity=tarantool,facility=user'} box.cfg{logger = 'tarantool.log'}
-- or
box.cfg{logger = 'file: tarantool.log'} box.cfg{logger = '| cronolog tarantool.log'}
-- or
box.cfg{logger = 'pipe: cronolog tarantool.log'}' box.cfg{logger='Log_file'}
log = require('log')
log.info('Log Line #1') box.cfg{}
socket = require('socket')
socket.tcp_server('0.0.0.0', 3302, function(s)
    while true do
      local request
      request = s:read("\n");
      if request == "" or request == nil then
        break
      end
      print(request)
    end
  end) cbc - Cipher Block Chaining cdata cfb - Cipher Feedback changed name of file or directory. client/server conn = mysql.connect({
    host = '127.0.0.1',
    port = 3306,
    user = 'p',
    password = 'p',
    db = 'test',
    raise = true
})
-- OR
conn = mysql.connect({
    host = 'unix/',
    port = '/var/run/mysqld/mysqld.sock'
}) conn = net_box.new('localhost:3301')
conn = net_box.new('127.0.0.1:3306', {wait_connect = false}) conn = pg.connect({
    host = '127.0.0.1',
    port = 5432,
    user = 'p',
    password = 'p',
    db = 'test'
}) conn object conn:call('function5') conn:close() conn:eval('return 5+5') conn:timeout(0.5).space.tester:update({1}, {{'=', 2, 15}}) console = require('console')
console.start() converted UUID converts Lua variable to a 1-byte integer, and stores the integer in the resulting string converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 4-byte float, and stores the float in the resulting string converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, big converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 8-byte double, and stores the double in the resulting string converts Lua variable to a sequence of bytes, and stores the sequence in the resulting string converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, low byte first created fiber object crypto = require('crypto')

-- print aes-192 digest of 'AB', with one step, then incrementally
print(crypto.cipher.aes192.cbc.encrypt('AB', 'key'))
c = crypto.cipher.aes192.cbc.encrypt.new()
c:init()
c:update('A', 'key')
c:update('B', 'key')
print(c:result())
c:free()

-- print sha-256 digest of 'AB', with one step, then incrementally
print(crypto.digest.sha256('AB'))
c = crypto.digest.sha256.new()
c:init()
c:update('A')
c:update('B')
print(c:result())
c:free() crypto.cipher.aes192.cbc.encrypt('string', 'key', 'initialization')
crypto.cipher.aes256.ecb.decrypt('string', 'key', 'initialization') crypto.cipher.aes256.cbc.encrypt('string', 'key') == digest.aes256cbc.encrypt('string', 'key')
crypto.digest.md4('string') == digest.md4('string')
crypto.digest.md5('string') == digest.md5('string')
crypto.digest.sha('string') == digest.sha('string')
crypto.digest.sha1('string') == digest.sha1('string')
crypto.digest.sha224('string') == digest.sha224('string')
crypto.digest.sha256('string') == digest.sha256('string')
crypto.digest.sha384('string') == digest.sha384('string')
crypto.digest.sha512('string') == digest.sha512('string') crypto.digest.md4('string')
crypto.digest.sha512('string') current system time (in microseconds since the epoch) as a 64-bit integer. The time is taken from the event loop clock. current system time (in seconds since the epoch) as a Lua number. The time is taken from the event loop clock, which makes this call very cheap, but still useful for constructing artificial tuple keys. d des    - des (with 56-bit binary strings using DES, though DES is not recommended) details about the file. digest = require('digest')

-- print crc32 of 'AB', with one step, then incrementally
print(digest.crc32('AB'))
c = digest.crc32.new()
c:update('A')
c:update('B')
print(c:result())

-- print murmur hash of 'AB', with one step, then incrementally
print(digest.murmur('AB'))
m = digest.murmur.new()
m:update('A')
m:update('B')
print(m:result()) directory name, that is, path name except for file name. dss - dss (using DSS) dss1 - dss (using DSS-1) due to :ref:`the implicit yield rules <atomic-the_implicit_yield_rules>` a local :samp:`box.space.{space-name}:select`:code:`{...}` does not yield, but a remote :samp:`conn.space.{space-name}:select`:code:`{...}` call does yield, so global variables or database tuples data may change when a remote :samp:`conn.space.{space-name}:select`:code:`{...}` occurs. dumped_value ecb - Electronic Codebook either a scalar value or a Lua table value. error checking existing file name. expected result f false fiber = require('fiber')
channel = fiber.channel(10)
function consumer_fiber()
    while true do
        local task = channel:get()
        ...
    end
end

function consumer2_fiber()
    while true do
        -- 10 seconds
        local task = channel:get(10)
        if task ~= nil then
            ...
        else
            -- timeout
        end
    end
end

function producer_fiber()
    while true do
        task = box.space...:select{...}
        ...
        if channel:is_empty() then
            -- channel is empty
        end

        if channel:is_full() then
            -- channel is full
        end

        ...
        if channel:has_readers() then
            -- there are some fibers
            -- that are waiting for data
        end
        ...

        if channel:has_writers() then
            -- there are some fibers
            -- that are waiting for readers
        end
        channel:put(task)
    end
end

function producer2_fiber()
    while true do
        task = box.space...select{...}
        -- 10 seconds
        if channel:put(task, 10) then
            ...
        else
            -- timeout
        end
    end
end fiber = require('fiber')
expd = require('expirationd')
box.cfg{}
e = box.schema.space.create('expirationd_test')
e:create_index('primary', {type = 'hash', parts = {1, 'unsigned'}})
e:replace{1, fiber.time() + 3}
e:replace{2, fiber.time() + 30}
function is_tuple_expired(args, tuple)
  if (tuple[2] < fiber.time()) then return true end
  return false
  end
expd.run_task('expirationd_test', e.id, is_tuple_expired)
retval = {}
fiber.sleep(2)
expd.task_stats()
fiber.sleep(2)
expd.task_stats()
expd.kill_task('expirationd_test')
e:drop()
os.exit() fiber object for the currently scheduled fiber. fiber object for the specified fiber. fiber object, for example the fiber object returned by :ref:`fiber.create <fiber-create>` fields which describe the file's block size, creation time, size, and other attributes. file handle (later - fh) file name file-handle as returned by ``fio.open()``. fixarray(2), fixstr(1), "A", fixstr(1), "B" fixmap(2), key(1), fixstr(1), "A", key(2), fixstr(2), "B". flag setting for _, tuple in scan_space.index[0]:pairs(nil, {iterator = box.index.ALL}) do
...
        if task.is_tuple_expired(task.args, tuple) then
        task.expired_tuples_count = task.expired_tuples_count + 1
        task.process_expired_tuple(task.space_id, task.args, tuple)
... function f()
  print("D")
end
jit.bc.dump(f) function hexdump(bytes)
    local result = ''
    for i = 1, #bytes do
        result = result .. string.format("%x", string.byte(bytes, i)) .. ' '
    end
    return result
end

msgpack = require('msgpack')
m1 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "seq"
                          }))
m2 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "map"
                          }))
print('array encoding: ', hexdump(m1))
print('map encoding: ', hexdump(m2)) git clone --recursive https://github.com/Sulverus/tdb
cd tdb
make
sudo make install prefix=/usr/share/tarantool/ git clone https://github.com/tarantool/mysql.git
cd mysql && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install git clone https://github.com/tarantool/pg.git
cd pg && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install host - a number, 0 (zero), meaning "all local interfaces"; host - a string containing "unix/"; host - a string representation of an IPv4 address or an IPv6 address; host:port i, I id of the fiber. information iterator function json = require('json')
json.cfg{encode_invalid_numbers = true}
x = 0/0
y = 1/0
json.encode({1, x, y, 2}) kill -HUP *process_id* l, L linked name. list of files whose names match the input string loaded_value local function expirationd_run_task(name, space_id, is_tuple_expired, options)
... log file has been reopened
2015-11-30 15:15:32.629 [27469] main/101/interactive I> Log Line #3 log.info('Log Line #2') log.info('Log Line #3') login (the user name which applies for accessing via the shard module) lua_object luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib luarocks install mysql [MYSQL_LIBDIR = *path*]
                       [MYSQL_INCDIR = *path*]
                       [--local] luarocks install pg POSTGRESQL_LIBDIR=/usr/local/postgresql/lib luarocks install pg [POSTGRESQL_LIBDIR = *path*]
                    [POSTGRESQL_INCDIR = *path*]
                    [--local] mask bits. maximum number of bytes to read for example 50 means "stop after 50 bytes" maximum number of seconds to wait for example 50 means "stop after 50 seconds". md4 - md4 (with 128-bit binary strings using MD4) md5 - md5 (with 128-bit binary strings using MD5) mdc2 - mdc2 (using MDC2) message, a table containing "host", "family" and "port" fields. message_sender.host = '18.44.0.1'
message_sender.family = 'AF_INET'
message_sender.port = 43065 msgpack.NULL mv Log_file Log_file.bak mysql = require('mysql') n n  -- go to next line
n  -- go to next line
e  -- enter evaluation mode
j  -- display j
-e -- exit evaluation mode
q  -- quit name of existing file or directory, name of test name of the fiber. net_box.self:is_connected() net_box.self:ping() net_box.self:wait_connected() new active and timeout values. new channel. new group uid. new name. new permissions new socket if success. new user uid. nil notguest:sesame@mail.ru:3301 num number number of a pre-defined error number of bytes to read number of context switches, backtrace, id, total memory, used memory, name for each fiber. number of seconds to sleep. number or number64 number, string numeric identifier of the fiber. ofb - Output Feedback offset within file where reading or writing begins one of ``'l'``, ``'b'``, ``'h'`` or ``'n'``. one or more strings to be concatenated. optional. see :ref:`above <csv-options>` original name. part of the message which will accompany the error password (the password for the login) path name path name of file. path of directory. path-name, which may contain wildcard characters. pg = require('pg') port port - a number. port - a number. If a port number is 0 (zero), the socket will be bound to a random local port. port - a string containing a path to a unix socket. port number position to seek to positive integer as great as the maximum number of slots (spaces for ``get`` or ``put`` messages) that might be pending at any given time. previous mask bits. ps -A | grep tarantool q, Q receiving redundancy (a number, minimum 1) result for ``sock:errno()``, result for ``sock:error()``. If there is no error, then ``sock:errno()`` will return 0 and ``sock:error()``. ripemd160 - rtype: table s, S same as nil scalar values to be formatted seconds or nanoseconds since epoch (1970-01-01 00:00:00), adjusted. seconds or nanoseconds since processor start. seconds or nanoseconds since the last time that the computer was booted. seconds or nanoseconds since thread start. see :ref:`above <csv-options>` sending separator for example '?' means "stop after a question mark" servers (a list of URIs of nodes and the zones the nodes are in) setup sha - sha (with 160-bit binary strings using SHA-0) sha1 - sha-1 (with 160-bit binary strings using SHA-1) sha224 - sha-224 (with 224-bit binary strings using SHA-2) sha256 - sha-256 (with 256-bit binary strings using SHA-2) sha384 - sha-384 (with 384-bit binary strings using SHA-2) sha512 - sha-512(with 512-bit binary strings using SHA-2). shard.init(*shard-configuration*) shard[*space-name*].insert{...}
shard[*space-name*].replace{...}
shard[*space-name*].delete{...}
shard[*space-name*].select{...}
shard[*space-name*].update{...}
shard[*space-name*].auto_increment{...} shard[*space-name*].q_insert{...}
shard[*space-name*].q_replace{...}
shard[*space-name*].q_delete{...}
shard[*space-name*].q_select{...}
shard[*space-name*].q_update{...}
shard[*space-name*].q_auto_increment{...} socket = require('socket')
sock = socket('AF_INET', 'SOCK_STREAM', 'tcp')
sock:sysconnect(0, 3301) socket('AF_INET', 'SOCK_STREAM', 'tcp') socket.SHUT_RD, socket.SHUT_WR, or socket.SHUT_RDWR. socket.tcp_server('localhost', 3302, function () end) state checking string string containing format specifiers string, table string, which is written to ``writable`` if specified sudo apt-get install libpq-dev sudo apt-get install tarantool-dev sudo apt-get install tarantool-shard tarantool-pool suffix table table. first element = seconds of CPU time; second element = whatever the function returns. tap = require('tap')
taptest = tap.test('test-name') taptest tarantool> -- Connection function. Usage: conn = mysql_connect()
tarantool> function mysql_connection()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   conn = mysql.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = mysql_connect()
---
... tarantool> -- input in file.csv is:
tarantool> -- a,"b,c ",d
tarantool> -- a\\211\\128b
tarantool> fio = require('fio')
---
...
tarantool> f = fio.open('./file.csv', {'O_RDONLY'})
---
...
tarantool> csv.load(f, {chunk_size = 4096})
---
- - - a
    - 'b,c '
    - d
  - - a\\211\\128b
...
tarantool> f:close(nn)
---
- true
... tarantool> box.cfg{}
...
tarantool> mysql = require('mysql')
---
... tarantool> box.cfg{}
...
tarantool> pg = require('pg')
---
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.error()
---
- error: Arbitrary message
...
tarantool> box.error(box.error.FUNCTION_ACCESS_DENIED, 'A', 'B', 'C')
---
- error: A access denied for user 'B' to function 'C'
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.schema.space.create('#')
---
- error: Invalid identifier '#' (expected letters, digits or an underscore)
...
tarantool> box.error.last()
---
- line: 278
  code: 70
  type: ClientError
  message: Invalid identifier '#' (expected letters, digits or an underscore)
  file: /tmp/buildd/tarantool-1.7.0.252.g1654e31~precise/src/box/key_def.cc
...
tarantool> box.error.clear()
---
...
tarantool> box.error.last()
---
- null
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'host1:33131', zone = '1' },
         >     { uri = 'host2:33131', zone = '2' },
         >     { uri = 'host3:33131', zone = '3' },
         >     { uri = 'host4:33131', zone = '4' },
         >     { uri = 'host5:33131', zone = '5' },
         >     { uri = 'host6:33131', zone = '6' },
         >     { uri = 'host7:33131', zone = '7' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '2',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:33131', zone = '1' },
         >     { uri = 'localhost:33132', zone = '2' },
         >     { uri = 'localhost:33133', zone = '3' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '3',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> conn:close()
---
... tarantool> conn:execute('select table_name from information_schema.tables')
---
- - table_name: ALL_PLUGINS
  - table_name: APPLICABLE_ROLES
  - table_name: CHARACTER_SETS
  <...>
- 78
... tarantool> conn:execute('select tablename from pg_tables')
---
- - tablename: pg_statistic
  - tablename: pg_type
  - tablename: pg_authid
  <...>
... tarantool> conn:ping()
---
- true
... tarantool> console = require('console')
---
...
tarantool> console.connect('198.18.44.44:3301')
---
...
198.18.44.44:3301> -- prompt is telling us that server is remote tarantool> console = require('console')
---
...
tarantool> console.listen('unix/:/tmp/X.sock')
... main/103/console/unix/:/tmp/X I> started
---
- fd: 6
  name:
    host: unix/
    family: AF_UNIX
    type: SOCK_STREAM
    protocol: 0
    port: /tmp/X.sock
... tarantool> csv = require('csv')
---
...
tarantool> csv.dump({'a','b,c ','d'})
---
- 'a,"b,c ",d

'
... tarantool> csv = require('csv')
---
...
tarantool> csv.load('a,"b,c ",d')
---
- - - a
    - 'b,c '
    - d
... tarantool> csv.load('a,b;c,d', {delimiter = ';'})
---
- - - a,b
    - c,d
... tarantool> csv.load('a\\211\\128b')
---
- - - a\211\128b
... tarantool> csv_table = csv.load('a,b,c')
---
...
tarantool> csv.dump(csv_table)
---
- 'a,b,c

'
... tarantool> digest = require('digest')
---
...
tarantool> function password_insert()
         >   box.space.tester:insert{1234, digest.sha1('^S^e^c^ret Wordpass')}
         >   return 'OK'
         > end
---
...
tarantool> function password_check(password)
         >   local t = box.space.tester:select{12345}
         >   if digest.sha1(password) == t[2] then
         >     return 'Password is valid'
         >   else
         >     return 'Password is not valid'
         >   end
         > end
---
...
tarantool> password_insert()
---
- 'OK'
... tarantool> digest.guava(10863919174838991, 11)
---
- 8
... tarantool> dostring('abc')
---
error: '[string "abc"]:1: ''='' expected near ''<eof>'''
...
tarantool> dostring('return 1')
---
- 1
...
tarantool> dostring('return ...', 'hello', 'world')
---
- hello
- world
...
tarantool> dostring([[
         >   local f = function(key)
         >     local t = box.space.tester:select{key}
         >     if t ~= nil then
         >       return t[1]
         >     else
         >       return nil
         >     end
         >   end
         >   return f(...)]], 1)
---
- null
... tarantool> fh = fio.open('/home/username/tmp.txt', {'O_RDWR', 'O_APPEND'})
---
...
tarantool> fh -- display file handle returned by fio.open
---
- fh: 11
... tarantool> fh:close() -- where fh = file-handle
---
- true
... tarantool> fh:fsync()
---
- true
... tarantool> fh:pread(25, 25)
---
- |
  elete from t8//
  insert in
... tarantool> fh:seek(20, 'SEEK_SET')
---
- 20
... tarantool> fh:stat()
---
- inode: 729866
  rdev: 0
  size: 100
  atime: 140942855
  mode: 33261
  mtime: 1409430660
  nlink: 1
  uid: 1000
  blksize: 4096
  gid: 1000
  ctime: 1409430660
  dev: 2049
  blocks: 8
... tarantool> fh:truncate(0)
---
- true
... tarantool> fh:write('new data')
---
- true
... tarantool> fiber = require('fiber')
---
...
tarantool> function f () fiber.sleep(1000); end
---
...
tarantool> fiber_function = fiber:create(f)
---
- error: '[string "fiber_function = fiber:create(f)"]:1: fiber.create(function, ...):
    bad arguments'
...
tarantool> fiber_function = fiber.create(f)
---
...
tarantool> fiber_function.storage.str1 = 'string'
---
...
tarantool> fiber_function.storage['str1']
---
- string
...
tarantool> fiber_function:cancel()
---
...
tarantool> fiber_function.storage['str1']
---
- error: '[string "return fiber_function.storage[''str1'']"]:1: the fiber is dead'
... tarantool> fiber = require('fiber')
---
...
tarantool> function function_name()
         >   fiber.sleep(1000)
         > end
---
...
tarantool> fiber_object = fiber.create(function_name)
---
... tarantool> fiber = require('fiber')
tarantool> function function_x()
         >   gvar = 0
         >   while 0 == 0 do
         >     gvar = gvar + 1
         >     fiber.sleep(2)
         >   end
         > end
---
... tarantool> fiber.find(101)
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.info()
---
- 101:
    csw: 7
    backtrace: []
    fid: 101
    memory:
      total: 65776
      used: 0
    name: interactive
... tarantool> fiber.kill(fiber.id())
---
- error: fiber is cancelled
... tarantool> fiber.self()
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.self():cancel()
---
- error: fiber is cancelled
... tarantool> fiber.self():name('non-interactive')
---
... tarantool> fiber.self():name()
---
- interactive
... tarantool> fiber.self():status()
---
- running
... tarantool> fiber.sleep(1.5)
---
... tarantool> fiber.status()
---
- running
... tarantool> fiber.testcancel()
---
- error: fiber is cancelled
... tarantool> fiber.time(), fiber.time()
---
- 1448466279.2415
- 1448466279.2415
... tarantool> fiber.time(), fiber.time64()
---
- 1448466351.2708
- 1448466351270762
... tarantool> fiber.yield()
---
... tarantool> fiber_object = fiber.self()
---
...
tarantool> fiber_object:id()
---
- 101
... tarantool> fiber_of_x:cancel()
---
...
tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  dead . gvar= 421
---
... tarantool> fid = fiber_of_x:id()
---
... tarantool> fio.basename('/path/to/my.lua', '.lua')
---
- my
... tarantool> fio.chmod('/home/username/tmp.txt', tonumber('0755', 8))
---
- true
...
tarantool> fio.chown('/home/username/tmp.txt', 'username', 'username')
---
- true
... tarantool> fio.cwd()
---
- /home/username/tarantool_sandbox
... tarantool> fio.dirname('path/to/my.lua')
---
- 'path/to/'
... tarantool> fio.glob('/etc/x*')
---
- - /etc/xdg
  - /etc/xml
  - /etc/xul-ext
... tarantool> fio.link('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
...
tarantool> fio.unlink('/home/username/tmp.txt2')
---
- true
... tarantool> fio.lstat('/etc')
---
- inode: 1048577
  rdev: 0
  size: 12288
  atime: 1421340698
  mode: 16877
  mtime: 1424615337
  nlink: 160
  uid: 0
  blksize: 4096
  gid: 0
  ctime: 1424615337
  dev: 2049
  blocks: 24
... tarantool> fio.mkdir('/etc')
---
- false
... tarantool> fio.pathjoin('/etc', 'default', 'myfile')
---
- /etc/default/myfile
... tarantool> fio.rename('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
... tarantool> fio.sync()
---
- true
... tarantool> fio.tempdir()
---
- /tmp/lG31e7
... tarantool> fio.truncate('/home/username/tmp.txt', 99999)
---
- true
... tarantool> fio.umask(tonumber('755', 8))
---
- 493
... tarantool> fun = require('fun')
---
...
tarantool> for _k, a in fun.range(3) do
         >   print(a)
         > end
1
2
3
---
... tarantool> function mysql_select ()
         >   local conn = mysql.connect({
         >     host = '127.0.0.1',
         >     port = 3306,
         >     user = 'root',
         >     db = 'test'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> mysql_select()
---
- 'MySQL row '
... tarantool> function pg_connect()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   p.user = 'postgres'
         >   p.password = 'postgres'
         >   local conn = pg.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = pg_connect()
---
... tarantool> function pg_select ()
         >   local conn = pg.connect({
         >     host = '127.0.0.1',
         >     port = 5432,
         >     user = 'postgres',
         >     password = 'postgres',
         >     db = 'postgres'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> pg_select()
---
- 'PostgreSQL row '
... tarantool> gvar = 0

tarantool> fiber_of_x = fiber.create(function_x)
---
... tarantool> json = require('json')
---
...
tarantool> json.decode('123')
---
- 123
...
tarantool> json.decode('[123, "hello"]')
---
- [123, 'hello']
...
tarantool> json.decode('{"hello": "world"}').hello
---
- world
... tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- '["A","B"]'
...
tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="map"}))
---
- '{"1":"A","2":"B"}'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- '[{"f2":"B","f1":"A"}]'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="seq"})})
---
- '[[]]'
... tarantool> json.encode({1, x, y, 2})
---
- '[1,nan,inf,2]
... tarantool> json=require('json')
---
...
tarantool> json.encode(123)
---
- '123'
...
tarantool> json.encode({123})
---
- '[123]'
...
tarantool> json.encode({123, 234, 345})
---
- '[123,234,345]'
...
tarantool> json.encode({abc = 234, cde = 345})
---
- '{"cde":345,"abc":234}'
...
tarantool> json.encode({hello = {'world'}})
---
- '{"hello":["world"]}'
... tarantool> load = function(readable, opts)
         >   opts = opts or {}
         >   local result = {}
         >   for i, tup in csv.iterate(readable, opts) do
         >     result[i] = tup
         >   end
         >   return result
         > end
---
...
tarantool> load('a,b,c')
---
- - - a
    - b
    - c
... tarantool> msgpack = require('msgpack')
---
...
tarantool> y = msgpack.encode({'a',1,'b',2})
---
...
tarantool> z = msgpack.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> box.space.tester:insert{20, msgpack.NULL, 20}
---
- [20, null, 20]
... tarantool> net_box = require('net.box')
---
...
tarantool> function example()
         >   local conn, wtuple
         >   if net_box.self:ping() then
         >     table.insert(ta, 'self:ping() succeeded')
         >     table.insert(ta, '  (no surprise -- self connection is pre-established)')
         >   end
         >   if box.cfg.listen == '3301' then
         >     table.insert(ta,'The local server listen address = 3301')
         >   else
         >     table.insert(ta, 'The local server listen address is not 3301')
         >     table.insert(ta, '(  (maybe box.cfg{...listen="3301"...} was not stated)')
         >     table.insert(ta, '(  (so connect will fail)')
         >   end
         >   conn = net_box.new('127.0.0.1:3301')
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester.')
         >   conn.space.tester:insert{800, 'data'}
         >   table.insert(ta, 'conn insert done on tester, index 0')
         >   table.insert(ta, '  primary key value = 800.')
         >   wtuple = conn.space.tester:select{800}
         >   table.insert(ta, 'conn select done on tester, index 0')
         >   table.insert(ta, '  number of fields = ' .. #wtuple)
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester')
         >   conn.space.tester:replace{800, 'New data', 'Extra data'}
         >   table.insert(ta, 'conn:replace done on tester')
         >   conn:timeout(0.5).space.tester:update({800}, {{'=', 2, 'Fld#1'}})
         >   table.insert(ta, 'conn update done on tester')
         >   conn:close()
         >   table.insert(ta, 'conn close done')
         > end
---
...
tarantool> ta = {}
---
...
tarantool> example()
---
...
tarantool> ta
---
- - self:ping() succeeded
  - '  (no surprise -- self connection is pre-established)'
  - The local server listen address = 3301
  - conn delete done on tester.
  - conn insert done on tester, index 0
  - '  primary key value = 800.'
  - conn select done on tester, index 0
  - '  number of fields = 1'
  - conn delete done on tester
  - conn:replace done on tester
  - conn update done on tester
  - conn close done
... tarantool> os.clock()
---
- 0.05
... tarantool> os.date("%A %B %d")
---
- Sunday April 24
... tarantool> os.execute('ls -l /usr')
total 200
drwxr-xr-x   2 root root 65536 Apr 22 15:49 bin
drwxr-xr-x  59 root root 20480 Apr 18 07:58 include
drwxr-xr-x 210 root root 65536 Apr 18 07:59 lib
drwxr-xr-x  12 root root  4096 Apr 22 15:49 local
drwxr-xr-x   2 root root 12288 Jan 31 09:50 sbin
---
... tarantool> os.exit()
user@user-shell:~/tarantool_sandbox$ tarantool> os.getenv('PATH')
---
- /usr/local/sbin:/usr/local/bin:/usr/sbin
... tarantool> os.remove('file')
---
- true
... tarantool> os.rename('local','foreign')
---
- null
- 'local: No such file or directory'
- 2
... tarantool> os.time()
---
- 1461516945
... tarantool> os.tmpname()
---
- /tmp/lua_7SW1m2
... tarantool> password_insert('Secret Password')
---
- 'Password is not valid'
... tarantool> pickle = require('pickle')
---
...
tarantool> box.space.tester:insert{0, 'hello world'}
---
- [0, 'hello world']
...
tarantool> box.space.tester:update({0}, {{'=', 2, 'bye world'}})
---
- [0, 'bye world']
...
tarantool> box.space.tester:update({0}, {
         >   {'=', 2, pickle.pack('iiA', 0, 3, 'hello')}
         > })
---
- [0, "\0\0\0\0\x03\0\0\0hello"]
...
tarantool> box.space.tester:update({0}, {{'=', 2, 4}})
---
- [0, 4]
...
tarantool> box.space.tester:update({0}, {{'+', 2, 4}})
---
- [0, 8]
...
tarantool> box.space.tester:update({0}, {{'^', 2, 4}})
---
- [0, 12]
... tarantool> pickle = require('pickle')
---
...
tarantool> tuple = box.space.tester:replace{0}
---
...
tarantool> string.len(tuple[1])
---
- 1
...
tarantool> pickle.unpack('b', tuple[1])
---
- 48
...
tarantool> pickle.unpack('bsi', pickle.pack('bsi', 255, 65535, 4294967295))
---
- 255
- 65535
- 4294967295
...
tarantool> pickle.unpack('ls', pickle.pack('ls', tonumber64('18446744073709551615'), 65535))
---
...
tarantool> num, num64, str = pickle.unpack('slA', pickle.pack('slA', 666,
         > tonumber64('666666666666666'), 'string'))
---
... tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  suspended . gvar= 399
---
... tarantool> shard.init(cfg)
2015-08-09 ... I> Sharding initialization started...
2015-08-09 ... I> establishing connection to cluster servers...
2015-08-09 ... I>  - localhost:3301 - connecting...
2015-08-09 ... I>  - localhost:3301 - connected
2015-08-09 ... I> connected to all servers
2015-08-09 ... I> started
2015-08-09 ... I> redundancy = 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> Adding localhost:3301 to shard 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> shards = 1
2015-08-09 ... I> Done
---
- true
...
tarantool> -- Now put something in ...
---
...
tarantool> shard.tester:insert{1,'Tuple #1'}
---
- - [1, 'Tuple #1']
... tarantool> shard.tester:select{1}
---
- - - [1, 'Tuple #1']
... tarantool> socket = require('socket')
---
...
tarantool> sock = socket.tcp_connect('tarantool.org', 80)
---
...
tarantool> type(sock)
---
- table
...
tarantool> sock:error()
---
- null
...
tarantool> sock:send("HEAD / HTTP/1.0rnHost: tarantool.orgrnrn")
---
- true
...
tarantool> sock:read(17)
---
- "HTTP/1.1 200 OKrn"
...
tarantool> sock:close()
---
- true
... tarantool> socket = require('socket')
---
...
tarantool> sock_1 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_1:bind('127.0.0.1')
---
- true
...
tarantool> sock_2 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_2:sendto('127.0.0.1', sock_1:name().port,'X')
---
- true
...
tarantool> message = sock_1:recvfrom()
---
...
tarantool> message
---
- X
...
tarantool> sock_1:close()
---
- true
...
tarantool> sock_2:close()
---
- true
... tarantool> strict = require('strict')
---
...
tarantool> strict.on()
---
...
tarantool> a = b -- strict mode is on so this will cause an error
---
- error: ... variable ''b'' is not declared'
...
tarantool> strict.off()
---
...
tarantool> a = b -- strict mode is off so this will not cause an error
---
... tarantool> taptest:ok(true, 'x')
ok - x
---
- true
...
tarantool> tap = require('tap')
---
...
tarantool> taptest = tap.test('test-name')
TAP version 13
---
...
tarantool> taptest:ok(1 + 1 == 2, 'X')
ok - X
---
- true
... tarantool> taptest:skip('message')
ok - message # skip
---
- true
... tarantool> tarantool = require('tarantool')
---
...
tarantool> tarantool
---
- build:
    target: Linux-x86_64-RelWithDebInfo
    options: cmake . -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_BACKTRACE=ON
    mod_format: so
    flags: ' -fno-common -fno-omit-frame-pointer -fno-stack-protector -fexceptions
      -funwind-tables -fopenmp -msse2 -std=c11 -Wall -Wextra -Wno-sign-compare -Wno-strict-aliasing
      -fno-gnu89-inline'
    compiler: /usr/bin/x86_64-linux-gnu-gcc /usr/bin/x86_64-linux-gnu-g++
  uptime: 'function: 0x408668e0'
  version: 1.7.0-66-g9093daa
  pid: 'function: 0x40866900'
...
tarantool> tarantool.pid()
---
- 30155
...
tarantool> tarantool.uptime()
---
- 108.64641499519
... tarantool> type(123456789012345), type(tonumber64(123456789012345))
---
- number
- number
...
tarantool> i = tonumber64('1000000000')
---
...
tarantool> type(i), i / 2, i - 2, i * 2, i + 2, i % 2, i ^ 2
---
- number
- 500000000
- 999999998
- 2000000000
- 1000000002
- 0
- 1000000000000000000
... tarantool> uuid = require('uuid')
---
...
tarantool> uuid(), uuid.bin(), uuid.str()
---
- 16ffedc8-cbae-4f93-a05e-349f3ab70baa
- !!binary FvG+Vy1MfUC6kIyeM81DYw==
- 67c999d2-5dce-4e58-be16-ac1bcb93160f
...
tarantool> uu = uuid()
---
...
tarantool> #uui:bin(), #uu:str(), type(uu), uu:isnil()
---
- 16
- 36
- cdata
- false
... tarantool> yaml = require('yaml')
---
...
tarantool> y = yaml.encode({'a', 1, 'b', 2})
---
...
tarantool> z = yaml.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> if yaml.NULL == nil then print('hi') end
hi
---
... tarantool> yaml = require('yaml')
---
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="sequence"}))
---
- |
  ---
  - A
  - B
  ...
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- |
  ---
  ['A', 'B']
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- |
  ---
  - {'f2': 'B', 'f1': 'A'}
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="mapping"})})
---
- |
  ---
  - f2: B
    f1: A
  ...
... task.worker_fiber = fiber.create(worker_loop, task)
log.info("expiration: task %q restarted", task.name)
...
fiber.sleep(expirationd.constants.check_interval)
... tcp_connect('127.0.0.1', 3301) tdb = require('tdb')
tdb.start() tdb = require('tdb')
tdb.start()
i = 1
j = 'a' .. i
print('end of program') teardown the :ref:`URI <index-uri>` of the target for the connection the URI of the local server the URI of the remote server the ``NO_SUCH_USER`` message is "``User '%s' is not found``" -- it includes one "``%s``" component which will be replaced with errtext. Thus a call to ``box.error(box.error.NO_SUCH_USER, 'joe')`` or ``box.error(45, 'joe')`` will result in an error with the accompanying message "``User 'joe' is not found``". the function to be associated with the fiber the id of the fiber to be cancelled. the message to be displayed. the new name of the fiber. the new position if success the number of bytes sent. the number of bytes that were decoded. the number of messages. the original contents formatted as a Lua table. the original contents formatted as a Lua table; the original value reformatted as a JSON string. the original value reformatted as a MsgPack string. the original value reformatted as a YAML string. the socket object value may change if sysconnect() succeeds. the specified fiber does not exist or cancel is not permitted. the status of ``fiber``. One of: “dead”, “suspended”, or “running”. the status of fiber. One of: “dead”, “suspended”, or “running”. the value placed on the channel by an earlier ``channel:put()``. timeout true true for success, false for error. true if blocked users are waiting. Otherwise false. true if connected, false on failure. true if success, false if failure. true if success, false on failure. true if the socket is now readable, false if timeout expired; true if the socket is now writable, false if timeout expired; true if the specified channel is already closed. Otherwise false. true if the specified channel is empty true if the specified channel is full (has no room for a new message). true if the value is all zero, otherwise false. true on success, false on error true on success, false on error. For example, if sock is already closed, sock:close() returns false. true or false. true when connected, false on failure. use Digest::CRC;
$d = Digest::CRC->new(width => 32, poly => 0x1EDC6F41, init => 0xFFFFFFFF, refin => 1, refout => 1);
$d->add('string');
print $d->digest; userdata username:password@host:port value to write vinyl = {
  run_age_wm = *number*,
  run_age_period = *number of seconds*,
  memory_limit = *number of gigabytes*,
  compact_wm = *number*,
  threads = *number*,
  run_age = *number*,
  run_prio = *number*,
} vinyl = {
  run_age_wm = 0,
  run_age_period = 0,
  memory_limit = 1,
  compact_wm = 2,
  threads = 5,
  run_age = 0,
  run_prio = 2,
} what to execute. what will be passed to function whatever is returned by the Lua code chunk. whatever is specified in errcode-number. where ``sql-statement`` is a string, and the optional ``parameters`` are extra values that can be plugged in to replace any question marks ("?"s) in the SQL statement. will cause the snapshot daemon to create a new snapshot each hour until it has created ten snapshots. After that, it will remove the oldest snapshot (and any associated write-ahead-log files) after creating a new one. zero or more scalar values which will be appended to, or substitute for, items in the Lua chunk. {} “Tarantool” is the name of the reusable asynchronous networking programming framework. “Target” is the platform tarantool was built on. Some platform-specific details may follow this line. Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-23 12:27+0300
PO-Revision-Date: 2016-09-22 21:19+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 "" "Crypto" is short for "Cryptography", which generally refers to the production of a digest value from a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's crypto module supports ten types of cryptographic hash functions (AES_, DES_, DSS_, MD4_, MD5_, MDC2_, RIPEMD_, SHA-0_, SHA-1_, SHA-2_). Some of the crypto functionality is also present in the :ref:`digest` module. The functions in crypto are: #!/usr/bin/env tarantool
box.cfg{
    listen              = os.getenv("LISTEN_URI"),
    slab_alloc_arena    = 0.1,
    pid_file            = "tarantool.pid",
    rows_per_wal        = 50
}
print('Starting ', arg[1]) #!/usr/bin/tarantool
local tap = require('tap')
test = tap.test("my test name")
test:plan(2)
test:ok(2 * 2 == 4, "2 * 2 is 4")
test:test("some subtests for test2", function(test)
    test:plan(2)
    test:is(2 + 2, 4, "2 + 2 is 4")
    test:isnt(2 + 3, 4, "2 + 3 is not 4")
end)
test:check() $ # Check that the include subdirectory exists
$ # by looking for /usr/include/postgresql/libpq-fe-h.
$ [ -f /usr/include/postgresql/libpq-fe.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the necessary .so file.
$ [ -f /usr/lib/x86_64-linux-gnu/libpq.so ] && echo "OK" || echo "Error"
OK

$ # Check that the psql client can connect using some factory defaults:
$ # port = 5432, user = 'postgres', user password = 'postgres',
$ # database = 'postgres'. These can be changed, provided one changes
$ # them in all places. Insert a row in database postgres, and quit.
$ psql -h 127.0.0.1 -p 5432 -U postgres -d postgres
Password for user postgres:
psql (9.3.10)
SSL connection (cipher: DHE-RSA-AES256-SHA, bits: 256)
Type "help" for help.

postgres=# CREATE TABLE test (s1 INT, s2 VARCHAR(50));
CREATE TABLE
postgres=# INSERT INTO test VALUES (1,'PostgreSQL row');
INSERT 0 1
postgres=# \q
$

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
        ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool master
$ # repository. The resultant display is normal for Ubuntu 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.7
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.7.0.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install pg POSTGRESQL_LIBDIR=/usr/lib/x86_64-linux-gnu --local
Installing http://rocks.tarantool.org/pg-scm-1.rockspec...
... (more info about building the Tarantool/PostgreSQL driver appears here)
pg scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/pg/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the
$ # name of this directory is $HOME/tarantool_sandbox.
$ # (Change "$HOME" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
cd $HOME/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-412-g803b15c
type 'help' for interactive help
tarantool> $ **tarantool**
# OR
$ **tarantool** *options*
# OR
$ **tarantool** *lua-initialization-file* **[** *arguments* **]** $ ./tarantool --version
Tarantool 1.7.0-1216-g73f7154
Target: Linux-x86_64-Debug
... $ :codebold:`tarantool example.lua` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`4: j = 'a' .. i`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`5: print('end of program')`
:codeblue:`(TDB)>` e
:codeblue:`(TDB)`  :codegreen:`Eval mode ON`
:codeblue:`(TDB)>` j
j       a1
:codeblue:`(TDB)>` -e
:codeblue:`(TDB)`  :codegreen:`Eval mode OFF`
:codeblue:`(TDB)>` q $ export LISTEN_URI=3301
$ ~/tarantool/src/tarantool script.lua ARG
... main/101/script.lua C> version 1.7.0-1216-g73f7154
... main/101/script.lua C> log level 5
... main/101/script.lua I> mapping 107374184 bytes for a shared arena...
... main/101/script.lua I> recovery start
... main/101/script.lua I> recovering from './00000000000000000000.snap'
... main/101/script.lua I> primary: bound to 0.0.0.0:3301
... main/102/leave_local_hot_standby I> ready to accept requests
Starting  ARG
... main C> entering the event loop $ export TMDIR=~/mysql-5.5
$ # Check that the include subdirectory exists by looking
$ # for .../include/mysql.h. (If this fails, there's a chance
$ # that it's in .../include/mysql/mysql.h instead.)
$ [ -f $TMDIR/include/mysql.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the
$ # necessary .so file.
$ [ -f $TMDIR/lib/libmysqlclient.so ] && echo "OK" || echo "Error"
OK

$ # Check that the mysql client can connect using some factory
$ # defaults: port = 3306, user = 'root', user password = '',
$ # database = 'test'. These can be changed, provided one uses
$ # the changed values in all places.
$ $TMDIR/bin/mysql --port=3306 -h 127.0.0.1 --user=root \
    --password= --database=test
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.35 MySQL Community Server (GPL)
...
Type 'help;' or '\h' for help. Type '\c' to clear ...

$ # Insert a row in database test, and quit.
mysql> CREATE TABLE IF NOT EXISTS test (s1 INT, s2 VARCHAR(50));
Query OK, 0 rows affected (0.13 sec)
mysql> INSERT INTO test.test VALUES (1,'MySQL row');
Query OK, 1 row affected (0.02 sec)
mysql> QUIT
Bye

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
    ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool
$ # master repository. The resultant display is normal for Ubuntu
$ # 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.6
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.6.6.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib --local
Installing http://rocks.tarantool.org/mysql-scm-1.rockspec...
... (more info about building the Tarantool/MySQL driver appears here)
mysql scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/mysql/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the name
$ # of this directory is /home/pgulutzan/tarantool_sandbox.
$ # (Change "/home/pgulutzan" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
$ cd /home/pgulutzan/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-222-g48b98bb
type 'help' for interactive help
tarantool> $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> cfg = {
         >   servers = {
         >       { uri = 'localhost:3301', zone = '1' },
         >   },
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3301;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' },
         >     { uri = 'localhost:3302', zone = '2' },
         >   },
         >   login = 'admin',
         >   password = 'password',
         >   redundancy = 1,
         >   binary = 3301,
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_2
$ cd ~/tarantool_sandbox_2
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3302}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' };
         >     { uri = 'localhost:3302', zone = '2' };
         >   };
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3302;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now get something out ...
tarantool> shard.tester:select{1} $ ps -ef | grep tarantool
1000     14939 14188  1 10:53 pts/2    00:00:13 tarantool <running> $ ps -ef | grep tarantool
1000     14939 14188  1 10:53 pts/2    00:00:16 tarantool <running>: sessions $ socat TCP:localhost:3302 ./tmp.txt $ ~/tarantool/src/tarantool
tarantool> box.cfg{log_level=3, logger='tarantool.txt'}
tarantool> log = require('log')
tarantool> log.error('Error')
tarantool> log.info('Info %s', box.info.version)
tarantool> os.exit()
$ less tarantool.txt 'R' if the socket is now readable, 'W' if the socket is now writable, 'RW' if the socket is now both readable and writable, '' (empty string) if timeout expired; '``SEEK_END``' = end of file, '``SEEK_CUR``' = current position, '``SEEK_SET``' = start of file. 'a' 'b' - big-endian, 'false' = c2 'fixmap' if metatable is 'map' = 80 otherwise 'fixarray' = 90 'fixmap(0)' = 80 -- nil is not stored when it is a missing map value 'fixmap(1)' + 'positive fixint' (for the key) + 'positive fixint' (for the value) = 81 00 05 'fixstr' = a1 61 'float 64' = cb 3f f8 00 00 00 00 00 00 'h' - endianness depends on host (default), 'l' - little-endian, 'n' - endianness depends on network 'nil' = c0 'positive fixint' = 7f 'true' = c3 'uint 16' = cd ff ff 'uint 32' = ce ff ff ff ff (the expected output is 3304160206). **Common Types and MsgPack Encodings** **Consistent Hash** **Example:** **Format specifiers** **List of error codes** **Logging example:** **Queue** **Redundancy** **Replica** **Result:** **Shard** **Socket functions** **Zone** **array** encoding: 92 a1 41 a1 42
**map** encoding:   82 01 a1 41 02 a1 42 *connection-name*:close() *connection-name*:execute(*sql-statement* [, *parameters*]) *connection-name*:ping() *connection_name* = mysql.connect(*connection options*) *connection_name* = pg.connect(*connection options*) -- Benchmark a function which sleeps 10 seconds.
-- NB: bench() will not calculate sleep time.
-- So the returned value will be {a number less than 10, 88}.
clock = require('clock')
fiber = require('fiber')
function f(param)
  fiber.sleep(param)
  return 88
end
clock.bench(f,10) -- Disassemble hexadecimal 97 which is the x86 code for xchg eax, edi
jit.dis_x86.disass('\x97') -- Disassemble hexadecimal 97 which is the x86-64 code for xchg eax, edi
jit.dis_x64.disass('\x97') -- Show the machine code of a Lua "for" loop
jit.dump.on('m')
local x = 0;
for i = 1, 1e6 do
  x = x + i
end
print(x)
jit.dump.off() -- Show what LuaJIT is doing for a Lua "for" loop
jit.v.on()
local x = 0
for i = 1, 1e6 do
    x = x + i
end
print(x)
jit.v.off() -- This will print an approximate number of years since 1970.
clock = require('clock')
print(clock.time() / (365*24*60*60)) -- This will print nanoseconds in the CPU since the start.
clock = require('clock')
print(clock.proc64()) -- This will print nanoseconds since the start.
clock = require('clock')
print(clock.monotonic64()) -- This will print seconds in the thread since the start.
clock = require('clock')
print(clock.thread64()) -- When nil is assigned to a Lua-table field, the field is null
tarantool> {nil, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a Lua-table field, the field is json.NULL
tarantool> {json.NULL, 'a', 'b'}
---
- - null
  - a
  - b
...
-- When json.NULL is assigned to a JSON field, the field is null
tarantool> json.encode({field2 = json.NULL, field1 = 'a', field3 = 'c'}
---
- '{"field2":null,"field1":"a","field3":"c"}'
... -- default process_expired_tuple function
local function default_tuple_drop(space_id, args, tuple)
    local key = fun.map(
        function(x) return tuple[x.fieldno] end,
        box.space[space_id].index[0].parts
    ):totable()
    box.space[space_id]:delete(key)
end ---
- - host: 188.93.56.70
    family: AF_INET
    type: SOCK_STREAM
    protocol: tcp
    port: 80
  - host: 188.93.56.70
    family: AF_INET
    type: SOCK_DGRAM
    protocol: udp
    port: 80
... 1 – ``SYSERROR`` 1.5 127 127.0.0.1:3301 16-byte binary string 16-byte string 2 – ``ERROR`` 2...0 [5257] main/101/interactive C> version 1.7.0-355-ga4f762d
2...1 [5257] main/101/interactive C> log level 3
2...1 [5261] main/101/spawner C> initialized
2...0 [5257] main/101/interactive [C]:-1 E> Error 2015-11-30 15:13:06.373 [27469] main/101/interactive I> Log Line #1`
2015-11-30 15:14:25.973 [27469] main/101/interactive I> Log Line #2` 3 – ``CRITICAL`` 3301 36-byte binary string 36-byte hexadecimal string 4 – ``WARNING`` 4294967295 5 – ``INFO`` 6 – ``DEBUG`` 65535 :codebold:`-e` :codebold:`bt` :codebold:`c` :codebold:`e` :codebold:`f` :codebold:`globals` :codebold:`h` :codebold:`locals` :codebold:`n` :codebold:`q` :ref:`csv.iterate() <csv-iterate>` is the low level of :ref:`csv.load() <csv-load>` and :ref:`csv.dump() <csv-dump>`. To illustrate that, here is a function which is the same as the :ref:`csv.load() <csv-load>` function, as seen in `the Tarantool source code`_. :ref:`io_collect_interval <cfg_networking-io_collect_interval>`, |br| :ref:`readahead <cfg_networking-readahead>`  |br| :ref:`panic_on_snap_error <cfg_binary_logging_snapshots-panic_on_snap_error>`, |br| :ref:`panic_on_wal_error <cfg_binary_logging_snapshots-panic_on_wal_error>`, |br| :ref:`rows_per_wal <cfg_binary_logging_snapshots-rows_per_wal>`, |br| :ref:`snap_io_rate_limit <cfg_binary_logging_snapshots-snap_io_rate_limit>`, |br| :ref:`wal_mode <cfg_binary_logging_snapshots-wal_mode>`, |br| :ref:`wal_dir_rescan_delay <cfg_binary_logging_snapshots-wal_dir_rescan_delay>` |br| :ref:`socket() <socket-socket>` :ref:`socket.getaddrinfo() <socket-getaddrinfo>` :ref:`socket.tcp_connect() <socket-tcp_connect>` :ref:`socket.tcp_server() <socket-tcp_server>` :ref:`socket_object:accept() <socket-accept>` :ref:`socket_object:close() <socket-close>` :ref:`socket_object:errno() <socket-error>` :ref:`socket_object:error() <socket-error>` :ref:`socket_object:getsockopt() <socket-getsockopt>` :ref:`socket_object:linger() <socket-linger>` :ref:`socket_object:listen() <socket-listen>` :ref:`socket_object:name() <socket-name>` :ref:`socket_object:nonblock() <socket-nonblock>` :ref:`socket_object:peer() <socket-peer>` :ref:`socket_object:read() <socket-read>` :ref:`socket_object:readable() <socket-readable>` :ref:`socket_object:recv() <socket-recv>` :ref:`socket_object:recvfrom() <socket-recvfrom>` :ref:`socket_object:send() <socket-send>` :ref:`socket_object:sendto() <socket-sendto>` :ref:`socket_object:setsockopt() <socket-setsockopt>` :ref:`socket_object:shutdown() <socket-shutdown>` :ref:`socket_object:sysconnect() <socket-sysconnect>` :ref:`socket_object:syswrite() <socket-syswrite>` :ref:`socket_object:wait() <socket-wait>` :ref:`socket_object:writable() <socket-writable>` :ref:`socket_object:write() <socket-send>` :ref:`uuid() <uuid-__call>` :ref:`uuid.bin() <uuid-bin>` :ref:`uuid.frombin() <uuid-frombin>` :ref:`uuid.fromstr() <uuid-fromstr>` :ref:`uuid.str() <uuid-str>` :ref:`uuid_object:bin() <uuid-object_bin>` :ref:`uuid_object:isnil() <uuid-isnil>` :ref:`uuid_object:str() <uuid-object_str>` :samp:`chunk-size = {number}` -- number of characters to read at once (usually for file-IO efficiency), default = 4096 :samp:`conn.space.{space-name}:delete(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:delete(...)`. :samp:`conn.space.{space-name}:get(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:get(...)`. :samp:`conn.space.{space-name}:insert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:insert(...)`. :samp:`conn.space.{space-name}:replace(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:replace(...)`. :samp:`conn.space.{space-name}:select`:code:`{...}` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:select`:code:`{...}`. :samp:`conn.space.{space-name}:update(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:update(...)`. :samp:`conn.space.{space-name}:upsert(...)` is the remote-call equivalent of the local call :samp:`box.space.{space-name}:upsert(...)`. :samp:`conn:eval({Lua-string})` evaluates and executes the expression in Lua-string, which may be any statement or series of statements. An :ref:`execute privilege <authentication-privileges>` is required; if the user does not have it, an administrator may grant it with :samp:`box.schema.user.grant({username}, 'execute', 'universe')`. :samp:`db = {database-name}` - string, default value is blank :samp:`delimiter = {string}` -- single-byte character to designate end-of-field, default = comma :samp:`host = {host-name}` - string, default value = 'localhost' :samp:`pass = {password}` or :samp:`password = {password}` - string, default value is blank :samp:`password = {password}` - string, default value is blank :samp:`port = {port-number}` - number, default value = 3306 :samp:`quote_char = {string}` -- single-byte character to designate encloser of string, default = quote mark :samp:`raise = {true|false}` - boolean, default value is false :samp:`skip_head_lines = {number}` -- number of lines to skip at the start (usually for a header), default 0 :samp:`user = {user-name}` - string, default value is operating-system user name :samp:`{function parameters}` = whatever values are required by the function. :samp:`{function}` = function or function reference; A "UUID" is a `Universally unique identifier`_. If an application requires that a value be unique only within a single computer or on a single database, then a simple counter is better than a UUID, because getting a UUID is time-consuming (it requires a syscall_). For clusters of computers, or widely distributed applications, UUIDs are better. A "digest" is a value which is returned by a function (usually a `Cryptographic hash function`_), applied against a string. Tarantool's digest module supports several types of cryptographic hash functions (AES_, MD4_, MD5_, SHA-0_, SHA-1_, SHA-2_) as well as a checksum function (CRC32_), two functions for base64_, and two non-cryptographic hash functions (guava_, murmur_). Some of the digest functionality is also present in the :ref:`crypto <crypto>` module. A complete copy of the data. The shard module handles both sharding and replication. One shard can contain one or more replicas. When a write occurs, the write is attempted on every replica in turn. The shard module does not use the built-in replication feature. A directory where database working files will be stored. The server switches to work_dir with :manpage:`chdir(2)` after start. Can be relative to the current directory. If not specified, defaults to the current directory. Other directory parameters may be relative to work_dir, for example |br| :codenormal:`box.cfg{work_dir='/home/user/A',wal_dir='B',snap_dir='C'}` |br| will put xlog files in /home/user/A/B, snapshot files in /home/user/A/C, and all other files or subdirectories in /home/user/A. A directory where snapshot (.snap) files will be stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. If not specified, defaults to work_dir. See also :ref:`wal_dir <cfg_basic-wal_dir>`. A directory where vinyl files or subdirectories will be stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. If not specified, defaults to work_dir. A directory where write-ahead log (.xlog) files are stored. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. Sometimes wal_dir and :ref:`snap_dir <cfg_basic-snap_dir>` are specified with different values, so that write-ahead log files and snapshot files can be stored on different disks. If not specified, defaults to work_dir. A duplicate key exists in a unique index. A fiber has all the features of a Lua coroutine_ and all the programming concepts that apply for Lua coroutines will apply for fibers as well. However, Tarantool has made some enhancements for fibers and has used fibers internally. So, although use of coroutines is possible and supported, use of fibers is recommended. A fiber is a set of instructions which are executed with cooperative multitasking. Fibers managed by the fiber module are associated with a user-supplied function called the *fiber function*. A fiber has three possible states: **running**, **suspended** or **dead**. When a fiber is created with :ref:`fiber.create() <fiber-create>`, it is running. When a fiber yields control with :ref:`fiber.sleep() <fiber-sleep>`, it is suspended. When a fiber ends (because the fiber function ends), it is dead. A list of strings or numbers. A method for parsing URIs is illustrated in :ref:`Cookbook recipes <cookbook-uri>`. A nil object A physical location where the nodes are closely connected, with the same security and backup and access points. The simplest example of a zone is a single computer with a single tarantool-server instance. A shard's replicas should be in different zones. A replica also binds to this port, and accepts connections, but these connections can only serve reads until the replica becomes a master. A runaway fiber can be stopped with :ref:`fiber_object.cancel <fiber_object-cancel>`. However, :ref:`fiber_object.cancel <fiber_object-cancel>` is advisory — it works only if the runaway fiber calls :ref:`fiber.testcancel() <fiber-testcancel>` occasionally. Most ``box.*`` functions, such as :ref:`box.space...delete() <box_space-delete>` or :ref:`box.space...update() <box_space-update>`, do call :ref:`fiber.testcancel() <fiber-testcancel>` but :ref:`box.space...select{} <box_space-select>` does not. In practice, a runaway fiber can only become unresponsive if it does many computations and does not check whether it has been cancelled. A special use of ``console.start()`` is with :ref:`initialization files <index-init_label>`. Normally, if one starts the tarantool server with :samp:`tarantool {initialization file}` there is no console. This can be remedied by adding these lines at the end of the initialization file: A subset of the tuples in the database partitioned according to the value returned by the consistent hash function. Usually each shard is on a separate node, or a separate set of nodes (for example if redundancy = 3 then the shard will be on three nodes). A table containing these fields: "host", "family", "type", "protocol", "port". A temporary list of recent update requests. Sometimes called "batching". Since updates to a sharded database can be slow, it may speed up throughput to send requests to a queue rather than wait for the update to finish on ever node. The shard module has functions for adding requests to the queue, which it will process without further intervention. Queuing is optional. A typical value is 3301. The listen parameter may also be set for local hot standby. A value comparable to Lua "nil" which may be useful as a placeholder in a tuple. Accept a new client connection and create a new connected socket. It is good practice to set the socket's blocking mode explicitly after accepting. Add the given string to the server's :ref:`Process title <administration-proctitle>` (what’s shown in the COMMAND column for :samp:`ps -ef` and :samp:`top -c` commands). Additionally one can see the uptime and the server version and the process id. Those information items can also be accessed with :ref:`box.info <box_introspection-box_info>` but use of the tarantool module is recommended. After :ref:`sleeping <fiber-sleep>` for two seconds, when the task has had time to do its iterations through the spaces, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 0". After sleeping for two more seconds, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 1". This shows that the is_tuple_expired() function eventually returned "true" for one of the tuples, because its timestamp field was more than three seconds old. After ``message_content, message_sender = recvfrom(1)`` the value of ``message_content`` might be a string containing 'X' and the value of ``message_sender`` might be a table containing All ``net.box`` methods are fiber-safe, that is, it is safe to share and use the same connection object across multiple concurrent fibers. In fact, it's perhaps the best programming practice with Tarantool. When multiple fibers use the same connection, all requests are pipelined through the same network socket, but each fiber gets back a correct response. Reducing the number of active sockets lowers the overhead of system calls and increases the overall server performance. There are, however, cases when a single connection is not enough — for example when it's necessary to prioritize requests or to use different authentication ids. All fibers are part of the fiber registry. This registry can be searched with :ref:`fiber.find() <fiber-find>` - via fiber id (fid), which is a numeric identifier. All remote calls support execution timeouts. Using a wrapper object makes the remote connection API compatible with the local one, removing the need for a separate ``timeout`` argument, which the local version would ignore. Once a request is sent, it cannot be revoked from the remote server even if a timeout expires: the timeout expiration only aborts the wait for the remote server response, not the request itself. Also, some MsgPack configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. Also, some YAML configuration settings for encoding can be changed, in the same way that they can be changed for :ref:`JSON <json-module_cfg>`. An error occurred during update of a field. An error occurred inside a Lua procedure. Another debugger example can be found here_. As well as executing Lua chunks or defining their own functions, you can exploit Tarantool's storage functionality with the ``box`` module and its submodules. At this point it is a good idea to check that the installation produced a file named ``driver.so``, and to check that this file is on a directory that is searched by the ``require`` request. At this point, if the above explanation is worthwhile, it's clear that ``expirationd.lua`` starts a background routine (fiber) which iterates through all the tuples in a space, sleeps cooperatively so that other fibers can operate at the same time, and - whenever it finds a tuple that has expired - deletes it from this space. Now the "``expirationd_run_task()``" function can be used in a test which creates sample data, lets the daemon run for a while, and prints results. Backtrace -- show the stack (in red), with program/function names and line numbers of whatever has been invoked to reach the current line. Basic parameters Begin by installing luarocks and making sure that tarantool is among the upstream servers, as in the instructions on `rocks.tarantool.org`_, the Tarantool luarocks page. Now execute this: Begin by making a ``require`` request for the mysql driver. We will assume that the name is ``mysql`` in further examples. Begin by making a ``require`` request for the pg driver. We will assume that the name is ``pg`` in further examples. Binary logging and snapshots Bind a socket to the given host/port. A UDP socket after binding can be used to receive data (see :ref:`socket_object.recvfrom <socket-recvfrom>`). A TCP socket can be used to accept new connections, after it has been put in listen mode. Справочник по встроенной библиотеке But if the configuration parameters include ``custom_proc_title='sessions'`` then the output looks like: By default strict mode is off, unless tarantool was built with the ``-DCMAKE_BUILD_TYPE=Debug`` option -- see the description of build options in section :ref:`building-from-source <building_from_source>`. By default, the log is sent to the standard error stream (``stderr``). If ``logger`` is specified, the log is sent to a file, or to a pipe, or to the system logger. By saying ``require('tarantool')``, one can answer some questions about how the tarantool server was built, such as "what flags were used", or "what was the version of the compiler". By setting log_level, one can enable logging of all classes below or equal to the given level. Tarantool prints its logs to the standard error stream by default, but this can be changed with the :ref:`logger <cfg_logging-logger>` configuration parameter. CSV-table has 3 fields, field#2 has "," so result has quote marks Call ``fiber.channel()`` to allocate space and get a channel object, which will be called channel for examples in this section. Call the other ``fiber-ipc`` routines, via channel, to send messages, receive messages, or check ipc status. Message exchange is synchronous. The channel is garbage collected when no one is using it, as with any other Lua object. Use object-oriented syntax, for example ``channel:put(message)`` rather than ``fiber.channel.put(message)``. Call ``require('net.box')`` to get a ``net.box`` object, which will be called ``net_box`` for examples in this section. Call ``net_box.new()`` to connect and get a connection object, which will be called ``conn`` for examples in this section. Call the other ``net.box()`` routines, passing ``conn:``, to execute requests on the remote box. Call :ref:`conn:close <socket-close>` to disconnect. Can't modify data on a replication slave. Cancel a fiber. Running and suspended fibers can be cancelled. After a fiber has been cancelled, attempts to operate on it will cause errors, for example :ref:`fiber_object:id() <fiber_object-id>` will cause ``error: the fiber is dead``. Change the fiber name. By default the Tarantool server's interactive-mode fiber is named 'interactive' and new fibers created due to :ref:`fiber.create <fiber-create>` are named 'lua'. Giving fibers distinct names makes it easier to distinguish them when using :ref:`fiber.info <fiber-info>`. Change the size of an open file. Differs from ``fio.truncate``, which changes the size of a closed file. Check if the current fiber has been cancelled and throw an exception if this is the case. Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line: Check whether the first argument equals the second argument. Displays extensive message if the result is false. Check whether the specified channel is empty (has no messages). Check whether the specified channel is empty and has readers waiting for a message (because they have issued ``channel:get()`` and then blocked). Check whether the specified channel is full and has writers waiting (because they have issued ``channel:put()`` and then blocked due to lack of room). Check whether the specified channel is full. Checks the number of tests performed. This check should only be done after all planned tests are complete, so ordinarily ``taptest:check()`` will only appear at the end of a script. Clears the record of errors, so functions like `box.error()` or `box.error.last()` will have no effect. Close (destroy) a socket. A closed socket should not be used any more. A socket is closed automatically when its userdata is garbage collected by Lua. Close a connection. Close a file that was opened with ``fio.open``. For details type "man 2 close". Close the channel. All waiters in the channel will be woken up. All following ``channel:put()`` or ``channel:get()`` operations will return an error (``nil``). Closing connection Опции комнандной строки Commas designate end-of-field, Common file manipulations Common pathname manipulations Concatenate partial string, separated by '/' to form a path name. Параметры конфигурации Configuration parameters have the form: |br| :extsamp:`{**{box.cfg}**}{[{*{key = value}*} [, {*{key = value ...}*}]]}` Справочник по настройке Configuration settings Configure tarantool and load mysql module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configure tarantool and load pg module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configuring the storage Connect a socket to a remote host. Connect an existing socket to a remote host. The argument values are the same as in tcp_connect(). The host must be an IP address. Connect to the server at :ref:`URI <index-uri>`, change the prompt from ':samp:`tarantool>`' to ':samp:`{uri}>`', and act henceforth as a client until the user ends the session or types :code:`control-D`. Connecting Connection objects are garbage collected just like any other objects in Lua, so an explicit destruction is not mandatory. However, since close() is a system call, it is good programming practice to close a connection explicitly when it is no longer needed, to avoid lengthy stalls of the garbage collector. Continue till next breakpoint or till program ends. Convert a JSON string to a Lua object. Convert a Lua object to a JSON string. Convert a Lua object to a MsgPack string. Convert a Lua object to a YAML string. Convert a MsgPack string to a Lua object. Convert a YAML string to a Lua object. Convert a string or a Lua number to a 64-bit integer. The result can be used in arithmetic, and the arithmetic will be 64-bit integer arithmetic rather than floating-point arithmetic. (Operations on an unconverted Lua number use floating-point arithmetic.) The ``tonumber64()`` function is added by Tarantool; the name is global. Counterpart to ``pickle.pack()``. Warning: if format specifier 'A' is used, it must be the last item. Create a Lua function that will connect to the MySQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a Lua function that will connect to the PostgreSQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a new TCP or UDP socket. The argument values are the same as in the `Linux socket(2) man page <http://man7.org/linux/man-pages/man2/socket.2.html>`_. Create a new communication channel. Create a new connection. The connection is established on demand, at the time of the first request. It is re-established automatically after a disconnect. The returned ``conn`` object supports methods for making remote requests, such as select, update or delete. Create and start a fiber. The fiber is created and begins to run immediately. Create or delete a directory. For details type "man 2 mkdir" or "man 2 rmdir". Коды ошибок от базы данных Debugger Commands Debugger prompts are blue, debugger hints and information are green, and the current line -- line 3 of example.lua -- is the default color. Now enter six debugger commands: Default values are: Deprecated. Do not use. Details are on `the shard section of github`_. Display a diagnostic message. Display a list of debugger commands. Display names and values of variables, for example the control variables of a Lua "for" statement. Display names of variables or functions which are defined as global. Display the fiber id, the program name, and the percentage of memory used, as a table. ER_FIBER_STACK ER_ILLEGAL_PARAMS ER_KEY_PART_COUNT ER_MEMORY_ISSUE ER_NONMASTER ER_NO_SUCH_INDEX ER_NO_SUCH_SPACE ER_PROC_LUA ER_TUPLE_FOUND ER_UPDATE_FIELD ER_WAL_IO Either: Emulate a request error, with text based on one of the pre-defined Tarantool errors defined in the file `errcode.h <https://github.com/tarantool/tarantool/blob/1.7/src/box/errcode.h>`_ in the source tree. Lua constants which correspond to those Tarantool errors are defined as members of ``box.error``, for example ``box.error.NO_SUCH_USER == 45``. Ensure that changes are written to disk. For details type "man 2 sync". Ensure that file changes are written to disk, for an open file. Compare ``fio.sync``, which is for all files. For details type "man 2 fsync" or "man 2 fdatasync". Enter evaluation mode. When the program is in evaluation mode, one can execute certain Lua statements that would be valid in the context. This is particularly useful for displaying the values of the program's variables. Other debugger commands will not work until one exits evaluation mode by typing :codebold:`-e`. Every data-access function in the box module has an analogue in the shard module, so (for example) to insert in table T in a sharded database one simply says ``shard.T:insert{...}`` instead of ``box.space.T:insert{...}``. A ``shard.T:select{}`` request without a primary key will search all shards. Every queued data-access function has an analogue in the shard module. The user must add an operation_id. The details of queued data-access functions, and of maintenance-related functions, are on `the shard section of github`_. Example Example Of Fiber Use Example Session Example setting: Example showing use of most of the net.box methods Example, creating a function which sets each option in a separate line: Example, using a table literal enclosed in {braces}: Example: Shard, Minimal Configuration Example: Shard, Scaling Out Example: shard.init syntax for one shard Example: shard.init syntax for three shards Execute a PING command. Execute a function, provided it has not been executed before. A passed value is checked to see whether the function has already been executed. If it has been executed before, nothing happens. If it has not been executed before, the function is invoked. For an explanation why ``box.once`` is useful, see the section :ref:`Preventing Duplicate Actions <index-preventing_duplicate_actions>`. Execute by passing to the shell. Execute these requests: Executing a statement Exit evaluation mode. Exit the program. If this is done on the server, then the server stops. Failed to write to disk. May mean: failed to record a change in the write-ahead log. Some sort of disk error. Fetch a message from a channel. If the channel is empty, ``channel:get()`` blocks until there is a message. Find out how many messages are on the channel. The answer is 0 if the channel is empty. First some terminology: Flags can be passed as a number or as string constants, for example '``O_RDONLY``', '``O_WRONLY``', '``O_RDWR``'. Flags can be combined by enclosing them in braces. For a commercial-grade example of a Lua rock that works with Tarantool, let us look at expirationd, which Tarantool supplies on GitHub_ with an Artistic license. The expirationd.lua program is lengthy (about 500 lines), so here we will only highlight the matters that will be enhanced by studying the full source later. For a list of available options, read `the source code of bc.lua`_. For a list of available options, read `the source code of dis_x64.lua`_. For a list of available options, read `the source code of dis_x86.lua`_. For a list of available options, read `the source code of dump.lua`_. For a list of available options, read `the source code of v.lua`_. For all MySQL statements, the request is: For all PostgreSQL statements, the request is: For all examples in this section the socket name will be sock and the function invocations will look like ``sock:function_name(...)``. For example, in Python, install the ``crcmod`` package and say: For example, ordinarily :samp:`ps -ef` shows the Tarantool server process thus: For example, the following code will interpret 0/0 (which is "not a number") and 1/0 (which is "infinity") as special values rather than nulls or errors: For example: For example: ``box.cfg{snapshot_period=3600}`` will cause the snapshot daemon to create a new database snapshot once per hour. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/mysql`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/pg`_. For more information on, read article about `Encryption Modes`_ For the local tarantool server there is a pre-created always-established connection object named :samp:`{net_box}.self`. Its purpose is to make polymorphic use of the ``net_box`` API easier. Therefore :samp:`conn = {net_box}.new('localhost:3301')` can be replaced by :samp:`conn = {net_box}.self`. However, there is an important difference between the embedded connection and a remote one. With the embedded connection, requests which do not modify data do not yield. When using a remote connection, due to :ref:`the implicit rules <atomic-the_implicit_yield_rules>` any request can yield, and database state may have changed by the time it regains control. For those who like to see things run, here are the exact steps to get expirationd through the test. Form a Lua iterator function for going through CSV records one field at a time. Use of an iterator is strongly recommended if the amount of data is large (ten or more megabytes). Four choices of block cipher modes are also available: From a user's point of view the MySQL and PostgreSQL rocks are very similar, so the following sections -- "MySQL Example" and "PostgreSQL Example" -- contain some redundancy. Function `box.once` Functions to create and delete links. For details type "man readlink", "man 2 link", "man 2 symlink", "man 2 unlink".. Get CSV-formatted input from ``readable`` and return a table as output. Usually ``readable`` is either a string or a file opened for reading. Usually :samp:`{options}` is not specified. Get ``expirationd.lua``. There are standard ways - it is after all part of a `standard rock <https://luarocks.org/modules/rtsisyk/expirationd>`_  - but for this purpose just copy the contents of expirationd.lua_ to a default directory. Get environment variable. Get socket flags. For a list of possible flags see ``sock:setsockopt()``. Get table input from ``csv-table`` and return a CSV-formatted string as output. Or, get table input from ``csv-table`` and put the output in ``writable``. Usually :samp:`{options}` is not specified. Usually ``writable``, if specified, is a file opened for writing. :ref:`csv.dump() <csv-dump>` is the reverse of :ref:`csv.load() <csv-load>`. Get the id of the fiber (fid), to be used in later displays. Getting the same results from digest and crypto modules Given a full path name, remove all but the final part (the file name). Also remove the suffix, if it is passed. Given a full path name, remove the final part (the file name). Go the site `github.com/tarantool/mysql`_. Follow the instructions there, saying: Go the site `github.com/tarantool/pg`_. Follow the instructions there, saying: Go to the next line, skipping over any function calls. Here are examples for all the common types, with the Lua-table representation on the left, with the MsgPack format name and encoding on the right. Here is an example of the tcp_server function, reading strings from the client and printing them. On the client side, the Linux socat utility will be used to ship a whole file for the tcp_server function to read. Here is an example with datagrams. Set up two connections on 127.0.0.1 (localhost): ``sock_1`` and ``sock_2``. Using ``sock_2``, send a message to ``sock_1``. Using ``sock_1``, receive a message. Display the received message. Close both connections. |br| This is not a useful way for a computer to communicate with itself, but shows that the system works. How many log records to store in a single write-ahead log file. When this limit is reached, Tarantool creates another WAL file named :samp:`{<first-lsn-in-wal>}.xlog`. This can be useful for simple rsync-based backups. How much memory Tarantool allocates to actually store tuples, in gigabytes. When the limit is reached, INSERT or UPDATE requests begin failing with error :errcode:`ER_MEMORY_ISSUE`. While the server does not go beyond the defined limit to allocate tuples, there is additional memory used to store indexes and connection information. Depending on actual configuration and workload, Tarantool can consume up to 20% more than the limit set here. How to ping How verbose the logging is. There are six log verbosity classes: However, because not all platforms are alike, for this example the assumption is that the user must check that the appropriate PostgreSQL files are present and must explicitly state where they are when building the Tarantool/PostgreSQL driver. One can use ``find`` or ``whereis`` to see what directories PostgreSQL files are installed in. If ``logger_nonblock`` equals true, Tarantool does not block on the log file descriptor when it’s not ready for write, and drops the message instead. If :ref:`log_level <cfg_logging-log_level>` is high, and a lot of messages go to the log file, setting ``logger_nonblock`` to true may improve logging performance at the cost of some log messages getting lost. If a later user calls the ``password_check()`` function and enters the wrong password, the result is an error. If one cuts and pastes the above, then the result, showing only the requests and responses for shard.init and shard.tester, should look approximately like this: If one of the URIs is "self" -- that is, if one of the URIs is for the same server that :codenormal:`box.cfg{}` is being executed on -- then it is ignored. Thus it is possible to use the same replication_source specification on multiple servers. If one wishes to start an interactive session on the same terminal after initialization is complete, one can use :ref:`console.start() <console-start>`. If processing a request takes longer than the given value (in seconds), warn about it in the log. Has effect only if :ref:`log_level <cfg_logging-log_level>` is more than or equal to 4 (WARNING). If replication_source is not an empty string, the server is considered to be a Tarantool :ref:`replica <index-box_replication>`. The replica server will try to connect to the master which replication_source specifies with a :ref:`URI <index-uri>` (Universal Resource Identifier), for example :samp:`{konstantin}:{secret_password}@{tarantool.org}:{3301}`. If the Tarantool server at :samp:`uri` requires authentication, the connection might look something like: :code:`console.connect('admin:secretpassword@distanthost.com:3301')`. If the ``logger`` string has the prefix "syslog:", then the string is interpreted as a message for the `syslogd <http://www.rfc-base.org/txt/rfc-5424.txt>`_ program which normally is running in the background of any Unix-like platform. One can optionally specify an ``identity``, a ``facility``, or both. The ``identity`` is an arbitrary string, default value = ``tarantool``, which will be placed at the beginning of all messages. The facility is an abbreviation for the name of one of the `syslog <https://en.wikipedia.org/wiki/Syslog>`_ facilities, default value = ``user``, which tell syslogd where the message should go. If the command to start Tarantool includes :codeitalic:`lua-initialization-file`, then Tarantool begins by invoking the Lua program in the file, which by convention may have the name "``script.lua``". The Lua program may get further arguments from the command line or may use operating-system functions, such as ``getenv()``. The Lua program almost always begins by invoking ``box.cfg()``, if the database server will be used or if ports need to be opened. For example, suppose ``script.lua`` contains the lines If there is an error while reading a write-ahead log file (at server start or to relay to a replica), abort. If there is an error while reading the snapshot file (at server start), abort. If there is more than one replication source in a cluster, specify an array of URIs, for example |br| :codenormal:`box.cfg{replication_source = {`:codeitalic:`uri#1,uri#2`:codenormal:`}}` |br| If timeout is provided, and the channel doesn't become empty for the duration of the timeout, ``channel:put()`` returns false. Otherwise it returns true. Illegal parameters. Malformed protocol message. In Perl, install the ``Digest::CRC`` module and run the following code: In certain circumstances a Unix domain socket may be used where a URI is expected, for example "unix/:/tmp/unix_domain_socket.sock" or simply "/tmp/unix_domain_socket.sock". In the current version of the binary protocol, error message, which is normally more descriptive than error code, is not present in server response. The actual message may contain a file name, a detailed reason or operating system error code. All such messages, however, are logged in the error log. Below follow only general descriptions of some popular codes. A complete list of errors can be found in file `errcode.h`_ in the source tree. In the following example, the user creates two functions, ``password_insert()`` which inserts a SHA-1_ digest of the word "**^S^e^c^ret Wordpass**" into a tuple set, and ``password_check()`` which requires input of a password. In this example a connection is made over the internet between the Tarantool server and tarantool.org, then an HTTP "head" message is sent, and a response is received: "``HTTP/1.1 200 OK``". This is not a useful way to communicate with this particular site, but shows that the system works. In this example: Incremental methods in the crypto module Incremental methods in the digest module Indicate how many tests will be performed. Файл инициализации Initialize. Initiates incremental MurmurHash. See :ref:`incremental methods <digest-incremental_digests>` notes. Initiates incremental crc32. See :ref:`incremental methods <digest-incremental_digests>` notes. Installation It is not supplied as part of the Tarantool repository; it must be installed separately. Here is the usual way: It will be necessary to install Tarantool's MySQL driver shared library, load it, and use it to connect to a MySQL server. After that, one can pass any MySQL statement to the server and receive results, including multiple result sets. It will be necessary to install Tarantool's PostgreSQL driver shared library, load it, and use it to connect to a PostgreSQL server. After that, one can pass any PostgreSQL statement to the server and receive results. Key part count is not the same as index part count Leading or trailing spaces are ignored, Like all Lua objects, dead fibers are garbage collected. The garbage collector frees pool allocator memory owned by the fiber, resets all fiber data, and returns the fiber (now called a fiber carcass) to the fiber pool. The carcass can be reused when another fiber is created. Line feeds, or line feeds plus carriage returns, designate end-of-record, Listen on :ref:`URI <index-uri>`. The primary way of listening for incoming requests is via the connection-information string, or URI, specified in :code:`box.cfg{listen=...}`. The alternative way of listening is via the URI specified in :code:`console.listen(...)`. This alternative way is called "administrative" or simply :ref:`"admin port" <administration-admin_ports>`. The listening is usually over a local host with a Unix domain socket. Local storage within the fiber. The storage can contain any number of named values, subject to memory limitations. Naming may be done with :samp:`{fiber_object}.storage.{name}` or :samp:`{fiber_object}.storage['{name}'].` or with a number :samp:`{fiber_object}.storage[{number}]`. Values may be either numbers or strings. The storage is garbage-collected when :samp:`{fiber_object}:cancel()` happens. Locate a fiber by its numeric id and cancel it. In other words, :ref:`fiber.kill() <fiber-kill>` combines :ref:`fiber.find() <fiber-find>` and :ref:`fiber_object:cancel() <fiber_object-cancel>`. Logging Lua `escape sequences`_ such as \\n or \\10 are legal within strings but not within files, Lua code Lua fun, also known as the Lua Functional Library, takes advantage of the features of LuaJIT to help users create complex functions. Inside the module are "sequence processors" such as map, filter, reduce, zip -- they take a user-written function as an argument and run it against every element in a sequence, which can be faster or more convenient than a user-written loop. Inside the module are "generators" such as range, tabulate, and rands -- they return a bounded or boundless series of values. Within the module are "reducers", "filters", "composers" ... or, in short, all the important features found in languages like Standard ML, Haskell, or Erlang. Lua iterator function Make a fiber, associate function_x with the fiber, and start function_x. It will immediately "detach" so it will be running independently of the caller. Make the function which will be associated with the fiber. This function contains an infinite loop (``while 0 == 0`` is always true). Each iteration of the loop adds 1 to a global variable named gvar, then goes to sleep for 2 seconds. The sleep causes an implicit :ref:`fiber.yield() <fiber-yield>`. Manage the rights to file objects, or ownership of file objects. For details type "man 2 chown" or "man 2 chmod". Miscellaneous Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits are significant if flags include `O_CREATE` or `O_TMPFILE`. Mode bits can be combined by enclosing them in braces. Mode bits can be passed as a number or as string constants, for example ''`S_IWUSR`". Mode bits can be combined by enclosing them in braces. Module `box` Module `clock` Module `console` Module `crypto` Module `csv` Module `digest` Module `expirationd` Module `fiber` Module `fio` Module `fun` Module `jit` Module `json` Module `log` Module `msgpack` Module `net.box` Module `os` Module `pickle` Module `shard` Module `socket` Module `strict` Module `tap` Module `tarantool` Module `tdb` Module `uuid` Module `yaml` Most configuration parameters are for allocating resources, opening ports, and specifying database behavior. All parameters are optional. A few parameters are dynamic, that is, they can be changed at runtime by calling ``box.cfg{}`` a second time. MySQL Example N Names Networking Now start Tarantool, using example.lua as the initialization file Now watch what happens on the first shell. The strings "A", "B", "C" are printed. Now, for the MySQL driver shared library, there are two ways to install: Now, for the PostgreSQL driver shared library, there are two ways to install: Now, say: Number of seconds between periodic scans of the write-ahead-log file directory, when checking for changes to write-ahead-log files for the sake of replication or local hot standby. Observe the result. It contains "MySQL row". So this is the row that was inserted into the MySQL database. And now it's been selected with the Tarantool client. Observe the result. It contains "PostgreSQL row". So this is the row that was inserted into the PostgreSQL database. And now it's been selected with the Tarantool client. Of course, expirationd can be customized to do different things by passing different parameters, which will be evident after looking in more detail at the source code. On Linux the listen ``backlog`` backlog may be from /proc/sys/net/core/somaxconn, on BSD the backlog may be ``SOMAXCONN``. On Terminal #1, say: On Terminal #1: put a message "Log Line #2" in the log file. |br| On Terminal #1: put a message "Log Line #3" in the log file. On Terminal #1: start an interactive Tarantool session, then say the logging will go to `Log_file`, then put a message "Log Line #1" in the log file: On Terminal #2, say: On Terminal #2: use ``kill -HUP`` to send a SIGHUP signal to the Tarantool server. The result of this is: Tarantool will open `Log_file` again, and the next log message will go to `Log_file`. (The same effect could be accomplished by executing log.rotate() on the server.) |br| On Terminal #2: use ``less`` to examine files. `Log_file.bak` will have these lines, except that the date and time will depend on when the example is done: On Terminal #2: use ``mv`` so the log file is now named `Log_file.bak`. The result of this is: the next log message will go to `Log_file.bak`. |br| On Terminal #2: use ``ps`` to find the process ID of the Tarantool server. |br| On the first shell, start Tarantool and say: On the second shell, create a file that contains a few lines. The contents don't matter. Suppose the first line contains A, the second line contains B, the third line contains C. Call this file "tmp.txt". On the second shell, use the socat utility to ship the tmp.txt file to the server's host and port: Open a file in preparation for reading or writing or seeking. Or, download from github tarantool/shard and compile as described in the README. Then, before using the module, say ``shard = require('shard')`` Or: Out of memory: :ref:`slab_alloc_arena <cfg_storage-slab_alloc_arena>` limit has been reached. Output a user-generated message to the :ref:`log file <cfg_logging-logger>`, given log_level_function_name = ``error`` or ``warn`` or ``info`` or ``debug``. Parameters: Parameters: (string) format-string = instructions; (string) time-since-epoch = number of seconds since 1970-01-01. If time-since-epoch is omitted, it is assumed to be the current time. Parameters: (string) name = name of file or directory which will be removed. Parameters: (string) variable-name = environment variable name. Parse and execute an arbitrary chunk of Lua code. This function is mainly useful to define and run Lua code without having to introduce changes to the global Lua environment. Pass or return a cipher derived from the string, key, and (optionally, sometimes) initialization vector. The four choices of algorithms: Pass or return a digest derived from the string. The twelve choices of algorithms: Pause for a while, while the detached function runs. Then ... Cancel the fiber. Then, once again ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit more depending how long the pause lasted). This time the status is dead because the cancel worked. Pause for a while, while the detached function runs. Then ... Display the fiber id, the fiber status, and gvar (gvar will have gone up a bit depending how long the pause lasted). The status is suspended because the fiber spends almost all its time sleeping or yielding. Perform non-random-access read or write on a file. For details type "man 2 read" or "man 2 write". Perform read/write random-access operation on a file, without affecting the current seek position of the file. For details type "man 2 pread" or "man 2 pwrite". Possible Errors: Redundancy should not be greater than the number of servers; the servers must be alive; two replicas of the same shard should not be in the same zone. Possible errors: If there is a compilation error, it is raised as a Lua error. Possible errors: On error, returns an empty string, followed by status, errno, errstr. In case the writing side has closed its end, returns the remainder read from the socket (possibly an empty string), followed by "eof" status. Possible errors: Returns nil, status, errno, errstr on error. Possible errors: cancel is not permitted for the specified fiber object. Possible errors: nil on error. Possible errors: nil. Possible errors: on error, returns status, errno, errstr. Possible errors: the connection will fail if the target Tarantool server was not initiated with :code:`box.cfg{listen=...}`. Possible errors: unknown format specifier. Possible values for ``facility`` are: auth, authpriv, cron, daemon, ftp, kern, lpr, mail, news, security, syslog, user, uucp, local0, local1, local2, local3, local4, local5, local6, local7. PostgreSQL Example Print an annotated list of all available options and exit. Print product name and version, for example: Prints a trace of LuaJIT's progress compiling and interpreting code Prints the byte code of a function. Prints the i386 assembler code of a string of bytes Prints the intermediate or machine code of following Lua code Prints the x86-64 assembler code of a string of bytes Purposes Put the following program in a default directory and call it "example.lua": Put the server in read-only mode. After this, any requests that try to change data will fail with error ER_READONLY. Quit immediately. Quote marks may enclose fields or parts of fields, Read ``size`` bytes from a connected socket. An internal read-ahead buffer is used to reduce the cost of this call. Read from a connected socket until some condition is true, and return the bytes that were read. Reading goes on until ``limit`` bytes have been read, or a delimiter has been read, or a timeout has expired. Readable file :file:`./file.csv` contains two CSV records. Explanation of fio is in section :ref:`fio <fio-section>`. Source CSV file and example respectively: Readable string contains 2-byte character = Cyrillic Letter Palochka: (This displays a palochka if and only if character set = UTF-8.) Readable string has 3 fields, field#2 has comma and space so use quote marks: Receive a message on a UDP socket. Recursive version of ``taptest:is(...)``, which can be be used to compare tables as well as scalar values. Reduce file size to a specified value. For details type "man 2 truncate". Reduce the throttling effect of :ref:`box.snapshot <admin-snapshot>` on INSERT/UPDATE/DELETE performance by setting a limit on how many megabytes per second it can write to disk. The same can be achieved by splitting :ref:`wal_dir <cfg_basic-wal_dir>` and :ref:`snap_dir <cfg_basic-snap_dir>` locations and moving snapshots to a separate disk. Справочники Remove file or directory. Rename a file or directory. Rename a file or directory. For details type "man 2 rename". Replication Retrieve information about the last error that occurred on a socket, if any. Errors do not cause throwing of exceptions so these functions are usually necessary. Return a formatted date. Return a list of files that match an input string. The list is constructed with a single flag that controls the behavior of the function: GLOB_NOESCAPE. For details type "man 3 glob". Return a name for a temporary file. Return all available data from the socket buffer if non-blocking. Rarely used. For details see `this description`_. Return information about all fibers. Return statistics about an open file. This differs from ``fio.stat`` which return statistics about a closed file. For details type "man 2 stat". Return the name of a directory that can be used to store temporary files. Return the name of the current working directory. Return the number of CPU seconds since the program start. Return the number of seconds since the epoch. Return the status of the current fiber. Return the status of the specified fiber. Returns 128-bit binary string = digest made with MD4. Returns 128-bit binary string = digest made with MD5. Returns 128-byte string = hexadecimal of a digest calculated with sha512. Returns 160-bit binary string = digest made with SHA-0.|br| Not recommended. Returns 160-bit binary string = digest made with SHA-1. Returns 224-bit binary string = digest made with SHA-2. Returns 256-bit binary string =  digest made with SHA-2. Returns 256-bit binary string = digest made with AES. Returns 32-bit binary string = digest made with MurmurHash. Returns 32-bit checksum made with CRC32. Returns 32-byte string = hexadecimal of a digest calculated with md4. Returns 32-byte string = hexadecimal of a digest calculated with md5. Returns 384-bit binary string =  digest made with SHA-2. Returns 40-byte string = hexadecimal of a digest calculated with sha. Returns 40-byte string = hexadecimal of a digest calculated with sha1. Returns 512-bit binary tring = digest made with SHA-2. Returns 56-byte string = hexadecimal of a digest calculated with sha224. Returns 64-byte string = hexadecimal of a digest calculated with sha256. Returns 96-byte string = hexadecimal of a digest calculated with sha384. Returns a description of the last error, as a Lua table with five members: "line" (number) Tarantool source file line number, "code" (number) error's number, "type", (string) error's C++ class, "message" (string) error's message, "file" (string) Tarantool source file. Additionally, if the error is a system error (for example due to a failure in socket or file io), there may be a sixth member: "errno" (number) C standard error number. Returns a number made with consistent hash. Returns a regular string from a base64 encoding. Returns array of random bytes with length = integer. Returns base64 encoding from a regular string. Returns information about a file object. For details type "man 2 lstat" or "man 2 stat". Справочник по сторонним библиотекам Round Trip: from string to table and back to string Run the server as a background task. The :ref:`logger <cfg_logging-logger>` and :ref:`pid_file <cfg_basic-pid_file>` parameters must be non-null for this to work. SO_ACCEPTCONN SO_BINDTODEVICE SO_BROADCAST SO_DEBUG SO_DOMAIN SO_DONTROUTE SO_ERROR SO_KEEPALIVE SO_MARK SO_OOBINLINE SO_PASSCRED SO_PEERCRED SO_PRIORITY SO_PROTOCOL SO_RCVBUF SO_RCVBUFFORCE SO_RCVLOWAT SO_RCVTIMEO SO_REUSEADDR SO_SNDBUF SO_SNDBUFFORCE SO_SNDLOWAT SO_SNDTIMEO SO_TIMESTAMP SO_TYPE Модули SQL DBMS See also :ref:`Modules <modules>`. See also :ref:`box.session.storage <box_session-storage>`. See also :ref:`fiber.time64 <fiber-time64>` and :ref:`os.clock() <os-clock>`. Semicolon instead of comma for the delimiter: Send a message on a UDP socket to a specified host. Send a message using a channel. If the channel is full, ``channel:put()`` blocks until there is a free slot in the channel. Send data over a connected socket. Serializing 'A' and 'B' with different ``__serialize`` values causes different results. To show this, here is a routine which encodes `{'A','B'}` both as an array and as a map, then displays each result in hexadecimal. Serializing 'A' and 'B' with different ``__serialize`` values causes different results: Set or clear the SO_LINGER flag. For a description of the flag, see the `Linux man page <http://man7.org/linux/man-pages/man1/loginctl.1.html>`_. Set socket flags. The argument values are the same as in the `Linux getsockopt(2) man page <http://man7.org/linux/man-pages/man2/setsockopt.2.html>`_. The ones that Tarantool accepts are: Set the auto-completion flag. If auto-completion is `true`, and the user is using tarantool as a client, then hitting the TAB key may cause tarantool to complete a word automatically. The default auto-completion value is `true`. Set the mask bits used when creating files or directories. For a detailed description type "man 2 umask". Setting SO_LINGER is done with ``sock:linger(active)``. Shift position in the file to the specified position. For details type "man 2 seek". Show whether connection is active or closed. Shutdown a reading end, a writing end, or both ends of a socket. Since ``box.cfg`` may contain many configuration parameters and since some of the parameters (such as directory addresses) are semi-permanent, it's best to keep ``box.cfg`` in a Lua file. Typically this Lua file is the initialization file which is specified on the tarantool command line. Size of the largest allocation unit. It can be increased if it is necessary to store large tuples. Size of the smallest allocation unit. It can be decreased if most of the tuples are very small. The value must be between 8 and 1048280 inclusive. Snapshot daemon Some configuration parameters and some functions depend on a URI, or "Universal Resource Identifier". The URI string format is similar to the `generic syntax for a URI schema <http://en.wikipedia.org/wiki/URI_scheme#Generic_syntax>`_. So it may contain (in order) a user name for login, a password, a host name or host IP address, and a port number. Only the port number is always mandatory. The password is mandatory if the user name is specified, unless the user name is 'guest'. So, formally, the URI syntax is ``[host:]port`` or ``[username:password@]host:port``. If host is omitted, then '0.0.0.0' or '[::]' is assumed, meaning respectively any IPv4 address or any IPv6 address, on the local machine. If username:password is omitted, then 'guest' is assumed. Some examples: Some functions in these modules are analogs to functions from `standard Lua libraries <http://www.lua.org/manual/>`_. For better results, we recommend using functions from Tarantool's built-in modules. Specify fiber-WAL-disk synchronization mode as: Start listening for incoming connections. Start the Tarantool server as described before. Start the console on the current interactive terminal. Start two shells. The first shell will be the server. The second shell will be the client. Start with two terminal shells, Terminal #1 and Terminal #2. Store the process id in this file. Can be relative to :ref:`work_dir <cfg_basic-work_dir>`. A typical value is “:file:`tarantool.pid`”. Submodule `box.error` Submodule `fiber-ipc` Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports for all crypto functions.. Suppose that a digest is done for a string 'A', then a new part 'B' is appended to the string, then a new digest is required. The new digest could be recomputed for the whole string 'AB', but it is faster to take what was computed before for 'A' and apply changes based on the new part 'B'. This is called multi-step or "incremental" digesting, which Tarantool supports with crc32 and with murmur... TAP version 13
1..2
ok - 2 * 2 is 4
    # Some subtests for test2
    1..2
    ok - 2 + 2 is 4,
    ok - 2 + 3 is not 4
    # Some subtests for test2: end
ok - some subtests for test2 Tarantool is started by entering the following command: Tarantool supplies DBMS connector modules with the module manager for Lua, LuaRocks. So the connector modules may be called "rocks". Tarantool supports file input/output with an API that is similar to POSIX syscalls. All operations are performed asynchronously. Multiple fibers can access the same file simultaneously. Tarantool uses `git describe <http://www.kernel.org/pub/software/scm/git/docs/git-describe.html>`_ to produce its version id, and this id can be used at any time to check out the corresponding source from our `git repository <http://github.com/tarantool/tarantool.git>`_. Test whether a value has a particular type. Displays a long message if the value is not of the specified type. The "admin" address is the URI to listen on. It has no default value, so it must be specified if connections will occur via an admin port. The parameter is expressed with URI = Universal Resource Identifier format, for example "/tmpdir/unix_domain_socket.sock", or a numeric TCP port. Connections are often made with telnet. A typical port value is 3313. The "for" instruction can be translated as "iterate through the index of the space that is being scanned", and within it, if the tuple is "expired" (for example, if the tuple has a timestamp field which is less than the current time), process the tuple as an expired tuple. The 'Error' line is visible in tarantool.txt preceded by the letter E. The 'Info' line is not present because the log_level is 3. The 3-number version follows the standard ``<major>-<minor>-<patch>`` scheme, in which ``<major>`` number is changed only rarely, ``<minor>`` is incremented for each new milestone and indicates possible incompatible changes, and ``<patch>`` stands for the number of bug fix releases made after the start of the milestone. For non-released versions only, there may be a commit number and commit SHA1 to indicate how much this particular build has diverged from the last release. The :code:`strict` module has functions for turning "strict mode" on or off. When strict mode is on, an attempt to use an undeclared global variable will cause an error. A global variable is considered "undeclared" if it has never had a value assigned to it. Often this is an indication of a programming error. The :ref:`snapshot_period <cfg_snapshot_daemon-snapshot_period>` and :ref:`snapshot_count <cfg_snapshot_daemon-snapshot_count>` configuration settings determine how long the intervals are, and how many snapshots should exist before removals occur. The JSON output structure can be specified with ``__serialize``: The MsgPack Specification_ page explains that the first encoding means: The MsgPack output structure can be specified with ``__serialize``: The Tarantool Debugger (abbreviation = tdb) can be used with any Lua program. The operational features include: setting breakpoints, examining variables, going forward one line at a time, backtracing, and showing information about fibers. The display features include: using different colors for different situations, including line numbers, and adding hints. The Tarantool rocks allow for connecting to an SQL server and executing SQL statements the same way that a MySQL or PostgreSQL client does. The SQL statements are visible as Lua methods. Thus Tarantool can serve as a "MySQL Lua Connector" or "PostgreSQL Lua Connector", which would be useful even if that was all Tarantool could do. But of course Tarantool is also a DBMS, so the module also is useful for any operations, such as database copying and accelerating, which work best when the application can work on both SQL and Tarantool inside the same Lua routine. The methods for connect/select/insert/etc. are similar to the ones in the :ref:`net.box <net_box-module>` module. The Tarantool server puts all diagnostic messages in a log file specified by the :ref:`logger <cfg_logging-logger>` configuration parameter. Diagnostic messages may be either system-generated by the server's internal code, or user-generated with the ``log.log_level_function_name`` function. The Tarantool shard module has facilities for creating shards, as well as analogues for the data-manipulation functions of the box library (select, insert, replace, update, delete). The `YAML collection style <http://yaml.org/spec/1.1/#id930798>`_ can be specified with ``__serialize``: The ``box.error`` function is for raising an error. The difference between this function and Lua's built-in ``error()`` function is that when the error reaches the client, its error code is preserved. In contrast, a Lua error would always be presented to the client as :errcode:`ER_PROC_LUA`. The ``clock`` module returns time values derived from the Posix / C CLOCK_GETTIME_ function or equivalent. Most functions in the module return a number of seconds; functions whose names end in "64" return a 64-bit number of nanoseconds. The ``facility`` setting is currently ignored but will be used in the future. The ``fiber-ipc`` submodule allows sending and receiving messages between different processes. The words "different processes" in this context mean different connections, different sessions, or different fibers. The ``fiber`` module allows for creating, running and managing *fibers*. The ``jit`` module has functions for tracing the LuaJIT Just-In-Time compiler's progress, showing the byte-code or assembler output that the compiler produces, and in general providing information about what LuaJIT does with Lua code. The ``msgpack`` module takes strings in MsgPack_ format and decodes them, or takes a series of non-MsgPack values and encodes them. The ``net.box`` module contains connectors to remote database systems. One variant, to be discussed later, is for connecting to MySQL or MariaDB or PostgreSQL — that variant is the subject of the :ref:`SQL DBMS modules <dbms_modules>` appendix. In this section the subject is the built-in variant, ``net.box``. This is for connecting to tarantool servers via a network. The ``sock:name()`` function is used to get information about the near side of the connection. If a socket was bound to ``xyz.com:45``, then ``sock:name`` will return information about ``[host:xyz.com, port:45]``. The equivalent POSIX function is ``getsockname()``. The ``sock:peer()`` function is used to get information about the far side of a connection. If a TCP connection has been made to a distant host ``tarantool.org:80``, ``sock:peer()`` will return information about ``[host:tarantool.org, port:80]``. The equivalent POSIX function is ``getpeername()``. The ``socket.getaddrinfo()`` function is useful for finding information about a remote site so that the correct arguments for ``sock:sysconnect()`` can be passed. The ``socket.tcp_server()`` function makes Tarantool act as a server that can accept connections. Usually the same objective is accomplished with ``box.cfg{listen=...)``. The ``socket`` module allows exchanging data via BSD sockets with a local or remote host in connection-oriented (TCP) or datagram-oriented (UDP) mode. Semantics of the calls in the ``socket`` API closely follow semantics of the corresponding POSIX calls. Function names and signatures are mostly compatible with `luasocket`_. The ``yaml`` module takes strings in YAML_ format and decodes them, or takes a series of non-YAML values and encodes them. The above code means: use `tcp_server()` to wait for a connection from any host on port 3302. When it happens, enter a loop that reads on the socket and prints what it reads. The "delimiter" for the read function is "\\n" so each `read()` will read a string as far as the next line feed, including the line feed. The actual output will be a line containing the current timestamp, a module name, 'E' or 'W' or 'I' or 'D' or 'R' depending on ``log_level_function_name``, and ``message``. Output will not occur if ``log_level_function_name`` is for a type greater than :ref:`log_level <cfg_logging-log_level>`. Messages may contain C-style format specifiers %d or %s, so :samp:`log.error('...%d...%s',{x},{y})` will work if x is a number and y is a string. The all-zero UUID value can be expressed as uuid.NULL, or as ``uuid.fromstr('00000000-0000-0000-0000-000000000000')``. The comparison with an all-zero value can also be expressed as ``uuid_with_type_cdata == uuid.NULL``. The connection-options parameter is a table. Possible options are: The console module allows one Tarantool server to access another Tarantool server, and allows one Tarantool server to start listening on an :ref:`admin port <administration-admin_ports>`. The console.connect function allows one Tarantool server, in interactive mode, to access another Tarantool server. Subsequent requests will appear to be handled locally, but in reality the requests are being sent to the remote server and the local server is acting as a client. Once connection is successful, the prompt will change and subsequent requests are sent to, and executed on, the remote server. Results are displayed on the local server. To return to local mode, enter :code:`control-D`. The contents of the ``box`` library can be inspected at runtime with ``box``, with no arguments. The submodules inside the box library are: ``box.schema``, ``box.tuple``, ``box.space``, ``box.index``, ``box.cfg``, ``box.info``, ``box.slab``, ``box.stat``. Every submodule contains one or more Lua functions. A few submodules contain members as well as functions. The functions allow data definition (create alter drop), data manipulation (insert delete update upsert select replace), and introspection (inspecting contents of spaces, accessing server configuration). The crc32 and crc32_update functions use the `CRC-32C (Castagnoli)`_ polynomial value: ``0x1EDC6F41`` / ``4812730177``. If it is necessary to be compatible with other checksum functions in other programming languages, ensure that the other functions use the same polynomial value. The csv module handles records formatted according to Comma-Separated-Values (CSV) rules. The database-specific requests (``cfg``, :ref:`space.create <box_schema-space_create>`, :ref:`create_index <box_space-create_index>`) should already be familiar. The default formatting rules are: The default user name is ‘guest’. A replica server does not accept data-change requests on the :ref:`listen <cfg_basic-listen>` port. The replication_source parameter is dynamic, that is, to enter master mode, simply set replication_source to an empty string and issue :code:`box.cfg{replication_source=`:samp:`{new-value}`:code:`}`. The default vinyl configuration can be changed with The discussion here in the reference is about incorporating and using two modules that have already been created: the "SQL DBMS rocks" for MySQL and PostgreSQL. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of MySQL had been installed on ~/mysql-5.5. The mysqld server is already running on the local host 127.0.0.1. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of PostgreSQL had been installed on /usr. The PostgreSQL server is already running on the local host 127.0.0.1. The following functions are equivalent. For example, the ``digest`` function and the ``crypto`` function will both produce the same result. The following sections describe all parameters for basic operation, for storage, for binary logging and snapshots, for replication, for networking, and for logging. The full documentation is `On the luafun section of github`_. However, the first chapter can be skipped because installation is already done, it's inside Tarantool. All that is needed is the usual :code:`require` request. After that, all the operations described in the Lua fun manual will work, provided they are preceded by the name returned by the :code:`require` request. For example: The function that can determine whether a UUID is an all-zero value is: The function which will be supplied to expirationd is :codenormal:`is_tuple_expired`, which is saying "if the second field of the tuple is less than the :ref:`current time <fiber-time>`  , then return true, otherwise return false". The functions for setting up and connecting are ``socket``, ``sysconnect``, ``tcp_connect``. The functions for sending data are ``send``, ``sendto``, ``write``, ``syswrite``. The functions for receiving data are ``recv``, ``recvfrom``, ``read``. The functions for waiting before sending/receiving data are ``wait``, ``readable``, ``writable``. The functions for setting flags are ``nonblock``, ``setsockopt``. The functions for stopping and disconnecting are ``shutdown``, ``close``. The functions for error checking are ``errno``, ``error``. The functions in digest are: The functions that can convert between different types of UUID are: The functions that can return a UUID are: The guava function uses the `Consistent Hashing`_ algorithm of the Google guava library. The first parameter should be a hash code; the second parameter should be the number of buckets; the returned value will be an integer between 0 and the number of buckets. For example, The interval between actions by the snapshot daemon, in seconds. If ``snapshot_period`` is set to a value greater than zero, and there is activity which causes change to a database, then the snapshot daemon will call :ref:`box.snapshot <admin-snapshot>` every ``snapshot_period`` seconds, creating a new snapshot file each time. The json module provides JSON manipulation routines. It is based on the `Lua-CJSON module by Mark Pulford`_. For a complete manual on Lua-CJSON please read `the official documentation`_. The key for getting the rock rolling is ``expd = require('expirationd')``. The "``require``" function is what reads in the program; it will appear in many later examples in this manual, when it's necessary to get a module that's not part of the Tarantool kernel. After the Lua variable expd has been assigned the value of the expirationd module, it's possible to invoke the module's ``run_task()`` function. The maximum number of snapshots that may exist on the snap_dir directory before the snapshot daemon will remove old snapshots. If snapshot_count equals zero, then the snapshot daemon does not remove old snapshots. For example: The monotonic time. Derived from C function clock_gettime(CLOCK_MONOTONIC). Monotonic time is similar to wall clock time but is not affected by changes to or from daylight saving time, or by changes done by a user. This is the best function to use with benchmarks that need to calculate elapsed time. The most important function is: The names are similar to the names that PostgreSQL itself uses. The number of replicas in each shard. The number of replicas per shard (redundancy) is 3. The number of servers is 3. The shard module will conclude that there is only one shard. The option names, except for `raise`, are similar to the names that MySQL's mysql client uses, for details see the MySQL manual at `dev.mysql.com/doc/refman/5.6/en/connecting.html`_. The `raise` option should be set to :codenormal:`true` if errors should be raised when encountered. To connect with a Unix socket rather than with TCP, specify ``host = 'unix/'`` and :samp:`port = {socket-name}`. The os module contains the functions :ref:`execute() <os-execute>`, :ref:`rename() <os-rename>`, :ref:`getenv() <os-getenv>`, :ref:`remove() <os-remove>`, :ref:`date() <os-date>`, :ref:`exit() <os-exit>`, :ref:`time() <os-time>`, :ref:`clock() <os-clock>`, :ref:`tmpname() <os-tmpname>`. Most of these functions are described in the Lua manual Chapter 22 `The Operating System Library <https://www.lua.org/pil/contents.html#22>`_. The other potential problem comes from fibers which never get scheduled, because they are not subscribed to any events, or because no relevant events occur. Such morphing fibers can be killed with :ref:`fiber.kill() <fiber-kill>` at any time, since :ref:`fiber.kill() <fiber-kill>` sends an asynchronous wakeup event to the fiber, and :ref:`fiber.testcancel() <fiber-testcancel>` is checked whenever such a wakeup event occurs. The output from the above script will look approximately like this: The possible options which can be passed to csv functions are: The processor time. Derived from C function clock_gettime(CLOCK_PROCESS_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a CPU. The read/write data port number or :ref:`URI <index-uri>` (Universal Resource Identifier) string. Has no default value, so **must be specified** if connections will occur from remote clients that do not use the :ref:`“admin port” <administration-admin_ports>`. Connections made with :samp:`listen={URI}` are sometimes called "binary protocol" or "primary port" connections. The recursion limit was reached when creating a new fiber. This usually indicates that a stored procedure is recursively invoking itself too often. The result of ``tap.test`` is an object, which will be called taptest in the rest of this discussion, which is necessary for ``taptest:plan()`` and all the other methods. The result of the json.encode request will look like this: The same configuration settings exist for json, for :ref:`MsgPack <msgpack-module>`, and for :ref:`YAML <yaml-module>`. >>>>>>> Fix every NOTE to be highlighted on site + JSON cfg rewritten The screen should now look like this: The server will sleep for io_collect_interval seconds between iterations of the event loop. Can be used to reduce CPU load in deployments in which the number of client connections is large, but requests are not so frequent (for example, each connection issues just a handful of requests per second). The shard module distributes according to a hash algorithm, that is, it applies a hash function to a tuple's primary-key value in order to decide which shard the tuple belongs to. The hash function is `consistent`_ so that changing the number of servers will not affect results for many keys. The specific hash function that the shard module uses is :ref:`digest.guava <digest-guava>` in the :codeitalic:`digest` module. The shard package is distributed separately from the main tarantool package. To acquire it, do a separate install. For example on Ubuntu say: The size of the read-ahead buffer associated with a client connection. The larger the buffer, the more memory an active connection consumes and the more requests can be read from the operating system buffer in a single system call. The rule of thumb is to make sure the buffer can contain at least a few dozen requests. Therefore, if a typical tuple in a request is large, e.g. a few kilobytes or even megabytes, the read-ahead buffer size should be increased. If batched request processing is not used, it’s prudent to leave this setting at its default. The snapshot daemon is a fiber which is constantly running. At intervals, it may make new snapshot (.snap) files and then may remove old snapshot files. If the snapshot daemon removes an old snapshot file, it will also remove any write-ahead log (.xlog) files that are older than the snapshot file and contain information that is present in the snapshot file. The specified index in the specified space does not exist. The specified space does not exist. The tap module streamlines the testing of other modules. It allows writing of tests in the `TAP protocol`_. The results from the tests can be parsed by standard TAP-analyzers so they can be passed to utilities such as `prove`_. Thus one can run tests and then use the results for statistics, decision-making, and so on. The thread time. Derived from C function clock_gettime(CLOCK_THREAD_CPUTIME_ID). This is the best function to use with benchmarks that need to calculate how much time has been spent within a thread within a CPU. The time that a function takes within a processor. This function uses clock.proc(), therefore it calculates elapsed CPU time. Therefore it is not useful for showing actual elapsed time. The wall clock time. Derived from C function clock_gettime(CLOCK_REALTIME). This is the best function for knowing what the official time is, as determined by the system administrator. There are configuration settings which affect the way that Tarantool encodes invalid numbers or types. They are all boolean ``true``/``false`` values There are no restrictions on the types of requests that can be entered, except those which are due to privilege restrictions -- by default the login to the remote server is done with user name = 'guest'. The remote server could allow for this by granting at least one privilege: :code:`box.schema.user.grant('guest','execute','universe')`. There are two shards, and each shard contains one replica. This requires two nodes. In real life the two nodes would be two computers, but for this illustration the requirement is merely: start two shells, which we'll call Terminal#1 and Terminal #2. There is only one shard, and that shard contains only one replica. So this isn't illustrating the features of either replication or sharding, it's only illustrating what the syntax is, and what the messages look like, that anyone could duplicate in a minute or two with the magic of cut-and-paste. This describes three shards. Each shard has two replicas. Since the number of servers is 7, and the number of replicas per shard is 2, and dividing 7 / 2 leaves a remainder of 1, one of the servers will not be used. This is not necessarily an error, because perhaps one of the servers in the list is not alive. This example assumes that MySQL 5.5 or MySQL 5.6 or MySQL 5.7 has been installed. Recent MariaDB versions will also work, the MariaDB C connector is used. The package that matters most is the MySQL client developer package, typically named something like libmysqlclient-dev. The file that matters most from this package is libmysqlclient.so or a similar name. One can use ``find`` or ``whereis`` to see what directories these files are installed in. This example assumes that PostgreSQL 8 or PostgreSQL 9 has been installed. More recent versions should also work. The package that matters most is the PostgreSQL developer package, typically named something like libpq-dev. On Ubuntu this can be installed with: This example should give a rough idea of what some functions for fibers should look like. It's assumed that the functions would be referenced in :ref:`fiber.create() <fiber-create>`. This example will work with the sandbox configuration described in the preface. That is, there is a space named tester with a numeric primary key. Assume that the database is nearly empty. Assume that the tarantool server is running on ``localhost 127.0.0.1:3301``. This function may be useful before invoking a function which might otherwise block indefinitely. This is a basic function which is used by other functions. Depending on the value of ``condition``, print 'ok' or 'not ok' along with debugging information. Displays the message. This is the negation of ``taptest:is(...)``. This method may change in the future. This must be called for every shard. The shard-configuration is a table with these fields: This reference covers Tarantool's built-in Lua modules. This reference covers all options and parameters which can be set for Tarantool on the command line or in an initialization file. This reference covers third-party Lua modules for Tarantool. This shows that what was inserted by Terminal #1 can be selected by Terminal #2, via the shard module. This will illustrate how "rotation" works, that is, what happens when the server is writing to a log and signals are used when archiving it. This will open the file ``tarantool.log`` for output on the server’s default directory. If the ``logger`` string has no prefix or has the prefix "file:", then the string is interpreted as a file path. This will start the program ``cronolog`` when the server starts, and will send all log messages to the standard input (``stdin``) of cronolog. If the ``logger`` string begins with '|' or has the prefix "pipe:", then the string is interpreted as a Unix `pipeline <https://en.wikipedia.org/wiki/Pipeline_%28Unix%29>`_. To call another DBMS from Tarantool, the essential requirements are: another DBMS, and Tarantool. The module which connects Tarantool to another DBMS may be called a "connector". Within the module there is a shared library which may be called a "driver". To end a session that began with ``mysql.connect``, the request is: To end a session that began with ``pg.connect``, the request is: To ensure that a connection is working, the request is: To initiate tdb within a Lua program and set a breakpoint, edit the program to include these lines: To run this example: put the script in a file named ./tap.lua, then make tap.lua executable by saying ``chmod a+x ./tap.lua``, then execute using Tarantool as a script processor by saying ./tap.lua. To see all the non-null parameters, say ``box.cfg`` (no parentheses). To see a particular parameter, for example the listen address, say ``box.cfg.listen``. To start the debugging session, execute the Lua program. Execution will stop at the breakpoint, and it will be possible to enter debugging commands. To use Tarantool binary protocol primitives from Lua, it's necessary to convert Lua variables to binary format. The ``pickle.pack()`` helper function is prototyped after Perl 'pack_'. Type: boolean |br| Default: false |br| Dynamic: no |br| Type: boolean |br| Default: false |br| Dynamic: yes |br| Type: boolean |br| Default: true |br| Dynamic: no |br| Type: boolean |br| Default: true |br| Dynamic: yes |br| Type: float |br| Default: 0.5 |br| Dynamic: **yes** |br| Type: float |br| Default: 1.0 |br| Dynamic: no |br| Type: float |br| Default: 1.1 |br| Dynamic: no |br| Type: float |br| Default: 2 |br| Dynamic: no |br| Type: float |br| Default: null |br| Dynamic: **yes** |br| Type: integer or string |br| Default: null |br| Dynamic: yes |br| Type: integer |br| Default: 0 |br| Dynamic: yes |br| Type: integer |br| Default: 1048576 |br| Dynamic: no |br| Type: integer |br| Default: 16 |br| Dynamic: no |br| Type: integer |br| Default: 16320 |br| Dynamic: **yes** |br| Type: integer |br| Default: 5 |br| Dynamic: **yes** |br| Type: integer |br| Default: 500000 |br| Dynamic: no |br| Type: integer |br| Default: 6 |br| Dynamic: yes |br| Type: string |br| Default: "." |br| Dynamic: no |br| Type: string |br| Default: "write" |br| Dynamic: **yes** |br| Type: string |br| Default: null |br| Dynamic: **yes** |br| Type: string |br| Default: null |br| Dynamic: no |br| Type: string |br| Default: null |br| Dynamic: yes |br| Typically a socket session will begin with the setup functions, will set one or more flags, will have a loop with sending and receiving functions, will end with the teardown functions -- as an example at the end of this section will show. Throughout, there may be error-checking and waiting functions for synchronization. To prevent a fiber containing socket functions from "blocking" other fibers, the :ref:`implicit yield rules <atomic-the_implicit_yield_rules>` will cause a yield so that other processes may take over, as is the norm for cooperative multitasking. UNIX user name to switch to after start. Универсальный код ресурса (URI) URI fragment URL or IP address UUID converted from cdata input value. UUID in 16-byte binary string UUID in 36-byte hexadecimal string Ultimately the tuple-expiry process leads to ``default_tuple_drop()`` which does a "delete" of a tuple from its original space. First the fun :ref:`fun <fun-module>` module is used, specifically fun.map_. Remembering that :codenormal:`index[0]` is always the space's primary key, and :codenormal:`index[0].parts[`:codeitalic:`N`:codenormal:`].fieldno` is always the field number for key part :codeitalic:`N`, fun.map() is creating a table from the primary-key values of the tuple. The result of fun.map() is passed to :ref:`space_object:delete() <box_space-delete>`. Use of a TCP socket over the Internet Use of a UDP socket on localhost Use slab_alloc_factor as the multiplier for computing the sizes of memory chunks that tuples are stored in. A lower value may result in less wasted memory depending on the total amount of memory available and the distribution of item sizes. Use tcp_server to accept file contents sent with socat Wait for connection to be active or closed. Wait until something is either readable or writable, or until a timeout value expires. Wait until something is readable, or until a timeout value expires. Wait until something is writable, or until a timeout value expires. We will assume that the name is 'conn' in further examples. What will appear on Terminal #1 is: a loop of error messages saying "Connection refused" and "server check failure". This is normal. It will go on until Terminal #2 process starts. What will appear on Terminal #2, at the end, should look like this: When called with a Lua-table argument, the code and reason have any user-desired values. The result will be those values. When called without arguments, ``box.error()`` re-throws whatever the last error was. When enclosed by quote marks, commas and line feeds and spaces are treated as ordinary characters, and a pair of quote marks "" is treated as a single quote mark. When logging to a file, tarantool reopens the log on SIGHUP. When log is a program, its pid is saved in the :ref:`log.logger_pid <log-logger_pid>` variable. You need to send it a signal to rotate logs. Whenever one hears "daemon" in Tarantool, one should suspect it's being done with a :doc:`fiber<../reference_lua/fiber>`. The program is making a fiber and turning control over to it so it runs occasionally, goes to sleep, then comes back for more. Will display ``# bad plan: ...`` if the number of completed tests is not equal to the number of tests specified by ``taptest:plan(...)``. With GitHub With LuaRocks With sharding, the tuples of a tuple set are distributed to multiple nodes, with a Tarantool database server on each node. With this arrangement, each server is handling only a subset of the total data, so larger loads can be handled by simply adding more computers to a network. Write as much as possible data to the socket buffer if non-blocking. Rarely used. For details see `this description`_. Yield control to the scheduler. Equivalent to :ref:`fiber.sleep(0) <fiber-sleep>`. Yield control to the transaction processor thread and sleep for the specified number of seconds. Only the current fiber can be made to sleep. [0] = 5 [0] = nil ``__serialize = "map" or "mapping"`` for a map ``__serialize = "seq" or "sequence"`` for an array ``__serialize="map"`` for a Flow Mapping map. ``__serialize="map"`` for a map ``__serialize="mapping"`` for a Block Mapping map, ``__serialize="seq"`` for a Flow Sequence array, ``__serialize="seq"`` for an array ``__serialize="sequence"`` for a Block Sequence array, ``byte-order`` can be one of next flags: ``cfg.encode_invalid_as_nil`` - use null for all unrecognizable types (default is false) ``cfg.encode_invalid_numbers`` - allow nan and inf (default is true) ``cfg.encode_load_metatables`` - load metatables (default is false) ``cfg.encode_use_tostring`` - use tostring for unrecognizable types (default is false) ``conn:call('func', '1', '2', '3')`` is the remote-call equivalent of ``func('1', '2', '3')``. That is, ``conn:call`` is a remote stored-procedure call. ``fh:pwrite`` returns true if success, false if failure. ``fh:pread`` returns the data that was read, or nil if failure. ``fh:read`` and ``fh:write`` affect the seek position within the file, and this must be taken into account when working on the same file from multiple fibers. It is possible to limit or prevent file access from other fibers with ``fiber.ipc``. ``fh:write`` returns true if success, false if failure. ``fh:read`` returns the data that was read, or nil if failure. ``fio.link`` and ``fio.symlink`` and ``fio.unlink`` return true if success, false if failure. ``fio.readlink`` returns the link value if success, nil if failure. ``fsync``: fibers wait for their data, :manpage:`fsync(2)` follows each :manpage:`write(2)`; ``none``: write-ahead log is not maintained; ``sock:nonblock()`` returns the current flag value. ``sock:nonblock(false)`` sets the flag to false and returns false. ``sock:nonblock(true)`` sets the flag to true and returns true. ``socket.getaddrinfo('tarantool.org', 'http')`` will return variable information such as ``taptest:fail('x')`` is equivalent to ``taptest:ok(false, 'x')``. Displays the message. ``taptest:skip('x')`` is equivalent to ``taptest:ok(true, 'x' .. '# skip')``. Displays the message. ``timeout(...)`` is a wrapper which sets a timeout for the request that follows it. ``write``: fibers wait for their data to be written to the write-ahead log (no :manpage:`fsync(2)`); a UUID a binary string containing all arguments, packed according to the format specifiers. a connected socket, if no error. a function a possible option is `wait_connect` a socket object on success a string formatted as JSON. a string formatted as MsgPack. a string formatted as YAML. a string of the requested length on success. a string, or any object which has a read() method, formatted according to the CSV rules a table which can be formatted according to the CSV rules. a value that will be checked a, A actual result aes128 - aes-128 (with 192-bit binary strings using AES) aes192 - aes-192 (with 192-bit binary strings using AES) aes256 - aes-256 (with 256-bit binary strings using AES) an arbitrary name to give for the test outputs. an empty string if there is nothing more to read, or a nil value if error, or a string up to ``limit`` bytes long, which may include the bytes that matched the ``delimiter`` expression. an expression which is true or false an unconnected socket, or nil. and `Log_file` will have and suppose the environment variable LISTEN_URI contains 3301, and suppose the command line is ``~/tarantool/src/tarantool script.lua ARG``. Then the screen might look like this: and the second encoding means: any object which has a write() method arguments, that must be passed to function b, B binary (a port number that this host is listening on, on the current host) (distinguishable from the 'listen' port specified by box.cfg) bool boolean boolean. box.cfg{
    snapshot_period = 3600,
    snapshot_count  = 10
} box.cfg{logger = 'syslog:identity=tarantool'}
-- or
box.cfg{logger = 'syslog:facility=user'}
-- or
box.cfg{logger = 'syslog:identity=tarantool,facility=user'} box.cfg{logger = 'tarantool.log'}
-- or
box.cfg{logger = 'file: tarantool.log'} box.cfg{logger = '| cronolog tarantool.log'}
-- or
box.cfg{logger = 'pipe: cronolog tarantool.log'}' box.cfg{logger='Log_file'}
log = require('log')
log.info('Log Line #1') box.cfg{}
socket = require('socket')
socket.tcp_server('0.0.0.0', 3302, function(s)
    while true do
      local request
      request = s:read("\n");
      if request == "" or request == nil then
        break
      end
      print(request)
    end
  end) cbc - Cipher Block Chaining cdata cfb - Cipher Feedback changed name of file or directory. client/server conn = mysql.connect({
    host = '127.0.0.1',
    port = 3306,
    user = 'p',
    password = 'p',
    db = 'test',
    raise = true
})
-- OR
conn = mysql.connect({
    host = 'unix/',
    port = '/var/run/mysqld/mysqld.sock'
}) conn = net_box.new('localhost:3301')
conn = net_box.new('127.0.0.1:3306', {wait_connect = false}) conn = pg.connect({
    host = '127.0.0.1',
    port = 5432,
    user = 'p',
    password = 'p',
    db = 'test'
}) conn object conn:call('function5') conn:close() conn:eval('return 5+5') conn:timeout(0.5).space.tester:update({1}, {{'=', 2, 15}}) console = require('console')
console.start() converted UUID converts Lua variable to a 1-byte integer, and stores the integer in the resulting string converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to a 2-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 4-byte float, and stores the float in the resulting string converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, big converts Lua variable to a 4-byte integer, and stores the integer in the resulting string, low byte first converts Lua variable to a 8-byte double, and stores the double in the resulting string converts Lua variable to a sequence of bytes, and stores the sequence in the resulting string converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, big endian, converts Lua variable to an 8-byte integer, and stores the integer in the resulting string, low byte first created fiber object crypto = require('crypto')

-- print aes-192 digest of 'AB', with one step, then incrementally
print(crypto.cipher.aes192.cbc.encrypt('AB', 'key'))
c = crypto.cipher.aes192.cbc.encrypt.new()
c:init()
c:update('A', 'key')
c:update('B', 'key')
print(c:result())
c:free()

-- print sha-256 digest of 'AB', with one step, then incrementally
print(crypto.digest.sha256('AB'))
c = crypto.digest.sha256.new()
c:init()
c:update('A')
c:update('B')
print(c:result())
c:free() crypto.cipher.aes192.cbc.encrypt('string', 'key', 'initialization')
crypto.cipher.aes256.ecb.decrypt('string', 'key', 'initialization') crypto.cipher.aes256.cbc.encrypt('string', 'key') == digest.aes256cbc.encrypt('string', 'key')
crypto.digest.md4('string') == digest.md4('string')
crypto.digest.md5('string') == digest.md5('string')
crypto.digest.sha('string') == digest.sha('string')
crypto.digest.sha1('string') == digest.sha1('string')
crypto.digest.sha224('string') == digest.sha224('string')
crypto.digest.sha256('string') == digest.sha256('string')
crypto.digest.sha384('string') == digest.sha384('string')
crypto.digest.sha512('string') == digest.sha512('string') crypto.digest.md4('string')
crypto.digest.sha512('string') current system time (in microseconds since the epoch) as a 64-bit integer. The time is taken from the event loop clock. current system time (in seconds since the epoch) as a Lua number. The time is taken from the event loop clock, which makes this call very cheap, but still useful for constructing artificial tuple keys. d des    - des (with 56-bit binary strings using DES, though DES is not recommended) details about the file. digest = require('digest')

-- print crc32 of 'AB', with one step, then incrementally
print(digest.crc32('AB'))
c = digest.crc32.new()
c:update('A')
c:update('B')
print(c:result())

-- print murmur hash of 'AB', with one step, then incrementally
print(digest.murmur('AB'))
m = digest.murmur.new()
m:update('A')
m:update('B')
print(m:result()) directory name, that is, path name except for file name. dss - dss (using DSS) dss1 - dss (using DSS-1) due to :ref:`the implicit yield rules <atomic-the_implicit_yield_rules>` a local :samp:`box.space.{space-name}:select`:code:`{...}` does not yield, but a remote :samp:`conn.space.{space-name}:select`:code:`{...}` call does yield, so global variables or database tuples data may change when a remote :samp:`conn.space.{space-name}:select`:code:`{...}` occurs. dumped_value ecb - Electronic Codebook either a scalar value or a Lua table value. error checking existing file name. expected result f false fiber = require('fiber')
channel = fiber.channel(10)
function consumer_fiber()
    while true do
        local task = channel:get()
        ...
    end
end

function consumer2_fiber()
    while true do
        -- 10 seconds
        local task = channel:get(10)
        if task ~= nil then
            ...
        else
            -- timeout
        end
    end
end

function producer_fiber()
    while true do
        task = box.space...:select{...}
        ...
        if channel:is_empty() then
            -- channel is empty
        end

        if channel:is_full() then
            -- channel is full
        end

        ...
        if channel:has_readers() then
            -- there are some fibers
            -- that are waiting for data
        end
        ...

        if channel:has_writers() then
            -- there are some fibers
            -- that are waiting for readers
        end
        channel:put(task)
    end
end

function producer2_fiber()
    while true do
        task = box.space...select{...}
        -- 10 seconds
        if channel:put(task, 10) then
            ...
        else
            -- timeout
        end
    end
end fiber = require('fiber')
expd = require('expirationd')
box.cfg{}
e = box.schema.space.create('expirationd_test')
e:create_index('primary', {type = 'hash', parts = {1, 'unsigned'}})
e:replace{1, fiber.time() + 3}
e:replace{2, fiber.time() + 30}
function is_tuple_expired(args, tuple)
  if (tuple[2] < fiber.time()) then return true end
  return false
  end
expd.run_task('expirationd_test', e.id, is_tuple_expired)
retval = {}
fiber.sleep(2)
expd.task_stats()
fiber.sleep(2)
expd.task_stats()
expd.kill_task('expirationd_test')
e:drop()
os.exit() fiber object for the currently scheduled fiber. fiber object for the specified fiber. fiber object, for example the fiber object returned by :ref:`fiber.create <fiber-create>` fields which describe the file's block size, creation time, size, and other attributes. file handle (later - fh) file name file-handle as returned by ``fio.open()``. fixarray(2), fixstr(1), "A", fixstr(1), "B" fixmap(2), key(1), fixstr(1), "A", key(2), fixstr(2), "B". flag setting for _, tuple in scan_space.index[0]:pairs(nil, {iterator = box.index.ALL}) do
...
        if task.is_tuple_expired(task.args, tuple) then
        task.expired_tuples_count = task.expired_tuples_count + 1
        task.process_expired_tuple(task.space_id, task.args, tuple)
... function f()
  print("D")
end
jit.bc.dump(f) function hexdump(bytes)
    local result = ''
    for i = 1, #bytes do
        result = result .. string.format("%x", string.byte(bytes, i)) .. ' '
    end
    return result
end

msgpack = require('msgpack')
m1 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "seq"
                          }))
m2 = msgpack.encode(setmetatable({'A', 'B'}, {
                             __serialize = "map"
                          }))
print('array encoding: ', hexdump(m1))
print('map encoding: ', hexdump(m2)) git clone --recursive https://github.com/Sulverus/tdb
cd tdb
make
sudo make install prefix=/usr/share/tarantool/ git clone https://github.com/tarantool/mysql.git
cd mysql && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install git clone https://github.com/tarantool/pg.git
cd pg && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install host - a number, 0 (zero), meaning "all local interfaces"; host - a string containing "unix/"; host - a string representation of an IPv4 address or an IPv6 address; host:port i, I id of the fiber. information iterator function json = require('json')
json.cfg{encode_invalid_numbers = true}
x = 0/0
y = 1/0
json.encode({1, x, y, 2}) kill -HUP *process_id* l, L linked name. list of files whose names match the input string loaded_value local function expirationd_run_task(name, space_id, is_tuple_expired, options)
... log file has been reopened
2015-11-30 15:15:32.629 [27469] main/101/interactive I> Log Line #3 log.info('Log Line #2') log.info('Log Line #3') login (the user name which applies for accessing via the shard module) lua_object luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib luarocks install mysql [MYSQL_LIBDIR = *path*]
                       [MYSQL_INCDIR = *path*]
                       [--local] luarocks install pg POSTGRESQL_LIBDIR=/usr/local/postgresql/lib luarocks install pg [POSTGRESQL_LIBDIR = *path*]
                    [POSTGRESQL_INCDIR = *path*]
                    [--local] mask bits. maximum number of bytes to read for example 50 means "stop after 50 bytes" maximum number of seconds to wait for example 50 means "stop after 50 seconds". md4 - md4 (with 128-bit binary strings using MD4) md5 - md5 (with 128-bit binary strings using MD5) mdc2 - mdc2 (using MDC2) message, a table containing "host", "family" and "port" fields. message_sender.host = '18.44.0.1'
message_sender.family = 'AF_INET'
message_sender.port = 43065 msgpack.NULL mv Log_file Log_file.bak mysql = require('mysql') n n  -- go to next line
n  -- go to next line
e  -- enter evaluation mode
j  -- display j
-e -- exit evaluation mode
q  -- quit name of existing file or directory, name of test name of the fiber. net_box.self:is_connected() net_box.self:ping() net_box.self:wait_connected() new active and timeout values. new channel. new group uid. new name. new permissions new socket if success. new user uid. nil notguest:sesame@mail.ru:3301 num number number of a pre-defined error number of bytes to read number of context switches, backtrace, id, total memory, used memory, name for each fiber. number of seconds to sleep. number or number64 number, string numeric identifier of the fiber. ofb - Output Feedback offset within file where reading or writing begins one of ``'l'``, ``'b'``, ``'h'`` or ``'n'``. one or more strings to be concatenated. optional. see :ref:`above <csv-options>` original name. part of the message which will accompany the error password (the password for the login) path name path name of file. path of directory. path-name, which may contain wildcard characters. pg = require('pg') port port - a number. port - a number. If a port number is 0 (zero), the socket will be bound to a random local port. port - a string containing a path to a unix socket. port number position to seek to positive integer as great as the maximum number of slots (spaces for ``get`` or ``put`` messages) that might be pending at any given time. previous mask bits. ps -A | grep tarantool q, Q receiving redundancy (a number, minimum 1) result for ``sock:errno()``, result for ``sock:error()``. If there is no error, then ``sock:errno()`` will return 0 and ``sock:error()``. ripemd160 - rtype: table s, S same as nil scalar values to be formatted seconds or nanoseconds since epoch (1970-01-01 00:00:00), adjusted. seconds or nanoseconds since processor start. seconds or nanoseconds since the last time that the computer was booted. seconds or nanoseconds since thread start. see :ref:`above <csv-options>` sending separator for example '?' means "stop after a question mark" servers (a list of URIs of nodes and the zones the nodes are in) setup sha - sha (with 160-bit binary strings using SHA-0) sha1 - sha-1 (with 160-bit binary strings using SHA-1) sha224 - sha-224 (with 224-bit binary strings using SHA-2) sha256 - sha-256 (with 256-bit binary strings using SHA-2) sha384 - sha-384 (with 384-bit binary strings using SHA-2) sha512 - sha-512(with 512-bit binary strings using SHA-2). shard.init(*shard-configuration*) shard[*space-name*].insert{...}
shard[*space-name*].replace{...}
shard[*space-name*].delete{...}
shard[*space-name*].select{...}
shard[*space-name*].update{...}
shard[*space-name*].auto_increment{...} shard[*space-name*].q_insert{...}
shard[*space-name*].q_replace{...}
shard[*space-name*].q_delete{...}
shard[*space-name*].q_select{...}
shard[*space-name*].q_update{...}
shard[*space-name*].q_auto_increment{...} socket = require('socket')
sock = socket('AF_INET', 'SOCK_STREAM', 'tcp')
sock:sysconnect(0, 3301) socket('AF_INET', 'SOCK_STREAM', 'tcp') socket.SHUT_RD, socket.SHUT_WR, or socket.SHUT_RDWR. socket.tcp_server('localhost', 3302, function () end) state checking string string containing format specifiers string, table string, which is written to ``writable`` if specified sudo apt-get install libpq-dev sudo apt-get install tarantool-dev sudo apt-get install tarantool-shard tarantool-pool suffix table table. first element = seconds of CPU time; second element = whatever the function returns. tap = require('tap')
taptest = tap.test('test-name') taptest tarantool> -- Connection function. Usage: conn = mysql_connect()
tarantool> function mysql_connection()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   conn = mysql.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = mysql_connect()
---
... tarantool> -- input in file.csv is:
tarantool> -- a,"b,c ",d
tarantool> -- a\\211\\128b
tarantool> fio = require('fio')
---
...
tarantool> f = fio.open('./file.csv', {'O_RDONLY'})
---
...
tarantool> csv.load(f, {chunk_size = 4096})
---
- - - a
    - 'b,c '
    - d
  - - a\\211\\128b
...
tarantool> f:close(nn)
---
- true
... tarantool> box.cfg{}
...
tarantool> mysql = require('mysql')
---
... tarantool> box.cfg{}
...
tarantool> pg = require('pg')
---
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.error()
---
- error: Arbitrary message
...
tarantool> box.error(box.error.FUNCTION_ACCESS_DENIED, 'A', 'B', 'C')
---
- error: A access denied for user 'B' to function 'C'
... tarantool> box.error{code = 555, reason = 'Arbitrary message'}
---
- error: Arbitrary message
...
tarantool> box.schema.space.create('#')
---
- error: Invalid identifier '#' (expected letters, digits or an underscore)
...
tarantool> box.error.last()
---
- line: 278
  code: 70
  type: ClientError
  message: Invalid identifier '#' (expected letters, digits or an underscore)
  file: /tmp/buildd/tarantool-1.7.0.252.g1654e31~precise/src/box/key_def.cc
...
tarantool> box.error.clear()
---
...
tarantool> box.error.last()
---
- null
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'host1:33131', zone = '1' },
         >     { uri = 'host2:33131', zone = '2' },
         >     { uri = 'host3:33131', zone = '3' },
         >     { uri = 'host4:33131', zone = '4' },
         >     { uri = 'host5:33131', zone = '5' },
         >     { uri = 'host6:33131', zone = '6' },
         >     { uri = 'host7:33131', zone = '7' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '2',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:33131', zone = '1' },
         >     { uri = 'localhost:33132', zone = '2' },
         >     { uri = 'localhost:33133', zone = '3' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '3',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> conn:close()
---
... tarantool> conn:execute('select table_name from information_schema.tables')
---
- - table_name: ALL_PLUGINS
  - table_name: APPLICABLE_ROLES
  - table_name: CHARACTER_SETS
  <...>
- 78
... tarantool> conn:execute('select tablename from pg_tables')
---
- - tablename: pg_statistic
  - tablename: pg_type
  - tablename: pg_authid
  <...>
... tarantool> conn:ping()
---
- true
... tarantool> console = require('console')
---
...
tarantool> console.connect('198.18.44.44:3301')
---
...
198.18.44.44:3301> -- prompt is telling us that server is remote tarantool> console = require('console')
---
...
tarantool> console.listen('unix/:/tmp/X.sock')
... main/103/console/unix/:/tmp/X I> started
---
- fd: 6
  name:
    host: unix/
    family: AF_UNIX
    type: SOCK_STREAM
    protocol: 0
    port: /tmp/X.sock
... tarantool> csv = require('csv')
---
...
tarantool> csv.dump({'a','b,c ','d'})
---
- 'a,"b,c ",d

'
... tarantool> csv = require('csv')
---
...
tarantool> csv.load('a,"b,c ",d')
---
- - - a
    - 'b,c '
    - d
... tarantool> csv.load('a,b;c,d', {delimiter = ';'})
---
- - - a,b
    - c,d
... tarantool> csv.load('a\\211\\128b')
---
- - - a\211\128b
... tarantool> csv_table = csv.load('a,b,c')
---
...
tarantool> csv.dump(csv_table)
---
- 'a,b,c

'
... tarantool> digest = require('digest')
---
...
tarantool> function password_insert()
         >   box.space.tester:insert{1234, digest.sha1('^S^e^c^ret Wordpass')}
         >   return 'OK'
         > end
---
...
tarantool> function password_check(password)
         >   local t = box.space.tester:select{12345}
         >   if digest.sha1(password) == t[2] then
         >     return 'Password is valid'
         >   else
         >     return 'Password is not valid'
         >   end
         > end
---
...
tarantool> password_insert()
---
- 'OK'
... tarantool> digest.guava(10863919174838991, 11)
---
- 8
... tarantool> dostring('abc')
---
error: '[string "abc"]:1: ''='' expected near ''<eof>'''
...
tarantool> dostring('return 1')
---
- 1
...
tarantool> dostring('return ...', 'hello', 'world')
---
- hello
- world
...
tarantool> dostring([[
         >   local f = function(key)
         >     local t = box.space.tester:select{key}
         >     if t ~= nil then
         >       return t[1]
         >     else
         >       return nil
         >     end
         >   end
         >   return f(...)]], 1)
---
- null
... tarantool> fh = fio.open('/home/username/tmp.txt', {'O_RDWR', 'O_APPEND'})
---
...
tarantool> fh -- display file handle returned by fio.open
---
- fh: 11
... tarantool> fh:close() -- where fh = file-handle
---
- true
... tarantool> fh:fsync()
---
- true
... tarantool> fh:pread(25, 25)
---
- |
  elete from t8//
  insert in
... tarantool> fh:seek(20, 'SEEK_SET')
---
- 20
... tarantool> fh:stat()
---
- inode: 729866
  rdev: 0
  size: 100
  atime: 140942855
  mode: 33261
  mtime: 1409430660
  nlink: 1
  uid: 1000
  blksize: 4096
  gid: 1000
  ctime: 1409430660
  dev: 2049
  blocks: 8
... tarantool> fh:truncate(0)
---
- true
... tarantool> fh:write('new data')
---
- true
... tarantool> fiber = require('fiber')
---
...
tarantool> function f () fiber.sleep(1000); end
---
...
tarantool> fiber_function = fiber:create(f)
---
- error: '[string "fiber_function = fiber:create(f)"]:1: fiber.create(function, ...):
    bad arguments'
...
tarantool> fiber_function = fiber.create(f)
---
...
tarantool> fiber_function.storage.str1 = 'string'
---
...
tarantool> fiber_function.storage['str1']
---
- string
...
tarantool> fiber_function:cancel()
---
...
tarantool> fiber_function.storage['str1']
---
- error: '[string "return fiber_function.storage[''str1'']"]:1: the fiber is dead'
... tarantool> fiber = require('fiber')
---
...
tarantool> function function_name()
         >   fiber.sleep(1000)
         > end
---
...
tarantool> fiber_object = fiber.create(function_name)
---
... tarantool> fiber = require('fiber')
tarantool> function function_x()
         >   gvar = 0
         >   while 0 == 0 do
         >     gvar = gvar + 1
         >     fiber.sleep(2)
         >   end
         > end
---
... tarantool> fiber.find(101)
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.info()
---
- 101:
    csw: 7
    backtrace: []
    fid: 101
    memory:
      total: 65776
      used: 0
    name: interactive
... tarantool> fiber.kill(fiber.id())
---
- error: fiber is cancelled
... tarantool> fiber.self()
---
- status: running
  name: interactive
  id: 101
... tarantool> fiber.self():cancel()
---
- error: fiber is cancelled
... tarantool> fiber.self():name('non-interactive')
---
... tarantool> fiber.self():name()
---
- interactive
... tarantool> fiber.self():status()
---
- running
... tarantool> fiber.sleep(1.5)
---
... tarantool> fiber.status()
---
- running
... tarantool> fiber.testcancel()
---
- error: fiber is cancelled
... tarantool> fiber.time(), fiber.time()
---
- 1448466279.2415
- 1448466279.2415
... tarantool> fiber.time(), fiber.time64()
---
- 1448466351.2708
- 1448466351270762
... tarantool> fiber.yield()
---
... tarantool> fiber_object = fiber.self()
---
...
tarantool> fiber_object:id()
---
- 101
... tarantool> fiber_of_x:cancel()
---
...
tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  dead . gvar= 421
---
... tarantool> fid = fiber_of_x:id()
---
... tarantool> fio.basename('/path/to/my.lua', '.lua')
---
- my
... tarantool> fio.chmod('/home/username/tmp.txt', tonumber('0755', 8))
---
- true
...
tarantool> fio.chown('/home/username/tmp.txt', 'username', 'username')
---
- true
... tarantool> fio.cwd()
---
- /home/username/tarantool_sandbox
... tarantool> fio.dirname('path/to/my.lua')
---
- 'path/to/'
... tarantool> fio.glob('/etc/x*')
---
- - /etc/xdg
  - /etc/xml
  - /etc/xul-ext
... tarantool> fio.link('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
...
tarantool> fio.unlink('/home/username/tmp.txt2')
---
- true
... tarantool> fio.lstat('/etc')
---
- inode: 1048577
  rdev: 0
  size: 12288
  atime: 1421340698
  mode: 16877
  mtime: 1424615337
  nlink: 160
  uid: 0
  blksize: 4096
  gid: 0
  ctime: 1424615337
  dev: 2049
  blocks: 24
... tarantool> fio.mkdir('/etc')
---
- false
... tarantool> fio.pathjoin('/etc', 'default', 'myfile')
---
- /etc/default/myfile
... tarantool> fio.rename('/home/username/tmp.txt', '/home/username/tmp.txt2')
---
- true
... tarantool> fio.sync()
---
- true
... tarantool> fio.tempdir()
---
- /tmp/lG31e7
... tarantool> fio.truncate('/home/username/tmp.txt', 99999)
---
- true
... tarantool> fio.umask(tonumber('755', 8))
---
- 493
... tarantool> fun = require('fun')
---
...
tarantool> for _k, a in fun.range(3) do
         >   print(a)
         > end
1
2
3
---
... tarantool> function mysql_select ()
         >   local conn = mysql.connect({
         >     host = '127.0.0.1',
         >     port = 3306,
         >     user = 'root',
         >     db = 'test'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> mysql_select()
---
- 'MySQL row '
... tarantool> function pg_connect()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   p.user = 'postgres'
         >   p.password = 'postgres'
         >   local conn = pg.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = pg_connect()
---
... tarantool> function pg_select ()
         >   local conn = pg.connect({
         >     host = '127.0.0.1',
         >     port = 5432,
         >     user = 'postgres',
         >     password = 'postgres',
         >     db = 'postgres'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> pg_select()
---
- 'PostgreSQL row '
... tarantool> gvar = 0

tarantool> fiber_of_x = fiber.create(function_x)
---
... tarantool> json = require('json')
---
...
tarantool> json.decode('123')
---
- 123
...
tarantool> json.decode('[123, "hello"]')
---
- [123, 'hello']
...
tarantool> json.decode('{"hello": "world"}').hello
---
- world
... tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- '["A","B"]'
...
tarantool> json.encode(setmetatable({'A', 'B'}, { __serialize="map"}))
---
- '{"1":"A","2":"B"}'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- '[{"f2":"B","f1":"A"}]'
...
tarantool> json.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="seq"})})
---
- '[[]]'
... tarantool> json.encode({1, x, y, 2})
---
- '[1,nan,inf,2]
... tarantool> json=require('json')
---
...
tarantool> json.encode(123)
---
- '123'
...
tarantool> json.encode({123})
---
- '[123]'
...
tarantool> json.encode({123, 234, 345})
---
- '[123,234,345]'
...
tarantool> json.encode({abc = 234, cde = 345})
---
- '{"cde":345,"abc":234}'
...
tarantool> json.encode({hello = {'world'}})
---
- '{"hello":["world"]}'
... tarantool> load = function(readable, opts)
         >   opts = opts or {}
         >   local result = {}
         >   for i, tup in csv.iterate(readable, opts) do
         >     result[i] = tup
         >   end
         >   return result
         > end
---
...
tarantool> load('a,b,c')
---
- - - a
    - b
    - c
... tarantool> msgpack = require('msgpack')
---
...
tarantool> y = msgpack.encode({'a',1,'b',2})
---
...
tarantool> z = msgpack.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> box.space.tester:insert{20, msgpack.NULL, 20}
---
- [20, null, 20]
... tarantool> net_box = require('net.box')
---
...
tarantool> function example()
         >   local conn, wtuple
         >   if net_box.self:ping() then
         >     table.insert(ta, 'self:ping() succeeded')
         >     table.insert(ta, '  (no surprise -- self connection is pre-established)')
         >   end
         >   if box.cfg.listen == '3301' then
         >     table.insert(ta,'The local server listen address = 3301')
         >   else
         >     table.insert(ta, 'The local server listen address is not 3301')
         >     table.insert(ta, '(  (maybe box.cfg{...listen="3301"...} was not stated)')
         >     table.insert(ta, '(  (so connect will fail)')
         >   end
         >   conn = net_box.new('127.0.0.1:3301')
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester.')
         >   conn.space.tester:insert{800, 'data'}
         >   table.insert(ta, 'conn insert done on tester, index 0')
         >   table.insert(ta, '  primary key value = 800.')
         >   wtuple = conn.space.tester:select{800}
         >   table.insert(ta, 'conn select done on tester, index 0')
         >   table.insert(ta, '  number of fields = ' .. #wtuple)
         >   conn.space.tester:delete{800}
         >   table.insert(ta, 'conn delete done on tester')
         >   conn.space.tester:replace{800, 'New data', 'Extra data'}
         >   table.insert(ta, 'conn:replace done on tester')
         >   conn:timeout(0.5).space.tester:update({800}, {{'=', 2, 'Fld#1'}})
         >   table.insert(ta, 'conn update done on tester')
         >   conn:close()
         >   table.insert(ta, 'conn close done')
         > end
---
...
tarantool> ta = {}
---
...
tarantool> example()
---
...
tarantool> ta
---
- - self:ping() succeeded
  - '  (no surprise -- self connection is pre-established)'
  - The local server listen address = 3301
  - conn delete done on tester.
  - conn insert done on tester, index 0
  - '  primary key value = 800.'
  - conn select done on tester, index 0
  - '  number of fields = 1'
  - conn delete done on tester
  - conn:replace done on tester
  - conn update done on tester
  - conn close done
... tarantool> os.clock()
---
- 0.05
... tarantool> os.date("%A %B %d")
---
- Sunday April 24
... tarantool> os.execute('ls -l /usr')
total 200
drwxr-xr-x   2 root root 65536 Apr 22 15:49 bin
drwxr-xr-x  59 root root 20480 Apr 18 07:58 include
drwxr-xr-x 210 root root 65536 Apr 18 07:59 lib
drwxr-xr-x  12 root root  4096 Apr 22 15:49 local
drwxr-xr-x   2 root root 12288 Jan 31 09:50 sbin
---
... tarantool> os.exit()
user@user-shell:~/tarantool_sandbox$ tarantool> os.getenv('PATH')
---
- /usr/local/sbin:/usr/local/bin:/usr/sbin
... tarantool> os.remove('file')
---
- true
... tarantool> os.rename('local','foreign')
---
- null
- 'local: No such file or directory'
- 2
... tarantool> os.time()
---
- 1461516945
... tarantool> os.tmpname()
---
- /tmp/lua_7SW1m2
... tarantool> password_insert('Secret Password')
---
- 'Password is not valid'
... tarantool> pickle = require('pickle')
---
...
tarantool> box.space.tester:insert{0, 'hello world'}
---
- [0, 'hello world']
...
tarantool> box.space.tester:update({0}, {{'=', 2, 'bye world'}})
---
- [0, 'bye world']
...
tarantool> box.space.tester:update({0}, {
         >   {'=', 2, pickle.pack('iiA', 0, 3, 'hello')}
         > })
---
- [0, "\0\0\0\0\x03\0\0\0hello"]
...
tarantool> box.space.tester:update({0}, {{'=', 2, 4}})
---
- [0, 4]
...
tarantool> box.space.tester:update({0}, {{'+', 2, 4}})
---
- [0, 8]
...
tarantool> box.space.tester:update({0}, {{'^', 2, 4}})
---
- [0, 12]
... tarantool> pickle = require('pickle')
---
...
tarantool> tuple = box.space.tester:replace{0}
---
...
tarantool> string.len(tuple[1])
---
- 1
...
tarantool> pickle.unpack('b', tuple[1])
---
- 48
...
tarantool> pickle.unpack('bsi', pickle.pack('bsi', 255, 65535, 4294967295))
---
- 255
- 65535
- 4294967295
...
tarantool> pickle.unpack('ls', pickle.pack('ls', tonumber64('18446744073709551615'), 65535))
---
...
tarantool> num, num64, str = pickle.unpack('slA', pickle.pack('slA', 666,
         > tonumber64('666666666666666'), 'string'))
---
... tarantool> print('#', fid, '. ', fiber_of_x:status(), '. gvar=', gvar)
# 102 .  suspended . gvar= 399
---
... tarantool> shard.init(cfg)
2015-08-09 ... I> Sharding initialization started...
2015-08-09 ... I> establishing connection to cluster servers...
2015-08-09 ... I>  - localhost:3301 - connecting...
2015-08-09 ... I>  - localhost:3301 - connected
2015-08-09 ... I> connected to all servers
2015-08-09 ... I> started
2015-08-09 ... I> redundancy = 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> Adding localhost:3301 to shard 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> shards = 1
2015-08-09 ... I> Done
---
- true
...
tarantool> -- Now put something in ...
---
...
tarantool> shard.tester:insert{1,'Tuple #1'}
---
- - [1, 'Tuple #1']
... tarantool> shard.tester:select{1}
---
- - - [1, 'Tuple #1']
... tarantool> socket = require('socket')
---
...
tarantool> sock = socket.tcp_connect('tarantool.org', 80)
---
...
tarantool> type(sock)
---
- table
...
tarantool> sock:error()
---
- null
...
tarantool> sock:send("HEAD / HTTP/1.0rnHost: tarantool.orgrnrn")
---
- true
...
tarantool> sock:read(17)
---
- "HTTP/1.1 200 OKrn"
...
tarantool> sock:close()
---
- true
... tarantool> socket = require('socket')
---
...
tarantool> sock_1 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_1:bind('127.0.0.1')
---
- true
...
tarantool> sock_2 = socket('AF_INET', 'SOCK_DGRAM', 'udp')
---
...
tarantool> sock_2:sendto('127.0.0.1', sock_1:name().port,'X')
---
- true
...
tarantool> message = sock_1:recvfrom()
---
...
tarantool> message
---
- X
...
tarantool> sock_1:close()
---
- true
...
tarantool> sock_2:close()
---
- true
... tarantool> strict = require('strict')
---
...
tarantool> strict.on()
---
...
tarantool> a = b -- strict mode is on so this will cause an error
---
- error: ... variable ''b'' is not declared'
...
tarantool> strict.off()
---
...
tarantool> a = b -- strict mode is off so this will not cause an error
---
... tarantool> taptest:ok(true, 'x')
ok - x
---
- true
...
tarantool> tap = require('tap')
---
...
tarantool> taptest = tap.test('test-name')
TAP version 13
---
...
tarantool> taptest:ok(1 + 1 == 2, 'X')
ok - X
---
- true
... tarantool> taptest:skip('message')
ok - message # skip
---
- true
... tarantool> tarantool = require('tarantool')
---
...
tarantool> tarantool
---
- build:
    target: Linux-x86_64-RelWithDebInfo
    options: cmake . -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_BACKTRACE=ON
    mod_format: so
    flags: ' -fno-common -fno-omit-frame-pointer -fno-stack-protector -fexceptions
      -funwind-tables -fopenmp -msse2 -std=c11 -Wall -Wextra -Wno-sign-compare -Wno-strict-aliasing
      -fno-gnu89-inline'
    compiler: /usr/bin/x86_64-linux-gnu-gcc /usr/bin/x86_64-linux-gnu-g++
  uptime: 'function: 0x408668e0'
  version: 1.7.0-66-g9093daa
  pid: 'function: 0x40866900'
...
tarantool> tarantool.pid()
---
- 30155
...
tarantool> tarantool.uptime()
---
- 108.64641499519
... tarantool> type(123456789012345), type(tonumber64(123456789012345))
---
- number
- number
...
tarantool> i = tonumber64('1000000000')
---
...
tarantool> type(i), i / 2, i - 2, i * 2, i + 2, i % 2, i ^ 2
---
- number
- 500000000
- 999999998
- 2000000000
- 1000000002
- 0
- 1000000000000000000
... tarantool> uuid = require('uuid')
---
...
tarantool> uuid(), uuid.bin(), uuid.str()
---
- 16ffedc8-cbae-4f93-a05e-349f3ab70baa
- !!binary FvG+Vy1MfUC6kIyeM81DYw==
- 67c999d2-5dce-4e58-be16-ac1bcb93160f
...
tarantool> uu = uuid()
---
...
tarantool> #uui:bin(), #uu:str(), type(uu), uu:isnil()
---
- 16
- 36
- cdata
- false
... tarantool> yaml = require('yaml')
---
...
tarantool> y = yaml.encode({'a', 1, 'b', 2})
---
...
tarantool> z = yaml.decode(y)
---
...
tarantool> z[1], z[2], z[3], z[4]
---
- a
- 1
- b
- 2
...
tarantool> if yaml.NULL == nil then print('hi') end
hi
---
... tarantool> yaml = require('yaml')
---
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="sequence"}))
---
- |
  ---
  - A
  - B
  ...
...
tarantool> yaml.encode(setmetatable({'A', 'B'}, { __serialize="seq"}))
---
- |
  ---
  ['A', 'B']
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="map"})})
---
- |
  ---
  - {'f2': 'B', 'f1': 'A'}
  ...
...
tarantool> yaml.encode({setmetatable({f1 = 'A', f2 = 'B'}, { __serialize="mapping"})})
---
- |
  ---
  - f2: B
    f1: A
  ...
... task.worker_fiber = fiber.create(worker_loop, task)
log.info("expiration: task %q restarted", task.name)
...
fiber.sleep(expirationd.constants.check_interval)
... tcp_connect('127.0.0.1', 3301) tdb = require('tdb')
tdb.start() tdb = require('tdb')
tdb.start()
i = 1
j = 'a' .. i
print('end of program') teardown the :ref:`URI <index-uri>` of the target for the connection the URI of the local server the URI of the remote server the ``NO_SUCH_USER`` message is "``User '%s' is not found``" -- it includes one "``%s``" component which will be replaced with errtext. Thus a call to ``box.error(box.error.NO_SUCH_USER, 'joe')`` or ``box.error(45, 'joe')`` will result in an error with the accompanying message "``User 'joe' is not found``". the function to be associated with the fiber the id of the fiber to be cancelled. the message to be displayed. the new name of the fiber. the new position if success the number of bytes sent. the number of bytes that were decoded. the number of messages. the original contents formatted as a Lua table. the original contents formatted as a Lua table; the original value reformatted as a JSON string. the original value reformatted as a MsgPack string. the original value reformatted as a YAML string. the socket object value may change if sysconnect() succeeds. the specified fiber does not exist or cancel is not permitted. the status of ``fiber``. One of: “dead”, “suspended”, or “running”. the status of fiber. One of: “dead”, “suspended”, or “running”. the value placed on the channel by an earlier ``channel:put()``. timeout true true for success, false for error. true if blocked users are waiting. Otherwise false. true if connected, false on failure. true if success, false if failure. true if success, false on failure. true if the socket is now readable, false if timeout expired; true if the socket is now writable, false if timeout expired; true if the specified channel is already closed. Otherwise false. true if the specified channel is empty true if the specified channel is full (has no room for a new message). true if the value is all zero, otherwise false. true on success, false on error true on success, false on error. For example, if sock is already closed, sock:close() returns false. true or false. true when connected, false on failure. use Digest::CRC;
$d = Digest::CRC->new(width => 32, poly => 0x1EDC6F41, init => 0xFFFFFFFF, refin => 1, refout => 1);
$d->add('string');
print $d->digest; userdata username:password@host:port value to write vinyl = {
  run_age_wm = *number*,
  run_age_period = *number of seconds*,
  memory_limit = *number of gigabytes*,
  compact_wm = *number*,
  threads = *number*,
  run_age = *number*,
  run_prio = *number*,
} vinyl = {
  run_age_wm = 0,
  run_age_period = 0,
  memory_limit = 1,
  compact_wm = 2,
  threads = 5,
  run_age = 0,
  run_prio = 2,
} what to execute. what will be passed to function whatever is returned by the Lua code chunk. whatever is specified in errcode-number. where ``sql-statement`` is a string, and the optional ``parameters`` are extra values that can be plugged in to replace any question marks ("?"s) in the SQL statement. will cause the snapshot daemon to create a new snapshot each hour until it has created ten snapshots. After that, it will remove the oldest snapshot (and any associated write-ahead-log files) after creating a new one. zero or more scalar values which will be appended to, or substitute for, items in the Lua chunk. {} “Tarantool” is the name of the reusable asynchronous networking programming framework. “Target” is the platform tarantool was built on. Some platform-specific details may follow this line. 