��          �               �   2   �   9   0  ;   j  2   �  ,   �               "  1   4     f     u  S   ~     �  �  �  2   �  9   �  ;   �  2   *  ,   ]     �     �     �  1   �     �     �  S        V   :ref:`ACID transactions <atomic-atomic_execution>` :ref:`Authentication and access control <authentication>` :ref:`Master-master replication <replication>` and sharding A drop-in replacement for Lua 5.1, based on LuaJIT ANSI SQL, Lua stored procedures and triggers Compatibility Extensibility High availability Packages for network and file I/O, HTTP, and more Query language Security Tarantool - Get your data in RAM. Get compute close to data. Enjoy the performance. Transactions Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-20 15:07+0300
PO-Revision-Date: 2016-06-07 13:44+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 :ref:`ACID transactions <atomic-atomic_execution>` :ref:`Authentication and access control <authentication>` :ref:`Master-master replication <replication>` and sharding A drop-in replacement for Lua 5.1, based on LuaJIT ANSI SQL, Lua stored procedures and triggers Compatibility Extensibility High availability Packages for network and file I/O, HTTP, and more Query language Security Tarantool - Get your data in RAM. Get compute close to data. Enjoy the performance. Transactions 