��    �      �              <  �
  =  #   �  �     %    �  '  �  �&  "  �)    �,     �/     0  	   0     '0     60  	   B0     L0     U0  ;   o0     �0  7   �0  4   �0    11     A2     P2     _2     m2     {2     �2     �2     �2     �2     �2  =   �2  @   3  [   Y3  >   �3  ;   �3  >   04  P   o4    �4  G   �5  �   6  �   7  r  8    �9  ,   �;  �   �;  �  }<  �   Y>  �   �>  z   �?  t   @  =  �@  >  �A     C  v    C  s   �C  
   D  3   D    JD    eE     �F  �   �F  .   DG  $   sG  b   �G  D   �G  V   @H  ;  �H  *  �I  �   �J     �K     �K  G   �K  4   BL  %   wL     �L  (   �L  +   �L     M     &M     <M     RM  ?  jM  )   �N  .   �N     O  J   O  }   [O  z   �O  c   TP  �   �P  �   gQ  Q   SR  N   �R  6   �R     +S  R  7S  �   �T     +U  o   8U  �   �U  �   �V     mW     �W     �W     �W     �W  A   �W  H   X  M   KX  	   �X  �   �X  �   DY  �   �Y     �Z     �Z  �   �Z  �   R[     �[  K   \     Y\     k\  "   |\  /   �\  �   �\    T]  g  f^  �  �_  �   vb  B   ,c  �   oc  �   d  �   �d  �   �e  �   �f  �  zg     i  ?   2i  %   ri  �   �i  �  %j  %   �k  �  �k  �   |m  �   
n  )  o  6  /p  �  fq    (s  Z   -t  f   �t  �   �t  C   �u  @   2v  7   sv  c   �v  �   w  6  �w  ;   �y  �   z  C   �z  �   {     	|     |    #|  �   ;}  �   �}  s   �~  !      @�  p   T�  x   ł  r   >�  R   ��  F   �  8   K�  ~   ��  ?   �     C�     Å  }   ܅  %   Z�     ��      ��  @   ��  !   ��  �   �  �   ��     ��  "   Ԉ  3   ��  ?  +�  D   k�  >   ��  Y  �  �  I�     ߎ  �   ��  �   ��  %   S�  "  y�  >  ��  L  ۓ  �  (�  ?   ��  �   ��      ��  K   ��  �   �  �  ��  �
  P�  #   	�  �   -�  %  �  �  :�  �  �  "  Ӹ    ��     �     #�  	   0�     :�     I�  	   U�     _�     h�  ;   ��     ��  7   ׿  4   �    D�     T�     c�     r�     ��     ��     ��     ��     ��     ��     ��  =   ��  @   +�  [   l�  >   ��  ;   �  >   C�  P   ��    ��  G   ��  �   "�  �    �  r   �    ��  ,   ��  �   ��  �  ��  �   l�  �   ��  z   ��  t   .�  =  ��  >  ��      �  v   3�  s   ��  
   �  3   )�    ]�    x�     ��  �   ��  .   W�  $   ��  b   ��  D   �  V   S�  ;  ��  *  ��  �   �     ��     ��  G   �  4   U�  %   ��     ��  (   ��  +   ��     !�     9�     O�     e�  ?  }�  )   ��  .   ��     �  J   #�  }   n�  z   ��  c   g�  �   ��  �   z�  Q   f�  N   ��  6   �     >�  R  J�  �   ��     >�  o   K�  �   ��  �   ��  #   ��     ��     ��     ��     ��  A   ��  H   1�  M   z�  	   ��  �   ��  �   s�  �   �     ��     ��  �   ��  �   ��     )�  K   <�     ��     ��  "   ��  /   ��  �   �    ��  g  ��  �  �  �   ��  B   d�  �   ��  �   I�  �   ��  �   ��  �   ��  �  ��     J�  ?   j�  %   ��  �   ��  �  ]�  %   ��  �  �  �   ��  �   B�  )  =�  6  g�  �  �    ` Z   e f   � �   ' C   & @   j 7   � c   � �   G 6  � ;   	 �   O	 C   
 �   H
    A    M   [ �   s �   � s   � !  V   x p   � x   � r   v R   � F   < 8   � ~   � ?   ;    {    � }    %   �    �     � @   � !   - �   O �       � "    3   / ?  c D   � >   � Y  ' �  �     �   7 �   � %   � "  � >  �! L  # �  `% ?   �' �   .(     �( K   �( �   >)  $ # Check that the include subdirectory exists
$ # by looking for /usr/include/postgresql/libpq-fe-h.
$ [ -f /usr/include/postgresql/libpq-fe.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the necessary .so file.
$ [ -f /usr/lib/x86_64-linux-gnu/libpq.so ] && echo "OK" || echo "Error"
OK

$ # Check that the psql client can connect using some factory defaults:
$ # port = 5432, user = 'postgres', user password = 'postgres',
$ # database = 'postgres'. These can be changed, provided one changes
$ # them in all places. Insert a row in database postgres, and quit.
$ psql -h 127.0.0.1 -p 5432 -U postgres -d postgres
Password for user postgres:
psql (9.3.10)
SSL connection (cipher: DHE-RSA-AES256-SHA, bits: 256)
Type "help" for help.

postgres=# CREATE TABLE test (s1 INT, s2 VARCHAR(50));
CREATE TABLE
postgres=# INSERT INTO test VALUES (1,'PostgreSQL row');
INSERT 0 1
postgres=# \q
$

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
        ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool master
$ # repository. The resultant display is normal for Ubuntu 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.7
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.7.0.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install pg POSTGRESQL_LIBDIR=/usr/lib/x86_64-linux-gnu --local
Installing http://rocks.tarantool.org/pg-scm-1.rockspec...
... (more info about building the Tarantool/PostgreSQL driver appears here)
pg scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/pg/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the
$ # name of this directory is $HOME/tarantool_sandbox.
$ # (Change "$HOME" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
cd $HOME/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-412-g803b15c
type 'help' for interactive help
tarantool> $ :codebold:`tarantool example.lua` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`4: j = 'a' .. i`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`5: print('end of program')`
:codeblue:`(TDB)>` e
:codeblue:`(TDB)`  :codegreen:`Eval mode ON`
:codeblue:`(TDB)>` j
j       a1
:codeblue:`(TDB)>` -e
:codeblue:`(TDB)`  :codegreen:`Eval mode OFF`
:codeblue:`(TDB)>` q $ export TMDIR=~/mysql-5.5
$ # Check that the include subdirectory exists by looking
$ # for .../include/mysql.h. (If this fails, there's a chance
$ # that it's in .../include/mysql/mysql.h instead.)
$ [ -f $TMDIR/include/mysql.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the
$ # necessary .so file.
$ [ -f $TMDIR/lib/libmysqlclient.so ] && echo "OK" || echo "Error"
OK

$ # Check that the mysql client can connect using some factory
$ # defaults: port = 3306, user = 'root', user password = '',
$ # database = 'test'. These can be changed, provided one uses
$ # the changed values in all places.
$ $TMDIR/bin/mysql --port=3306 -h 127.0.0.1 --user=root \
    --password= --database=test
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.35 MySQL Community Server (GPL)
...
Type 'help;' or '\h' for help. Type '\c' to clear ...

$ # Insert a row in database test, and quit.
mysql> CREATE TABLE IF NOT EXISTS test (s1 INT, s2 VARCHAR(50));
Query OK, 0 rows affected (0.13 sec)
mysql> INSERT INTO test.test VALUES (1,'MySQL row');
Query OK, 1 row affected (0.02 sec)
mysql> QUIT
Bye

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
    ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool
$ # master repository. The resultant display is normal for Ubuntu
$ # 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.6
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.6.6.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib --local
Installing http://rocks.tarantool.org/mysql-scm-1.rockspec...
... (more info about building the Tarantool/MySQL driver appears here)
mysql scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/mysql/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the name
$ # of this directory is /home/pgulutzan/tarantool_sandbox.
$ # (Change "/home/pgulutzan" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
$ cd /home/pgulutzan/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-222-g48b98bb
type 'help' for interactive help
tarantool> $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> cfg = {
         >   servers = {
         >       { uri = 'localhost:3301', zone = '1' },
         >   },
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3301;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' },
         >     { uri = 'localhost:3302', zone = '2' },
         >   },
         >   login = 'admin',
         >   password = 'password',
         >   redundancy = 1,
         >   binary = 3301,
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_2
$ cd ~/tarantool_sandbox_2
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3302}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' };
         >     { uri = 'localhost:3302', zone = '2' };
         >   };
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3302;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now get something out ...
tarantool> shard.tester:select{1} **Consistent Hash** **Example:** **Queue** **Redundancy** **Replica** **Shard** **Zone** *connection-name*:close() *connection-name*:execute(*sql-statement* [, *parameters*]) *connection-name*:ping() *connection_name* = mysql.connect(*connection options*) *connection_name* = pg.connect(*connection options*) -- default process_expired_tuple function
local function default_tuple_drop(space_id, args, tuple)
    local key = fun.map(
        function(x) return tuple[x.fieldno] end,
        box.space[space_id].index[0].parts
    ):totable()
    box.space[space_id]:delete(key)
end :codebold:`-e` :codebold:`bt` :codebold:`c` :codebold:`e` :codebold:`f` :codebold:`globals` :codebold:`h` :codebold:`locals` :codebold:`n` :codebold:`q` :samp:`db = {database-name}` - string, default value is blank :samp:`host = {host-name}` - string, default value = 'localhost' :samp:`pass = {password}` or :samp:`password = {password}` - string, default value is blank :samp:`password = {password}` - string, default value is blank :samp:`port = {port-number}` - number, default value = 3306 :samp:`raise = {true|false}` - boolean, default value is false :samp:`user = {user-name}` - string, default value is operating-system user name A complete copy of the data. The shard module handles both sharding and replication. One shard can contain one or more replicas. When a write occurs, the write is attempted on every replica in turn. The shard module does not use the built-in replication feature. A module is an optional library which enhances Tarantool functionality. A physical location where the nodes are closely connected, with the same security and backup and access points. The simplest example of a zone is a single computer with a single tarantool-server instance. A shard's replicas should be in different zones. A subset of the tuples in the database partitioned according to the value returned by the consistent hash function. Usually each shard is on a separate node, or a separate set of nodes (for example if redundancy = 3 then the shard will be on three nodes). A temporary list of recent update requests. Sometimes called "batching". Since updates to a sharded database can be slow, it may speed up throughput to send requests to a queue rather than wait for the update to finish on ever node. The shard module has functions for adding requests to the queue, which it will process without further intervention. Queuing is optional. After :ref:`sleeping <fiber-sleep>` for two seconds, when the task has had time to do its iterations through the spaces, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 0". After sleeping for two more seconds, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 1". This shows that the is_tuple_expired() function eventually returned "true" for one of the tuples, because its timestamp field was more than three seconds old. Another debugger example can be found here_. At this point it is a good idea to check that the installation produced a file named ``driver.so``, and to check that this file is on a directory that is searched by the ``require`` request. At this point, if the above explanation is worthwhile, it's clear that ``expirationd.lua`` starts a background routine (fiber) which iterates through all the tuples in a space, sleeps cooperatively so that other fibers can operate at the same time, and - whenever it finds a tuple that has expired - deletes it from this space. Now the "``expirationd_run_task()``" function can be used in a test which creates sample data, lets the daemon run for a while, and prints results. Backtrace -- show the stack (in red), with program/function names and line numbers of whatever has been invoked to reach the current line. Begin by installing luarocks and making sure that tarantool is among the upstream servers, as in the instructions on `rocks.tarantool.org`_, the Tarantool luarocks page. Now execute this: Begin by making a ``require`` request for the mysql driver. We will assume that the name is ``mysql`` in further examples. Begin by making a ``require`` request for the pg driver. We will assume that the name is ``pg`` in further examples. Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line: Closing connection Configure tarantool and load mysql module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configure tarantool and load pg module. Make sure that tarantool doesn't reply "error" for the call to "require()". Connecting Continue till next breakpoint or till program ends. Create a Lua function that will connect to the MySQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a Lua function that will connect to the PostgreSQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Debugger Commands Debugger prompts are blue, debugger hints and information are green, and the current line -- line 3 of example.lua -- is the default color. Now enter six debugger commands: Details are on `the shard section of github`_. Display a list of debugger commands. Display names and values of variables, for example the control variables of a Lua "for" statement. Display names of variables or functions which are defined as global. Display the fiber id, the program name, and the percentage of memory used, as a table. Enter evaluation mode. When the program is in evaluation mode, one can execute certain Lua statements that would be valid in the context. This is particularly useful for displaying the values of the program's variables. Other debugger commands will not work until one exits evaluation mode by typing :codebold:`-e`. Every data-access function in the box module has an analogue in the shard module, so (for example) to insert in table T in a sharded database one simply says ``shard.T:insert{...}`` instead of ``box.space.T:insert{...}``. A ``shard.T:select{}`` request without a primary key will search all shards. Every queued data-access function has an analogue in the shard module. The user must add an operation_id. The details of queued data-access functions, and of maintenance-related functions, are on `the shard section of github`_. Example Example Session Example, creating a function which sets each option in a separate line: Example, using a table literal enclosed in {braces}: Example: Shard, Minimal Configuration Example: Shard, Scaling Out Example: shard.init syntax for one shard Example: shard.init syntax for three shards Execute these requests: Executing a statement Exit evaluation mode. First some terminology: For a commercial-grade example of a Lua rock that works with Tarantool, let us look at expirationd, which Tarantool supplies on GitHub_ with an Artistic license. The expirationd.lua program is lengthy (about 500 lines), so here we will only highlight the matters that will be enhanced by studying the full source later. For all MySQL statements, the request is: For all PostgreSQL statements, the request is: For example: For examples of creating one's own module with Lua or C, see `this link`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/mysql`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/pg`_. For those who like to see things run, here are the exact steps to get expirationd through the test. From a user's point of view the MySQL and PostgreSQL rocks are very similar, so the following sections -- "MySQL Example" and "PostgreSQL Example" -- contain some redundancy. Get ``expirationd.lua``. There are standard ways - it is after all part of a `standard rock <https://luarocks.org/modules/rtsisyk/expirationd>`_  - but for this purpose just copy the contents of expirationd.lua_ to a default directory. Go the site `github.com/tarantool/mysql`_. Follow the instructions there, saying: Go the site `github.com/tarantool/pg`_. Follow the instructions there, saying: Go to the next line, skipping over any function calls. How to ping However, because not all platforms are alike, for this example the assumption is that the user must check that the appropriate PostgreSQL files are present and must explicitly state where they are when building the Tarantool/PostgreSQL driver. One can use ``find`` or ``whereis`` to see what directories PostgreSQL files are installed in. If one cuts and pastes the above, then the result, showing only the requests and responses for shard.init and shard.tester, should look approximately like this: Installation It is not supplied as part of the Tarantool repository; it must be installed separately. Here is the usual way: It will be necessary to install Tarantool's MySQL driver shared library, load it, and use it to connect to a MySQL server. After that, one can pass any MySQL statement to the server and receive results, including multiple result sets. It will be necessary to install Tarantool's PostgreSQL driver shared library, load it, and use it to connect to a PostgreSQL server. After that, one can pass any PostgreSQL statement to the server and receive results. Lua rocks reference Module `expirationd` Module `shard` Module `tdb` MySQL Example Now start Tarantool, using example.lua as the initialization file Now, for the MySQL driver shared library, there are two ways to install: Now, for the PostgreSQL driver shared library, there are two ways to install: Now, say: Observe the result. It contains "MySQL row". So this is the row that was inserted into the MySQL database. And now it's been selected with the Tarantool client. Observe the result. It contains "PostgreSQL row". So this is the row that was inserted into the PostgreSQL database. And now it's been selected with the Tarantool client. Of course, expirationd can be customized to do different things by passing different parameters, which will be evident after looking in more detail at the source code. On Terminal #1, say: On Terminal #2, say: Or, download from github tarantool/shard and compile as described in the README. Then, before using the module, say ``shard = require('shard')`` Possible Errors: Redundancy should not be greater than the number of servers; the servers must be alive; two replicas of the same shard should not be in the same zone. PostgreSQL Example Put the following program in a default directory and call it "example.lua": Quit immediately. SQL DBMS Modules See also :ref:`Modules <modules>`. Start the Tarantool server as described before. Tarantool supplies DBMS connector modules with the module manager for Lua, LuaRocks. So the connector modules may be called "rocks". The "for" instruction can be translated as "iterate through the index of the space that is being scanned", and within it, if the tuple is "expired" (for example, if the tuple has a timestamp field which is less than the current time), process the tuple as an expired tuple. The Tarantool Debugger (abbreviation = tdb) can be used with any Lua program. The operational features include: setting breakpoints, examining variables, going forward one line at a time, backtracing, and showing information about fibers. The display features include: using different colors for different situations, including line numbers, and adding hints. The Tarantool rocks allow for connecting to an SQL server and executing SQL statements the same way that a MySQL or PostgreSQL client does. The SQL statements are visible as Lua methods. Thus Tarantool can serve as a "MySQL Lua Connector" or "PostgreSQL Lua Connector", which would be useful even if that was all Tarantool could do. But of course Tarantool is also a DBMS, so the module also is useful for any operations, such as database copying and accelerating, which work best when the application can work on both SQL and Tarantool inside the same Lua routine. The methods for connect/select/insert/etc. are similar to the ones in the :ref:`net.box <net_box-module>` module. The Tarantool shard module has facilities for creating shards, as well as analogues for the data-manipulation functions of the box library (select, insert, replace, update, delete). The connection-options parameter is a table. Possible options are: The database-specific requests (``cfg``, :ref:`space.create <box_schema-space_create>`, :ref:`create_index <box_space-create_index>`) should already be familiar. The discussion here in the reference is about incorporating and using two modules that have already been created: the "SQL DBMS rocks" for MySQL and PostgreSQL. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of MySQL had been installed on ~/mysql-5.5. The mysqld server is already running on the local host 127.0.0.1. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of PostgreSQL had been installed on /usr. The PostgreSQL server is already running on the local host 127.0.0.1. The function which will be supplied to expirationd is :codenormal:`is_tuple_expired`, which is saying "if the second field of the tuple is less than the :ref:`current time <fiber-time>`  , then return true, otherwise return false". The key for getting the rock rolling is ``expd = require('expirationd')``. The "``require``" function is what reads in the program; it will appear in many later examples in this manual, when it's necessary to get a module that's not part of the Tarantool kernel. After the Lua variable expd has been assigned the value of the expirationd module, it's possible to invoke the module's ``run_task()`` function. The most important function is: The names are similar to the names that PostgreSQL itself uses. The number of replicas in each shard. The number of replicas per shard (redundancy) is 3. The number of servers is 3. The shard module will conclude that there is only one shard. The option names, except for `raise`, are similar to the names that MySQL's mysql client uses, for details see the MySQL manual at `dev.mysql.com/doc/refman/5.6/en/connecting.html`_. The `raise` option should be set to :codenormal:`true` if errors should be raised when encountered. To connect with a Unix socket rather than with TCP, specify ``host = 'unix/'`` and :samp:`port = {socket-name}`. The screen should now look like this: The shard module distributes according to a hash algorithm, that is, it applies a hash function to a tuple's primary-key value in order to decide which shard the tuple belongs to. The hash function is `consistent`_ so that changing the number of servers will not affect results for many keys. The specific hash function that the shard module uses is :ref:`digest.guava <digest-guava>` in the :codeitalic:`digest` module. The shard package is distributed separately from the main tarantool package. To acquire it, do a separate install. For example on Ubuntu say: There are two shards, and each shard contains one replica. This requires two nodes. In real life the two nodes would be two computers, but for this illustration the requirement is merely: start two shells, which we'll call Terminal#1 and Terminal #2. There is only one shard, and that shard contains only one replica. So this isn't illustrating the features of either replication or sharding, it's only illustrating what the syntax is, and what the messages look like, that anyone could duplicate in a minute or two with the magic of cut-and-paste. This describes three shards. Each shard has two replicas. Since the number of servers is 7, and the number of replicas per shard is 2, and dividing 7 / 2 leaves a remainder of 1, one of the servers will not be used. This is not necessarily an error, because perhaps one of the servers in the list is not alive. This example assumes that MySQL 5.5 or MySQL 5.6 or MySQL 5.7 has been installed. Recent MariaDB versions will also work, the MariaDB C connector is used. The package that matters most is the MySQL client developer package, typically named something like libmysqlclient-dev. The file that matters most from this package is libmysqlclient.so or a similar name. One can use ``find`` or ``whereis`` to see what directories these files are installed in. This example assumes that PostgreSQL 8 or PostgreSQL 9 has been installed. More recent versions should also work. The package that matters most is the PostgreSQL developer package, typically named something like libpq-dev. On Ubuntu this can be installed with: This must be called for every shard. The shard-configuration is a table with these fields: This shows that what was inserted by Terminal #1 can be selected by Terminal #2, via the shard module. To call another DBMS from Tarantool, the essential requirements are: another DBMS, and Tarantool. The module which connects Tarantool to another DBMS may be called a "connector". Within the module there is a shared library which may be called a "driver". To end a session that began with ``mysql.connect``, the request is: To end a session that began with ``pg.connect``, the request is: To ensure that a connection is working, the request is: To initiate tdb within a Lua program and set a breakpoint, edit the program to include these lines: To start the debugging session, execute the Lua program. Execution will stop at the breakpoint, and it will be possible to enter debugging commands. Ultimately the tuple-expiry process leads to ``default_tuple_drop()`` which does a "delete" of a tuple from its original space. First the fun :ref:`fun <fun-module>` module is used, specifically fun.map_. Remembering that :codenormal:`index[0]` is always the space's primary key, and :codenormal:`index[0].parts[`:codeitalic:`N`:codenormal:`].fieldno` is always the field number for key part :codeitalic:`N`, fun.map() is creating a table from the primary-key values of the tuple. The result of fun.map() is passed to :ref:`space_object:delete() <box_space-delete>`. We will assume that the name is 'conn' in further examples. What will appear on Terminal #1 is: a loop of error messages saying "Connection refused" and "server check failure". This is normal. It will go on until Terminal #2 process starts. What will appear on Terminal #2, at the end, should look like this: Whenever one hears "daemon" in Tarantool, one should suspect it's being done with a :doc:`fiber<../reference_lua/fiber>`. The program is making a fiber and turning control over to it so it runs occasionally, goes to sleep, then comes back for more. With GitHub With LuaRocks With sharding, the tuples of a tuple set are distributed to multiple nodes, with a Tarantool database server on each node. With this arrangement, each server is handling only a subset of the total data, so larger loads can be handled by simply adding more computers to a network. binary (a port number that this host is listening on, on the current host) (distinguishable from the 'listen' port specified by box.cfg) conn = mysql.connect({
    host = '127.0.0.1',
    port = 3306,
    user = 'p',
    password = 'p',
    db = 'test',
    raise = true
})
-- OR
conn = mysql.connect({
    host = 'unix/',
    port = '/var/run/mysqld/mysqld.sock'
}) conn = pg.connect({
    host = '127.0.0.1',
    port = 5432,
    user = 'p',
    password = 'p',
    db = 'test'
}) fiber = require('fiber')
expd = require('expirationd')
box.cfg{}
e = box.schema.space.create('expirationd_test')
e:create_index('primary', {type = 'hash', parts = {1, 'unsigned'}})
e:replace{1, fiber.time() + 3}
e:replace{2, fiber.time() + 30}
function is_tuple_expired(args, tuple)
  if (tuple[2] < fiber.time()) then return true end
  return false
  end
expd.run_task('expirationd_test', e.id, is_tuple_expired)
retval = {}
fiber.sleep(2)
expd.task_stats()
fiber.sleep(2)
expd.task_stats()
expd.kill_task('expirationd_test')
e:drop()
os.exit() for _, tuple in scan_space.index[0]:pairs(nil, {iterator = box.index.ALL}) do
...
        if task.is_tuple_expired(task.args, tuple) then
        task.expired_tuples_count = task.expired_tuples_count + 1
        task.process_expired_tuple(task.space_id, task.args, tuple)
... git clone --recursive https://github.com/Sulverus/tdb
cd tdb
make
sudo make install prefix=/usr/share/tarantool/ git clone https://github.com/tarantool/mysql.git
cd mysql && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install git clone https://github.com/tarantool/pg.git
cd pg && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install local function expirationd_run_task(name, space_id, is_tuple_expired, options)
... login (the user name which applies for accessing via the shard module) luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib luarocks install mysql [MYSQL_LIBDIR = *path*]
                       [MYSQL_INCDIR = *path*]
                       [--local] luarocks install pg POSTGRESQL_LIBDIR=/usr/local/postgresql/lib luarocks install pg [POSTGRESQL_LIBDIR = *path*]
                    [POSTGRESQL_INCDIR = *path*]
                    [--local] mysql = require('mysql') n  -- go to next line
n  -- go to next line
e  -- enter evaluation mode
j  -- display j
-e -- exit evaluation mode
q  -- quit password (the password for the login) pg = require('pg') redundancy (a number, minimum 1) servers (a list of URIs of nodes and the zones the nodes are in) shard.init(*shard-configuration*) shard[*space-name*].insert{...}
shard[*space-name*].replace{...}
shard[*space-name*].delete{...}
shard[*space-name*].select{...}
shard[*space-name*].update{...}
shard[*space-name*].auto_increment{...} shard[*space-name*].q_insert{...}
shard[*space-name*].q_replace{...}
shard[*space-name*].q_delete{...}
shard[*space-name*].q_select{...}
shard[*space-name*].q_update{...}
shard[*space-name*].q_auto_increment{...} sudo apt-get install libpq-dev sudo apt-get install tarantool-dev sudo apt-get install tarantool-shard tarantool-pool tarantool> -- Connection function. Usage: conn = mysql_connect()
tarantool> function mysql_connection()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   conn = mysql.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = mysql_connect()
---
... tarantool> box.cfg{}
...
tarantool> mysql = require('mysql')
---
... tarantool> box.cfg{}
...
tarantool> pg = require('pg')
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'host1:33131', zone = '1' },
         >     { uri = 'host2:33131', zone = '2' },
         >     { uri = 'host3:33131', zone = '3' },
         >     { uri = 'host4:33131', zone = '4' },
         >     { uri = 'host5:33131', zone = '5' },
         >     { uri = 'host6:33131', zone = '6' },
         >     { uri = 'host7:33131', zone = '7' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '2',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:33131', zone = '1' },
         >     { uri = 'localhost:33132', zone = '2' },
         >     { uri = 'localhost:33133', zone = '3' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '3',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> conn:close()
---
... tarantool> conn:execute('select table_name from information_schema.tables')
---
- - table_name: ALL_PLUGINS
  - table_name: APPLICABLE_ROLES
  - table_name: CHARACTER_SETS
  <...>
- 78
... tarantool> conn:execute('select tablename from pg_tables')
---
- - tablename: pg_statistic
  - tablename: pg_type
  - tablename: pg_authid
  <...>
... tarantool> conn:ping()
---
- true
... tarantool> function mysql_select ()
         >   local conn = mysql.connect({
         >     host = '127.0.0.1',
         >     port = 3306,
         >     user = 'root',
         >     db = 'test'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> mysql_select()
---
- 'MySQL row '
... tarantool> function pg_connect()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   p.user = 'postgres'
         >   p.password = 'postgres'
         >   local conn = pg.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = pg_connect()
---
... tarantool> function pg_select ()
         >   local conn = pg.connect({
         >     host = '127.0.0.1',
         >     port = 5432,
         >     user = 'postgres',
         >     password = 'postgres',
         >     db = 'postgres'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> pg_select()
---
- 'PostgreSQL row '
... tarantool> shard.init(cfg)
2015-08-09 ... I> Sharding initialization started...
2015-08-09 ... I> establishing connection to cluster servers...
2015-08-09 ... I>  - localhost:3301 - connecting...
2015-08-09 ... I>  - localhost:3301 - connected
2015-08-09 ... I> connected to all servers
2015-08-09 ... I> started
2015-08-09 ... I> redundancy = 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> Adding localhost:3301 to shard 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> shards = 1
2015-08-09 ... I> Done
---
- true
...
tarantool> -- Now put something in ...
---
...
tarantool> shard.tester:insert{1,'Tuple #1'}
---
- - [1, 'Tuple #1']
... tarantool> shard.tester:select{1}
---
- - - [1, 'Tuple #1']
... task.worker_fiber = fiber.create(worker_loop, task)
log.info("expiration: task %q restarted", task.name)
...
fiber.sleep(expirationd.constants.check_interval)
... tdb = require('tdb')
tdb.start() tdb = require('tdb')
tdb.start()
i = 1
j = 'a' .. i
print('end of program') where ``sql-statement`` is a string, and the optional ``parameters`` are extra values that can be plugged in to replace any question marks ("?"s) in the SQL statement. Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-01 13:35+0300
PO-Revision-Date: 2016-08-19 13:21+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 $ # Check that the include subdirectory exists
$ # by looking for /usr/include/postgresql/libpq-fe-h.
$ [ -f /usr/include/postgresql/libpq-fe.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the necessary .so file.
$ [ -f /usr/lib/x86_64-linux-gnu/libpq.so ] && echo "OK" || echo "Error"
OK

$ # Check that the psql client can connect using some factory defaults:
$ # port = 5432, user = 'postgres', user password = 'postgres',
$ # database = 'postgres'. These can be changed, provided one changes
$ # them in all places. Insert a row in database postgres, and quit.
$ psql -h 127.0.0.1 -p 5432 -U postgres -d postgres
Password for user postgres:
psql (9.3.10)
SSL connection (cipher: DHE-RSA-AES256-SHA, bits: 256)
Type "help" for help.

postgres=# CREATE TABLE test (s1 INT, s2 VARCHAR(50));
CREATE TABLE
postgres=# INSERT INTO test VALUES (1,'PostgreSQL row');
INSERT 0 1
postgres=# \q
$

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
        ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool master
$ # repository. The resultant display is normal for Ubuntu 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.7
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.7.0.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install pg POSTGRESQL_LIBDIR=/usr/lib/x86_64-linux-gnu --local
Installing http://rocks.tarantool.org/pg-scm-1.rockspec...
... (more info about building the Tarantool/PostgreSQL driver appears here)
pg scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/pg/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the
$ # name of this directory is $HOME/tarantool_sandbox.
$ # (Change "$HOME" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
cd $HOME/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-412-g803b15c
type 'help' for interactive help
tarantool> $ :codebold:`tarantool example.lua` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` $ :codebold:`tarantool example.lua`
:codeblue:`(TDB)`  :codegreen:`Tarantool debugger v.0.0.3. Type h for help`
example.lua
:codeblue:`(TDB)`  :codegreen:`[example.lua]`
:codeblue:`(TDB)`  :codenormal:`3: i = 1`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`4: j = 'a' .. i`
:codeblue:`(TDB)>` n
:codeblue:`(TDB)`  :codenormal:`5: print('end of program')`
:codeblue:`(TDB)>` e
:codeblue:`(TDB)`  :codegreen:`Eval mode ON`
:codeblue:`(TDB)>` j
j       a1
:codeblue:`(TDB)>` -e
:codeblue:`(TDB)`  :codegreen:`Eval mode OFF`
:codeblue:`(TDB)>` q $ export TMDIR=~/mysql-5.5
$ # Check that the include subdirectory exists by looking
$ # for .../include/mysql.h. (If this fails, there's a chance
$ # that it's in .../include/mysql/mysql.h instead.)
$ [ -f $TMDIR/include/mysql.h ] && echo "OK" || echo "Error"
OK

$ # Check that the library subdirectory exists and has the
$ # necessary .so file.
$ [ -f $TMDIR/lib/libmysqlclient.so ] && echo "OK" || echo "Error"
OK

$ # Check that the mysql client can connect using some factory
$ # defaults: port = 3306, user = 'root', user password = '',
$ # database = 'test'. These can be changed, provided one uses
$ # the changed values in all places.
$ $TMDIR/bin/mysql --port=3306 -h 127.0.0.1 --user=root \
    --password= --database=test
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.35 MySQL Community Server (GPL)
...
Type 'help;' or '\h' for help. Type '\c' to clear ...

$ # Insert a row in database test, and quit.
mysql> CREATE TABLE IF NOT EXISTS test (s1 INT, s2 VARCHAR(50));
Query OK, 0 rows affected (0.13 sec)
mysql> INSERT INTO test.test VALUES (1,'MySQL row');
Query OK, 1 row affected (0.02 sec)
mysql> QUIT
Bye

$ # Install luarocks
$ sudo apt-get -y install luarocks | grep -E "Setting up|already"
Setting up luarocks (2.0.8-2) ...

$ # Set up the Tarantool rock list in ~/.luarocks,
$ # following instructions at rocks.tarantool.org
$ mkdir ~/.luarocks
$ echo "rocks_servers = {[[http://rocks.tarantool.org/]]}" >> \
    ~/.luarocks/config.lua

$ # Ensure that the next "install" will get files from Tarantool
$ # master repository. The resultant display is normal for Ubuntu
$ # 12.04 precise
$ cat /etc/apt/sources.list.d/tarantool.list
deb http://tarantool.org/dist/1.7/ubuntu/ precise main
deb-src http://tarantool.org/dist/1.7/ubuntu/ precise main

$ # Install tarantool-dev. The displayed line should show version = 1.6
$ sudo apt-get -y install tarantool-dev | grep -E "Setting up|already"
Setting up tarantool-dev (1.6.6.222.g48b98bb~precise-1) ...
$

$ # Use luarocks to install locally, that is, relative to $HOME
$ luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib --local
Installing http://rocks.tarantool.org/mysql-scm-1.rockspec...
... (more info about building the Tarantool/MySQL driver appears here)
mysql scm-1 is now built and installed in ~/.luarocks/

$ # Ensure driver.so now has been created in a place
$ # tarantool will look at
$ find ~/.luarocks -name "driver.so"
~/.luarocks/lib/lua/5.1/mysql/driver.so

$ # Change directory to a directory which can be used for
$ # temporary tests. For this example we assume that the name
$ # of this directory is /home/pgulutzan/tarantool_sandbox.
$ # (Change "/home/pgulutzan" to whatever is the user's actual
$ # home directory for the machine that's used for this test.)
$ cd /home/pgulutzan/tarantool_sandbox

$ # Start the Tarantool server. Do not use a Lua initialization file.

$ tarantool
tarantool: version 1.7.0-222-g48b98bb
type 'help' for interactive help
tarantool> $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> cfg = {
         >   servers = {
         >       { uri = 'localhost:3301', zone = '1' },
         >   },
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3301;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_1
$ cd ~/tarantool_sandbox_1
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3301}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' },
         >     { uri = 'localhost:3302', zone = '2' },
         >   },
         >   login = 'admin',
         >   password = 'password',
         >   redundancy = 1,
         >   binary = 3301,
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now put something in ...
tarantool> shard.tester:insert{1,'Tuple #1'} $ mkdir ~/tarantool_sandbox_2
$ cd ~/tarantool_sandbox_2
$ rm -r *.snap
$ rm -r *.xlog
$ ~/tarantool-1.7/src/tarantool

tarantool> box.cfg{listen = 3302}
tarantool> box.schema.space.create('tester')
tarantool> box.space.tester:create_index('primary', {})
tarantool> box.schema.user.passwd('admin', 'password')
tarantool> console = require('console')
tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:3301', zone = '1' };
         >     { uri = 'localhost:3302', zone = '2' };
         >   };
         >   login = 'admin';
         >   password = 'password';
         >   redundancy = 1;
         >   binary = 3302;
         > }
tarantool> shard = require('shard')
tarantool> shard.init(cfg)
tarantool> -- Now get something out ...
tarantool> shard.tester:select{1} **Consistent Hash** **Example:** **Queue** **Redundancy** **Replica** **Shard** **Zone** *connection-name*:close() *connection-name*:execute(*sql-statement* [, *parameters*]) *connection-name*:ping() *connection_name* = mysql.connect(*connection options*) *connection_name* = pg.connect(*connection options*) -- default process_expired_tuple function
local function default_tuple_drop(space_id, args, tuple)
    local key = fun.map(
        function(x) return tuple[x.fieldno] end,
        box.space[space_id].index[0].parts
    ):totable()
    box.space[space_id]:delete(key)
end :codebold:`-e` :codebold:`bt` :codebold:`c` :codebold:`e` :codebold:`f` :codebold:`globals` :codebold:`h` :codebold:`locals` :codebold:`n` :codebold:`q` :samp:`db = {database-name}` - string, default value is blank :samp:`host = {host-name}` - string, default value = 'localhost' :samp:`pass = {password}` or :samp:`password = {password}` - string, default value is blank :samp:`password = {password}` - string, default value is blank :samp:`port = {port-number}` - number, default value = 3306 :samp:`raise = {true|false}` - boolean, default value is false :samp:`user = {user-name}` - string, default value is operating-system user name A complete copy of the data. The shard module handles both sharding and replication. One shard can contain one or more replicas. When a write occurs, the write is attempted on every replica in turn. The shard module does not use the built-in replication feature. A module is an optional library which enhances Tarantool functionality. A physical location where the nodes are closely connected, with the same security and backup and access points. The simplest example of a zone is a single computer with a single tarantool-server instance. A shard's replicas should be in different zones. A subset of the tuples in the database partitioned according to the value returned by the consistent hash function. Usually each shard is on a separate node, or a separate set of nodes (for example if redundancy = 3 then the shard will be on three nodes). A temporary list of recent update requests. Sometimes called "batching". Since updates to a sharded database can be slow, it may speed up throughput to send requests to a queue rather than wait for the update to finish on ever node. The shard module has functions for adding requests to the queue, which it will process without further intervention. Queuing is optional. After :ref:`sleeping <fiber-sleep>` for two seconds, when the task has had time to do its iterations through the spaces, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 0". After sleeping for two more seconds, ``expd.task_stats()`` will print out a report showing how many tuples have expired -- "expired_count: 1". This shows that the is_tuple_expired() function eventually returned "true" for one of the tuples, because its timestamp field was more than three seconds old. Another debugger example can be found here_. At this point it is a good idea to check that the installation produced a file named ``driver.so``, and to check that this file is on a directory that is searched by the ``require`` request. At this point, if the above explanation is worthwhile, it's clear that ``expirationd.lua`` starts a background routine (fiber) which iterates through all the tuples in a space, sleeps cooperatively so that other fibers can operate at the same time, and - whenever it finds a tuple that has expired - deletes it from this space. Now the "``expirationd_run_task()``" function can be used in a test which creates sample data, lets the daemon run for a while, and prints results. Backtrace -- show the stack (in red), with program/function names and line numbers of whatever has been invoked to reach the current line. Begin by installing luarocks and making sure that tarantool is among the upstream servers, as in the instructions on `rocks.tarantool.org`_, the Tarantool luarocks page. Now execute this: Begin by making a ``require`` request for the mysql driver. We will assume that the name is ``mysql`` in further examples. Begin by making a ``require`` request for the pg driver. We will assume that the name is ``pg`` in further examples. Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line Check the instructions for :ref:`Downloading and installing a binary package <user_guide_getting_started-downloading_and_installing_a_binary_package>` that apply for the environment where tarantool was installed. In addition to installing ``tarantool``, install ``tarantool-dev``. For example, on Ubuntu, add the line: Closing connection Configure tarantool and load mysql module. Make sure that tarantool doesn't reply "error" for the call to "require()". Configure tarantool and load pg module. Make sure that tarantool doesn't reply "error" for the call to "require()". Connecting Continue till next breakpoint or till program ends. Create a Lua function that will connect to the MySQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Create a Lua function that will connect to the PostgreSQL server, (using some factory default values for the port and user and password), retrieve one row, and display the row. For explanations of the statement types used here, read the Lua tutorial earlier in the Tarantool user manual. Debugger Commands Debugger prompts are blue, debugger hints and information are green, and the current line -- line 3 of example.lua -- is the default color. Now enter six debugger commands: Details are on `the shard section of github`_. Display a list of debugger commands. Display names and values of variables, for example the control variables of a Lua "for" statement. Display names of variables or functions which are defined as global. Display the fiber id, the program name, and the percentage of memory used, as a table. Enter evaluation mode. When the program is in evaluation mode, one can execute certain Lua statements that would be valid in the context. This is particularly useful for displaying the values of the program's variables. Other debugger commands will not work until one exits evaluation mode by typing :codebold:`-e`. Every data-access function in the box module has an analogue in the shard module, so (for example) to insert in table T in a sharded database one simply says ``shard.T:insert{...}`` instead of ``box.space.T:insert{...}``. A ``shard.T:select{}`` request without a primary key will search all shards. Every queued data-access function has an analogue in the shard module. The user must add an operation_id. The details of queued data-access functions, and of maintenance-related functions, are on `the shard section of github`_. Example Example Session Example, creating a function which sets each option in a separate line: Example, using a table literal enclosed in {braces}: Example: Shard, Minimal Configuration Example: Shard, Scaling Out Example: shard.init syntax for one shard Example: shard.init syntax for three shards Execute these requests: Executing a statement Exit evaluation mode. First some terminology: For a commercial-grade example of a Lua rock that works with Tarantool, let us look at expirationd, which Tarantool supplies on GitHub_ with an Artistic license. The expirationd.lua program is lengthy (about 500 lines), so here we will only highlight the matters that will be enhanced by studying the full source later. For all MySQL statements, the request is: For all PostgreSQL statements, the request is: For example: For examples of creating one's own module with Lua or C, see `this link`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/mysql`_. For further information, including examples of rarely-used requests, see the README.md file at `github.com/tarantool/pg`_. For those who like to see things run, here are the exact steps to get expirationd through the test. From a user's point of view the MySQL and PostgreSQL rocks are very similar, so the following sections -- "MySQL Example" and "PostgreSQL Example" -- contain some redundancy. Get ``expirationd.lua``. There are standard ways - it is after all part of a `standard rock <https://luarocks.org/modules/rtsisyk/expirationd>`_  - but for this purpose just copy the contents of expirationd.lua_ to a default directory. Go the site `github.com/tarantool/mysql`_. Follow the instructions there, saying: Go the site `github.com/tarantool/pg`_. Follow the instructions there, saying: Go to the next line, skipping over any function calls. How to ping However, because not all platforms are alike, for this example the assumption is that the user must check that the appropriate PostgreSQL files are present and must explicitly state where they are when building the Tarantool/PostgreSQL driver. One can use ``find`` or ``whereis`` to see what directories PostgreSQL files are installed in. If one cuts and pastes the above, then the result, showing only the requests and responses for shard.init and shard.tester, should look approximately like this: Installation It is not supplied as part of the Tarantool repository; it must be installed separately. Here is the usual way: It will be necessary to install Tarantool's MySQL driver shared library, load it, and use it to connect to a MySQL server. After that, one can pass any MySQL statement to the server and receive results, including multiple result sets. It will be necessary to install Tarantool's PostgreSQL driver shared library, load it, and use it to connect to a PostgreSQL server. After that, one can pass any PostgreSQL statement to the server and receive results. Справочник по Lua rocks Модуль `expirationd` Модуль `shard` Module `tdb` MySQL Example Now start Tarantool, using example.lua as the initialization file Now, for the MySQL driver shared library, there are two ways to install: Now, for the PostgreSQL driver shared library, there are two ways to install: Now, say: Observe the result. It contains "MySQL row". So this is the row that was inserted into the MySQL database. And now it's been selected with the Tarantool client. Observe the result. It contains "PostgreSQL row". So this is the row that was inserted into the PostgreSQL database. And now it's been selected with the Tarantool client. Of course, expirationd can be customized to do different things by passing different parameters, which will be evident after looking in more detail at the source code. On Terminal #1, say: On Terminal #2, say: Or, download from github tarantool/shard and compile as described in the README. Then, before using the module, say ``shard = require('shard')`` Possible Errors: Redundancy should not be greater than the number of servers; the servers must be alive; two replicas of the same shard should not be in the same zone. PostgreSQL Example Put the following program in a default directory and call it "example.lua": Quit immediately. Модули SQL СУБД See also :ref:`Modules <modules>`. Start the Tarantool server as described before. Tarantool supplies DBMS connector modules with the module manager for Lua, LuaRocks. So the connector modules may be called "rocks". The "for" instruction can be translated as "iterate through the index of the space that is being scanned", and within it, if the tuple is "expired" (for example, if the tuple has a timestamp field which is less than the current time), process the tuple as an expired tuple. The Tarantool Debugger (abbreviation = tdb) can be used with any Lua program. The operational features include: setting breakpoints, examining variables, going forward one line at a time, backtracing, and showing information about fibers. The display features include: using different colors for different situations, including line numbers, and adding hints. The Tarantool rocks allow for connecting to an SQL server and executing SQL statements the same way that a MySQL or PostgreSQL client does. The SQL statements are visible as Lua methods. Thus Tarantool can serve as a "MySQL Lua Connector" or "PostgreSQL Lua Connector", which would be useful even if that was all Tarantool could do. But of course Tarantool is also a DBMS, so the module also is useful for any operations, such as database copying and accelerating, which work best when the application can work on both SQL and Tarantool inside the same Lua routine. The methods for connect/select/insert/etc. are similar to the ones in the :ref:`net.box <net_box-module>` module. The Tarantool shard module has facilities for creating shards, as well as analogues for the data-manipulation functions of the box library (select, insert, replace, update, delete). The connection-options parameter is a table. Possible options are: The database-specific requests (``cfg``, :ref:`space.create <box_schema-space_create>`, :ref:`create_index <box_space-create_index>`) should already be familiar. The discussion here in the reference is about incorporating and using two modules that have already been created: the "SQL DBMS rocks" for MySQL and PostgreSQL. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of MySQL had been installed on ~/mysql-5.5. The mysqld server is already running on the local host 127.0.0.1. The example was run on an Ubuntu 12.04 ("precise") machine where tarantool had been installed in a /usr subdirectory, and a copy of PostgreSQL had been installed on /usr. The PostgreSQL server is already running on the local host 127.0.0.1. The function which will be supplied to expirationd is :codenormal:`is_tuple_expired`, which is saying "if the second field of the tuple is less than the :ref:`current time <fiber-time>`  , then return true, otherwise return false". The key for getting the rock rolling is ``expd = require('expirationd')``. The "``require``" function is what reads in the program; it will appear in many later examples in this manual, when it's necessary to get a module that's not part of the Tarantool kernel. After the Lua variable expd has been assigned the value of the expirationd module, it's possible to invoke the module's ``run_task()`` function. The most important function is: The names are similar to the names that PostgreSQL itself uses. The number of replicas in each shard. The number of replicas per shard (redundancy) is 3. The number of servers is 3. The shard module will conclude that there is only one shard. The option names, except for `raise`, are similar to the names that MySQL's mysql client uses, for details see the MySQL manual at `dev.mysql.com/doc/refman/5.6/en/connecting.html`_. The `raise` option should be set to :codenormal:`true` if errors should be raised when encountered. To connect with a Unix socket rather than with TCP, specify ``host = 'unix/'`` and :samp:`port = {socket-name}`. The screen should now look like this: The shard module distributes according to a hash algorithm, that is, it applies a hash function to a tuple's primary-key value in order to decide which shard the tuple belongs to. The hash function is `consistent`_ so that changing the number of servers will not affect results for many keys. The specific hash function that the shard module uses is :ref:`digest.guava <digest-guava>` in the :codeitalic:`digest` module. The shard package is distributed separately from the main tarantool package. To acquire it, do a separate install. For example on Ubuntu say: There are two shards, and each shard contains one replica. This requires two nodes. In real life the two nodes would be two computers, but for this illustration the requirement is merely: start two shells, which we'll call Terminal#1 and Terminal #2. There is only one shard, and that shard contains only one replica. So this isn't illustrating the features of either replication or sharding, it's only illustrating what the syntax is, and what the messages look like, that anyone could duplicate in a minute or two with the magic of cut-and-paste. This describes three shards. Each shard has two replicas. Since the number of servers is 7, and the number of replicas per shard is 2, and dividing 7 / 2 leaves a remainder of 1, one of the servers will not be used. This is not necessarily an error, because perhaps one of the servers in the list is not alive. This example assumes that MySQL 5.5 or MySQL 5.6 or MySQL 5.7 has been installed. Recent MariaDB versions will also work, the MariaDB C connector is used. The package that matters most is the MySQL client developer package, typically named something like libmysqlclient-dev. The file that matters most from this package is libmysqlclient.so or a similar name. One can use ``find`` or ``whereis`` to see what directories these files are installed in. This example assumes that PostgreSQL 8 or PostgreSQL 9 has been installed. More recent versions should also work. The package that matters most is the PostgreSQL developer package, typically named something like libpq-dev. On Ubuntu this can be installed with: This must be called for every shard. The shard-configuration is a table with these fields: This shows that what was inserted by Terminal #1 can be selected by Terminal #2, via the shard module. To call another DBMS from Tarantool, the essential requirements are: another DBMS, and Tarantool. The module which connects Tarantool to another DBMS may be called a "connector". Within the module there is a shared library which may be called a "driver". To end a session that began with ``mysql.connect``, the request is: To end a session that began with ``pg.connect``, the request is: To ensure that a connection is working, the request is: To initiate tdb within a Lua program and set a breakpoint, edit the program to include these lines: To start the debugging session, execute the Lua program. Execution will stop at the breakpoint, and it will be possible to enter debugging commands. Ultimately the tuple-expiry process leads to ``default_tuple_drop()`` which does a "delete" of a tuple from its original space. First the fun :ref:`fun <fun-module>` module is used, specifically fun.map_. Remembering that :codenormal:`index[0]` is always the space's primary key, and :codenormal:`index[0].parts[`:codeitalic:`N`:codenormal:`].fieldno` is always the field number for key part :codeitalic:`N`, fun.map() is creating a table from the primary-key values of the tuple. The result of fun.map() is passed to :ref:`space_object:delete() <box_space-delete>`. We will assume that the name is 'conn' in further examples. What will appear on Terminal #1 is: a loop of error messages saying "Connection refused" and "server check failure". This is normal. It will go on until Terminal #2 process starts. What will appear on Terminal #2, at the end, should look like this: Whenever one hears "daemon" in Tarantool, one should suspect it's being done with a :doc:`fiber<../reference_lua/fiber>`. The program is making a fiber and turning control over to it so it runs occasionally, goes to sleep, then comes back for more. With GitHub With LuaRocks With sharding, the tuples of a tuple set are distributed to multiple nodes, with a Tarantool database server on each node. With this arrangement, each server is handling only a subset of the total data, so larger loads can be handled by simply adding more computers to a network. binary (a port number that this host is listening on, on the current host) (distinguishable from the 'listen' port specified by box.cfg) conn = mysql.connect({
    host = '127.0.0.1',
    port = 3306,
    user = 'p',
    password = 'p',
    db = 'test',
    raise = true
})
-- OR
conn = mysql.connect({
    host = 'unix/',
    port = '/var/run/mysqld/mysqld.sock'
}) conn = pg.connect({
    host = '127.0.0.1',
    port = 5432,
    user = 'p',
    password = 'p',
    db = 'test'
}) fiber = require('fiber')
expd = require('expirationd')
box.cfg{}
e = box.schema.space.create('expirationd_test')
e:create_index('primary', {type = 'hash', parts = {1, 'unsigned'}})
e:replace{1, fiber.time() + 3}
e:replace{2, fiber.time() + 30}
function is_tuple_expired(args, tuple)
  if (tuple[2] < fiber.time()) then return true end
  return false
  end
expd.run_task('expirationd_test', e.id, is_tuple_expired)
retval = {}
fiber.sleep(2)
expd.task_stats()
fiber.sleep(2)
expd.task_stats()
expd.kill_task('expirationd_test')
e:drop()
os.exit() for _, tuple in scan_space.index[0]:pairs(nil, {iterator = box.index.ALL}) do
...
        if task.is_tuple_expired(task.args, tuple) then
        task.expired_tuples_count = task.expired_tuples_count + 1
        task.process_expired_tuple(task.space_id, task.args, tuple)
... git clone --recursive https://github.com/Sulverus/tdb
cd tdb
make
sudo make install prefix=/usr/share/tarantool/ git clone https://github.com/tarantool/mysql.git
cd mysql && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install git clone https://github.com/tarantool/pg.git
cd pg && cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo
make
make install local function expirationd_run_task(name, space_id, is_tuple_expired, options)
... login (the user name which applies for accessing via the shard module) luarocks install mysql MYSQL_LIBDIR=/usr/local/mysql/lib luarocks install mysql [MYSQL_LIBDIR = *path*]
                       [MYSQL_INCDIR = *path*]
                       [--local] luarocks install pg POSTGRESQL_LIBDIR=/usr/local/postgresql/lib luarocks install pg [POSTGRESQL_LIBDIR = *path*]
                    [POSTGRESQL_INCDIR = *path*]
                    [--local] mysql = require('mysql') n  -- go to next line
n  -- go to next line
e  -- enter evaluation mode
j  -- display j
-e -- exit evaluation mode
q  -- quit password (the password for the login) pg = require('pg') redundancy (a number, minimum 1) servers (a list of URIs of nodes and the zones the nodes are in) shard.init(*shard-configuration*) shard[*space-name*].insert{...}
shard[*space-name*].replace{...}
shard[*space-name*].delete{...}
shard[*space-name*].select{...}
shard[*space-name*].update{...}
shard[*space-name*].auto_increment{...} shard[*space-name*].q_insert{...}
shard[*space-name*].q_replace{...}
shard[*space-name*].q_delete{...}
shard[*space-name*].q_select{...}
shard[*space-name*].q_update{...}
shard[*space-name*].q_auto_increment{...} sudo apt-get install libpq-dev sudo apt-get install tarantool-dev sudo apt-get install tarantool-shard tarantool-pool tarantool> -- Connection function. Usage: conn = mysql_connect()
tarantool> function mysql_connection()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   conn = mysql.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = mysql_connect()
---
... tarantool> box.cfg{}
...
tarantool> mysql = require('mysql')
---
... tarantool> box.cfg{}
...
tarantool> pg = require('pg')
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'host1:33131', zone = '1' },
         >     { uri = 'host2:33131', zone = '2' },
         >     { uri = 'host3:33131', zone = '3' },
         >     { uri = 'host4:33131', zone = '4' },
         >     { uri = 'host5:33131', zone = '5' },
         >     { uri = 'host6:33131', zone = '6' },
         >     { uri = 'host7:33131', zone = '7' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '2',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> cfg = {
         >   servers = {
         >     { uri = 'localhost:33131', zone = '1' },
         >     { uri = 'localhost:33132', zone = '2' },
         >     { uri = 'localhost:33133', zone = '3' }
         >   },
         >   login = 'tester',
         >   password = 'pass',
         >   redundancy = '3',
         >   binary = 33131,
         > }
---
...
tarantool> shard.init(cfg)
---
... tarantool> conn:close()
---
... tarantool> conn:execute('select table_name from information_schema.tables')
---
- - table_name: ALL_PLUGINS
  - table_name: APPLICABLE_ROLES
  - table_name: CHARACTER_SETS
  <...>
- 78
... tarantool> conn:execute('select tablename from pg_tables')
---
- - tablename: pg_statistic
  - tablename: pg_type
  - tablename: pg_authid
  <...>
... tarantool> conn:ping()
---
- true
... tarantool> function mysql_select ()
         >   local conn = mysql.connect({
         >     host = '127.0.0.1',
         >     port = 3306,
         >     user = 'root',
         >     db = 'test'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> mysql_select()
---
- 'MySQL row '
... tarantool> function pg_connect()
         >   local p = {}
         >   p.host = 'widgets.com'
         >   p.db = 'test'
         >   p.user = 'postgres'
         >   p.password = 'postgres'
         >   local conn = pg.connect(p)
         >   return conn
         > end
---
...
tarantool> conn = pg_connect()
---
... tarantool> function pg_select ()
         >   local conn = pg.connect({
         >     host = '127.0.0.1',
         >     port = 5432,
         >     user = 'postgres',
         >     password = 'postgres',
         >     db = 'postgres'
         >   })
         >   local test = conn:execute('SELECT * FROM test WHERE s1 = 1')
         >   local row = ''
         >   for i, card in pairs(test) do
         >       row = row .. card.s2 .. ' '
         >       end
         >   conn:close()
         >   return row
         > end
---
...
tarantool> pg_select()
---
- 'PostgreSQL row '
... tarantool> shard.init(cfg)
2015-08-09 ... I> Sharding initialization started...
2015-08-09 ... I> establishing connection to cluster servers...
2015-08-09 ... I>  - localhost:3301 - connecting...
2015-08-09 ... I>  - localhost:3301 - connected
2015-08-09 ... I> connected to all servers
2015-08-09 ... I> started
2015-08-09 ... I> redundancy = 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> Adding localhost:3301 to shard 1
2015-08-09 ... I> Zone len=1 THERE
2015-08-09 ... I> shards = 1
2015-08-09 ... I> Done
---
- true
...
tarantool> -- Now put something in ...
---
...
tarantool> shard.tester:insert{1,'Tuple #1'}
---
- - [1, 'Tuple #1']
... tarantool> shard.tester:select{1}
---
- - - [1, 'Tuple #1']
... task.worker_fiber = fiber.create(worker_loop, task)
log.info("expiration: task %q restarted", task.name)
...
fiber.sleep(expirationd.constants.check_interval)
... tdb = require('tdb')
tdb.start() tdb = require('tdb')
tdb.start()
i = 1
j = 'a' .. i
print('end of program') where ``sql-statement`` is a string, and the optional ``parameters`` are extra values that can be plugged in to replace any question marks ("?"s) in the SQL statement. 