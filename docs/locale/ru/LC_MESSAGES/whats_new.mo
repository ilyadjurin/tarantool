��          t               �   p   �   �   >  V   �     %  /   D  /   t  �   �  '   (     P     m  �  y  �       �  �     '   �  N   �  N   3  �   �  M   Z	  %   �	     �	   Automatic replication cluster bootstrap (to make it easier to configure a new replication cluster) is supported. For smaller feature changes and bug fixes, see closed `milestones <https://github.com/tarantool/tarantool/milestones?state=closed>`_ at GitHub. Here is a summary of significant changes introduced in specific versions of Tarantool. The LuaJIT version is updated. The ``space_object:dec()`` function is removed. The ``space_object:inc()`` function is removed. The disk-based storge engine, which was called `sophia` or `phia` in earlier versions, is superseded by the `vinyl` storage engine. There are new types for indexed fields. What's new in Tarantool 1.7? What's new? Project-Id-Version: Tarantool 1.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-22 20:32+0300
PO-Revision-Date: 2016-09-23 05:34+0300
Last-Translator: 
Language: ru
Language-Team: 
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 У кластера репликации появилась возможность самонастройки, что существенно упрощает настройку нового кластера. Более мелкие изменения и исправления дефектов указаны в отчетах о `выпущенных стабильных релизах (milestone = closed) <https://github.com/tarantool/tarantool/milestones?state=closed>`_ на GitHub. Здесь собрана информация о существенных изменениях, которые произошли в конкретных версиях Tarantool'а. Обновлена версия LuaJIT. Функция ``space_object:dec()`` объявлена устаревшей. Функция ``space_object:inc()`` объявлена устаревшей. Дисковый движок, который в более ранних версиях Tarantool'а назывался `sophia` и `phia`, заменен новым движком под названием `vinyl`. Добавлены новые типы индексируемых полей. Что нового в Tarantool 1.7? Что нового? 