# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ilya/projects/doc

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ilya/projects/doc

# Utility rule file for sphinx-linkcheck.

# Include the progress variables for this target.
include CMakeFiles/sphinx-linkcheck.dir/progress.make

CMakeFiles/sphinx-linkcheck:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/ilya/projects/doc/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Linkcheck filter"
	venv/bin/sphinx-build -b linkcheck -d /home/ilya/projects/doc/_html_build/ -c html /home/ilya/projects/doc /home/ilya/projects/doc/_html_build/

sphinx-linkcheck: CMakeFiles/sphinx-linkcheck
sphinx-linkcheck: CMakeFiles/sphinx-linkcheck.dir/build.make

.PHONY : sphinx-linkcheck

# Rule to build all files generated by this target.
CMakeFiles/sphinx-linkcheck.dir/build: sphinx-linkcheck

.PHONY : CMakeFiles/sphinx-linkcheck.dir/build

CMakeFiles/sphinx-linkcheck.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/sphinx-linkcheck.dir/cmake_clean.cmake
.PHONY : CMakeFiles/sphinx-linkcheck.dir/clean

CMakeFiles/sphinx-linkcheck.dir/depend:
	cd /home/ilya/projects/doc && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ilya/projects/doc /home/ilya/projects/doc /home/ilya/projects/doc /home/ilya/projects/doc /home/ilya/projects/doc/CMakeFiles/sphinx-linkcheck.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/sphinx-linkcheck.dir/depend

